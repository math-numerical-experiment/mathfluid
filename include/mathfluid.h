/**
 * @file mathfluid.h
 * @brief Header of the library
 */

#ifndef _MATHFLUID_H_
#define _MATHFLUID_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <complex.h>
#include <string.h>
#include <unistd.h>
#include <glib.h>
#include <glib-object.h>
#include <glib/gstdio.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_odeiv2.h>
#include <leveldb/c.h>

/**
 * \addtogroup MathFluidUtils
 * @{
 */

typedef struct {
  FILE *io;
  char *path;
  char *buffer;
  size_t buffer_size;
} MathFluidFile;

#define mathfluid_utils_random_integer(max_abs) (rand() % (2 * max_abs) - max_abs)
long mathfluid_utils_string_to_long (const char *str);
double mathfluid_utils_string_to_double (const char *str);
gchar **mathfluid_utils_split_string_numbers (int *count, const char *str);
double *mathfluid_utils_split_convert_string_numbers (int *count, const char *str);
void mathfluid_utils_string_numbers_to_array_of_double (double *nums, int n, const char *str, const char *err_message);
double *mathfluid_utils_string_numbers_to_allocated_array_of_double (int n, const char *str, const char *err_message);
void mathfluid_utils_array_of_double_fprintf (FILE *out, int n, double *ary, const char *format, const char *splitter);
void mathfluid_utils_array_of_double_printf (int n, double *ary, const char *format, const char *splitter);
void mathfluid_utils_complex_double_fprintf (FILE *out, complex double num, const char *format, const char *splitter);
void mathfluid_utils_complex_double_printf (complex double num, const char *format, const char *splitter);
void mathfluid_utils_array_of_complex_double_fprintf (FILE *out, int n, complex double *ary, const char *format, const char *splitter);
void mathfluid_utils_array_of_complex_double_printf (int n, complex double *ary, const char *format, const char *splitter);
gboolean mathfluid_utils_remove_recursively (const char *path);
char *mathfluid_utils_tmpdir_root (void);
void mathfluid_utils_tmpdir_set_root (const char *root_tmpdir);
void mathfluid_utils_tmpdir_clear (void);
char *mathfluid_utils_tmpdir_get_subdir (const char *prefix);
MathFluidFile *mathfluid_utils_file_open (const char *path);
void mathfluid_utils_file_close (MathFluidFile *file);
char *mathfluid_utils_file_get_data_line (MathFluidFile *file);
void mathfluid_utils_srand (void);

/** @} */  /* End of MathFluidUtils */

/**
 * \addtogroup MathFluidARGV
 * @{
 */

typedef struct {
  GPtrArray *array;
  GPtrArray *allocated;
} MathFluidARGV;

MathFluidARGV *mathfluid_argv_alloc ();
MathFluidARGV *mathfluid_argv_alloc_parse (int argc, char **argv, gboolean read_stdin, int index_read_file);
void mathfluid_argv_free (MathFluidARGV *mathfluid_argv);
void mathfluid_argv_reset (MathFluidARGV *mathfluid_argv);
void mathfluid_argv_append (MathFluidARGV *mathfluid_argv, int argc, char **argv);
char *mathfluid_argv_take_out (MathFluidARGV *mathfluid_argv, int index);
gboolean mathfluid_argv_read_file (MathFluidARGV *mathfluid_argv, char *filename);
void mathfluid_argv_parse_file_option (MathFluidARGV *mathfluid_argv, char *file_option);
#define mathfluid_argv_argc(mathfluid_argv) (mathfluid_argv->array->len)
#define mathfluid_argv_pointer(mathfluid_argv) ((char **) mathfluid_argv->array->pdata)

/** @} */  /* End of MathFluidARGV */

/**
 * \addtogroup MathFluidThreadPool
 * @{
 */

typedef struct {
  int task_number;
  int total_task_number;
  gpointer data;
  void (*function) (int task_number, int total_task_number, gpointer data);
} MathFluidThreadArgument;

typedef struct {
  GCond cond;
  GMutex mutex;
  gboolean finished;
  MathFluidThreadArgument argument[1];
} MathFluidThreadSignal;

typedef struct {
  GThreadPool *pool;
  GPtrArray *signal_array;
  GArray *unused_signal_id;
  int max_thread_number;
} MathFluidThreadPool;

typedef struct {
  GPtrArray *array;
  GRWLock lock[1];
  void *data;
  void *(*variable_alloc) (void *data, int index);
} MathFluidThreadVariable;

MathFluidThreadPool *mathfluid_thread_pool_get (void);
void mathfluid_thread_pool_initialize (int max_thread_number);
void mathfluid_thread_pool_finalize (void);
GArray *mathfluid_thread_pool_push_tasks (int total_task_number, void (*function) (int task_number, int total_task_number, gpointer data), gpointer data);
void mathfluid_thread_pool_wait_tasks (GArray *signal_id);
int mathfluid_thread_pool_max_thread_number (void);

MathFluidThreadVariable *mathfluid_thread_variable_alloc (void *(*variable_alloc) (void *data, int index), void (*variable_free) (void *variable), void *data);
void mathfluid_thread_variable_free (MathFluidThreadVariable *thread_variable);
void mathfluid_thread_variable_clear (MathFluidThreadVariable *thread_variable);
void *mathfluid_thread_variable_get (MathFluidThreadVariable *thread_variable, int index);

/** @} */  /* End of MathFluidThreadPool */

/**
 * \addtogroup MathFluidLogger
 * @{
 */

typedef enum {
  MATH_FLUID_LOG_DEBUG,
  MATH_FLUID_LOG_INFO,
  MATH_FLUID_LOG_WARN,
  MATH_FLUID_LOG_ERROR,
  MATH_FLUID_LOG_FATAL,
  MATH_FLUID_NO_LOG,
} MathFluidLogLevel;

typedef struct {
  int id;
  MathFluidLogLevel level;
  void (* func) (FILE *out, va_list args);
} MathFluidLogFunc;

typedef struct {
  FILE *out;
  MathFluidLogLevel level;
  MathFluidLogFunc *log_func;
  int len;
  char *time_format;
  gboolean fflush_after_logging;
  gboolean time_logging;
} MathFluidLogger;

#define MATHFLUID_LOGGER_DEFAULT_TIME_FORMAT "%Y-%m-%d/%H:%M:%S"
MathFluidLogger *mathfluid_logger_alloc (FILE *out, int len, MathFluidLogFunc *log_func_array);
void mathfluid_logger_free (MathFluidLogger *logger);
void mathfluid_logger_set_log_level (MathFluidLogger *logger, MathFluidLogLevel level);
void mathfluid_logger_set_fflush_after_logging (MathFluidLogger *logger, gboolean fflush_after_logging);
void mathfluid_logger_set_time_logging (MathFluidLogger *logger, gboolean time_logging);
void mathfluid_logger_set_time_format (MathFluidLogger *logger, char *time_format);
void mathfluid_logger_log (MathFluidLogger *logger, int id, ...);
void mathfluid_logger_copy_settings (MathFluidLogger *logger, MathFluidLogger *logger_src);

/** @} */  /* End of MathFluidLogger */

/**
 * \addtogroup MathFluidNormFunc
 * @{
 */

typedef double MathFluidNormFunc (int dimension, double *coordinate, void *norm_args);
double mathfluid_norm_euclid (int dimension, double *coordinate, void *norm_args);
double mathfluid_norm_maximum (int dimension, double *coordinate, void *norm_args);

/** @} */  /* End of MathFluidNormFunc */

/**
 * \addtogroup DataContainer
 * @{
 */

typedef GPtrArray DataContainer;

/** @} */  /* End of DataContainer */

/**
 * \addtogroup GenericParameter
 * @{
 */

/**
 * Parameter set that has real numbers and integers
 */

typedef DataContainer GenericParameter;

enum {
  GENERIC_PARAMETER_ARRAY_DOUBLE,
  GENERIC_PARAMETER_ARRAY_INT,
  GENERIC_PARAMETER_KEY_DOUBLE,
  GENERIC_PARAMETER_KEY_INT,
  GENERIC_PARAMETER_DATA,
  GENERIC_PARAMETER_N
};

/** @} */  /* End of GenericParameter */

/**
 * \addtogroup SystemState
 * @{
 */

typedef struct {
  uint dimension;
  double time;
  double* coordinate;
} SystemState;

/** @} */  /* End of SystemState */

/**
 * \addtogroup SystemODE
 * @{
 */

/**
 * System of autonomous ordinary differential equation:
 * dy/dt = f(y)
 */
typedef struct {
  uint dimension;
  int (* time_derivative) (double *dydt, const double *y, DataContainer *parameters);
  /**
   * Jacobian of time derivative is not necessarily needed and may be a null pointer.
   */
  int (* jacobian_of_time_derivative) (double *dfdy, const double *y, DataContainer *parameters);
  /**
   * Parameter derivative of time derivative is not necessarily needed and may be a null pointer.
   */
  int (* parameter_derivative_of_time_derivative) (double *dydp, const double *y, DataContainer *parameters, const char *key);
  /**
   * parameters->data are passed to functions
   * SystemODE::time_derivative and SystemODE::jacobian as last arguments.
   */
  DataContainer *parameters;
  void (* parameter_set) (DataContainer *parameters, const char *key, GValue *val);
  void (* parameter_get) (GValue *val, DataContainer *parameters, const char *key);
} SystemODE;

typedef enum {
  /**
   * Allocate GSL ODE driver by gsl_odeiv2_driver_alloc_y_new.
   */
  SYSTEM_ODE_DRIVER_Y,
  /**
   * Allocate GSL ODE driver by gsl_odeiv2_driver_alloc_yp_new.
   */
  SYSTEM_ODE_DRIVER_YP,
  /**
   * Allocate GSL ODE driver by gsl_odeiv2_driver_alloc_standard_new.
   */
  SYSTEM_ODE_DRIVER_STANDARD,
  /**
   * Allocate GSL ODE driver by gsl_odeiv2_driver_alloc_scaled_new.
   */
  SYSTEM_ODE_DRIVER_SCALED
} SystemODEDriverType;

#define SYSTEM_ODE_SOLVER_DEFAULT_INITIAL_STEP_SIZE 1e-10
#define SYSTEM_ODE_SOLVER_DEFAULT_EVOLUTION_ERROR 1e-8

typedef struct {
  gsl_odeiv2_system gsl_system[1];
  gsl_odeiv2_driver *gsl_driver;
  SystemODEDriverType gsl_driver_type;
  gsl_odeiv2_step_type *gsl_step_type;
  GPtrArray *gsl_driver_arguments;
  double initial_step_size;
  int gsl_driver_status;
} SystemODESolver;

typedef struct {
  SystemODE *system;
  SystemODESolver *solver;
  gboolean system_free;
  gboolean solver_free;
} SystemODEContainer;

/** @} */  /* End of SystemODE */

/**
 * \addtogroup DynamicalSystem
 * @{
 */

/**
 * Data structure of dynamical system that is defined as autonomous system.
 */
typedef struct {
  /**
   * A dimension of system
   */
  uint dimension;
  /**
   * A pointer to store data that represents parameters of the system and is used freely.
   */
  DataContainer *data_container;
  /**
   * A function to evolve state to t1
   */
  gboolean (* evolve) (SystemState *state, DataContainer *data_container, const double t1);
  /**
   * A function to calculate vector field.
   */
  gboolean (* time_derivative) (double *vec, DataContainer *data, const double *x);
  /**
   * A function to calculate Jacobian of time derivative with respect to the coordinate of phase space.
   */
  gboolean (* jacobian_of_time_derivative) (double *jac, DataContainer *data, const double *x);
  /**
   * A function to calculate derivative with respect to the specified parameter of the time derivative of orbit.
   */
  gboolean (* parameter_derivative_of_time_derivative) (double *vec, DataContainer *data_container, const char *parameter_key, const double *x);
  /**
   * A function to set the parameter
   */
  void (* parameter_set) (DataContainer *data_container, const char *key, GValue *val);
  /**
   * A function to get the parameter
   */
  void (* parameter_get) (GValue *val, DataContainer *data_container, const char *key);
} DynamicalSystem;

/** @} */  /* End of DynamicalSystem */

/**
 * \addtogroup MatrixVectorProduct
 * @{
 */

typedef DataContainer MatrixVectorProduct;

enum {
  MATRIX_VECTOR_PRODUCT_FUNCTION,
  MATRIX_VECTOR_PRODUCT_DATA,
  MATRIX_VECTOR_PRODUCT_N
};

typedef MatrixVectorProduct InverseMatrixVectorProduct;

enum {
  INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_PRODUCT,
  INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_NUMBER_SHIFT,
  INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_DIMENSION,
  INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_N
};

enum {
  INVERSE_MATRIX_VECTOR_PRODUCT_DATA_GMRES,
  INVERSE_MATRIX_VECTOR_PRODUCT_DATA_MAX_ITERATION,
  INVERSE_MATRIX_VECTOR_PRODUCT_DATA_N
};

typedef MatrixVectorProduct JacobianVectorProduct;

/** @} */  /* End of MatrixVectorProduct */

/**
 * \addtogroup ArnoldiMethod
 * @{
 */

typedef struct {
  uint dimension;
  uint max_iteration;
  int iterate;
  double min_norm;
  gsl_vector **basis;
  double *hessenberg_column;
  gsl_vector *w;
  MatrixVectorProduct *matrix_vector_product;
} ArnoldiMethod;

/** @} */  /* End of ArnoldiMethod */

/**
 * \addtogroup GMRESSolver
 * @{
 */

typedef enum {
  GMRES_NO_RESTART = 0,
  GMRES_RESTART_FIXED = 1
} GMRESRestartType;

#define GMRES_SOLVER_MIN_ERROR 1e-10

typedef struct {
  uint dimension;
  /**
   * Iteration number of current step
   */
  int step_iterate;
  int total_iterate;
  GMRESRestartType restart_type;
  int restart;
  /**
   * The iteration of GMRES method finishes when the residual is less than GMRESSolver::error.
   * The value is the maximum number of ||solver->vector_b|| * solver->error_residual_ratio
   * and solver->min_error.
   */
  double error;
  double error_residual_ratio;
  /**
   * Maximum error is determined by residual vector and GMRESSolver::error_residual_ratio
   * and therefore may be too small value.
   * Because the iteration of GMRES fails for too small error so that it is less than machine epsilon,
   * GMRESSolver::min_error determines the minimum of GMRESSolver::error.
   * The default value is GMRES_SOLVER_MIN_ERROR=1e-10.
   */
  double min_error;
  double residual;
  double *vector_x0;
  double *vector_b;
  double *vector_e;
  double **hessenberg;
  double *givens_rotation;
  MatrixVectorProduct *matrix_vector_product;
  ArnoldiMethod *arnoldi_method;
  MathFluidLogger *logger;
} GMRESSolver;

/** @} */  /* End of GMRESSolver */

/**
 * \addtogroup DerivativeOfSystem
 * @{
 */

#define DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION 1e-8
#define DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION 1e-4
#define DERIVATIVE_OF_SYSTEM_DEFAULT_PARAMETER_DERIVATIVE_PERTURBATION 1e-4

/**
 * Settings to calculate some derivatives.
 * This structure includes sizes of perturbations for numerical derivatives,
 * switches whether we use exact derivatives, and dynamical system
 */
typedef struct _DerivativeOfSystem DerivativeOfSystem;

struct _DerivativeOfSystem {
  /**
   * Time evolution to calculate approximate product of Jacobian of the vector field and a vector.
   * The default value is DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION.
   */
  double time_derivative_perturbation;
  double space_derivative_perturbation;
  double parameter_derivative_perturbation;
  gboolean ignore_exact_jacobian;
  gboolean ignore_exact_time_derivative;
  gboolean ignore_exact_parameter_derivative;
  DynamicalSystem *dynamical_system;
};

/** @} */  /* End of DerivativeOfSystem */

/**
 * \addtogroup JacobianFreeGMRES
 * @{
 */

#define JACOBIAN_FREE_GMRES_DEFAULT_ERROR_RESIDUAL_RATIO 1e-10

typedef struct _JacobianFreeGMRES JacobianFreeGMRES;

/**
 * Structure to use GMRES method for dynamical systems
 * so that it needs derivatives of the dynamical systems.
 * In particular, JacobianFreeGMRES is used by SearchEquilibrium, SearchPeriodicOrbit, and Parameter Continuation.
 */
struct _JacobianFreeGMRES {
  /**
   * Maximum number of iterations of GMRES method
   */
  int gmres_max_iteration;
  /**
   * When JacobianFreeGMRES::ignore_gmres_convergence is TRUE,
   * jacobian_free_gmres_solve returns a vector value if the method does not converge.
   */
  gboolean ignore_gmres_convergence;
  DynamicalSystem *dynamical_system;
  void *data;
  void (* product) (double *av, JacobianFreeGMRES *jacobian_free_gmres, const double *v);
  /**
   * Settings of derivatives of dynamical systems.
   * Note that JacobianFreeGMRES::derivative is not explicitly used in routines of GMRES method
   * is prepared in order to use jacobian_free_gmres_product_jacobian_vector and so on
   * in JacobianFreeGMRES::product.
   */
  DerivativeOfSystem *derivative;
  GMRESSolver *gmres_solver;
};

/** @} */  /* End of JacobianFreeGMRES */

/**
 * \addtogroup JacobianFreeNewtonGMRES
 * @{
 */

typedef struct _JacobianFreeNewtonGMRES JacobianFreeNewtonGMRES;

struct _JacobianFreeNewtonGMRES {
  /**
   * Current iteration number
   */
  int iterate;
  /**
   * Modified vector multiplied by 2^(-damping_parameter)
   */
  int damping_parameter;
  /**
   * To store the value 2^(-damping_parameter)
   */
  double damping_coefficient;
  /**
   * Maximum error that is Euclid norm of a modified vector.
   * This is used to test convergence of Newton method and
   * if the norm of a modified vector is less than max_error then Newton method stops.
   */
  double max_error;
  /**
   * Cache of Euclid norm of current modified vector
   */
  double error_cache;
  /**
   * Practical error that is used to test convergence of Newton method.
   * If vector_b is larger than the practical error
   * then test of convergence returns FALSE.
   * If the value is not positive then the practical error is not tested.
   */
  double practical_error;
  /**
   * Maximum uniform norm of solution.
   * If uniform norm of current solution is larger than this value then iteration stops.
   * If the value is not positive then uniform norms are not checked.
   */
  double max_uniform_norm;
  gsl_vector *vector;
  gsl_vector *vector_last;
  gsl_vector *vector_diff;
  gsl_vector *vector_zero;
  gsl_vector *vector_b;
  gboolean vector_b_updated;
  void (* function) (double *value, JacobianFreeGMRES *gmres, const double *pt);
  void (* after_iteration) (JacobianFreeNewtonGMRES *newton_gmres);
  /**
   * Function to calculate norm, which is used to test convergence of Newton method.
   * The default function is mathfluid_norm_euclid.
   * Note that this function is NOT used to test convergence of GMRES method.
   */
  MathFluidNormFunc *norm_function;
  /**
   * Additional arguments to norm_function
   */
  void *norm_args;
  JacobianFreeGMRES *gmres;
  MathFluidLogger *logger;
};

/** @} */  /* End of JacobianFreeNewtonGMRES */

/**
 * \addtogroup RayleighRitz
 * @{
 */

typedef struct {
  uint dimension;
  uint basis_number;
  double *basis;
  MatrixVectorProduct *product;
  gsl_matrix *matrix;
} RayleighRitz;

/** @} */  /* End of RayleighRitz */

/**
 * \addtogroup SimultaneousIteration
 * @{
 */

typedef struct {
  uint dimension;
  uint basis_number;
  gsl_matrix *qr;
  gsl_matrix *q;
  gsl_matrix *r;
  MatrixVectorProduct *product;
} SimultaneousIteration;

/** @} */  /* End of SimultaneousIteration */

/**
 * \addtogroup SearchEquilibrium
 * @{
 */

typedef struct {
  JacobianFreeGMRES *gmres;
  JacobianFreeNewtonGMRES *newton_gmres;
} SearchEquilibrium;

/** @} */  /* End of SearchEquilibrium */

/**
 * \addtogroup SearchPeriodicOrbit
 * @{
 */

typedef enum {
  ITERATE_PERIODIC_ORBIT_SUCCESS,
  ITERATE_PERIODIC_ORBIT_ERROR_NO_SOLUTION,
  ITERATE_PERIODIC_ORBIT_ERROR_NEGATIVE_PERIOD
} IteratePeriodicOrbitError;

typedef struct {
  gboolean pt_after_evolution_of_period_updated;
  SystemState *pt_after_evolution_of_period;
  JacobianFreeGMRES *gmres;
  JacobianFreeNewtonGMRES *newton_gmres;
} SearchPeriodicOrbit;

/** @} */  /* End of SearchPeriodicOrbit */

/**
 * \addtogroup ParameterContinuationCorrectorGaussNewton
 * @{
 */

typedef struct {
  DataContainer *setting;
  /**
   * Current iteration number
   */
  int iterate;
  /**
   * To store the value 2^(-damping_parameter)
   */
  double damping_coefficient;
  /**
   * Maximum error that is Euclid norm of a modified vector.
   * This is used to test convergence of Newton method and
   * if the norm of a modified vector is less than max_error then Newton method stops.
   */
  double max_error;
  /**
   * Cache of Euclid norm of current modified vector
   */
  double error_cache;
  /**
   * Practical error that is used to test convergence of Newton method.
   * If vector_b is larger than the practical error
   * then test of convergence returns FALSE.
   * If the value is not positive then the practical error is not tested.
   */
  double practical_error;
  /**
   * Maximum uniform norm of solution.
   * If uniform norm of current solution is larger than this value then iteration stops.
   * If the value is not positive then uniform norms are not checked.
   */
  double max_uniform_norm;
  gsl_vector *vector;
  gsl_vector *vector_last;
  gsl_vector *vector_diff;
  gsl_vector *vector_zero;
  gsl_vector *vector_b0;
  gsl_vector *vector_b;
  gboolean vector_b_updated;
  void (* function) (double *value, JacobianFreeGMRES *gmres, const double *pt);
  /**
   * Function to calculate norm, which is used to test convergence of Newton method.
   * The default function is mathfluid_norm_euclid.
   * Note that this function is NOT used to test convergence of GMRES method.
   */
  MathFluidNormFunc *norm_function;
  /**
   * Additional arguments to norm_function
   */
  void *norm_args;
  JacobianFreeGMRES *gmres;
  MathFluidLogger *logger;
} ParameterContinuationCorrectorGaussNewton;

/** @} */  /* End of ParameterContinuationCorrectorGaussNewton */

/**
 * \addtogroup ParameterContinuationCorrector
 * @{
 */

typedef enum {
  PARAMETER_CONTINUATION_CORRECTION_KELLER,
  PARAMETER_CONTINUATION_CORRECTION_GAUSS_NEWTON
} ParameterContinuationCorrection;

typedef struct {
  DataContainer *setting;
  JacobianFreeGMRES *gmres_keller;
  JacobianFreeNewtonGMRES *newton_keller;
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
} ParameterContinuationCorrector;

/** @} */  /* End of ParameterContinuationCorrector */

/**
 * \addtogroup ParameterContinuation
 * @{
 */

#define PARAMETER_CONTINUATION_DEFAULT_NEWTON_MAX_ITERATION 1000
#define PARAMETER_CONTINUATION_DEFAULT_STEP_LENGTH 1.0

typedef enum {
  PARAMETER_CONTINUATION_DIRECTION_POSITIVE,
  PARAMETER_CONTINUATION_DIRECTION_NEGATIVE
} ParameterContinuationDirection;

typedef enum {
  PARAMETER_CONTINUATION_STEP_LENGTH_FIXED,
  PARAMETER_CONTINUATION_STEP_LENGTH_ADJUSTED
} ParameterContinuationStepLengthControl;

typedef struct {
  /**
   * Maximum number of iterations to calculate equilibria on correction step
   */
  int newton_max_iteration;
  /**
   * Step size of arc length when we increase or decrease coordinate along the arc length
   */
  double step_length;
  /**
   * Minimum step size of arc length when we use PARAMETER_CONTINUATION_STEP_LENGTH_ADJUSTED
   */
  double min_step_length;
  /**
   * Current coordinate along the arc length
   */
  double current_arc_length;
  /**
   * Key of parameter of continuation
   */
  char *parameter_key;
  /**
   * Setting of predictor_gmres
   */
  DataContainer *predictor_setting;
  /**
   * GMRES method to create initial vector of Newton method, that is, predictor
   */
  JacobianFreeGMRES *predictor_gmres;
  /**
   * Newton method to find an equilibrium from an initial point, thit is, corrector
   */
  ParameterContinuationCorrector *corrector;
  /**
   * Last tangent vector created by predictor
   */
  gsl_vector *tangent_vector_last;
  /**
   * Vector b of linear simultaneous equiation Ax = b of predictor,
   * which is (0, 0, ..., 0, 1).
   */
  gsl_vector *predictor_b;
  /**
   * Current normalized tangent vector to calculate initial vector of correction step,
   * which is same as vector obtained at prediction step
   */
  gsl_vector *tangent_vector_current;
  /**
   * Current solution
   */
  gsl_vector *equilibrium_with_parameter;
  /**
   * Type of step length control
   */
  ParameterContinuationStepLengthControl control;
  MathFluidLogger *logger;
} ParameterContinuation;

/** @} */  /* End of ParameterContinuation */

/**
 * \addtogroup LevelDBStackCache
 * @{
 */

typedef enum {
  LEVELDB_STACK_CACHE_THREAD_WAITING,
  LEVELDB_STACK_CACHE_THREAD_READING,
  LEVELDB_STACK_CACHE_THREAD_WRITING
} LevelDBStackCacheThreadType;

typedef struct {
  GPtrArray *stack;
  char *db_path;
  int db_current_number;
  int db_cache_max_number;
  leveldb_t *db;
  leveldb_options_t *db_options;
  leveldb_readoptions_t *db_roptions;
  leveldb_writeoptions_t *db_woptions;
  char *(*data_dump) (size_t *size, gpointer data);
  gpointer (*data_load) (char *dump, size_t size);
  void (*data_free) (gpointer data);
  LevelDBStackCacheThreadType db_thread_working;
  GMutex db_mutex[1];
  GCond db_cond[1];
  GThreadPool *db_rw_thread_pool;
} LevelDBStackCache;

/** @} */  /* End of LevelDBStackCache */

/**
 * \addtogroup LyapunovVector
 * @{
 */

/**
 * To store forward and backward Lyapunov vectors, and covariant Lyapunov vectors.
 */
typedef struct {
  int number;
  SystemState *state;
  double **vector;
} LyapunovVector;

typedef struct {
  LevelDBStackCache *stack;
} LyapunovVectorSet;

/** @} */  /* End of LyapunovVector */

/**
 * \addtogroup LyapunovEvolve
 * @{
 */

typedef enum {
  LYAPUNOV_EVOLVE_ALGORITHM_EULER,
  LYAPUNOV_EVOLVE_ALGORITHM_RK4,
} LyapunovEvolveAlgorithm;

typedef struct {
  DynamicalSystem *dynamical_system;
  MathFluidThreadVariable *thread_variable;
  DerivativeOfSystem *derivative;
  gboolean use_thread;
  LyapunovEvolveAlgorithm algorithm;
} LyapunovEvolve;

/** @} */  /* End of LyapunovEvolve */

/**
 * \addtogroup LyapunovAnalysis
 * @{
 */

typedef struct {
  int number;                   /* Number of exponents and vectors to be calculated */
  double time_step;
  char *cache_directory;
  LyapunovEvolve *evolve;
} LyapunovAnalysis;

/** @} */  /* End of LyapunovAnalysis */

DataContainer *data_container_alloc (int n, ...);
void data_container_free (DataContainer *data_container);
DataContainer *data_container_duplicate (DataContainer *data_container);
void data_container_add (DataContainer *data_container, int n, ...);
void *data_container_utils_return_pointer (void *pointer);
double *data_container_utils_double_duplicate (double *val);
int *data_container_utils_int_duplicate (int *val);
DataContainer *data_container_generic_parameter_alloc (int dim_double, double *array_double, char **keys_double, int dim_int, int *array_int, char **keys_int, DataContainer *data);

/**
 * \addtogroup DataContainer
 * @{
 */
#define data_container_data_ptr(data_container, i) (g_ptr_array_index((GPtrArray *) data_container, i * 3))
#define data_container_size(data_container) (((GPtrArray *) data_container)->len / 3)

/** @} */  /* End of DataContainer */

GenericParameter *generic_parameter_alloc (void);
void generic_parameter_set_double (GenericParameter *params, int dim, double *nums, char **keys);
void generic_parameter_set_int (GenericParameter *params, int dim, int *nums, char **keys);
void generic_parameter_data_set (GenericParameter *params, DataContainer *data);
GenericParameter *generic_parameter_duplicate (GenericParameter *params);
void generic_parameter_free (GenericParameter *params);
void generic_parameter_set (GenericParameter *params, const char *key, GValue *val);
void generic_parameter_get (GValue *val, GenericParameter *params, const char *key);
int *generic_parameter_array_int (GenericParameter *params);
double *generic_parameter_array_double (GenericParameter *params);

/**
 * \addtogroup GenericParameter
 * @{
 */
#define generic_parameter_data(params) ((DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_DATA))
#define generic_parameter_int(params, i) (g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_INT), int, i))
#define generic_parameter_double(params, i) (g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_DOUBLE), double, i))
/** @} */  /* End of GenericParameter */

SystemState *system_state_alloc (uint dimension);
SystemState *system_state_alloc2 (uint dimension, double time, const double *coordinate);
SystemState *system_state_alloc_copy (SystemState *state);
void system_state_free (SystemState *state);
void system_state_copy (SystemState *dest, SystemState *src);
void system_state_set_coordinate (SystemState *state, const double *vec);
void system_state_set_time (SystemState *state, double time);
void system_state_set_zero (SystemState *state);
void system_state_copy_coordinate (double *coordinate, SystemState *state);
gsl_vector_view system_state_coordinate_vector_view (SystemState *state);
void system_state_fprintf (FILE *out, SystemState *state);

/**
 * \addtogroup SystemState
 * @{
 */
#define system_state_get_time(state) (state->time)
#define system_state_get_coordinate(state) (state->coordinate)
#define system_state_get_dimension(state) (state->dimension)
/** @} */  /* End of SystemState */

SystemODE *system_ode_alloc (uint dimension, int (* time_derivative) (double *dydt, const double *y, DataContainer *params),
                             int (* jacobian) (double *dfdy, const double *y, DataContainer *params),
                             int (* parameter_derivative) (double *dydp, const double *y, DataContainer *params, const char *key),
                             DataContainer *parameters,
                             void (* parameter_set) (DataContainer *parameters, const char *key, GValue *val),
                             void (* parameter_get) (GValue *val, DataContainer *parameters, const char *key));
void system_ode_free (SystemODE *system_ode);
SystemODE *system_ode_duplicate (SystemODE *system_ode);
void system_ode_parameter_set (SystemODE *system, const char *key, GValue *val);
void system_ode_parameter_get (GValue *val, SystemODE *system, const char *key);
gboolean system_ode_time_derivative (double *time_derivative, SystemODE *system, const double *y);
gboolean system_ode_jacobian_of_time_derivative (double *jac, SystemODE *system, const double *y);
gboolean system_ode_parameter_derivative_of_time_derivative (double *vec, SystemODE *system, const char *parameter_key, const double *y);

SystemODESolver *system_ode_solver_alloc (SystemODE *system);
SystemODESolver *system_ode_solver_alloc2 (SystemODE *system, SystemODEDriverType driver_type, ...);
void system_ode_solver_free (SystemODESolver *solver);
SystemODESolver *system_ode_solver_duplicate (SystemODESolver *solver);
void system_ode_solver_set_driver (SystemODESolver *solver, SystemODEDriverType driver_type, ...);
gboolean system_ode_solver_evolve (SystemState *state, SystemODESolver *solver, const double t1);
/**
 * \addtogroup SystemODE
 * @{
 */
#define system_ode_time_jacobian_of_time_derivative_defined_p(system_ode) (system_ode->jacobian_of_time_derivative)
#define system_ode_parameter_derivative_of_time_derivative_defined_p(system_ode) (system_ode->parameter_derivative_of_time_derivative)
#define system_ode_parameters(system) (system->parameters)
#define system_ode_solver_system(solver) ((SystemODE *) solver->gsl_system->params)
/** @} */  /* End of SystemODE */

DynamicalSystem *dynamical_system_alloc (uint dimension, DataContainer *data_container, gboolean (* evolve) (SystemState *state, DataContainer *data_container, const double t1));
void dynamical_system_set_time_derivative (DynamicalSystem *dynamical_system, gboolean (* time_derivative) (double *vec, DataContainer *data_container, const double *x));
void dynamical_system_set_jacobian_of_time_derivative (DynamicalSystem *dynamical_system, gboolean (* jacobian) (double *dfdy, DataContainer *data_container, const double *x));
void dynamical_system_set_parameter_derivative_of_time_derivative (DynamicalSystem *dynamical_system, gboolean (*parameter_derivative_of_time_derivative) (double *vec, DataContainer *data_container, const char *parameter_key, const double *x));
void dynamical_system_set_parameter_accessor (DynamicalSystem *dynamical_system, void (* parameter_set) (DataContainer *data_container, const char *key, GValue *val), void (* parameter_get) (GValue *val, DataContainer *data_container, const char *key));
void dynamical_system_free (DynamicalSystem *dynamical_system);
DynamicalSystem *dynamical_system_duplicate (DynamicalSystem *dynamical_system);
void dynamical_system_parameter_set (DynamicalSystem *dynamical_system, const char *key, GValue *val);
void dynamical_system_parameter_get (GValue *val, DynamicalSystem *dynamical_system, const char *key);
void dynamical_system_parameter_set_double (DynamicalSystem *dynamical_system, const char *key, double val);
double dynamical_system_parameter_get_double (DynamicalSystem *dynamical_system, const char *key);
void dynamical_system_parameter_set_int (DynamicalSystem *dynamical_system, const char *key, int val);
int dynamical_system_parameter_get_int (DynamicalSystem *dynamical_system, const char *key);
gboolean dynamical_system_evolve (SystemState *state, DynamicalSystem *dynamical_system, const double t1);
gboolean dynamical_system_time_derivative (double *vec, DynamicalSystem *dynamical_system, const double *x);
gboolean dynamical_system_numerical_time_derivative (double *vec, DynamicalSystem *dynamical_system, double t, const double *x);
gboolean dynamical_system_jacobian_of_time_derivative (double *jac, DynamicalSystem *dynamical_system, const double *x);
gboolean dynamical_system_product_jacobian_of_time_derivative (double *av, DynamicalSystem *dynamical_system, const double *x, const double *vec);
gboolean dynamical_system_suitable_matrix_free_product_jacobian_of_time_derivative (double *product, DynamicalSystem *dynamical_system, double time, double perturbation, const double *x, const double *vec);
gboolean dynamical_system_matrix_free_product_jacobian_of_time_derivative_with_exact_time_derivative (double *product, DynamicalSystem *dynamical_system, double perturbation, const double *x, const double *vec);
gboolean dynamical_system_matrix_free_product_jacobian_of_time_derivative_without_exact_time_derivative (double *product, DynamicalSystem *dynamical_system, double t, double perturbation, const double *x, const double *vec);
gboolean dynamical_system_matrix_free_product_jacobian_of_orbit (double *product, DynamicalSystem *dynamical_system, double t, double perturbation, const double *x, const double *vec);
gboolean dynamical_system_parameter_derivative_of_time_derivative (double *vec, DynamicalSystem *dynamical_system, const char* parameter_key, const double *x);
gboolean dynamical_system_suitable_parameter_derivative_of_time_derivative (double *vec, DynamicalSystem *dynamical_system, double time, double parameter_perturbation, const char *parameter_key, const double *x);
gboolean dynamical_system_parameter_derivative_of_time_derivative_with_exact_time_derivative (double *vec, DynamicalSystem *dynamical_system, double parameter_perturbation, const char *parameter_key, const double *x);
gboolean dynamical_system_parameter_derivative_of_time_derivative_without_exact_time_derivative (double *vec, DynamicalSystem *dynamical_system, double time, double parameter_perturbation, const char *parameter_key, const double *x);
int dynamical_system_eigen_of_linearized_system (double complex **eigenvalue, double complex **eigenvector, DynamicalSystem *dynamical_system, const double *x);

DynamicalSystem *dynamical_system_ode_alloc (SystemODESolver *solver, gboolean system_free, gboolean solver_free);
SystemODE *dynamical_system_ode_system_ode (DynamicalSystem *dynamical_system);
SystemODESolver *dynamical_system_ode_system_ode_solver (DynamicalSystem *dynamical_system);

/**
 * \addtogroup DynamicalSystem
 * @{
 */
#define dynamical_system_dimension(dynamical_system) (dynamical_system->dimension)
#define dynamical_system_time_derivative_defined_p(dynamical_system) (dynamical_system->time_derivative)
#define dynamical_system_jacobian_of_time_derivative_defined_p(dynamical_system) (dynamical_system->jacobian_of_time_derivative)
#define dynamical_system_parameter_derivative_of_time_derivative_defined_p(dynamical_system) (dynamical_system->parameter_derivative_of_time_derivative)
/** @} */  /* End of DynamicalSystem */

MatrixVectorProduct *matrix_vector_product_alloc (DataContainer *matrix_data, void (* product) (double *product, DataContainer *matrix_data, const double *vector));
void matrix_vector_product_free (MatrixVectorProduct *matrix_vector_product);
MatrixVectorProduct *matrix_vector_product_duplicate (MatrixVectorProduct *matrix_vector_product);
void matrix_vector_product_calculate (double *product, MatrixVectorProduct *matrix_vector_product, const double *vector);
MatrixVectorProduct *usual_matrix_vector_product_alloc (int size_row, int size_column, const double *data);
InverseMatrixVectorProduct *inverse_matrix_vector_product_alloc (uint dimension, double error_residual_ratio, int gmres_max_iteration, MatrixVectorProduct *matrix_vector_product, double number_shift);
GMRESSolver *inverse_matrix_vector_product_gmres_ptr (InverseMatrixVectorProduct *matrix_vector_product);
double inverse_matrix_vector_product_number_shift (InverseMatrixVectorProduct *matrix_vector_product);
JacobianVectorProduct *jacobian_of_vector_field_product_alloc (DynamicalSystem *dynamical_system, const double *pt);
DerivativeOfSystem *jacobian_of_vector_field_product_derivative_of_system_ptr (JacobianVectorProduct *matrix_vector_product);

/**
 * \addtogroup MatrixVectorProduct
 * @{
 */
#define matrix_vector_product_matrix_data_ptr(matrix_vector_product) ((DataContainer *) data_container_data_ptr((DataContainer *) matrix_vector_product, MATRIX_VECTOR_PRODUCT_DATA))
#define matrix_vector_product_function_ptr(matrix_vector_product) ((void (*) (double *product, void *data, const double *vector)) data_container_data_ptr((DataContainer *) matrix_vector_product, MATRIX_VECTOR_PRODUCT_FUNCTION))
/** @} */  /* End of MatrixVectorProduct */

ArnoldiMethod *arnoldi_method_alloc (uint dimension, uint max_iteration, MatrixVectorProduct *matrix_vector_product);
void arnoldi_method_free (ArnoldiMethod *arnoldi_method);
void arnoldi_method_set_min_norm (ArnoldiMethod *arnoldi_method , double min_norm);
void arnoldi_method_set (ArnoldiMethod *arnoldi_method, const double *v);
void arnoldi_method_set_randomly (ArnoldiMethod *arnoldi_method);
gboolean arnoldi_method_iterate (ArnoldiMethod *arnoldi_method);
const double *arnoldi_method_hessenberg_column (ArnoldiMethod *arnoldi_method);
const double *arnoldi_method_basis_ptr (ArnoldiMethod *arnoldi_method, int ind);
double *arnoldi_method_basis_matrix (ArnoldiMethod *arnoldi_method);
double *arnoldi_method_iterate_successively (int *row_number, int *column_number, ArnoldiMethod *arnoldi_method);

/**
 * \addtogroup ArnoldiMethod
 * @{
 */
#define arnoldi_method_matrix_vector_product(out, arnoldi_method, in) matrix_vector_product_calculate(out, arnoldi_method->matrix_vector_product, in)
/** @} */  /* End of ArnoldiMethod */

GMRESSolver *gmres_solver_alloc (uint dimension, double error_residual_ratio, MatrixVectorProduct *matrix_vector_product);
GMRESSolver *gmres_solver_restarted_alloc (uint dimension, int restart, double error_residual_ratio, MatrixVectorProduct *matrix_vector_product);
void gmres_solver_free (GMRESSolver *solver);
GMRESSolver *gmres_solver_duplicate (GMRESSolver *solver);
void gmres_solver_set (GMRESSolver *solver, const double *x0, const double *b);
double *gmres_solver_solution (GMRESSolver *solver);
void gmres_solver_set_error_residual_ratio (GMRESSolver *solver, double error_residual_ratio);
void gmres_solver_set_min_error (GMRESSolver *solver, double min_error);
void gmres_solver_set_restart_parameter (GMRESSolver *solver, GMRESRestartType restart_type, ...);
void gmres_solver_set_restart_parameter_va_list (GMRESSolver *solver, GMRESRestartType restart_type, va_list args);
gboolean gmres_solver_converged_p (GMRESSolver *solver);
gboolean gmres_solver_iterate (GMRESSolver *solver);
double *gmres_solver_iterate_successively (GMRESSolver *solver, int max_iteration);
/**
 * \addtogroup GMRESSolver
 * @{
 */
#define gmres_solver_initial_solution_set_p(solver) (solver->vector_b && solver->vector_x0)
#define gmres_solver_dimension(solver) (solver->dimension)
#define gmres_solver_set_log_level(solver, log_level) mathfluid_logger_set_log_level(solver->logger, log_level)
/** @} */  /* End of GMRESSolver */

DerivativeOfSystem *derivative_of_system_alloc (DynamicalSystem *dynamical_system);
void derivative_of_system_free (DerivativeOfSystem *derivative_of_system);
DerivativeOfSystem *derivative_of_system_duplicate (DerivativeOfSystem *derivative_of_system);
void derivative_of_system_set_time_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double time_evolution);
void derivative_of_system_set_space_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double perturbation);
void derivative_of_system_set_parameter_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double perturbation);
void derivative_of_system_set_ignore_exact_jacobian (DerivativeOfSystem *derivative_of_system, gboolean ignore);
void derivative_of_system_set_ignore_exact_time_derivative (DerivativeOfSystem *derivative_of_system, gboolean ignore);
void derivative_of_system_set_ignore_exact_parameter_derivative (DerivativeOfSystem *derivative_of_system, gboolean ignore);
void derivative_of_system_jacobian_vector_product (double *av, DerivativeOfSystem *derivative_of_system, const double *pt, const double *v);
void derivative_of_system_time_derivative (double *time_derivative, DerivativeOfSystem *derivative_of_system, const double *x);
void derivative_of_system_parameter_derivative (double *parameter_derivative, DerivativeOfSystem *derivative_of_system, const char* parameter_key, const double *x);

JacobianFreeGMRES *jacobian_free_gmres_alloc (DynamicalSystem *dynamical_system, int dimension, void *data, void (* product) (double *av, JacobianFreeGMRES *jacobian_free_gmres, const double *v));
void jacobian_free_gmres_free (JacobianFreeGMRES *jacobian_free_gmres);
void jacobian_free_gmres_set_max_iteration (JacobianFreeGMRES *jacobian_free_gmres, int max_iteration);
void jacobian_free_gmres_set_ignore_gmres_convergence (JacobianFreeGMRES *jacobian_free_gmres, gboolean ignore);
double *jacobian_free_gmres_solve (JacobianFreeGMRES *jacobian_free_gmres, const double *x0, const double *b);
void jacobian_free_gmres_product_jacobian_vector (double *av, JacobianFreeGMRES *jacobian_free_gmres, const double *pt, const double *v);
void jacobian_free_gmres_time_derivative (double *time_derivative, JacobianFreeGMRES *jacobian_free_gmres, const double *x);
void jacobian_free_gmres_parameter_derivative (double *parameter_derivative, JacobianFreeGMRES *jacobian_free_gmres, const char* parameter_key, const double *x);
/**
 * \addtogroup JacobianFreeGMRES
 * @{
 */
#define jacobian_free_gmres_set(jacobian_free_gmres, x0, b) gmres_solver_set(jacobian_free_gmres->gmres_solver, x0, b)
#define jacobian_free_gmres_solution(jacobian_free_gmres) gmres_solver_solution(jacobian_free_gmres->gmres_solver)
#define jacobian_free_gmres_set_error_residual_ratio(jacobian_free_gmres, error_residual_ratio) gmres_solver_set_error_residual_ratio(jacobian_free_gmres->gmres_solver, error_residual_ratio)
#define jacobian_free_gmres_set_min_error(jacobian_free_gmres, min_error) gmres_solver_set_min_error(jacobian_free_gmres->gmres_solver, min_error)
#define jacobian_free_gmres_set_restart_parameter(jacobian_free_gmres, restart_type, ...) gmres_solver_set_restart_parameter(jacobian_free_gmres->gmres_solver, restart_type, __VA_ARGS__)
#define jacobian_free_gmres_set_restart_parameter_va_list(jacobian_free_gmres, restart_type, args) gmres_solver_set_restart_parameter_va_list(jacobian_free_gmres->gmres_solver, restart_type, args)
#define jacobian_free_gmres_converged_p(jacobian_free_gmres) gmres_solver_converged_p(jacobian_free_gmres->gmres_solver)
#define jacobian_free_gmres_dimension(jacobian_free_gmres) gmres_solver_dimension(jacobian_free_gmres->gmres_solver)
#define jacobian_free_gmres_dynamical_system(jacobian_free_gmres) (jacobian_free_gmres->dynamical_system)
#define jacobian_free_gmres_set_log_level(jacobian_free_gmres, log_level) mathfluid_logger_set_log_level(jacobian_free_gmres->gmres_solver->logger, log_level)
/**
 * Set time step size of numerical differential of vector field, which is used when the differential can not be calculated directly.
 */
#define jacobian_free_gmres_set_time_derivative_perturbation(jacobian_free_gmres, time_evolution) derivative_of_system_set_time_derivative_perturbation(jacobian_free_gmres->derivative, time_evolution)
/**
 * Set perturbation scale of Jacobian free product
 */
#define jacobian_free_gmres_set_space_derivative_perturbation(jacobian_free_gmres, perturbation) derivative_of_system_set_space_derivative_perturbation(jacobian_free_gmres->derivative, perturbation)
/**
 * Set perturbation scale of Jacobian free product
 */
#define jacobian_free_gmres_set_parameter_derivative_perturbation(jacobian_free_gmres, perturbation) derivative_of_system_set_parameter_derivative_perturbation(jacobian_free_gmres->derivative, perturbation)
#define jacobian_free_gmres_set_ignore_exact_jacobian(jacobian_free_gmres, ignore) derivative_of_system_set_ignore_exact_jacobian(jacobian_free_gmres->derivative, ignore)
/** @} */  /* End of JacobianFreeGMRES */

JacobianFreeNewtonGMRES *jacobian_free_newton_gmres_alloc (JacobianFreeGMRES *gmres, void (* function) (double *value, JacobianFreeGMRES *gmres, const double *pt), int damping_parameter, double max_error);
void jacobian_free_newton_gmres_free (JacobianFreeNewtonGMRES *newton_gmres);
void jacobian_free_newton_gmres_set_function_after_iteration (JacobianFreeNewtonGMRES *newton_gmres, void (* after_iteration) (JacobianFreeNewtonGMRES *newton_gmres));
void jacobian_free_newton_gmres_set_norm_function (JacobianFreeNewtonGMRES *newton_gmres, MathFluidNormFunc *func, void *norm_args);
void jacobian_free_newton_gmres_set_log_level (JacobianFreeNewtonGMRES *newton_gmres, MathFluidLogLevel level);
void jacobian_free_newton_gmres_set_max_error (JacobianFreeNewtonGMRES *newton_gmres, double max_error);
void jacobian_free_newton_gmres_set_practical_error (JacobianFreeNewtonGMRES *newton_gmres, double practical_error);
void jacobian_free_newton_gmres_set_max_uniform_norm (JacobianFreeNewtonGMRES *newton_gmres, double max_uniform_norm);
void jacobian_free_newton_gmres_set_damping_parameter (JacobianFreeNewtonGMRES *newton_gmres, int damping_parameter);
gboolean jacobian_free_newton_gmres_solution_copy (double *vector, JacobianFreeNewtonGMRES *newton_gmres);
double *jacobian_free_newton_gmres_solution (JacobianFreeNewtonGMRES *newton_gmres);
void jacobian_free_newton_gmres_set_initial_solution (JacobianFreeNewtonGMRES *newton_gmres, const double *vector);
double jacobian_free_newton_gmres_current_error (JacobianFreeNewtonGMRES *newton_gmres);
gboolean jacobian_free_newton_gmres_converged_p (JacobianFreeNewtonGMRES *newton_gmres);
gboolean jacobian_free_newton_gmres_iterate (JacobianFreeNewtonGMRES *newton_gmres);
int jacobian_free_newton_gmres_iterate_successively (JacobianFreeNewtonGMRES *newton_gmres, uint max_iteration);
int jacobian_free_newton_gmres_iterate_successively_monotonic_convergence (JacobianFreeNewtonGMRES *newton_gmres, uint max_iteration);
/**
 * \addtogroup JacobianFreeNewtonGMRES
 * @{
 */
#define jacobian_free_newton_gmres_dimension(newton_gmres) (jacobian_free_gmres_dimension(newton_gmres->gmres))
#define jacobian_free_newton_gmres_solution_ptr(newton_gmres) (newton_gmres->vector->data)
#define jacobian_free_newton_gmres_max_error(newton_gmres) (newton_gmres->max_error)
#define jacobian_free_newton_gmres_practical_error(newton_gmres) (newton_gmres->practical_error)
#define jacobian_free_newton_gmres_max_uniform_norm(newton_gmres) (newton_gmres->max_uniform_norm)
#define jacobian_free_newton_gmres_damping_parameter(newton_gmres) (newton_gmres->damping_parameter)
/** @} */  /* End of JacobianFreeNewtonGMRES */

RayleighRitz *rayleigh_ritz_alloc (uint dimension, MatrixVectorProduct *product);
void rayleigh_ritz_free (RayleighRitz *rayleigh_ritz);
void rayleigh_ritz_set_basis (RayleighRitz *rayleigh_ritz, uint basis_number, double *basis);
void rayleigh_ritz_set_matrix_vector_product (RayleighRitz *rayleigh_ritz, MatrixVectorProduct *product);
void rayleigh_ritz_set_matrix (RayleighRitz *rayleigh_ritz, const double *matrix);
int rayleigh_ritz_eigen (double complex **eigenvalue, double complex **eigenvector, RayleighRitz *rayleigh_ritz);
void rayleigh_ritz_convert_inverse_eigenvalues (double complex *eigenvalue, int num, double number_shift);
void rayleigh_ritz_eigen_sort_by_absolute_value (double complex *eigenvalue, double complex *eigenvector, int num, int dim);

SimultaneousIteration *simultaneous_iteration_alloc (uint dimension, uint basis_number, MatrixVectorProduct *product);
void simultaneous_iteration_free (SimultaneousIteration *simultaneous_iteration);
void simultaneous_iteration_set_qr (SimultaneousIteration *simultaneous_iteration, const double *qr);
void simultaneous_iteration_set_qr_randomly (SimultaneousIteration *simultaneous_iteration);
void simultaneous_iteration_decompose (SimultaneousIteration *simultaneous_iteration);
void simultaneous_iteration_product (SimultaneousIteration *simultaneous_iteration);
void simultaneous_iteration_iterate_successively (SimultaneousIteration *simultaneous_iteration, int number_iteration);
double *simultaneous_iteration_eigenvalues (double **eigenvector_max, SimultaneousIteration *simultaneous_iteration);
/**
 * \addtogroup SimultaneousIteration
 * @{
 */
#define simultaneous_iteration_qr_ptr(simultaneous_iteration) simultaneous_iteration->qr->data
#define simultaneous_iteration_q_ptr(simultaneous_iteration) simultaneous_iteration->q->data
#define simultaneous_iteration_r_ptr(simultaneous_iteration) simultaneous_iteration->r->data
/** @} */  /* End of SimultaneousIteration */

SearchEquilibrium *search_equilibrium_alloc (DynamicalSystem *dynamical_system, int damping_parameter, double error);
void search_equilibrium_free (SearchEquilibrium *search);
void search_equilibrium_set_log_level (SearchEquilibrium *search, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level);
/**
 * \addtogroup SearchEquilibrium
 * @{
 */
#define search_equilibrium_solution_copy(vector, search) jacobian_free_newton_gmres_solution_copy(vector, search->newton_gmres)
#define search_equilibrium_solution(search) jacobian_free_newton_gmres_solution(search->newton_gmres)
#define search_equilibrium_set(search, vector) jacobian_free_newton_gmres_set_initial_solution(search->newton_gmres, vector)
#define search_equilibrium_iterate(search) jacobian_free_newton_gmres_iterate(search->newton_gmres)
#define search_equilibrium_iterate_successively(search, max_iteration) jacobian_free_newton_gmres_iterate_successively(search->newton_gmres, max_iteration)
#define search_equilibrium_dynamical_system(search) jacobian_free_gmres_dynamical_system(search->gmres)
/**
 * Return the pointer of solver->vector. If the solver is freed then the returned point is freed too.
 */
#define search_equilibrium_solution_ptr(search) (search->newton_gmres->vector->data)
#define search_equilibrium_dimension(search) (jacobian_free_gmres_dimension(search->gmres))
#define search_equilibrium_set_gmres_max_iteration(search, max_iteration) jacobian_free_gmres_set_max_iteration(search->gmres, max_iteration)
#define search_equilibrium_set_gmres_error_residual_ratio(search, ratio) jacobian_free_gmres_set_error_residual_ratio(search->gmres, ratio)
#define search_equilibrium_set_gmres_min_error(search, min_error) jacobian_free_gmres_set_min_error(search->gmres, min_error)
#define search_equilibrium_set_gmres_restart_parameter(search, restart_type, ...) jacobian_free_gmres_set_restart_parameter(search->gmres, restart_type, __VA_ARGS__)
#define search_equilibrium_set_gmres_restart_parameter_va_list(search, restart_type, args) jacobian_free_gmres_set_restart_parameter_va_list(search->gmres, restart_type, args)
#define search_equilibrium_set_space_derivative_perturbation(search, perturbation) jacobian_free_gmres_set_space_derivative_perturbation(search->gmres, perturbation)
#define search_equilibrium_set_time_derivative_perturbation(search, perturbation) jacobian_free_gmres_set_time_derivative_perturbation(search->gmres, perturbation)
#define search_equilibrium_set_ignore_gmres_convergence(search, ignore) jacobian_free_gmres_set_ignore_gmres_convergence(search->gmres, ignore)
#define search_equilibrium_set_ignore_exact_jacobian(search, ignore) jacobian_free_gmres_set_ignore_exact_jacobian(search->gmres, ignore)
#define search_equilibrium_newton_max_error(search) jacobian_free_newton_gmres_max_error(search->newton_gmres)
#define search_equilibrium_newton_practical_error(search) jacobian_free_newton_gmres_practical_error(search->newton_gmres)
#define search_equilibrium_newton_max_uniform_norm(search) jacobian_free_newton_gmres_max_uniform_norm(search->newton_gmres)
#define search_equilibrium_newton_damping_parameter(search) jacobian_free_newton_gmres_damping_parameter(search->newton_gmres)
#define search_equilibrium_set_newton_max_error(search, max_error) jacobian_free_newton_gmres_max_error(search->newton_gmres, max_error)
#define search_equilibrium_set_newton_practical_error(search, practical_error) jacobian_free_newton_gmres_practical_error(search->newton_gmres, practical_error)
#define search_equilibrium_set_newton_max_uniform_norm(search, max_uniform_norm) jacobian_free_newton_gmres_max_uniform_norm(search->newton_gmres, max_uniform_norm)
#define search_equilibrium_set_newton_max_uniform_norm(search, max_uniform_norm) jacobian_free_newton_gmres_max_uniform_norm(search->newton_gmres, max_uniform_norm)
#define search_equilibrium_set_newton_damping_parameter(search, damping_parameter) jacobian_free_newton_gmres_damping_parameter(search->newton_gmres, damping_parameter)
#define search_equilibrium_set_newton_norm_function(search, norm_function, norm_args) jacobian_free_newton_gmres_set_norm_function(search->newton_gmres, norm_function, norm_args)
/** @} */  /* End of SearchEquilibrium */

ParameterContinuationCorrectorGaussNewton *parameter_continuation_corrector_gauss_newton_alloc (DynamicalSystem *dynamical_system, DataContainer *continuation_setting, double max_error);
void parameter_continuation_corrector_gauss_newton_free (ParameterContinuationCorrectorGaussNewton *gauss_newton);
void parameter_continuation_corrector_gauss_newton_set_function_after_iteration (ParameterContinuationCorrectorGaussNewton *gauss_newton, void (* after_iteration) (ParameterContinuationCorrectorGaussNewton *gauss_newton));
void parameter_continuation_corrector_gauss_newton_set_norm_function (ParameterContinuationCorrectorGaussNewton *gauss_newton, MathFluidNormFunc *func, void *norm_args);
void parameter_continuation_corrector_gauss_newton_set_log_level (ParameterContinuationCorrectorGaussNewton *gauss_newton, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level);
void parameter_continuation_corrector_gauss_newton_set_max_error (ParameterContinuationCorrectorGaussNewton *gauss_newton, double max_error);
void parameter_continuation_corrector_gauss_newton_set_practical_error (ParameterContinuationCorrectorGaussNewton *gauss_newton, double practical_error);
void parameter_continuation_corrector_gauss_newton_set_max_uniform_norm (ParameterContinuationCorrectorGaussNewton *gauss_newton, double max_uniform_norm);
gboolean parameter_continuation_corrector_gauss_newton_solution_copy (double *vector, ParameterContinuationCorrectorGaussNewton *gauss_newton);
double *parameter_continuation_corrector_gauss_newton_solution (ParameterContinuationCorrectorGaussNewton *gauss_newton);
void parameter_continuation_corrector_gauss_newton_set_initial_solution (ParameterContinuationCorrectorGaussNewton *gauss_newton, const double *vector);
double parameter_continuation_corrector_gauss_newton_current_error (ParameterContinuationCorrectorGaussNewton *gauss_newton);
gboolean parameter_continuation_corrector_gauss_newton_converged_p (ParameterContinuationCorrectorGaussNewton *gauss_newton);
gboolean parameter_continuation_corrector_gauss_newton_iterate (ParameterContinuationCorrectorGaussNewton *gauss_newton);
int parameter_continuation_corrector_gauss_newton_iterate_successively (ParameterContinuationCorrectorGaussNewton *gauss_newton, uint max_iteration);
int parameter_continuation_corrector_gauss_newton_iterate_successively_monotonic_convergence (ParameterContinuationCorrectorGaussNewton *gauss_newton, uint max_iteration);
/**
 * \addtogroup ParameterContinuationCorrectorGaussNewton
 * @{
 */
#define parameter_continuation_corrector_gauss_newton_dimension(gauss_newton) (jacobian_free_gmres_dimension(gauss_newton->gmres))
#define parameter_continuation_corrector_gauss_newton_solution_ptr(gauss_newton) (gauss_newton->vector->data)
#define parameter_continuation_corrector_gauss_newton_max_error(gauss_newton) (gauss_newton->max_error)
#define parameter_continuation_corrector_gauss_newton_practical_error(gauss_newton) (gauss_newton->practical_error)
#define parameter_continuation_corrector_gauss_newton_max_uniform_norm(gauss_newton) (gauss_newton->max_uniform_norm)
#define parameter_continuation_corrector_gauss_newton_damping_parameter(gauss_newton) (gauss_newton->damping_parameter)
/** @} */  /* End of ParameterContinuationCorrectorGaussNewton */

ParameterContinuationCorrector *parameter_continuation_corrector_alloc (ParameterContinuationCorrection correction_type, DynamicalSystem *dynamical_system, const char *parameter_key, int newton_damping_parameter, double newton_error);
void parameter_continuation_corrector_free (ParameterContinuationCorrector *corrector);
void parameter_continuation_corrector_set_tangent_vector_and_point (ParameterContinuationCorrector *corrector, gsl_vector *tangent_vector, gsl_vector *pt_last);
void parameter_continuation_gmres_setting_set_tangent_vector (DataContainer *setting, gsl_vector *tangent_vector);
void parameter_continuation_gmres_setting_set_pt_last (DataContainer *setting, gsl_vector *pt_last);
int parameter_continuation_corrector_search_equilibrium (ParameterContinuationCorrector *corrector, const double *equilibrium_with_parameter_initial, int newton_max_iteration);
gboolean parameter_continuation_corrector_solution_copy (double *vector, ParameterContinuationCorrector *corrector);
double parameter_continuation_corrector_next_step_length (ParameterContinuationCorrector *corrector, int iter, double step_length);
void parameter_continuation_corrector_set_log_level (ParameterContinuationCorrector *corrector, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level);
void parameter_continuation_corrector_set_ignore_exact_jacobian (ParameterContinuationCorrector *corrector, gboolean ignore);
void parameter_continuation_corrector_set_space_derivative_perturbation (ParameterContinuationCorrector *corrector, double space_derivative_perturbation);
void parameter_continuation_corrector_set_time_derivative_perturbation (ParameterContinuationCorrector *corrector, double time_derivative_perturbation);
void parameter_continuation_corrector_set_parameter_derivative_perturbation (ParameterContinuationCorrector *corrector, double parameter_perturbation);
void parameter_continuation_corrector_gmres_set_max_iteration (ParameterContinuationCorrector *corrector, int max_iteration);
void parameter_continuation_corrector_gmres_set_error_residual_ratio (ParameterContinuationCorrector *corrector, double ratio);
void parameter_continuation_corrector_gmres_set_min_error (ParameterContinuationCorrector *corrector, double min_error);
void parameter_continuation_corrector_gmres_set_restart_parameter (ParameterContinuationCorrector *corrector, GMRESRestartType restart_type, ...);
void parameter_continuation_corrector_gmres_set_restart_parameter_va_list (ParameterContinuationCorrector *corrector, GMRESRestartType restart_type, va_list args);
void parameter_continuation_corrector_gmres_set_ignore_gmres_convergence (ParameterContinuationCorrector *corrector, gboolean ignore);
double parameter_continuation_corrector_newton_gmres_max_error (ParameterContinuationCorrector *corrector);
double parameter_continuation_corrector_newton_gmres_practical_error (ParameterContinuationCorrector *corrector);
double parameter_continuation_corrector_newton_gmres_max_uniform_norm (ParameterContinuationCorrector *corrector);
int parameter_continuation_corrector_newton_gmres_damping_parameter (ParameterContinuationCorrector *corrector);
void parameter_continuation_corrector_newton_gmres_set_max_error (ParameterContinuationCorrector *corrector, double max_error);
void parameter_continuation_corrector_newton_gmres_set_practical_error (ParameterContinuationCorrector *corrector, double practical_error);
void parameter_continuation_corrector_newton_gmres_set_max_uniform_norm (ParameterContinuationCorrector *corrector, double max_uniform_norm);
void parameter_continuation_corrector_newton_gmres_set_damping_parameter (ParameterContinuationCorrector *corrector, int damping_parameter);
void parameter_continuation_corrector_newton_gmres_set_norm_function (ParameterContinuationCorrector *corrector, MathFluidNormFunc norm_function, void *norm_args);

ParameterContinuation *parameter_continuation_alloc (ParameterContinuationCorrection correction_method, DynamicalSystem *dynamical_system, const char *parameter_key, ParameterContinuationDirection direction, int newton_damping_parameter, double newton_error);
void parameter_continuation_free (ParameterContinuation *continuation);
void parameter_continuation_set_log_level (ParameterContinuation *continuation, MathFluidLogLevel continuation_level, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level);
void parameter_continuation_reset_arc_length (ParameterContinuation *continuation);
void parameter_continuation_set_step_length_control (ParameterContinuation *continuation, ParameterContinuationStepLengthControl control, ...);
void parameter_continuation_set_newton_max_iteration (ParameterContinuation *continuation, int newton_max_iteration);
void parameter_continuation_set_ignore_exact_jacobian (ParameterContinuation *continuation, gboolean ignore);
void parameter_continuation_set_space_derivative_perturbation (ParameterContinuation *continuation, double space_derivative_perturbation);
void parameter_continuation_set_time_derivative_perturbation (ParameterContinuation *continuation, double time_derivative_perturbation);
void parameter_continuation_set_parameter_derivative_perturbation (ParameterContinuation *continuation, double parameter_perturbation);
void parameter_continuation_set_equilibrium (ParameterContinuation *continuation, const double *equilibrium);
void parameter_continuation_get_equilibrium (double *equilibrium, double *parameter, ParameterContinuation *continuation);
gboolean parameter_continuation_step_forward_until (ParameterContinuation *continuation, double max_arc_length, void (* func) (ParameterContinuation *continuation, void *data), void *data);
/**
 * \addtogroup ParameterContinuation
 * @{
 */
#define parameter_continuation_extended_dimension(continuation) (jacobian_free_gmres_dimension(continuation->predictor_gmres))
#define parameter_continuation_system_dimension(continuation) (jacobian_free_gmres_dimension(continuation->predictor_gmres) - 1)
#define parameter_continuation_dynamical_system(continuation) (jacobian_free_gmres_dynamical_system(continuation->predictor_gmres))
#define parameter_continuation_set_gmres_predictor_max_iteration(continuation, max_iteration) jacobian_free_gmres_set_max_iteration(continuation->predictor_gmres, max_iteration)
#define parameter_continuation_set_gmres_corrector_max_iteration(continuation, max_iteration) parameter_continuation_corrector_gmres_set_max_iteration(continuation->corrector, max_iteration)
#define parameter_continuation_set_gmres_predictor_error_residual_ratio(continuation, ratio) jacobian_free_gmres_set_error_residual_ratio(continuation->predictor_gmres, ratio)
#define parameter_continuation_set_gmres_corrector_error_residual_ratio(continuation, ratio) parameter_continuation_corrector_gmres_set_error_residual_ratio(continuation->corrector, ratio)
#define parameter_continuation_set_gmres_predictor_min_error(continuation, min_error) jacobian_free_gmres_set_min_error(continuation->predictor_gmres, min_error)
#define parameter_continuation_set_gmres_corrector_min_error(continuation, min_error) parameter_continuation_corrector_gmres_set_min_error(continuation->corrector, min_error)
#define parameter_continuation_set_gmres_predictor_restart_parameter(continuation, restart_type, ...) jacobian_free_gmres_set_restart_parameter(continuation->predictor_gmres, restart_type, __VA_ARGS__)
#define parameter_continuation_set_gmres_corrector_restart_parameter(continuation, restart_type, ...) parameter_continuation_corrector_gmres_set_restart_parameter(continuation->corrector, restart_type, __VA_ARGS__)
#define parameter_continuation_set_gmres_predictor_restart_parameter_va_list(continuation, restart_type, args) jacobian_free_gmres_set_restart_parameter_va_list(continuation->predictor_gmres, restart_type, args)
#define parameter_continuation_set_gmres_corrector_restart_parameter_va_list(continuation, restart_type, args) parameter_continuation_corrector_gmres_set_restart_parameter_va_list(continuation->corrector, restart_type, args)
#define parameter_continuation_set_gmres_predictor_ignore_convergence(continuation, ignore) jacobian_free_gmres_set_ignore_gmres_convergence(continuation->predictor_gmres, ignore)
#define parameter_continuation_set_gmres_corrector_ignore_convergence(continuation, ignore) parameter_continuation_corrector_gmres_set_ignore_gmres_convergence(continuation->corrector, ignore)
#define parameter_continuation_newton_max_error(continuation) parameter_continuation_corrector_newton_gmres_max_error(continuation->corrector)
#define parameter_continuation_newton_practical_error(continuation) parameter_continuation_corrector_newton_gmres_practical_error(continuation->corrector)
#define parameter_continuation_newton_max_uniform_norm(continuation) parameter_continuation_corrector_newton_gmres_max_uniform_norm(continuation->corrector)
#define parameter_continuation_newton_damping_parameter(continuation) parameter_continuation_corrector_newton_gmres_damping_parameter(continuation->corrector)
#define parameter_continuation_set_newton_max_error(continuation, max_error) parameter_continuation_corrector_newton_gmres_set_max_error(continuation->corrector, max_error)
#define parameter_continuation_set_newton_practical_error(continuation, practical_error) parameter_continuation_corrector_newton_gmres_set_practical_error(continuation->corrector, practical_error)
#define parameter_continuation_set_newton_max_uniform_norm(continuation, max_uniform_norm) parameter_continuation_corrector_newton_gmres_set_max_uniform_norm(continuation->corrector, max_uniform_norm)
#define parameter_continuation_set_newton_damping_parameter(continuation, damping_parameter) parameter_continuation_corrector_newton_gmres_set_damping_parameter(continuation->corrector, damping_parameter)
#define parameter_continuation_set_newton_norm_function(continuation, norm_function, norm_args) parameter_continuation_corrector_newton_gmres_set_norm_function(continuation->corrector, norm_function, norm_args)
/** @} */  /* End of ParameterContinuation */

SearchPeriodicOrbit *search_periodic_orbit_alloc (DynamicalSystem *dynamical_system, int damping_parameter, double max_error, double practical_error);
void search_periodic_orbit_free (SearchPeriodicOrbit *search);
void search_periodic_orbit_set_log_level (SearchPeriodicOrbit *search, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level);
void search_periodic_orbit_set2 (SearchPeriodicOrbit *search, const double *point, double period);
/**
 * \addtogroup SearchPeriodicOrbit
 * @{
 */
#define search_periodic_orbit_solution_copy(vector, search) jacobian_free_newton_gmres_solution_copy(vector, search->newton_gmres)
#define search_periodic_orbit_solution(search) jacobian_free_newton_gmres_solution(search->newton_gmres)
#define search_periodic_orbit_set(search, vector) jacobian_free_newton_gmres_set_initial_solution(search->newton_gmres, vector)
#define search_periodic_orbit_iterate(search) jacobian_free_newton_gmres_iterate(search->newton_gmres)
#define search_periodic_orbit_iterate_successively(search, max_iteration) jacobian_free_newton_gmres_iterate_successively(search->newton_gmres, max_iteration)
#define search_periodic_orbit_dynamical_system(search) jacobian_free_gmres_dynamical_system(search->gmres)
/**
 * Return the pointer of solver->vector. If the solver is freed then the returned point is freed too.
 */
#define search_periodic_orbit_solution_ptr(search) (search->newton_gmres->vector->data)
#define search_periodic_orbit_system_dimension(search) (dynamical_system_dimension(search_periodic_orbit_dynamical_system(search)))
#define search_periodic_orbit_total_dimension(search) (jacobian_free_gmres_dimension(search->gmres))
#define search_periodic_orbit_current_initial_point_ptr(search) (jacobian_free_newton_gmres_solution_ptr(search->newton_gmres))
#define search_periodic_orbit_current_period(search) ((search_periodic_orbit_current_initial_point_ptr(search))[search_periodic_orbit_system_dimension(search)])
#define search_periodic_orbit_set_gmres_max_iteration(search, max_iteration) jacobian_free_gmres_set_max_iteration(search->gmres, max_iteration)
#define search_periodic_orbit_set_gmres_error_residual_ratio(search, ratio) jacobian_free_gmres_set_error_residual_ratio(search->gmres, ratio)
#define search_periodic_orbit_set_gmres_min_error(search, min_error) jacobian_free_gmres_set_min_error(search->gmres, min_error)
#define search_periodic_orbit_set_gmres_restart_parameter(search, restart_type, ...) jacobian_free_gmres_set_restart_parameter(search->gmres, restart_type, __VA_ARGS__)
#define search_periodic_orbit_set_gmres_restart_parameter_va_list(search, restart_type, args) jacobian_free_gmres_set_restart_parameter_va_list(search->gmres, restart_type, args)
#define search_periodic_orbit_set_space_derivative_perturbation(search, perturbation) jacobian_free_gmres_set_space_derivative_perturbation(search->gmres, perturbation)
#define search_periodic_orbit_set_time_derivative_perturbation(search, perturbation) jacobian_free_gmres_set_time_derivative_perturbation(search->gmres, perturbation)
#define search_periodic_orbit_set_ignore_gmres_convergence(search, ignore) jacobian_free_gmres_set_ignore_gmres_convergence(search->gmres, ignore)
#define search_periodic_orbit_set_ignore_exact_jacobian(search, ignore) jacobian_free_gmres_set_ignore_exact_jacobian(search->gmres, ignore)
#define search_periodic_orbit_newton_max_error(search) jacobian_free_newton_gmres_max_error(search->newton_gmres)
#define search_periodic_orbit_newton_practical_error(search) jacobian_free_newton_gmres_practical_error(search->newton_gmres)
#define search_periodic_orbit_newton_max_uniform_norm(search) jacobian_free_newton_gmres_max_uniform_norm(search->newton_gmres)
#define search_periodic_orbit_newton_damping_parameter(search) jacobian_free_newton_gmres_damping_parameter(search->newton_gmres)
#define search_periodic_orbit_set_newton_max_error(search, max_error) jacobian_free_newton_gmres_max_error(search->newton_gmres, max_error)
#define search_periodic_orbit_set_newton_practical_error(search, practical_error) jacobian_free_newton_gmres_practical_error(search->newton_gmres, practical_error)
#define search_periodic_orbit_set_newton_max_uniform_norm(search, max_uniform_norm) jacobian_free_newton_gmres_max_uniform_norm(search->newton_gmres, max_uniform_norm)
#define search_periodic_orbit_set_newton_damping_parameter(search, damping_parameter) jacobian_free_newton_gmres_damping_parameter(search->newton_gmres, damping_parameter)
#define search_periodic_orbit_set_newton_norm_function(search, norm_function, norm_args) jacobian_free_newton_gmres_set_norm_function(search->newton_gmres, norm_function, norm_args)
/** @} */  /* End of SearchPeriodicOrbit */

LevelDBStackCache *leveldb_stack_cache_alloc (char *(*data_dump) (size_t *size, gpointer data), gpointer (*data_load) (char *dump, size_t size), void (*data_free) (gpointer data));
void leveldb_stack_cache_set_db (LevelDBStackCache *stack_cache, const char *path);
void leveldb_stack_cache_free (LevelDBStackCache *stack_cache, gboolean destroy);
void leveldb_stack_cache_flush (LevelDBStackCache *stack_cache);
void leveldb_stack_cache_push (LevelDBStackCache *stack_cache, gpointer data);
gpointer leveldb_stack_cache_pop (LevelDBStackCache *stack_cache);

LyapunovVector *lyapunov_vector_alloc (int dimension, int number);
LyapunovVector *lyapunov_vector_alloc_set (LyapunovAnalysis *analysis, SystemState *state, gsl_matrix *vectors);
void lyapunov_vector_free (LyapunovVector *lyapunov_vector);
double *lyapunov_vector_double_array_row_major_order (LyapunovVector *lyapunov_vector);
LyapunovVectorSet *lyapunov_vector_set_alloc (void);
void lyapunov_vector_set_set_db (LyapunovVectorSet *vector_set, const char *path);
void lyapunov_vector_set_free (LyapunovVectorSet *vector_set);
void lyapunov_vector_set_push (LyapunovVectorSet *vector_set, LyapunovVector *vector);
LyapunovVector *lyapunov_vector_set_pop (LyapunovVectorSet *vector_set);
void lyapunov_vector_set_fprintf (FILE *out, LyapunovVectorSet *lyapunov_vectors, const char *number_format, const char *number_splitter);

LyapunovAnalysis *lyapunov_analysis_alloc (DynamicalSystem *dynamical_system, int num, double time_step);
void lyapunov_analysis_free (LyapunovAnalysis *analysis);
void lyapunov_analysis_set_use_thread (LyapunovAnalysis *analysis, gboolean use_thread);
void lyapunov_analysis_set_space_derivative_perturbation (LyapunovAnalysis *analysis, double perturbation);
void lyapunov_analysis_set_ignore_exact_jacobian (LyapunovAnalysis *analysis, gboolean ignore);
void lyapunov_analysis_set_cache_directory_path (LyapunovAnalysis *analysis, const char *cache_directory);
double *lyapunov_analysis_lyapunov_exponents (LyapunovAnalysis *analysis, double t, const double *x);
GPtrArray *lyapunov_analysis_finite_time_lyapunov_exponents (LyapunovAnalysis *analysis, double t1, double t2, const double *x);
LyapunovVectorSet *lyapunov_analysis_backward_lyapunov_vectors (LyapunovAnalysis *analysis, double t1, double t2, const double *x);
LyapunovVectorSet *lyapunov_analysis_covariant_lyapunov_vectors (LyapunovAnalysis *analysis, double t1, double t2, double t3, const double *x);
LyapunovVectorSet *lyapunov_analysis_covariant_lyapunov_vectors_with_iteration (LyapunovAnalysis *analysis, double t1, double t2, double t3, const double *x, int initial_iterate, int last_iterate);
LyapunovVector *lyapunov_analysis_backward_lyapunov_vectors_of_equilibrium (LyapunovAnalysis *analysis, int iterate, const double *equilibrium);
LyapunovVector *lyapunov_analysis_covariant_lyapunov_vectors_of_equilibrium (double **lyapunov_exponents, LyapunovAnalysis *analysis, int iterate1, int iterate2, const double *equilibrium);
GPtrArray *lyapunov_analysis_finite_time_covariant_lyapunov_exponents (LyapunovAnalysis *analysis, LyapunovVectorSet *covariant_lyapunov_vectors);
GPtrArray *lyapunov_analysis_finite_time_covariant_lyapunov_exponents2 (LyapunovAnalysis *analysis, const char *path_covariant_lyapunov_vectors);
GPtrArray *lyapunov_analysis_covariant_lyapunov_vectors_test (LyapunovAnalysis *analysis, const char *path_clv);
#define lyapunov_analysis_dynamical_system(analysis) (analysis->evolve->dynamical_system)
#define lyapunov_analysis_system_dimension(analysis) (analysis->evolve->dynamical_system->dimension)

void g_array_free_completely (GArray *array);
GArray *g_array_duplicate (GArray *array);

void gsl_vector_set_components (gsl_vector *v, const double *src);
gsl_vector *gsl_vector_alloc_set_components (const size_t n, const double *src);
void gsl_vector_copy_components (double *components, gsl_vector *v);
double *gsl_vector_alloc_copy_components (gsl_vector *v);
gsl_vector *gsl_vector_duplicate (gsl_vector *vector);
void gsl_vector_set_random_normalized_components (gsl_vector *vector);
void gsl_matrix_set_elements (gsl_matrix *m, const double *src);
gsl_matrix *gsl_matrix_alloc_set_elements (const size_t n1, const size_t n2, const double *src);
void gsl_matrix_copy_elements (double *elements, gsl_matrix *m);
double *gsl_matrix_alloc_copy_elements (gsl_matrix *m);
gsl_matrix *gsl_matrix_duplicate (gsl_matrix *matrix);
void gsl_matrix_orthonormalize_columns (gsl_matrix *q);
void gsl_matrix_set_random_normalized_orthogonal_columns (gsl_matrix *q);
void gsl_matrix_normalize_all_columns (gsl_matrix *a);
gsl_matrix *gsl_matrix_alloc_random_upper_triangular_matrix (int dim);

int gsl_wrap_eigen_with_overwrite (double complex **eigenvalue, double complex **eigenvector, gsl_matrix *m);
int gsl_wrap_eigen (double complex **eigenvalue, double complex **eigenvector, gsl_matrix *m);
void gsl_wrap_qr_decomp_with_overwrite (gsl_matrix *q, gsl_matrix *r, gsl_matrix *qr);
void gsl_wrap_qr_decomp (gsl_matrix *q, gsl_matrix *r, gsl_matrix *qr);

#endif /* _MATHFLUID_H_ */
