#include "low_dimensional_system/low_dimensional_system.h"
#include "miscellaneous_system/miscellaneous_system.h"

int analyses_main_function (int argc, char **argv)
{
  low_dimensional_system_init(argv[0]);
  miscellaneous_system_init(argv[0]);
  return subcommand_list_global_execute_command(argc, argv);
}
