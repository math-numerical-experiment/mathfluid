describe MathFluid::FileParametrized do
  it "should yield one lines" do
    parametrized = MathFluid::FileParametrized.new(File.join(__dir__, "samples/test_file_parametrized.txt"), :parameter_min => 1.2, :parameter_max => 1.25)
    ary = parametrized.each_line.to_a
    expect(ary.size).to eq(1)
  end
end
