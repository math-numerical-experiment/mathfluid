require "json"

module MathFluid
  module HeaderJSON
    def each_header_line(path, &blcok)
      if block_given?
        MathFluid::Utils.open(path, "r") do |io|
          io.each_line do |line|
            if /^#/ =~ line
              yield(line)
            else
              break
            end
          end
        end
      else
        to_enum(:each_header_line, path)
      end
    end
    module_function :each_header_line

    def parse_header(path)
      json_data = ""
      each_header_line(path) do |line|
        json_data << line[1..-1]
      end
      JSON.parse(json_data)
    rescue => e
      puts "Error when parsing #{path}"
      raise e
    end
    module_function :parse_header
  end
end
