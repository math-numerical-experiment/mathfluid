module MathFluid
  class FileOneDimensionalData < MathFluid::FileTimeIndexed
    def section_display(opts = {})
      MathFluid::Gnuplot.new(opts).execute do |io|
        each_line do |time, line|
          io.puts "plot '-' with lines title 'Time: #{time}'"
          line.split.each do |str|
            io.puts str
          end
          io.puts 'e'
          $stdout.puts "Please put ENTER to display next"
          $stdin.gets
        end
      end
    end
  end
end
