module MathFluid
  class FileParametrized
    # @param [String] path Path of file whose lines parameter data
    # @param [Hash] opts   Options
    # @option opts [Float] :parameter_min Minimum parameter of target lines
    # @option opts [Float] :parameter_max Maximum parameter of target lines
    # @option opts [Integer] :parameter_index    Index number of parameter
    def initialize(path, opts = {})
      @path = path
      if !(String === @path) || !File.exist?(@path)
        raise "File does not exist: #{@path.inspect}"
      end
      @parameter_min = opts[:parameter_min]
      @parameter_max = opts[:parameter_max]
      @parameter_index_number = opts[:parameter_index] || 0
    end

    def get_parameter(line)
      if /([^\s]\s+){#{@parameter_index_number}}([^\s]+)(\s|$)/ =~ line
        Float(Regexp.last_match[2])
      else
        nil
      end
    end

    def header_comment
      MathFluid::HeaderJSON.each_header_line(@path).to_a.join
    end

    # Yield lines that are not comment. Note that parameter values of the lines are removed
    def each_line_not_comment
      if block_given?
        MathFluid::Utils.open(@path, "r") do |io|
          io.each_line do |line_raw|
            next if /^#/ =~ line_raw
            parameter = get_parameter(line_raw)
            yield(parameter, line_raw)
          end
        end
      else
        to_enum(:each_line_not_comment)
      end
    end
    private :each_line_not_comment

    def parameter_interval_include?(parameter)
      if @parameter_min && @parameter_min > parameter
        return false
      end
      if @parameter_max && @parameter_max < parameter
        return false
      end
      true
    end
    private :parameter_interval_include?

    def each_line_in_interval(&block)
      last = :initial
      each_line_not_comment do |parameter, line_raw|
        if parameter && parameter_interval_include?(parameter)
          if last == :break
            yield(nil, nil)
          end
          last = { :parameter => parameter, :line_raw => line_raw }
          yield(parameter, line_raw)
        else
          if last != :initial
            last = :break
          end
        end
      end
    end
    private :each_line_in_interval

    # Yield each data [parameter, line_raw].
    # If there are skips of data lines, yield [nil, nil].
    def each_line(&block)
      if block_given?
        each_line_in_interval(&block)
      else
        to_enum(:each_line)
      end
    end

    def extract(opts = {})
      out = opts[:io] || $stdout
      unless opts[:remove_header]
        out.print header_comment
      end
      each_line do |parameter, line_raw|
        if parameter
          out.puts line_raw
        else
          out.puts ""
        end
      end
    end

    def extremum(opts = {})
      out = opts[:io] || $stdout
      unless opts[:remove_header]
        out.print header_comment
      end
      variation_last = nil
      parameter_last = nil
      ind = 0
      each_line do |parameter, line_raw|
        if parameter
          if parameter_last
            variation = parameter - parameter_last
            if variation_last
              if variation * variation_last < 0.0
                out.puts "# #{variation > 0.0 ? "minimal" : "maximal"} #{ind}"
                out.puts line_raw
              end
            end
            variation_last = variation
          end
          parameter_last = parameter
          ind += 1
        else
          parameter_last = nil
          variation_last = nil
        end
      end
    end

    def show_size(opts = {})
      out = opts[:io] || $stdout
      num_rows = 0
      num_columns = 0
      each_line do |parameter, line_raw|
        num_rows += 1
        if num_columns == 0
          num_columns = line_raw.split.size
        end
      end
      out.puts "#{num_rows}\t#{num_columns}"
    end
  end
end
