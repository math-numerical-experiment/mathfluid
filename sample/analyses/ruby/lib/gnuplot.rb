module MathFluid
  class Gnuplot
    DEFAULT_GNUPLOT_PATH = "gnuplot"

    @gnuplot_commands_on_system = ["set term x11 noraise nopersist"]

    def self.gnuplot_commands_on_system
      @gnuplot_commands_on_system
    end

    def self.gnuplot_commands_on_system_set(array_commands)
      @gnuplot_commands_on_system = array_commands
    end

    # @param [Hash] opts   Options
    # @option opts [String] :path Path of Gnuplot
    # @option opts [String] :load Path of a file that store commands of Gnuplot before plotting
    # @option opts [Array]  :commands Array of Gnuplot commands before plotting
    def initialize(opts = {})
      @path = opts[:path] || DEFAULT_GNUPLOT_PATH
      @load_file = opts[:load]
      @initial_commands = opts[:commands]
    end

    def commands_print(io, &block)
      self.class.gnuplot_commands_on_system.each do |cmd|
        io.puts cmd
      end
      if @load_file
        io.puts File.read(@load_file)
      end
      if @initial_commands
        @initial_commands.each do |cmd|
          io.puts cmd
        end
      end
      yield(io)
    end
    private :commands_print

    # @param [Hash] opts   Options
    # @option opts [String] :file Output commands of Gnuplot to a file
    def execute(opts = {}, &block)
      unless block_given?
        raise "Block must be given"
      end
      if opts[:file]
        io = MathFluid::Utils.open(opts[:file], "w")
      else
        io = IO.popen(@path, "w")
      end
      commands_print(io, &block)
      io.close
    end

    module Utils
      def thin_data(ary, data_size)
        if ary.size <= 2 || ary.size <= data_size
          ary.dup
        else
          ary_new = [ary[0]]
          num_inside = ary.size - 2
          num_interval, rest = num_inside.divmod(data_size - 1)
          rest_data = data_size - 2
          num_next = num_interval
          if rest > 0
            num_next += 1
            rest -= 1
          end
          (1..num_inside).each do |i|
            if i == num_next && rest_data > 0
              rest_data -= 1
              ary_new << ary[i]
              num_next += num_interval
              if rest > 0
                num_next += 1
                rest -= 1
              end
            else
              ary_new << nil
            end
          end
          ary_new << ary[-1]
        end
      end
      module_function :thin_data
    end
  end
end
