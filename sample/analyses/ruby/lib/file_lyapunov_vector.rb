require "matrix"

module MathFluid
  class FileLyapunovVector
    DEFAULT_NUMBER_SPLITTER = "\t"
    NUMBER_FORMAT = "%.14f"

    # @param [String] path Path of file of Lyapunov vectors
    # @param [Hash] opts   Options
    # @option opts [Float] :time_min Minimum time of data
    # @option opts [Float] :time_max Maximum time of data
    # @option opts [Float] :orbit_time_interval    Time interval of data of orbit
    # @option opts [Float] :vector_time_interval   Time interval of data of Lyapunov vector
    # @option opts [String] :splitter     String of splitter
    def initialize(path, opts = {})
      @path = path
      if !(String === @path) || !File.exist?(@path)
        raise "File does not exist: #{@path.inspect}"
      end
      @orbit_time_interval = opts[:orbit_time_interval] || 0.0
      @vector_time_interval = opts[:vector_time_interval] || 0.0
      @opts_time_indexed_file = opts.slice(:time_min, :time_max, :time_difference_threshold)
      @header = load_header(@path)
      @dimension = @header["application"]["dimension"]
      @target_number = @header["application"]["target_number"]
      @splitter = opts[:splitter] || DEFAULT_NUMBER_SPLITTER
    end

    def load_header(path)
      header = MathFluid::HeaderJSON.parse_header(path)
      if (header["application"]["description"] != "Lyapunov analysis") || (/^[BC]LV$/ !~ header["application"]["analysis_type"])
        raise "Invalid file: File does not save BLV or CLV"
      end
      header
    end
    private :load_header

    def parse_line(line)
      nums = line.split.map(&:to_f)
      ary = nums.each_slice(@dimension).to_a
      pt = ary.shift
      [pt, ary]
    end
    private :parse_line

    def each_vector_data(time_interval, &block)
      if block_given?
        file_time_indexed = MathFluid::FileTimeIndexed.new(@path, @opts_time_indexed_file.merge(:time_interval => time_interval))
        file_time_indexed.each_line do |time, line, line_raw|
          pt, vectors = parse_line(line)
          yield(time, pt, vectors)
        end
      else
        to_enum(:each_vector_data, time_interval)
      end
    end
    private :each_vector_data

    def each_vector(&block)
      each_vector_data(@vector_time_interval, &block)
    end

    def array_to_gnuplot(ary)
      ary.map(&:to_s).join(" ")
    end
    private :array_to_gnuplot

    # @param [Hash] opts   Options
    # @option opts [Array]  :vector Array of integers to display only specified vectors
    # @option opts [String] :gp Command of Gnuplot before plotting
    # @option opts [String] :gp_load File of Gnuplot before plotting
    # @option opts [Hash]   :gp_title Titles of data. Default is { :orbit => "orbit", :vector => "vector %d" }
    def display(opts = {})
      data_title = opts[:gp_title] || { :orbit => "orbit", :vector => "vector %d" }
      ary_vector_index = (opts[:vector] || @target_number.times.to_a)
      if ary_vector_index.max >= @target_number
        raise "Can not display vector #{ary_vector_index.max}"
      end
      if @dimension == 2
        cmd_plot = "plot '-' with lines"
      elsif @dimension == 3
        cmd_plot = "splot '-' with lines"
      else
        raise "Not support dimension #{@dimension}"
      end
      if data_title[:orbit]
        cmd_plot << " title '#{data_title[:orbit].gsub("'", "\\\\'")}'"
      else
        cmd_plot << " notitle"
      end
      @target_number.times do |i|
        if ind = ary_vector_index.index(i)
          cmd_plot << ", '-' with vector"
          if data_title[:vector]
            cmd_plot << (" title '#{data_title[:vector].gsub("'", "\\\\'")}'" % (i + 1))
          else
            cmd_plot << " notitle"
          end
        end
      end
      str_orbit = ""
      str_vector = ary_vector_index.size.times.map { |i| "" }
      each_vector_data(@orbit_time_interval) do |time, pt, vectors|
        str_orbit << array_to_gnuplot(pt) << "\n"
      end
      each_vector do |time, pt, vectors|
        pt_str = array_to_gnuplot(pt)
        @target_number.times do |i|
          if ind = ary_vector_index.index(i)
            str_vector[ind] << pt_str << " " << array_to_gnuplot(vectors[i]) << "\n"
          end
        end
      end
      MathFluid::Gnuplot.new(:load => opts[:gp_load], :commands => opts[:gp]).execute do |io|
        io.puts cmd_plot
        io.print str_orbit
        io.puts "e"
        str_vector.each do |str|
          io.print str
          io.puts "e"
        end
        $stdout.puts "Press ENTER to exit"
        $stdin.gets
      end
    end

    def orthogonality_test
      each_vector do |time, pt, vectors|
        str = NUMBER_FORMAT % time
        vectors = vectors.map do |ary|
          Vector[*ary]
        end
        vectors.each_index do |i|
          ((i + 1)...(vectors.size)).each do |j|
            str << @splitter << (NUMBER_FORMAT % vectors[i].inner_product(vectors[j]))
          end
        end
        puts str
      end
    end

    def convert_to_splot(vector_index, opts = {})
      io = opts[:io] || $stdout
      coordinate_start = opts[:coordinate_start] || 0.0
      coordinate_end = opts[:coordinate_end] || (coordinate_start + (@dimension - 1).to_f)
      coordinate_step = (coordinate_end - coordinate_start) / (@dimension - 1).to_f
      output_format = "#{NUMBER_FORMAT}#{@splitter}#{NUMBER_FORMAT}#{@splitter}%s\n"
      each_vector do |time, pt, vectors|
        unless vec = vectors[vector_index]
          raise "Target vector does not exist"
        end
        MathFluid::Gnuplot::Utils.thin_data(vec, @dimension).each_with_index do |val, i|
          if val
            crd = (i == (@dimension - 1) ? coordinate_end : coordinate_start + coordinate_step * i)
            io.printf(output_format, time, crd, val)
          end
        end
        io.print("\n")
      end
    end

    def inner_product(vector_index1, vector_index2, opts = {})
      io = opts[:io] || $stdout
      output_format = "#{NUMBER_FORMAT}#{@splitter}#{NUMBER_FORMAT}\n"
      each_vector do |time, pt, vectors|
        unless ((vec1 = vectors[vector_index1]) && (vec2 = vectors[vector_index2]))
          raise "Target vectors do not exist"
        end
        v1 = Vector[*vec1]
        v2 = Vector[*vec2]
        product = v1.inner_product(v2)
        io.printf(output_format, time, product)
      end
    end
  end

  class FileLyapunovVectorCompare
    def initialize(opts = {})
      @no_time_test = opts[:no_time_test]
      @splitter = opts[:splitter] || MathFluid::FileLyapunovVector::DEFAULT_NUMBER_SPLITTER
    end

    def each_vectors(file_lv1, file_lv2)
      file_vectors1 = file_lv1.each_vector
      file_vectors2 = file_lv2.each_vector
      begin
        while true
          time1, pt1, vectors1 = file_vectors1.next
          time2, pt2, vectors2 = file_vectors2.next
          if !@no_time_test && (time1 != time2)
            raise "Time does not agree"
          end
          yield(time1, vectors1, vectors2)
        end
      rescue StopIteration
      end
    end

    # Calculate absolute values of inner products of corresponding vectors
    def compare(file_lv1, file_lv2, opts = {})
      io = opts[:io] || $stdout
      abs_val = opts[:abs]
      output_format = "#{@splitter}#{MathFluid::FileLyapunovVector::NUMBER_FORMAT}"
      each_vectors(file_lv1, file_lv2) do |time1, vectors1, vectors2|
        io.printf(MathFluid::FileLyapunovVector::NUMBER_FORMAT, time1)
        vectors1.each_index do |i|
          v1 = Vector[*vectors1[i]]
          v2 = Vector[*vectors2[i]]
          product = v1.inner_product(v2)
          product = product.abs if abs_val
          io.printf(output_format, product)
        end
        io.printf("\n")
      end
    end
  end
end
