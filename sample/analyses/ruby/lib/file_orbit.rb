module MathFluid
  class FileOrbit < MathFluid::FileTimeIndexed
    NUMBER_FORMAT = "%.14f"

    def convert_to_splot(opts = {})
      io = opts[:io] || $stdout
      number_points = opts[:number_points]
      coordinate_start = opts[:coordinate_start] || 0.0
      coordinate_end = nil
      coordinate_step = nil
      max_dim = nil
      time_offset = opts[:time_offset]
      output_format = "#{NUMBER_FORMAT}#{@splitter}#{NUMBER_FORMAT}#{@splitter}%s\n"
      each_line do |time, line, line_raw|
        time = time - time_offset if time_offset
        unless coordinate_end
          max_dim = line.split.size
          coordinate_end = opts[:coordinate_end] || (coordinate_start + (max_dim - 1).to_f)
          coordinate_step = (coordinate_end - coordinate_start) / (max_dim - 1).to_f
          number_points ||= max_dim
        end
        data = line.split.map(&:to_f)
        MathFluid::Gnuplot::Utils.thin_data(data, number_points).each_with_index do |val, i|
          if val
            crd = (i == (max_dim - 1) ? coordinate_end : coordinate_start + coordinate_step * i)
            io.printf(output_format, time, crd, val)
          end
        end
        io.print("\n")
      end
    end
  end
end
