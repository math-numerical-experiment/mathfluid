require "zlib"
require "xz"

module MathFluid
  module Utils
    def self.parse_index_string(v)
      case v
      when /^\d+$/
        val = v.to_i
      when /^\d+\.+\d+$/
        val = eval(v)
      else
        raise "Invalid index string: #{v}"
      end
      val
    end

    def self.open(path, mode, &block)
      case path
      when /\.gz$/
        if mode == "w"
          Zlib::GzipWriter.open(path, &block)
        elsif mode == "r"
          Zlib::GzipReader.open(path, &block)
        else
          raise "Invalid mode for gz: #{mode}"
        end
      when /\.xz$/
        if mode == "w"
          XZ::StreamWriter.open(path, &block)
        elsif mode == "r"
          XZ::StreamReader.open(path, &block)
        else
          raise "Invalid mode for xz: #{mode}"
        end
      else
        Kernel.open(path, mode, &block)
      end
    end
  end
end
