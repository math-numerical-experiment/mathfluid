module MathFluid
  class FileNumberTSV
    DEFAULT_NUMBER_SPLITTER = "\t"

    # @param [String] path Path of file whose lines are indexed by sorted time
    # @param [Hash] opts   Options
    # @option opts [String] :splitter     String of splitter
    def initialize(path, opts = {})
      @path = path
      if !(String === @path) || !File.exist?(@path)
        raise "File does not exist: #{@path.inspect}"
      end
      @splitter = opts[:splitter] || DEFAULT_NUMBER_SPLITTER
    end

    def header_comment
      MathFluid::HeaderJSON.each_header_line(@path).to_a.join
    end

    def each_line(&block)
      if block_given?
        MathFluid::Utils.open(@path, "r") do |io|
          io.each_line do |line_raw|
            next if /^#/ =~ line_raw
            ary = line_raw.split(@splitter)
            yield(ary.map(&:to_f))
          end
        end
      else
        to_enum(:each_line)
      end
    end
  end
end
