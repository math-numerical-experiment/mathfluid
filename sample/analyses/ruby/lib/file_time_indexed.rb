module MathFluid
  # Class of files with time index.
  # We assume that the value of time increases monotonically.
  class FileTimeIndexed
    DEFAULT_NUMBER_SPLITTER = "\t"

    # @param [String] path Path of file whose lines are indexed by sorted time
    # @param [Hash] opts   Options
    # @option opts [Float] :time_min Minimum time of target lines
    # @option opts [Float] :time_max Maximum time of target lines
    # @option opts [Float] :time_at  Time of a target line
    # @option opts [Float] :time_interval   Minimum time interval of data
    # @option opts [Integer] :time_index    Index number of Time
    # @option opts [boolean] :time_nonexistent  Time of a step is line number
    # @option opts [Array] :data_index    Array of Range or Integer that specify indices of extracted data
    # @option opts [String] :splitter     String of splitter
    def initialize(path, opts = {})
      @path = path
      if !(String === @path) || !File.exist?(@path)
        raise "File does not exist: #{@path.inspect}"
      end
      @time_min = opts[:time_min]
      @time_max = opts[:time_max]
      @time_at = opts[:time_at]
      @time_interval = opts[:time_interval] || 0.0
      if v = opts[:time_difference_threshold]
        @time_min -= v if @time_min
        @time_max += v if @time_max
        @time_interval -= v if @time_interval
      end
      @time_index_number = opts[:time_index] || 0
      @time_nonexistent = opts[:time_nonexistent]
      @data_index = opts[:data_index]
      @splitter = opts[:splitter] || DEFAULT_NUMBER_SPLITTER
      if (@time_min || @time_max) && @time_at
        raise "Can not set :time_min or :time_max and :time_at at once"
      end
    end

    def get_time(line)
      if /([^\s]\s+){#{@time_index_number}}([^\s]+)(\s|$)/ =~ line
        Float(Regexp.last_match[2])
      else
        nil
      end
    end

    def get_line_without_time(line)
      if @time_nonexistent
        line
      elsif /([^\s]\s+){#{@time_index_number}}[^\s]+(.*$)/ =~ line
        Regexp.last_match[1].to_s + Regexp.last_match[2].to_s
      else
        nil
      end
    end
    private :get_line_without_time

    def header_comment
      MathFluid::HeaderJSON.each_header_line(@path).to_a.join
    end

    def data_index_condition_match?(cond, ind)
      case cond
      when Integer
        cond == ind
      when Range
        cond.include?(ind)
      else
        raise "Invalid condition of data index: #{cond.inspect}"
      end
    end
    private :data_index_condition_match?

    # Yield lines that are not comment. Note that time values of the lines are removed
    def each_line_not_comment
      if block_given?
        line_index = -1
        MathFluid::Utils.open(@path, "r") do |io|
          io.each_line do |line_raw|
            next if /^#/ =~ line_raw
            line_index += 1
            if @time_nonexistent
              time = line_index.to_f
            else
              time = get_time(line_raw)
            end
            if @data_index
              ary = line_raw.split
              ary_new = []
              ary.each_with_index do |val, ind|
                @data_index.each do |cond|
                  if data_index_condition_match?(cond, ind)
                    ary_new << val
                    break
                  end
                end
              end
              line = (ary_new.empty? ? nil : ary_new.join(@splitter))
            else
              line = get_line_without_time(line_raw)
            end
            yield(time, line.strip, line_raw) if line
          end
        end
      else
        to_enum(:each_line_not_comment)
      end
    end
    private :each_line_not_comment

    def line_time_at
      last = { :time_diff => nil, :line => nil, :time => nil, :line_raw => nil }
      each_line_not_comment do |time, line, line_raw|
        current_time_diff = (time - @time_at).abs
        if last[:time_diff] && (last[:time_diff] < current_time_diff)
          break
        end
        last[:time_diff] = current_time_diff
        last[:line] = line
        last[:time] = time
        last[:line_raw] = line_raw
      end
      [last[:time], last[:line], last[:line_raw]]
    end
    private :line_time_at

    def each_line_in_interval(&block)
      last = { :time => nil, :line => nil, :line_raw => nil }
      each_line_not_comment do |time, line, line_raw|
        if @time_min && (time < @time_min)
          next
        elsif @time_max && (time > @time_max)
          break
        end
        if !last[:time] || (time - last[:time] >= @time_interval)
          last[:time] = time
          last[:line] = line
          last[:line_raw] = line_raw
          yield(time, line, line_raw)
        end
      end
    end
    private :each_line_in_interval

    # Yield each data [time, line, line_raw].
    # The difference of "line" and "line_raw" is that "line" do not have time data.
    def each_line(&block)
      if block_given?
        if @time_at
          if ary = line_time_at
            yield(*ary)
          end
        else
          each_line_in_interval(&block)
        end
      else
        to_enum(:each_line)
      end
    end

    def extract(opts = {})
      out = opts[:io] || $stdout
      if opts[:remove_time] && @data_index
        raise "Can not remove time when columns of data are specified"
      end
      unless opts[:remove_header]
        out.print header_comment
      end
      each_line do |time, line, line_raw|
        if opts[:remove_time] || @data_index
          out.puts line
        else
          out.puts line_raw
        end
      end
    end

    def find(index, value)
      last_sign = nil
      last_time = nil
      ret = []
      if index < 0
        raise "Index number must be nonnegative integer: #{index}"
      end
      each_line do |time, line, line_raw|
        unless str = line.split[index]
          raise "The value at index is not found"
        end
        val = str.to_f
        cur_sign = (val - value <=> 0.0)
        if last_sign && (cur_sign != last_sign)
          ret << last_time
        end
        last_sign = cur_sign
        last_time = time
      end
      ret
    end

    def filter(opts = {})
      out = opts[:io] || $stdout
      if opts[:average]
        m = MathFluid::FileTimeIndexed::ArrayState.method(:average)
      elsif opts[:max]
        m = MathFluid::FileTimeIndexed::ArrayState.method(:max)
      elsif opts[:min]
        m = MathFluid::FileTimeIndexed::ArrayState.method(:min)
      else
        raise "Set among :average, :max, and :min"
      end
      each_line do |time, line, line_raw|
        val = m.call(line.split.map(&:to_f))
        out.puts "#{time}#{@splitter}#{val}"
      end
    end

    module ArrayState
      def average(ary)
        ary.inject(0.0) { |sum, val| sum += val } / ary.size.to_f
      end
      module_function :average

      def max(ary)
        ary.max
      end
      module_function :max

      def min(ary)
        ary.min
      end
      module_function :min
    end

    class CLIOptions
      @cli_options = {}

      def self.get(opts = {})
        @cli_options[opts[:key] || :default] ||= self.new
      end

      def self.create(path, opts = {})
        cl = opts.delete(:class) || MathFluid::FileTimeIndexed
        cl.new(path, self.get(opts).options)
      end

      def self.clear(opts = {})
        self.get(opts).clear
      end

      def initialize
        @options = {}
      end

      def clear
        @options.clear
      end

      def options
        @options
      end

      def define_basic_option(opt, *args)
        args.each do |option_key|
          case option_key
          when :time_at
            opt.on("--time-at NUM", Float, "Time of data") do |v|
              @options[:time_at] = v
            end
          when :time_min
            opt.on("--time-min NUM", Float, "Minimum time of data") do |v|
              @options[:time_min] = v
            end
          when :time_max
            opt.on("--time-max NUM", Float, "Maximum time of data") do |v|
              @options[:time_max] = v
            end
          when :time_interval
            opt.on("--time-interval NUM", Float, "Time interval of data") do |v|
              @options[:time_interval] = v
            end
          when :time_nonexistent
            opt.on("--time-nonexistent", "Use line numbers as time of steps") do |v|
              @options[:time_nonexistent] = true
            end
          when :time_difference_threshold
            opt.on("--time-difference-threshold NUM", Float, "Threshold for --time-min, --time-max, and --time-interval") do |v|
              @options[:time_difference_threshold] = v
            end
          when :data_index
            opt.on("--data-index VAL", String, "Index of data to extract only parts of lines") do |v|
              @options[:data_index] ||= []
              @options[:data_index] << MathFluid::Utils.parse_index_string(v)
            end
          else
            raise "Invalid key of basic option: #{option_key.inspect}"
          end
        end
      end
    end
  end
end
