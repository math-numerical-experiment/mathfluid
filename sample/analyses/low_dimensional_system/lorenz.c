#include "system_low_dim_definition.h"

#define LORENZ_SYSTEM_DIM 3

enum {
  LORENZ_PRM_SIGMA,
  LORENZ_PRM_BETA,
  LORENZ_PRM_RHO,
  LORENZ_N_PRMS
};

static char *lorenz_param_names[] = { "sigma", "beta", "rho", NULL };

/*
 * x' = \sigma (y - x)
 * y' = x (\rho - z) - y
 * z' = xy - \beta z
 * params = { \sigma, \beta, \rho }
 */
static int lorenz_function (double f[], const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  f[0] = prms[LORENZ_PRM_SIGMA] * (x[1] - x[0]);
  f[1] = x[0] * (prms[LORENZ_PRM_RHO] - x[2]) - x[1];
  f[2] = x[0] * x[1] - prms[LORENZ_PRM_BETA] * x[2];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 * -\sigma     \sigma    0
 * \rho - z    -1        x
 * y           x         -\beta
 * params = { \sigma, \beta, \rho }
 */
static int lorenz_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, LORENZ_SYSTEM_DIM, LORENZ_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  gsl_matrix_set(m, 0, 0, -prms[LORENZ_PRM_SIGMA]);
  gsl_matrix_set(m, 0, 1, prms[LORENZ_PRM_SIGMA]);
  gsl_matrix_set(m, 0, 2, 0.0);
  gsl_matrix_set(m, 1, 0, prms[LORENZ_PRM_RHO] - x[2]);
  gsl_matrix_set(m, 1, 1, -1.0);
  gsl_matrix_set(m, 1, 2, -x[0]);
  gsl_matrix_set(m, 2, 0, x[1]);
  gsl_matrix_set(m, 2, 1, x[0]);
  gsl_matrix_set(m, 2, 2, -prms[LORENZ_PRM_BETA]);

  return GSL_SUCCESS;
}

static void lorenz_system_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[LORENZ_PRM_SIGMA] = 10.0;
  option_data->params[LORENZ_PRM_BETA] = 8.0 / 3.0;
  option_data->params[LORENZ_PRM_RHO] = 28.0;
}

void lorenz_system_low_dim_definition_register (void)
{
  system_low_dim_definition_register("lorenz", "\
  System is given by the following:\n\
    x' = sigma (y - x)\n\
    y' = x (rho - z) - y\n\
    z' = xy - beta z",
                             LORENZ_SYSTEM_DIM, LORENZ_N_PRMS, lorenz_param_names,
                             lorenz_function, lorenz_jacobian,
                             lorenz_system_option_data_set_default);
}
