#include "system_low_dim_definition.h"

typedef struct _SystemLowDimCalcOrbitClass SystemLowDimCalcOrbitClass;
typedef struct _SystemLowDimCalcOrbit SystemLowDimCalcOrbit;

struct _SystemLowDimCalcOrbitClass {
  CmdCalcOrbitClass parent;
};

struct _SystemLowDimCalcOrbit {
  CmdCalcOrbit parent;
};

#define TYPE_SYSTEM_LOW_DIM_CALC_ORBIT (system_low_dim_calc_orbit_get_type ())
#define SYSTEM_LOW_DIM_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_LOW_DIM_CALC_ORBIT, SystemLowDimCalcOrbit))
#define SYSTEM_LOW_DIM_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_SYSTEM_LOW_DIM_CALC_ORBIT, SystemLowDimCalcOrbitClass))
#define IS_SYSTEM_LOW_DIM_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_LOW_DIM_CALC_ORBIT))
#define IS_SYSTEM_LOW_DIM_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_SYSTEM_LOW_DIM_CALC_ORBIT))
#define SYSTEM_LOW_DIM_CALC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SYSTEM_LOW_DIM_CALC_ORBIT, SystemLowDimCalcOrbitClass))

GType system_low_dim_calc_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(SystemLowDimCalcOrbit, system_low_dim_calc_orbit, TYPE_CMD_CALC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, system_low_dim_interface_init))

static void system_low_dim_calc_orbit_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_calc_orbit_parent_class)->dispose(gobject);
}

static void system_low_dim_calc_orbit_finalize (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_calc_orbit_parent_class)->finalize(gobject);
}

static void system_low_dim_calc_orbit_init (SystemLowDimCalcOrbit *self)
{
}

static void system_low_dim_calc_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  option_data->time_max = 100.0;
  option_data->time_step = 0.01;
  option_data->active_options[CALC_ORBIT_OPTION_VECTOR_FIELD] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_MAX] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void system_low_dim_calc_orbit_class_init (SystemLowDimCalcOrbitClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = system_low_dim_calc_orbit_dispose;
  gobject_class->finalize = system_low_dim_calc_orbit_finalize;

  command_class->set_default_value = system_low_dim_calc_orbit_set_default_value;
}

gpointer system_low_dim_calc_orbit_new (DefinitionSystemLowDim *definition)
{
  SystemLowDimCalcOrbit *system_low_dim_calc_orbit;
  SystemLowDimSystemArgs args[1];
  args->activate_numerical_differential = FALSE;
  args->definition = definition;
  system_low_dim_calc_orbit = command_new(TYPE_SYSTEM_LOW_DIM_CALC_ORBIT, (gpointer) args, "system-dimension", definition->system_dim, NULL);
  command_short_description_set(system_low_dim_calc_orbit, "-- calculate orbit");
  command_summary_set(system_low_dim_calc_orbit, definition->description);
  return (gpointer) system_low_dim_calc_orbit;
}
