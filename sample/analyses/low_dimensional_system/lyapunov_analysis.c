#include "system_low_dim_definition.h"

typedef struct _SystemLowDimLyapunovAnalysisClass SystemLowDimLyapunovAnalysisClass;
typedef struct _SystemLowDimLyapunovAnalysis SystemLowDimLyapunovAnalysis;

struct _SystemLowDimLyapunovAnalysisClass {
  CmdLyapunovAnalysisClass parent;
};

struct _SystemLowDimLyapunovAnalysis {
  CmdLyapunovAnalysis parent;
};

#define TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS (system_low_dim_lyapunov_analysis_get_type ())
#define SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS, SystemLowDimLyapunovAnalysis))
#define SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS, SystemLowDimLyapunovAnalysisClass))
#define IS_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS))
#define IS_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS))
#define SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS, SystemLowDimLyapunovAnalysisClass))

GType system_low_dim_lyapunov_analysis_get_type ();

G_DEFINE_TYPE_WITH_CODE(SystemLowDimLyapunovAnalysis, system_low_dim_lyapunov_analysis, TYPE_CMD_LYAPUNOV_ANALYSIS,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, system_low_dim_interface_init))

static void system_low_dim_lyapunov_analysis_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_lyapunov_analysis_parent_class)->dispose(gobject);
}

static void system_low_dim_lyapunov_analysis_finalize (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_lyapunov_analysis_parent_class)->finalize(gobject);
}

static void system_low_dim_lyapunov_analysis_init (SystemLowDimLyapunovAnalysis *self)
{
}

static void system_low_dim_lyapunov_analysis_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  option_data->time_step = 0.01;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_VERBOSE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void system_low_dim_lyapunov_analysis_class_init (SystemLowDimLyapunovAnalysisClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = system_low_dim_lyapunov_analysis_dispose;
  gobject_class->finalize = system_low_dim_lyapunov_analysis_finalize;

  command_class->set_default_value = system_low_dim_lyapunov_analysis_set_default_value;
}

gpointer system_low_dim_lyapunov_analysis_new (DefinitionSystemLowDim *definition)
{
  SystemLowDimLyapunovAnalysis *system_low_dim_lyapunov_analysis;
  SystemLowDimSystemArgs args[1];
  args->activate_numerical_differential = TRUE;
  args->definition = definition;
  system_low_dim_lyapunov_analysis = command_new(TYPE_SYSTEM_LOW_DIM_LYAPUNOV_ANALYSIS, (gpointer) args, "system-dimension", definition->system_dim, NULL);
  command_short_description_set(system_low_dim_lyapunov_analysis, "-- Lyapunov analysis");
  command_summary_set(system_low_dim_lyapunov_analysis, definition->description);
  return (gpointer) system_low_dim_lyapunov_analysis;
}
