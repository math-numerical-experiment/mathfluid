#ifndef _SYSTEM_LOW_DIM_DEFINITION_H_
#define _SYSTEM_LOW_DIM_DEFINITION_H_

#include "system_low_dim.h"

void system_low_dim_interface_init (SystemDefineInterface *iface);
void system_low_dim_definition_init (void);
void system_low_dim_definition_register (char *name, char *description, int system_dim, int param_dim,
                                 char **param_names,
                                 int (*time_derivative) (double f[], const double x[], void *params),
                                 int (*jacobian) (double *dfdy, const double x[], void *params),
                                 void (*system_option_data_set_default) (SystemLowDimOptionData *option_data));
DefinitionSystemLowDim *system_low_dim_definition_get (const char *name);
int system_low_dim_definition_execute (int argc, char **argv, gpointer (*object_create) (DefinitionSystemLowDim *definition));
GPtrArray *system_low_dim_definition_name_array ();

void lorenz_system_low_dim_definition_register (void);
void van_der_pol_system_low_dim_definition_register (void);
void linear_system_low_dim_definition_register (void);
void elliptic_rotation_definition_register (void);
void radial_pitchfork_definition_register (void);
void saddle_node_definition_register (void);
void ode_example01_definition_register (void);
void ode_example02_definition_register (void);
void ode_example03_definition_register (void);

#define SYSTEM_LOW_DIM_DEFINITION_REGISTER_ALL() {      \
  lorenz_system_low_dim_definition_register();          \
  van_der_pol_system_low_dim_definition_register();     \
  linear_system_low_dim_definition_register();          \
  elliptic_rotation_definition_register();      \
  radial_pitchfork_definition_register();       \
  saddle_node_definition_register();            \
  ode_example01_definition_register();              \
  ode_example02_definition_register();              \
  ode_example03_definition_register();              \
  }

gpointer system_low_dim_calc_orbit_new (DefinitionSystemLowDim *definition);
gpointer system_low_dim_search_equilibrium_new (DefinitionSystemLowDim *definition);
gpointer system_low_dim_search_periodic_orbit_new (DefinitionSystemLowDim *definition);
gpointer system_low_dim_lyapunov_analysis_new (DefinitionSystemLowDim *definition);
gpointer system_low_dim_rayleigh_ritz_new (DefinitionSystemLowDim *definition);
gpointer system_low_dim_parameter_continuation_new (DefinitionSystemLowDim *definition);

#endif /* _SYSTEM_LOW_DIM_DEFINITION_H_ */
