#include "system_low_dim_definition.h"

#define ELLIPTIC_ROTATION_DIM 2

enum {
  ELLIPTIC_ROTATION_PRM_A,
  ELLIPTIC_ROTATION_PRM_B,
  ELLIPTIC_ROTATION_N_PRMS
};

static char *elliptic_rotation_param_names[] = { "a", "b", NULL };

static int elliptic_rotation_function (double f[], const double x[], void *params)
{
  double *prms, val_sqrt;
  prms = (double *) params;

  val_sqrt = sqrt((x[1]-2*x[0])*(x[1]-2*x[0])+(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0));
  if (val_sqrt > 0) {
    f[0] = 2*(prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)/val_sqrt+prms[ELLIPTIC_ROTATION_PRM_B]*(x[1]-2*x[0]))+prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_B]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0);
    f[1] = 3*(prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_B]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0))+4*(prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)/val_sqrt+prms[ELLIPTIC_ROTATION_PRM_B]*(x[1]-2*x[0]));
  } else {
    f[0] = 0.0;
    f[1] = 0.0;
  }

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 */
static int elliptic_rotation_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms;
  double val_sq, val_sqrt, val_3sqrt;
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, ELLIPTIC_ROTATION_DIM, ELLIPTIC_ROTATION_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  prms = (double *) params;
  val_sq = (x[1]-2*x[0])*(x[1]-2*x[0])+(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0);
  val_sqrt = sqrt(val_sq);
  val_3sqrt = pow(val_sq, (-3.0E+0)/2.0E+0);

  gsl_matrix_set(m, 0, 0, 2*(3.0E+0*prms[ELLIPTIC_ROTATION_PRM_A]/(2.0E+0*val_sqrt)-prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(3*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)-4*(x[1]-2*x[0]))*val_3sqrt/2.0E+0-2*prms[ELLIPTIC_ROTATION_PRM_B])-2*prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])*(3*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)-4*(x[1]-2*x[0]))*val_3sqrt/2.0E+0+(-3.0E+0)*prms[ELLIPTIC_ROTATION_PRM_B]/2.0E+0);
  gsl_matrix_set(m, 0, 1, 2*(-prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt/2.0E+0-prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(2*(x[1]-2*x[0])+x[1]/2.0E+0+(-3.0E+0)*x[0]/2.0E+0)*val_3sqrt/2.0E+0+prms[ELLIPTIC_ROTATION_PRM_B])+prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])*(2*(x[1]-2*x[0])+x[1]/2.0E+0+(-3.0E+0)*x[0]/2.0E+0)*val_3sqrt/2.0E+0+prms[ELLIPTIC_ROTATION_PRM_B]/2.0E+0);
  gsl_matrix_set(m, 1, 0, 4*(3.0E+0*prms[ELLIPTIC_ROTATION_PRM_A]/(2.0E+0*val_sqrt)-prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(3*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)-4*(x[1]-2*x[0]))*val_3sqrt/2.0E+0-2*prms[ELLIPTIC_ROTATION_PRM_B])+3*(-2*prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])*(3*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)-4*(x[1]-2*x[0]))*val_3sqrt/2.0E+0+(-3.0E+0)*prms[ELLIPTIC_ROTATION_PRM_B]/2.0E+0));
  gsl_matrix_set(m, 1, 1, 3*(prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt-prms[ELLIPTIC_ROTATION_PRM_A]*(x[1]-2*x[0])*(2*(x[1]-2*x[0])+x[1]/2.0E+0+(-3.0E+0)*x[0]/2.0E+0)*val_3sqrt/2.0E+0+prms[ELLIPTIC_ROTATION_PRM_B]/2.0E+0)+4*(-prms[ELLIPTIC_ROTATION_PRM_A]/val_sqrt/2.0E+0-prms[ELLIPTIC_ROTATION_PRM_A]*(3.0E+0*x[0]/2.0E+0-x[1]/2.0E+0)*(2*(x[1]-2*x[0])+x[1]/2.0E+0+(-3.0E+0)*x[0]/2.0E+0)*val_3sqrt/2.0E+0+prms[ELLIPTIC_ROTATION_PRM_B]));

  return GSL_SUCCESS;
}

static void elliptic_rotation_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[ELLIPTIC_ROTATION_PRM_A] = 0.0;
  option_data->params[ELLIPTIC_ROTATION_PRM_B] = 1.0;
}

void elliptic_rotation_definition_register (void)
{
  system_low_dim_definition_register("elliptic_rotation","\
  System is the composition T phi T^{-1};\n\
    T (x, y) = (x + 2y, 3x + 4y)\n\
    and phi is the flow defined by\n\
    r' = a * r\n\
    theta' = b * theta",
                             ELLIPTIC_ROTATION_DIM, ELLIPTIC_ROTATION_N_PRMS, elliptic_rotation_param_names,
                             elliptic_rotation_function, elliptic_rotation_jacobian,
                             elliptic_rotation_option_data_set_default);
}
