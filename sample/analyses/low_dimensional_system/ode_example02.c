#include "system_low_dim_definition.h"

#define ODE_EXAMPLE02_DIM 2

enum {
  ODE_EXAMPLE02_N_PRMS
};

static char *ode_example02_param_names[] = { NULL };

static int ode_example02_function (double f[], const double x[], void *params)
{
  /* double *prms; */
  /* prms = (double *) params; */
  f[0] = -x[1];
  f[1] = x[0];
  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 */
static int ode_example02_jacobian (double *dfdy, const double x[], void *params)
{
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, ODE_EXAMPLE02_DIM, ODE_EXAMPLE02_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  /* double *prms; */
  /* prms = (double *) params; */

  gsl_matrix_set(m, 0, 0, 0.0);
  gsl_matrix_set(m, 0, 1, -1.0);
  gsl_matrix_set(m, 1, 0, 1.0);
  gsl_matrix_set(m, 1, 1, 0.0);

  return GSL_SUCCESS;
}

static void ode_example02_option_data_set_default (SystemLowDimOptionData *option_data)
{
}

void ode_example02_definition_register (void)
{
  system_low_dim_definition_register("ode_example02", "\
    x' = -y\n\
    y' = x",
                                     ODE_EXAMPLE02_DIM, ODE_EXAMPLE02_N_PRMS, ode_example02_param_names,
                                     ode_example02_function, ode_example02_jacobian,
                                     ode_example02_option_data_set_default);
}
