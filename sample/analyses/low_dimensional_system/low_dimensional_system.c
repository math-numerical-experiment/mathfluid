#include "system_low_dim_definition.h"

static int low_dimensional_system_execute (int argc, char **argv)
{
  int code;
  gchar *basename;
  basename = subcommand_list_get_basename(argv[0]);
  if (g_strcmp0(basename, "calculate_orbit") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_calc_orbit_new);
  } else if (g_strcmp0(basename, "search_equilibrium") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_search_equilibrium_new);
  } else if (g_strcmp0(basename, "search_periodic_orbit") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_search_periodic_orbit_new);
  } else if (g_strcmp0(basename, "lyapunov_analysis") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_lyapunov_analysis_new);
  } else if (g_strcmp0(basename, "rayleigh_ritz") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_rayleigh_ritz_new);
  } else if (g_strcmp0(basename, "parameter_continuation") == 0) {
    code = system_low_dim_definition_execute(argc, argv, system_low_dim_parameter_continuation_new);
  } else {
    fprintf(stderr, "Not support program \"%s\"\n", basename);
    abort();
  }
  g_free(basename);
  return code;
}

void low_dimensional_system_init (char *program_path)
{
  gchar *basename;
  basename = subcommand_list_get_basename(program_path);
  system_low_dim_definition_init();
  if ((g_strcmp0(basename, "calculate_orbit") == 0) || (g_strcmp0(basename, "search_equilibrium") == 0) ||
      (g_strcmp0(basename, "search_periodic_orbit") == 0) || (g_strcmp0(basename, "lyapunov_analysis") == 0) ||
      (g_strcmp0(basename, "rayleigh_ritz") == 0) || (g_strcmp0(basename, "parameter_continuation") == 0)) {
    GPtrArray *subcommand_name_array;
    gchar *subcommand_name;
    int i;
    subcommand_name_array = system_low_dim_definition_name_array();
    for (i = 0; i < subcommand_name_array->len; i++) {
      subcommand_name = g_ptr_array_index(subcommand_name_array, i);
      subcommand_list_global_register_subcommand(basename, subcommand_name, low_dimensional_system_execute);
    }
    g_ptr_array_free(subcommand_name_array, TRUE);
  } else {
    fprintf(stderr, "Not support program \"%s\"\n", basename);
    abort();
  }
  g_free(basename);
}
