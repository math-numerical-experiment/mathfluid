#ifndef _LOW_DIMENSIONAL_SYSTEM_H_
#define _LOW_DIMENSIONAL_SYSTEM_H_

#include "../command_common/command_common.h"
#include <json-glib/json-glib.h>

void low_dimensional_system_init (char *program_path);

#endif /* _LOW_DIMENSIONAL_SYSTEM_H_ */
