#include "system_low_dim_definition.h"

#define ODE_EXAMPLE01_DIM 3

enum {
  ODE_EXAMPLE01_PRM_A,
  ODE_EXAMPLE01_PRM_B,
  ODE_EXAMPLE01_PRM_C,
  ODE_EXAMPLE01_PRM_D,
  ODE_EXAMPLE01_N_PRMS
};

static char *ode_example01_param_names[] = { "A", "B", "C", "D", NULL };

static int ode_example01_function (double f[], const double x[], void *params)
{
  double *prms, c, s, a;
  prms = (double *) params;

  a = atan(prms[ODE_EXAMPLE01_PRM_D] * x[2]);
  c = cos(a);
  s = sin(a);
  f[0] = prms[ODE_EXAMPLE01_PRM_A] * c * x[0] - prms[ODE_EXAMPLE01_PRM_B] * s * x[1];
  f[1] = prms[ODE_EXAMPLE01_PRM_A] * s * x[0] + prms[ODE_EXAMPLE01_PRM_B] * c * x[1];
  f[2] = prms[ODE_EXAMPLE01_PRM_C] * atan(x[2]);

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 */
static int ode_example01_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms, c, s, a, y, diff_a;
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, ODE_EXAMPLE01_DIM, ODE_EXAMPLE01_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  prms = (double *) params;

  a = atan(prms[ODE_EXAMPLE01_PRM_D] * x[2]);
  c = cos(a);
  s = sin(a);
  y = prms[ODE_EXAMPLE01_PRM_D] * x[2];
  diff_a = prms[ODE_EXAMPLE01_PRM_D] / (1.0 + y * y);

  gsl_matrix_set(m, 0, 0, prms[ODE_EXAMPLE01_PRM_A] * c);
  gsl_matrix_set(m, 0, 1, -prms[ODE_EXAMPLE01_PRM_B] * s);
  gsl_matrix_set(m, 0, 2, (-prms[ODE_EXAMPLE01_PRM_A] * s * x[0] - prms[ODE_EXAMPLE01_PRM_B] * c * x[1]) * diff_a);
  gsl_matrix_set(m, 1, 0, prms[ODE_EXAMPLE01_PRM_A] * s);
  gsl_matrix_set(m, 1, 1, prms[ODE_EXAMPLE01_PRM_B] * c);
  gsl_matrix_set(m, 1, 2, (prms[ODE_EXAMPLE01_PRM_A] * c * x[0] - prms[ODE_EXAMPLE01_PRM_B] * s * x[1]) * diff_a);
  gsl_matrix_set(m, 2, 0, 0.0);
  gsl_matrix_set(m, 2, 1, 0.0);
  gsl_matrix_set(m, 2, 2, prms[ODE_EXAMPLE01_PRM_C] / (1.0 + x[2] * x[2]));

  return GSL_SUCCESS;
}

static void ode_example01_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[ODE_EXAMPLE01_PRM_A] = 2.0;
  option_data->params[ODE_EXAMPLE01_PRM_B] = 1.0;
  option_data->params[ODE_EXAMPLE01_PRM_C] = -1.0;
  option_data->params[ODE_EXAMPLE01_PRM_D] = 0.2;
}

void ode_example01_definition_register (void)
{
  system_low_dim_definition_register("ode_example01", "\
    x' = A x cos(arctan(D z)) - B y sin(arctan(D z))\n\
    y' = A x sin(arctan(D z)) + B y cos(arctan(D z))\n\
    z' = C arctan(D z)",
                                     ODE_EXAMPLE01_DIM, ODE_EXAMPLE01_N_PRMS, ode_example01_param_names,
                                     ode_example01_function, ode_example01_jacobian,
                                     ode_example01_option_data_set_default);
}
