#include "system_low_dim_definition.h"

typedef struct _SystemLowDimParameterContinuationClass SystemLowDimParameterContinuationClass;
typedef struct _SystemLowDimParameterContinuation SystemLowDimParameterContinuation;

struct _SystemLowDimParameterContinuationClass {
  CmdParameterContinuationClass parent;
};

struct _SystemLowDimParameterContinuation {
  CmdParameterContinuation parent;
};

#define TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION (system_low_dim_parameter_continuation_get_type ())
#define SYSTEM_LOW_DIM_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION, SystemLowDimParameterContinuation))
#define SYSTEM_LOW_DIM_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION, SystemLowDimParameterContinuationClass))
#define IS_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION))
#define IS_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION))
#define SYSTEM_LOW_DIM_PARAMETER_CONTINUATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION, SystemLowDimParameterContinuationClass))

GType system_low_dim_parameter_continuation_get_type ();

G_DEFINE_TYPE_WITH_CODE(SystemLowDimParameterContinuation, system_low_dim_parameter_continuation, TYPE_CMD_PARAMETER_CONTINUATION,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, system_low_dim_interface_init))

static void system_low_dim_parameter_continuation_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_parameter_continuation_parent_class)->dispose(gobject);
}

static void system_low_dim_parameter_continuation_finalize (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_parameter_continuation_parent_class)->finalize(gobject);
}

static void system_low_dim_parameter_continuation_init (SystemLowDimParameterContinuation *self)
{
}

static void system_low_dim_parameter_continuation_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdParameterContinuationOptionData *option_data;
  option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_VERBOSE] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_KEY] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DIRECTION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_FIXED] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_ADJUSTED] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_MAX_ARC_LENGTH] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_GMRES_CORRECTOR_MAX_ITERATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_MAX_ITERATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_METHOD] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void system_low_dim_parameter_continuation_class_init (SystemLowDimParameterContinuationClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = system_low_dim_parameter_continuation_dispose;
  gobject_class->finalize = system_low_dim_parameter_continuation_finalize;

  command_class->set_default_value = system_low_dim_parameter_continuation_set_default_value;
}

gpointer system_low_dim_parameter_continuation_new (DefinitionSystemLowDim *definition)
{
  SystemLowDimParameterContinuation *system_low_dim_parameter_continuation;
  SystemLowDimSystemArgs args[1];
  args->activate_numerical_differential = TRUE;
  args->definition = definition;
  system_low_dim_parameter_continuation = command_new(TYPE_SYSTEM_LOW_DIM_PARAMETER_CONTINUATION, (gpointer) args, "system-dimension", definition->system_dim, NULL);
  command_short_description_set(system_low_dim_parameter_continuation, "-- Parameter continuation");
  command_summary_set(system_low_dim_parameter_continuation, definition->description);
  return (gpointer) system_low_dim_parameter_continuation;
}
