#include "system_low_dim_definition.h"

#define SADDLE_NODE_SYSTEM_DIM 2

enum {
  SADDLE_NODE_PRM_MU,
  SADDLE_NODE_PRM_A,
  SADDLE_NODE_PRM_B,
  SADDLE_NODE_N_PRMS
};

static char *saddle_node_param_names[] = { "mu", "a", "b", NULL };

/*
 * x' = a(x^2 - mu)
 * y' = -by
 */
static int saddle_node_function (double f[], const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  f[0] = prms[SADDLE_NODE_PRM_A] * (x[0] * x[0] - prms[SADDLE_NODE_PRM_MU]);
  f[1] = -prms[SADDLE_NODE_PRM_B] * x[1];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 * -2ax    0
 * 0     -b
 */
static int saddle_node_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, SADDLE_NODE_SYSTEM_DIM, SADDLE_NODE_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  gsl_matrix_set(m, 0, 0, 2.0 * prms[SADDLE_NODE_PRM_A] * x[0]);
  gsl_matrix_set(m, 0, 1, 0.0);
  gsl_matrix_set(m, 1, 0, 0.0);
  gsl_matrix_set(m, 1, 1, -prms[SADDLE_NODE_PRM_B]);

  return GSL_SUCCESS;
}

static void saddle_node_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[SADDLE_NODE_PRM_MU] = 0.0;
  option_data->params[SADDLE_NODE_PRM_A] = 1.0;
  option_data->params[SADDLE_NODE_PRM_B] = 1.0;
}

void saddle_node_definition_register (void)
{
  system_low_dim_definition_register("saddle_node", "\
  System is given by the following:\n\
    x' = a(x^2 - mu)\n\
    y' = -by",
                             SADDLE_NODE_SYSTEM_DIM, SADDLE_NODE_N_PRMS, saddle_node_param_names,
                             saddle_node_function, saddle_node_jacobian,
                             saddle_node_option_data_set_default);
}
