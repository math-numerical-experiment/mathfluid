#include "system_low_dim_definition.h"

#define SYSTEM_LOW_DIM_ERROR_EVOLUTION 1e-8
#define SYSTEM_LOW_DIM_INITIAL_STEP_SIZE 1e-10

enum {
  CONTAINER_DEFINITION_INDEX,
  CONTAINER_DEFINITION_N
};

static void system_low_dim_evolution_error_set (DynamicalSystem *dynamical_system, double error, double initial_step_size)
{
  SystemODESolver *ode_solver;
  ode_solver = ((SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, 0))->solver;
  system_ode_solver_set_driver(ode_solver, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk8pd, initial_step_size, error, 0.0);
}

static int system_low_dim_time_derivative (double *dydt, const double *y, GenericParameter *params)
{
  DefinitionSystemLowDim *definition;
  definition = data_container_data_ptr(generic_parameter_data(params), CONTAINER_DEFINITION_INDEX);
  definition->time_derivative(dydt, y, generic_parameter_array_double(params));
  return GSL_SUCCESS;
}

static int system_low_dim_jacobian (double *dfdy, const double *y, GenericParameter *params)
{
  DefinitionSystemLowDim *definition;
  definition = data_container_data_ptr(generic_parameter_data(params), CONTAINER_DEFINITION_INDEX);
  definition->jacobian(dfdy, y, generic_parameter_array_double(params));
  return GSL_SUCCESS;
}

DynamicalSystem *system_low_dim_system_alloc (SystemLowDimOptionData *option_data)
{
  DataContainer *container_definition;
  GenericParameter *parameters;
  SystemODE *system_ode;
  SystemODESolver *solver;
  int (* jacobian) (double *dfdy, const double *y, DataContainer *params);
  if (option_data->definition->jacobian) {
    jacobian = system_low_dim_jacobian;
  } else {
    jacobian = NULL;
  }
  container_definition = data_container_alloc(CONTAINER_DEFINITION_N, option_data->definition, data_container_utils_return_pointer, NULL);
  parameters = generic_parameter_alloc();
  generic_parameter_set_double(parameters, option_data->definition->param_dim, option_data->params, option_data->definition->param_names);
  generic_parameter_data_set(parameters, container_definition);
  system_ode = system_ode_alloc(option_data->definition->system_dim, system_low_dim_time_derivative, jacobian, NULL, parameters, generic_parameter_set, generic_parameter_get);
  solver = system_ode_solver_alloc(system_ode);
  return dynamical_system_ode_alloc(solver, TRUE, TRUE);
}

static DynamicalSystem *system_low_dim_dynamical_system_create (gpointer ptr_option_data)
{
  DynamicalSystem *dynamical_system;
  SystemLowDimOptionData *option_data;
  option_data = (SystemLowDimOptionData *) ptr_option_data;
  dynamical_system = system_low_dim_system_alloc(option_data);
  system_low_dim_evolution_error_set(dynamical_system, option_data->error_evolution, option_data->initial_step_size);
  if (option_data->use_numerical_differential) {
    dynamical_system_ode_system_ode(dynamical_system)->jacobian_of_time_derivative = NULL;
  }
  return dynamical_system;
}

static gpointer system_low_dim_option_data_alloc (gpointer optional_args)
{
  int i;
  SystemLowDimOptionData *option_data;
  option_data = (SystemLowDimOptionData *) g_malloc(sizeof(SystemLowDimOptionData));
  if (optional_args) {
    SystemLowDimSystemArgs *args;
    args = (SystemLowDimSystemArgs *) optional_args;
    option_data->definition = args->definition;
    option_data->params = (double *) g_malloc(sizeof(double) * args->definition->param_dim);
  } else {
    fprintf(stderr, "Can not find system!\n");
    abort();
  }
  option_data->error_evolution = SYSTEM_LOW_DIM_ERROR_EVOLUTION;
  option_data->initial_step_size = SYSTEM_LOW_DIM_INITIAL_STEP_SIZE;
  option_data->use_numerical_differential = FALSE;
  for (i = 0; i < SYSTEM_LOW_DIM_N_OPTS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void system_low_dim_option_data_free (gpointer ptr_option_data)
{
  SystemLowDimOptionData *prms;
  prms = (SystemLowDimOptionData *) ptr_option_data;
  g_free(prms->params);
  g_free(prms);
}

static void system_low_dim_option_data_command_line_arguments_define (CmdOpts *cmd_opts, SystemLowDimOptionData *option_data)
{
  if (option_data->definition->param_names) {
    char *name, *desc;
    int i;
    i = 0;
    while ((name = option_data->definition->param_names[i])) {
      desc = g_strdup_printf("Parameter %s", name);
      cmd_opts_system_option_alloc_add(cmd_opts, name, 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[i], desc, "NUM");
      g_free(desc);
      i += 1;
    }
  }
}

static void system_low_dim_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_option_data)
{
  SystemLowDimOptionData *option_data;
  option_data = (SystemLowDimOptionData *) ptr_option_data;
  system_low_dim_option_data_command_line_arguments_define(cmd_opts, option_data);
  if (option_data->active_options[SYSTEM_LOW_DIM_OPT_ERROR_EVOLUTION]) {
    cmd_opts_system_option_alloc_add(cmd_opts, "error-evolution", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_evolution, "Error of evolution", "NUM");
  }
  if (option_data->active_options[SYSTEM_LOW_DIM_OPT_INITIAL_STEP_SIZE]) {
    cmd_opts_system_option_alloc_add(cmd_opts, "initial-step-size", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->initial_step_size, "Initial step size of evolution", "NUM");
  }
  if (option_data->active_options[SYSTEM_LOW_DIM_OPT_USE_NUMERICAL_DIFFERENTIAL]) {
    cmd_opts_system_option_alloc_add(cmd_opts, "use-numerical-differential", 0, 0, G_OPTION_ARG_NONE, &option_data->use_numerical_differential, "Use numerical differentials of Jacobian", NULL);
  }
}

static void system_low_dim_option_data_set_default (gpointer ptr_option_data, gpointer optional_args)
{
  SystemLowDimOptionData *option_data;
  option_data = (SystemLowDimOptionData *) ptr_option_data;
  if (optional_args) {
    SystemLowDimSystemArgs *args;
    args = (SystemLowDimSystemArgs *) optional_args;
    option_data->active_options[SYSTEM_LOW_DIM_OPT_ERROR_EVOLUTION] = TRUE;
    option_data->active_options[SYSTEM_LOW_DIM_OPT_INITIAL_STEP_SIZE] = TRUE;
    if (args->activate_numerical_differential) {
      option_data->active_options[SYSTEM_LOW_DIM_OPT_USE_NUMERICAL_DIFFERENTIAL] = TRUE;
    }
    if (args->definition->system_option_data_set_default) {
      args->definition->system_option_data_set_default(option_data);
    }
  }
}

static void system_low_dim_header_json_save (JsonBuilder *builder, gpointer ptr_option_data)
{
  int i;
  SystemLowDimOptionData *option_data;
  option_data = (SystemLowDimOptionData *) ptr_option_data;
  json_builder_set_member_name(builder, "name");
  json_builder_add_string_value(builder, option_data->definition->name);
  json_builder_set_member_name(builder, "params");
  json_builder_begin_array(builder);
  for (i = 0; i < option_data->definition->param_dim; i++) {
    json_builder_add_double_value(builder, option_data->params[i]);
  }
  json_builder_end_array(builder);
  json_builder_set_member_name(builder, "error_evolution");
  json_builder_add_double_value(builder, option_data->error_evolution);
  json_builder_set_member_name(builder, "initial_step_size");
  json_builder_add_double_value(builder, option_data->initial_step_size);
  json_builder_set_member_name(builder, "use_numerical_differential");
  json_builder_add_boolean_value(builder, option_data->use_numerical_differential);
}

void system_low_dim_interface_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = system_low_dim_dynamical_system_create;
  iface->option_data_alloc = system_low_dim_option_data_alloc;
  iface->option_data_free = system_low_dim_option_data_free;
  iface->command_line_arguments_define = system_low_dim_command_line_arguments_define;
  iface->option_data_set_default = system_low_dim_option_data_set_default;
  iface->header_json_save = system_low_dim_header_json_save;
}
