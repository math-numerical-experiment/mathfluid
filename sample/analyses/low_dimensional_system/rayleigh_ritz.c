#include "system_low_dim_definition.h"

typedef struct _SystemLowDimRayleighRitzClass SystemLowDimRayleighRitzClass;
typedef struct _SystemLowDimRayleighRitz SystemLowDimRayleighRitz;

struct _SystemLowDimRayleighRitzClass {
  CmdRayleighRitzClass parent;
};

struct _SystemLowDimRayleighRitz {
  CmdRayleighRitz parent;
};

#define TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ (system_low_dim_rayleigh_ritz_get_type ())
#define SYSTEM_LOW_DIM_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ, SystemLowDimRayleighRitz))
#define SYSTEM_LOW_DIM_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ, SystemLowDimRayleighRitzClass))
#define IS_SYSTEM_LOW_DIM_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ))
#define IS_SYSTEM_LOW_DIM_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ))
#define SYSTEM_LOW_DIM_RAYLEIGH_RITZ_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ, SystemLowDimRayleighRitzClass))

GType system_low_dim_rayleigh_ritz_get_type ();

G_DEFINE_TYPE_WITH_CODE(SystemLowDimRayleighRitz, system_low_dim_rayleigh_ritz, TYPE_CMD_RAYLEIGH_RITZ,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, system_low_dim_interface_init))

static void system_low_dim_rayleigh_ritz_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_rayleigh_ritz_parent_class)->dispose(gobject);
}

static void system_low_dim_rayleigh_ritz_finalize (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_rayleigh_ritz_parent_class)->finalize(gobject);
}

static void system_low_dim_rayleigh_ritz_init (SystemLowDimRayleighRitz *self)
{
}

static void system_low_dim_rayleigh_ritz_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_VERBOSE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_DIMENSION_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SIMULTANEOUS_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_ARNOLDI_METHOD] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_FROM_JACOBIAN] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SORT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void system_low_dim_rayleigh_ritz_class_init (SystemLowDimRayleighRitzClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = system_low_dim_rayleigh_ritz_dispose;
  gobject_class->finalize = system_low_dim_rayleigh_ritz_finalize;

  command_class->set_default_value = system_low_dim_rayleigh_ritz_set_default_value;
}

gpointer system_low_dim_rayleigh_ritz_new (DefinitionSystemLowDim *definition)
{
  SystemLowDimRayleighRitz *system_low_dim_rayleigh_ritz;
  SystemLowDimSystemArgs args[1];
  args->activate_numerical_differential = TRUE;
  args->definition = definition;
  system_low_dim_rayleigh_ritz = command_new(TYPE_SYSTEM_LOW_DIM_RAYLEIGH_RITZ, (gpointer) args, "system-dimension", definition->system_dim, NULL);
  command_short_description_set(system_low_dim_rayleigh_ritz, "-- Rayleigh-Ritz method");
  command_summary_set(system_low_dim_rayleigh_ritz, definition->description);
  return (gpointer) system_low_dim_rayleigh_ritz;
}
