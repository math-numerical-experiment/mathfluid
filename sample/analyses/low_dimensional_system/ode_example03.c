#include "system_low_dim_definition.h"

#define ODE_EXAMPLE03_DIM 2

enum {
  ODE_EXAMPLE03_PRM_A,
  ODE_EXAMPLE03_PRM_B,
  ODE_EXAMPLE03_PRM_C,
  ODE_EXAMPLE03_N_PRMS
};

static char *ode_example03_param_names[] = { "A", "B", "C", NULL };

static int ode_example03_function (double f[], const double x[], void *params)
{
  double *prms, square, square_root, n;
  prms = (double *) params;

  square = x[0] * x[0] + x[1] * x[1];
  square_root = sqrt(square);
  if (square_root == 0.0) {
    fprintf(stderr, "Can not calculate vector field: denominator is zero\n");
    abort();
  }
  n = (prms[0] * square + prms[1]) / square_root;

  f[0] = n * x[0] - prms[2] * x[1];
  f[1] = n * x[1] + prms[2] * x[0];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 */
static int ode_example03_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms, square, square_root, n, num, dndx, dndy;
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, ODE_EXAMPLE03_DIM, ODE_EXAMPLE03_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  prms = (double *) params;

  square = x[0] * x[0] + x[1] * x[1];
  square_root = sqrt(square);
  if (square_root == 0.0) {
    fprintf(stderr, "Can not calculate vector field: denominator is zero\n");
    abort();
  }
  n = (prms[0] * square + prms[1]) / square_root;
  num = (prms[0] * square_root - (prms[1] / square_root)) / square;
  dndx = x[0] * num;
  dndy = x[1] * num;

  gsl_matrix_set(m, 0, 0, x[0] * dndx + n);
  gsl_matrix_set(m, 0, 1, x[0] * dndy - prms[2]);
  gsl_matrix_set(m, 1, 0, x[1] * dndx + prms[2]);
  gsl_matrix_set(m, 1, 1, x[1] * dndy + n);

  return GSL_SUCCESS;
}

static void ode_example03_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[ODE_EXAMPLE03_PRM_A] = 1.0;
  option_data->params[ODE_EXAMPLE03_PRM_B] = 1.0;
  option_data->params[ODE_EXAMPLE03_PRM_C] = 1.0;
}

void ode_example03_definition_register (void)
{
  system_low_dim_definition_register("ode_example03", "\
    x' = x (A (x^2 + y^2) + B) / \\sqrt{x^2 + y^2} - C y\n\
    y' = y (A (x^2 + y^2) + B) / \\sqrt{x^2 + y^2} + C x",
                                     ODE_EXAMPLE03_DIM, ODE_EXAMPLE03_N_PRMS, ode_example03_param_names,
                                     ode_example03_function, ode_example03_jacobian,
                                     ode_example03_option_data_set_default);
}
