#include "system_low_dim_definition.h"

#define RADIAL_PITCHFORK_SYSTEM_DIM 2

enum {
  RADIAL_PITCHFORK_PRM_A,
  RADIAL_PITCHFORK_PRM_OMEGA,
  RADIAL_PITCHFORK_N_PRMS
};

static char *radial_pitchfork_param_names[] = { "a", "omega", NULL };

/*
 * x' = x (a - x^2 - y^2) - omega y
 * y' = y (a - x^2 - y^2) + omega x
 */
static int radial_pitchfork_function (double f[], const double x[], void *params)
{
  double *prms, v;
  prms = (double *) params;

  v = (prms[RADIAL_PITCHFORK_PRM_A] - x[0] * x[0] - x[1] * x[1]);
  f[0] = x[0] * v - prms[RADIAL_PITCHFORK_PRM_OMEGA] * x[1];
  f[1] = x[1] * v + prms[RADIAL_PITCHFORK_PRM_OMEGA] * x[0];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 * (a - 3x^2 - y^2)    -2x y - omega
 * -2x y + omega       (a - x^2 - 3y^2)
 */
static int radial_pitchfork_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms, x2, y2, v;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, RADIAL_PITCHFORK_SYSTEM_DIM, RADIAL_PITCHFORK_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;

  x2 = x[0] * x[0];
  y2 = x[1] * x[1];
  v = -2.0 * x[0] * x[1];
  gsl_matrix_set(m, 0, 0, (prms[RADIAL_PITCHFORK_PRM_A] - 3.0 * x2 - y2));
  gsl_matrix_set(m, 0, 1, v - prms[RADIAL_PITCHFORK_PRM_OMEGA]);
  gsl_matrix_set(m, 1, 0, v + prms[RADIAL_PITCHFORK_PRM_OMEGA]);
  gsl_matrix_set(m, 1, 1, (prms[RADIAL_PITCHFORK_PRM_A] - x2 - 3.0 * y2));

  return GSL_SUCCESS;
}

static void radial_pitchfork_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[RADIAL_PITCHFORK_PRM_A] = 1.0;
  option_data->params[RADIAL_PITCHFORK_PRM_OMEGA] = 1.0;
}

void radial_pitchfork_definition_register ()
{
  system_low_dim_definition_register("radial_pitchfork", "\
  System is given by the following:\n\
    x' = x (a - x^2 - y^2) - omega y\n\
    y' = y (a - x^2 - y^2) + omega x\n\
    that is,\n\
    theta' = omega\n\
    r' = a r - r^3",
                             RADIAL_PITCHFORK_SYSTEM_DIM, RADIAL_PITCHFORK_N_PRMS, radial_pitchfork_param_names,
                             radial_pitchfork_function, radial_pitchfork_jacobian,
                             radial_pitchfork_option_data_set_default);
}
