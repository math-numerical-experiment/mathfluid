#include "system_low_dim_definition.h"

#define LINEAR_SYSTEM_DIM 2

enum {
  LINEAR_SYSTEM_PRM_A,
  LINEAR_SYSTEM_PRM_B,
  LINEAR_SYSTEM_PRM_C,
  LINEAR_SYSTEM_PRM_D,
  LINEAR_SYSTEM_N_PRMS
};

static char *linear_system_param_names[] = { NULL };

/*
 * x' = a x + b y
 * y' = c x + d y
 */
static int linear_system_function (double f[], const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  f[0] = prms[LINEAR_SYSTEM_PRM_A] * x[0] + prms[LINEAR_SYSTEM_PRM_B] * x[0];
  f[1] = prms[LINEAR_SYSTEM_PRM_C] * x[0] + prms[LINEAR_SYSTEM_PRM_D] * x[1];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 * a b
 * c d
 */
static int linear_system_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, LINEAR_SYSTEM_DIM, LINEAR_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  gsl_matrix_set(m, 0, 0, prms[LINEAR_SYSTEM_PRM_A]);
  gsl_matrix_set(m, 0, 1, prms[LINEAR_SYSTEM_PRM_B]);
  gsl_matrix_set(m, 1, 0, prms[LINEAR_SYSTEM_PRM_C]);
  gsl_matrix_set(m, 1, 1, prms[LINEAR_SYSTEM_PRM_D]);

  return GSL_SUCCESS;
}

static void linear_system_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[LINEAR_SYSTEM_PRM_A] = 1.0;
  option_data->params[LINEAR_SYSTEM_PRM_B] = 0.0;
  option_data->params[LINEAR_SYSTEM_PRM_C] = 0.0;
  option_data->params[LINEAR_SYSTEM_PRM_D] = 1.0;
}

void linear_system_low_dim_definition_register (void)
{
  system_low_dim_definition_register("linear_system", "\
  System is given by the following:\n\
    x' = a x + b y\n\
    y' = c x + d y",
                             LINEAR_SYSTEM_DIM, LINEAR_SYSTEM_N_PRMS, linear_system_param_names,
                             linear_system_function, linear_system_jacobian,
                             linear_system_option_data_set_default);
}
