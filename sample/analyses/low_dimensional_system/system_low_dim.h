#ifndef _SYSTEM_LOW_DIM_H_
#define _SYSTEM_LOW_DIM_H_

#include "../command_common/command_common.h"
#include <json-glib/json-glib.h>

typedef struct _DefinitionSystemLowDim DefinitionSystemLowDim;
typedef struct _SystemLowDimOptionData SystemLowDimOptionData;
typedef struct _SystemLowDimSystemArgs SystemLowDimSystemArgs;

struct _DefinitionSystemLowDim {
  char *name;
  char *description;
  char **param_names;
  int system_dim;
  int param_dim;
  int (*time_derivative) (double f[], const double x[], void *params);
  int (*jacobian) (double *dfdy, const double x[], void *params);
  void (*system_option_data_set_default) (SystemLowDimOptionData *option_data);
};

/**
 * An argument of command_new and functions of children classes of Command
 */
struct _SystemLowDimSystemArgs {
  DefinitionSystemLowDim *definition;
  gboolean activate_numerical_differential;
};

enum {
  SYSTEM_LOW_DIM_OPT_ERROR_EVOLUTION,
  SYSTEM_LOW_DIM_OPT_INITIAL_STEP_SIZE,
  SYSTEM_LOW_DIM_OPT_USE_NUMERICAL_DIFFERENTIAL,
  SYSTEM_LOW_DIM_N_OPTS
};

struct _SystemLowDimOptionData {
  double *params;
  double error_evolution;
  double initial_step_size;
  gboolean use_numerical_differential;
  gboolean active_options[SYSTEM_LOW_DIM_N_OPTS];
  DefinitionSystemLowDim *definition;
};

#endif /* _SYSTEM_LOW_DIM_H_ */
