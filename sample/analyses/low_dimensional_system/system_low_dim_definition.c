#include "system_low_dim_definition.h"

static GPtrArray *system_low_dim_definition_array = NULL;

void system_low_dim_definition_register (char *name, char *description, int system_dim, int param_dim,
                                 char **param_names,
                                 int (*time_derivative) (double f[], const double x[], void *params),
                                 int (*jacobian) (double *dfdy, const double x[], void *params),
                                 void (*system_option_data_set_default) (SystemLowDimOptionData *option_data))
{
  DefinitionSystemLowDim *definition;
  if (!system_low_dim_definition_array) {
    system_low_dim_definition_array = g_ptr_array_new();
  }
  definition = (DefinitionSystemLowDim *) g_malloc(sizeof(DefinitionSystemLowDim));
  definition->name = name;
  definition->description = description;
  definition->system_dim = system_dim;
  definition->param_dim = param_dim;
  definition->param_names = param_names;
  definition->time_derivative = time_derivative;
  definition->jacobian = jacobian;
  definition->system_option_data_set_default = system_option_data_set_default;
  g_ptr_array_add(system_low_dim_definition_array, definition);
}

DefinitionSystemLowDim *system_low_dim_definition_get (const char *name)
{
  DefinitionSystemLowDim *definition = NULL;
  if (system_low_dim_definition_array) {
    DefinitionSystemLowDim *d;
    int i;
    for (i = 0; i < system_low_dim_definition_array->len; i++) {
      d = (DefinitionSystemLowDim *) g_ptr_array_index(system_low_dim_definition_array, i);
      if (g_ascii_strcasecmp(name, d->name) == 0) {
        definition = d;
        break;
      }
    }
  }
  return definition;
}

void system_low_dim_definition_init (void)
{
  static gboolean initialized = FALSE;
  if (!initialized) {
    SYSTEM_LOW_DIM_DEFINITION_REGISTER_ALL();
    initialized = TRUE;
  }
}

static DefinitionSystemLowDim *system_low_dim_definition_find (char *name)
{
  DefinitionSystemLowDim *definition = NULL;
  system_low_dim_definition_init();
  if (name) {
    definition = system_low_dim_definition_get(name);
  }
  if (!definition) {
    fprintf(stderr, "Invalid name of system: %s\n", name);
    exit(1);
  }
  return definition;
}

int system_low_dim_definition_execute (int argc, char **argv, gpointer (*object_create) (DefinitionSystemLowDim *definition))
{
  DefinitionSystemLowDim *definition;
  gpointer ptr_object;
  MathFluidARGV *mathfluid_argv;
  char *name;
  mathfluid_argv = mathfluid_argv_alloc_parse(argc, argv, TRUE, 1);
  name = mathfluid_argv_take_out(mathfluid_argv, 1);
  definition = system_low_dim_definition_find(name);
  ptr_object = object_create(definition);
  command_process_command_line_arguments(ptr_object, mathfluid_argv);
  command_dynamical_system_create(ptr_object);
  command_execute(ptr_object);
  g_object_unref(ptr_object);
  mathfluid_argv_free(mathfluid_argv);
  return 0;
}

GPtrArray *system_low_dim_definition_name_array ()
{
  GPtrArray *array;
  int i;
  DefinitionSystemLowDim *d;
  array = g_ptr_array_new();
  if (system_low_dim_definition_array != NULL) {
    for (i = 0; i < system_low_dim_definition_array->len; i++) {
      d = (DefinitionSystemLowDim *) g_ptr_array_index(system_low_dim_definition_array, i);
      g_ptr_array_add(array, d->name);
    }
  }
  return array;
}
