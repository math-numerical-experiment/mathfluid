#include "system_low_dim_definition.h"

typedef struct _SystemLowDimSearchEquilibriumClass SystemLowDimSearchEquilibriumClass;
typedef struct _SystemLowDimSearchEquilibrium SystemLowDimSearchEquilibrium;

struct _SystemLowDimSearchEquilibriumClass {
  CmdSearchEquilibriumClass parent;
};

struct _SystemLowDimSearchEquilibrium {
  CmdSearchEquilibrium parent;

  char **range_strings;
};

#define TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM (system_low_dim_search_equilibrium_get_type ())
#define SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM, SystemLowDimSearchEquilibrium))
#define SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM, SystemLowDimSearchEquilibriumClass))
#define IS_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM))
#define IS_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM))
#define SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM, SystemLowDimSearchEquilibriumClass))

GType system_low_dim_search_equilibrium_get_type ();

G_DEFINE_TYPE_WITH_CODE(SystemLowDimSearchEquilibrium, system_low_dim_search_equilibrium, TYPE_CMD_SEARCH_EQUILIBRIUM,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, system_low_dim_interface_init))

static void system_low_dim_search_equilibrium_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(system_low_dim_search_equilibrium_parent_class)->dispose(gobject);
}

static void system_low_dim_search_equilibrium_finalize (GObject *gobject)
{
  SystemLowDimSearchEquilibrium *self;
  self = SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM(gobject);
  if (self->range_strings) {
    int i, dim;
    dim = cmd_search_equilibrium_dimension(self);
    for (i = 0; i < dim; i++) {
      if (self->range_strings[i]) {
        g_free(self->range_strings[i]);
      }
    }
    g_free(self->range_strings);
  }
  G_OBJECT_CLASS(system_low_dim_search_equilibrium_parent_class)->finalize(gobject);
}

static void system_low_dim_search_equilibrium_init (SystemLowDimSearchEquilibrium *self)
{
  self->range_strings = NULL;
}

static void system_low_dim_search_equilibrium_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_VERBOSE] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void system_low_dim_search_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  SystemLowDimSearchEquilibrium *self;
  int i, dim;
  self = SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM(ptr_self);
  dim = cmd_search_equilibrium_dimension(self);
  self->range_strings = (char **) g_malloc(sizeof(char *) * dim);
  for (i = 0; i < dim; i++) {
    self->range_strings[i]= NULL;
  }
  if (dim <= 3) {
    if (dim > 0) {
      cmd_opts_application_option_alloc_add(cmd_opts, "x-range", 'x', 0, G_OPTION_ARG_STRING, &self->range_strings[0], "Search range of x: STEP,MIN,MAX or VAL", "NUMS");
    }
    if (dim > 1) {
      cmd_opts_application_option_alloc_add(cmd_opts, "y-range", 'y', 0, G_OPTION_ARG_STRING, &self->range_strings[1], "Search range of y: STEP,MIN,MAX or VAL", "NUMS");
    }
    if (dim > 2) {
      cmd_opts_application_option_alloc_add(cmd_opts, "z-range", 'z', 0, G_OPTION_ARG_STRING, &self->range_strings[1], "Search range of z: STEP,MIN,MAX or VAL", "NUMS");
    }
  }

  COMMAND_CLASS(system_low_dim_search_equilibrium_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void system_low_dim_search_process_data (gpointer ptr_self)
{
  int i, dim;
  SystemLowDimSearchEquilibrium *self;
  CmdSearchEquilibrium *cmd_search;
  self = SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM(ptr_self);
  dim = cmd_search_equilibrium_dimension(self);
  cmd_search = CMD_SEARCH_EQUILIBRIUM(ptr_self);

  for (i = 0; i < dim; i++) {
    if (self->range_strings[i]) {
      cmd_search_equilibrium_option_range_set(cmd_search, i, self->range_strings[i]);
    }
  }

  COMMAND_CLASS(system_low_dim_search_equilibrium_parent_class)->process_data(ptr_self);
}

static void system_low_dim_search_equilibrium_class_init (SystemLowDimSearchEquilibriumClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = system_low_dim_search_equilibrium_dispose;
  gobject_class->finalize = system_low_dim_search_equilibrium_finalize;

  command_class->command_line_arguments_define = system_low_dim_search_command_line_arguments_define;
  command_class->process_data = system_low_dim_search_process_data;
  command_class->set_default_value = system_low_dim_search_equilibrium_set_default_value;
}

gpointer system_low_dim_search_equilibrium_new (DefinitionSystemLowDim *definition)
{
  SystemLowDimSearchEquilibrium *system_low_dim_search_equilibrium;
  SystemLowDimSystemArgs args[1];
  char *desc;
  args->activate_numerical_differential = TRUE;
  args->definition = definition;
  system_low_dim_search_equilibrium = command_new(TYPE_SYSTEM_LOW_DIM_SEARCH_EQUILIBRIUM, (gpointer) args, "system-dimension", definition->system_dim, NULL);
  command_short_description_set(system_low_dim_search_equilibrium, "-- search for equilibria");
  desc = g_strconcat(definition->description,
    "\n\n\
  Strings of options x, y, and z:\n\
    Comma separated three numbers \"STEP_SIZE, MIN, MAX\" that mean search range or one number that is a fixed coordinate", NULL);
  command_summary_set(system_low_dim_search_equilibrium, desc);
  g_free(desc);
  return (gpointer) system_low_dim_search_equilibrium;
}
