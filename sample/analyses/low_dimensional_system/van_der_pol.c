#include "system_low_dim_definition.h"

#define VAN_DER_POL_SYSTEM_DIM 2

enum {
  VAN_DER_POL_PRM_MU,
  VAN_DER_POL_N_PRMS
};

static char *van_der_pol_param_names[] = { "mu", NULL };

/*
 * x'' - \mu (1 - x^2)x' + x = 0
 * params = { \mu }
 *
 * x' = y
 * y' = \mu (1 - x^2)y - x
 */
static int van_der_pol_function (double f[], const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  f[0] = x[1];
  f[1] = prms[VAN_DER_POL_PRM_MU] * (1.0 - x[0] * x[0]) * x[1] - x[0];

  return GSL_SUCCESS;
}

/*
 * Jacobian matrix
 * 0                1
 * -2\mu xy - 1     \mu (1 - x^2)
 * params = { \mu }
 */
static int van_der_pol_jacobian (double *dfdy, const double x[], void *params)
{
  double *prms;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, VAN_DER_POL_SYSTEM_DIM, VAN_DER_POL_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix;
  gsl_matrix_set(m, 0, 0, 0.0);
  gsl_matrix_set(m, 0, 1, 1.0);
  gsl_matrix_set(m, 1, 0, -2.0 * prms[VAN_DER_POL_PRM_MU] * x[0] * x[1] - 1.0);
  gsl_matrix_set(m, 1, 1, prms[VAN_DER_POL_PRM_MU] * (1.0 - x[0] * x[0]));

  return GSL_SUCCESS;
}

static void van_der_pol_system_option_data_set_default (SystemLowDimOptionData *option_data)
{
  option_data->params[VAN_DER_POL_PRM_MU] = 1.0;
}

void van_der_pol_system_low_dim_definition_register (void)
{
  system_low_dim_definition_register("van_der_pol", "\
  System is given by the following:\n\
    x'' - mu (1 - x^2)x' + x = 0,\n\
    that is,\n\
    x' = y\n\
    y' = mu (1 - x^2)y - x",
                             VAN_DER_POL_SYSTEM_DIM, VAN_DER_POL_N_PRMS, van_der_pol_param_names,
                             van_der_pol_function, van_der_pol_jacobian,
                             van_der_pol_system_option_data_set_default);
}
