#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdLyapunovAnalysis, cmd_lyapunov_analysis, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_lyapunov_analysis_set_dimension (gpointer ptr_self, int dim)
{
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  option_data->dimension = dim;
  if (option_data->initial_point) {
    g_free(option_data->initial_point);
    option_data->initial_point = NULL;
  }
  if (option_data->dimension > 0) {
    int i;
    option_data->initial_point = (double *) g_malloc(sizeof(double) * option_data->dimension);
    for (i = 0; i < option_data->dimension; i++) {
      option_data->initial_point[i] = 0.0;
    }
  }
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_lyapunov_analysis_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_lyapunov_analysis_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_lyapunov_analysis_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdLyapunovAnalysis *self = CMD_LYAPUNOV_ANALYSIS(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, cmd_lyapunov_analysis_dimension(self));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_lyapunov_analysis_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_lyapunov_analysis_parent_class)->dispose(gobject);
}

static CmdLyapunovAnalysisOptionData *cmd_lyapunov_analysis_option_data_alloc ()
{
  int i;
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = (CmdLyapunovAnalysisOptionData *) g_malloc(sizeof(CmdLyapunovAnalysisOptionData));
  option_data->time_step = 1e-4;
  option_data->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  option_data->analysis_parameter = NULL;
  option_data->analysis_parameter_dim = 0;
  option_data->initial_point = NULL;
  option_data->dimension = 0;
  option_data->target_number = 0;
  option_data->string_initial_point = NULL;
  option_data->string_exponent = NULL;
  option_data->string_finite_time_exponent = NULL;
  option_data->string_finite_time_covariant_exponent = NULL;
  option_data->string_finite_time_covariant_exponent_from_clv = NULL;
  option_data->string_clv = NULL;
  option_data->string_blv = NULL;
  option_data->clv_initial_iterate = -1;
  option_data->clv_last_iterate = -1;
  option_data->string_clv_test = NULL;
  option_data->cache_directory = NULL;
  option_data->analysis_type = NOT_SPECIFIED;
  for (i = 0; i < N_LYAPUNOV_ANALYSIS_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_lyapunov_analysis_option_data_free (CmdLyapunovAnalysisOptionData *option_data)
{
  if (option_data) {
    if (option_data->initial_point) {
      g_free(option_data->initial_point);
    }
    if (option_data->analysis_parameter) {
      g_free(option_data->analysis_parameter);
    }
    if (option_data->string_initial_point) {
      g_free(option_data->string_initial_point);
    }
    if (option_data->string_exponent) {
      g_free(option_data->string_exponent);
    }
    if (option_data->string_finite_time_exponent) {
      g_free(option_data->string_finite_time_exponent);
    }
    if (option_data->string_finite_time_covariant_exponent) {
      g_free(option_data->string_finite_time_covariant_exponent);
    }
    if (option_data->string_finite_time_covariant_exponent_from_clv) {
      g_free(option_data->string_finite_time_covariant_exponent_from_clv);
    }
    if (option_data->string_clv) {
      g_free(option_data->string_clv);
    }
    if (option_data->string_blv) {
      g_free(option_data->string_blv);
    }
    if (option_data->string_clv_test) {
      g_free(option_data->string_clv_test);
    }
    if (option_data->cache_directory) {
      g_free(option_data->cache_directory);
    }
    g_free(option_data);
  }
}

static void cmd_lyapunov_analysis_finalize (GObject *gobject)
{
  CmdLyapunovAnalysis *self;
  self = CMD_LYAPUNOV_ANALYSIS(gobject);
  cmd_lyapunov_analysis_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_lyapunov_analysis_parent_class)->finalize(gobject);
}

static void cmd_lyapunov_analysis_init (CmdLyapunovAnalysis *self)
{
  self->option_data = cmd_lyapunov_analysis_option_data_alloc();
}

static void cmd_lyapunov_analysis_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;

  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT] || option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "point", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_point, "Initial point specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "number", 0, 0, G_OPTION_ARG_INT, &option_data->target_number, "Number of Lyapunov exponents and vectors", "NUM");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_TIME_STEP]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-step", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_step, "Time step of evolution of orbit", "NUM");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_SPACE_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "space-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->space_derivative_perturbation, "Initial scale of vectors that are evolved by Jacobian", "NUM");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_EXPONENT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "le", 0, 0, G_OPTION_ARG_STRING, &option_data->string_exponent, "Calculate Lyapunov exponents", "TIME_MAX");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "ftle", 0, 0, G_OPTION_ARG_STRING, &option_data->string_finite_time_exponent, "Calculate finite time Lyapunov exponents", "T1,T2");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "ftcle", 0, 0, G_OPTION_ARG_STRING, &option_data->string_finite_time_covariant_exponent, "Calculate finite time covariant Lyapunov exponents", "T1,T2,T3");
    if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR]) {
      cmd_opts_application_option_alloc_add(cmd_opts, "ftcle-from-clv", 0, 0, G_OPTION_ARG_STRING, &option_data->string_finite_time_covariant_exponent_from_clv, "Calculate finite time covariant Lyapunov exponents from CLVs", "PATH");
    }
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "clv", 0, 0, G_OPTION_ARG_STRING, &option_data->string_clv, "Calculate covariant Lyapunov vectors between T1 and T2; Evolution between T2 and T3 is backward process", "T1,T2,T3");
    cmd_opts_application_option_alloc_add(cmd_opts, "clv-init-iterate", 0, 0, G_OPTION_ARG_INT, &option_data->clv_initial_iterate, "Iteration at an initial point", "NUM");
    cmd_opts_application_option_alloc_add(cmd_opts, "clv-last-iterate", 0, 0, G_OPTION_ARG_INT, &option_data->clv_last_iterate, "Iteration at a last point", "NUM");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "blv", 0, 0, G_OPTION_ARG_STRING, &option_data->string_blv, "Calculate backward Lyapunov vectors between T1 and T2", "T1,T2");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "clv-test", 0, 0, G_OPTION_ARG_STRING, &option_data->string_clv_test, "Test evolving covariant Lyapunov vectors by Jacobian", "PATH");
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "use-database", 0, 0, G_OPTION_ARG_NONE, &option_data->use_database, "Use database on disk", NULL);
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_VERBOSE]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_VERBOSE] = TRUE;
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_lyapunov_analysis_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void cmd_lyapunov_analysis_process_data (gpointer ptr_self)
{
  CmdLyapunovAnalysis *self;
  CmdLyapunovAnalysisOptionData *option_data;
  self = CMD_LYAPUNOV_ANALYSIS(ptr_self);
  option_data = self->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_point);
  /* if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] && option_data->string_finite_time_covariant_exponent_from_clv) { */
  /*   JsonParser *json_parser; */
  /*   json_parser = command_utils_get_json_header_parser(option_data->string_finite_time_covariant_exponent_from_clv); */
  /*   if (!json_parser) { */
  /*     fprintf(stderr, "Can not parse header\n"); */
  /*     abort(); */
  /*   } */
  /* } */
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT]) {
    command_utils_point_data_load(option_data->initial_point, option_data->string_initial_point, option_data->dimension);
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT_WITH_DIM]) {
    if (option_data->string_initial_point) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_point);
      cmd_lyapunov_analysis_set_dimension(ptr_self, dim);
      memcpy(option_data->initial_point, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_point) {
    g_free(option_data->string_initial_point);
    option_data->string_initial_point = NULL;
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_EXPONENT] && option_data->string_exponent) {
    option_data->analysis_type = LYAPUNOV_EXPONENT;
    option_data->analysis_parameter_dim = 1;
    option_data->analysis_parameter = mathfluid_utils_string_numbers_to_allocated_array_of_double(option_data->analysis_parameter_dim, option_data->string_exponent, "Please specify one time value to calculate Lyapunov exponent\n");
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT] && option_data->string_finite_time_exponent) {
    option_data->analysis_type = FINITE_TIME_LYAPUNOV_EXPONENT;
    option_data->analysis_parameter_dim = 2;
    option_data->analysis_parameter = mathfluid_utils_string_numbers_to_allocated_array_of_double(option_data->analysis_parameter_dim, option_data->string_finite_time_exponent, "Please specify two time values to calculate finite time Lyapunov exponents\n");
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] && option_data->string_finite_time_covariant_exponent) {
    option_data->analysis_type = FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT;
    option_data->analysis_parameter_dim = 3;
    option_data->analysis_parameter = mathfluid_utils_string_numbers_to_allocated_array_of_double(option_data->analysis_parameter_dim, option_data->string_finite_time_covariant_exponent, "Please specify three time values to calculate finite time covariant Lyapunov exponents\n");
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] && option_data->string_finite_time_covariant_exponent_from_clv) {
    option_data->analysis_type = FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT_FROM_CLV;
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR] && option_data->string_clv) {
    option_data->analysis_type = COVARIANT_LYAPUNOV_VECTOR;
    option_data->analysis_parameter_dim = 3;
    option_data->analysis_parameter = mathfluid_utils_string_numbers_to_allocated_array_of_double(option_data->analysis_parameter_dim, option_data->string_clv, "Please specify three time values to calculate covariant Lyapunov vectors\n");
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR] && option_data->string_blv) {
    option_data->analysis_type = BACKWARD_LYAPUNOV_VECTOR;
    option_data->analysis_parameter_dim = 2;
    option_data->analysis_parameter = mathfluid_utils_string_numbers_to_allocated_array_of_double(option_data->analysis_parameter_dim, option_data->string_blv, "Please specify two time values to calculate backward Lyapunov vectors\n");
  } else if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST] && option_data->string_clv_test) {
    option_data->analysis_type = COVARIANT_LYAPUNOV_VECTOR_TEST;
  }
  if (option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE] && option_data->use_database) {
    option_data->cache_directory = mathfluid_utils_tmpdir_get_subdir("lyapunov");
  }
  COMMAND_CLASS(cmd_lyapunov_analysis_parent_class)->process_data(ptr_self);
}

static void lyapunov_vectors_print_free (LyapunovVectorSet *lyapunov_vectors, CmdLyapunovAnalysis *self)
{
  lyapunov_vector_set_fprintf(stdout, lyapunov_vectors, command_base_number_format(self), command_base_number_splitter(self));
}

/**
 * Calculate and output Lyapunov exponents from calculation of backward Lyapunov vectors,
 * which is composed of forward iterations of vectors in tangent spaces.
 */
static void cmd_lyapunov_analysis_execute_le_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  double *lyapunov_exponents;
  lyapunov_exponents = lyapunov_analysis_lyapunov_exponents(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->initial_point);
  mathfluid_utils_array_of_double_printf(self->option_data->dimension, lyapunov_exponents, command_base_number_format(self), command_base_number_splitter(self));
  printf("\n");
  g_free(lyapunov_exponents);
}

/**
 * Print array of (self->target_number + 1) numbers
 */
static void cmd_lyapunov_analysis_ftle_print (CmdLyapunovAnalysis *self, double *lyapunov_exponents)
{
  mathfluid_utils_array_of_double_printf(cmd_lyapunov_analysis_target_number(self) + 1, lyapunov_exponents, command_base_number_format(self), command_base_number_splitter(self));
  printf("\n");
}

/**
 * Calculate and output finite time Lyapunov exponents which are Lyapunov exponents of short time intervals
 * at each time step
 */
static void cmd_lyapunov_analysis_execute_ftle_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  int i;
  GPtrArray *finite_time_lyapunov_exponents;
  double *lyapunov_exponents;
  finite_time_lyapunov_exponents = lyapunov_analysis_finite_time_lyapunov_exponents(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->analysis_parameter[1], self->option_data->initial_point);
  for (i = 0; i < finite_time_lyapunov_exponents->len; i++) {
    lyapunov_exponents = g_ptr_array_index(finite_time_lyapunov_exponents, i);
    cmd_lyapunov_analysis_ftle_print(self, lyapunov_exponents);
  }
  g_ptr_array_free(finite_time_lyapunov_exponents, TRUE);
}

static void cmd_lyapunov_analysis_execute_blv_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  LyapunovVectorSet *backward_lyapunov_vectors;
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = self->option_data;
  backward_lyapunov_vectors = lyapunov_analysis_backward_lyapunov_vectors(lyapunov_analysis, option_data->analysis_parameter[0], option_data->analysis_parameter[1], option_data->initial_point);
  lyapunov_vectors_print_free(backward_lyapunov_vectors, self);
}

static void cmd_lyapunov_analysis_execute_clv_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  LyapunovVectorSet *covariant_lyapunov_vectors;
  if ((self->option_data->clv_initial_iterate <= 0) && (self->option_data->clv_last_iterate <= 0)) {
    covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->analysis_parameter[1], self->option_data->analysis_parameter[2], self->option_data->initial_point);
  } else {
    covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors_with_iteration(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->analysis_parameter[1], self->option_data->analysis_parameter[2], self->option_data->initial_point, self->option_data->clv_initial_iterate, self->option_data->clv_last_iterate);
  }
  lyapunov_vectors_print_free(covariant_lyapunov_vectors, self);
}

static void cmd_lyapunov_analysis_execute_clv_test (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  GPtrArray *inner_products;
  int i;
  double *ary_double;
  inner_products = lyapunov_analysis_covariant_lyapunov_vectors_test(lyapunov_analysis, self->option_data->string_clv_test);
  for (i = 0; i < inner_products->len; i++) {
    ary_double = (double *) g_ptr_array_index(inner_products, i);
    cmd_lyapunov_analysis_ftle_print(self, ary_double);
  }
  g_ptr_array_free(inner_products, TRUE);
}

static void cmd_lyapunov_analysis_finite_time_covariant_lyapunov_exponents_printf_free (CmdLyapunovAnalysis *self, LyapunovAnalysis *lyapunov_analysis, GPtrArray *finite_time_covariant_lyapunov_exponents)
{
  int i;
  double *lyapunov_exponents;
  for (i = 0; i < finite_time_covariant_lyapunov_exponents->len; i++) {
    lyapunov_exponents = g_ptr_array_index(finite_time_covariant_lyapunov_exponents, i);
    cmd_lyapunov_analysis_ftle_print(self, lyapunov_exponents);
  }
  g_ptr_array_free(finite_time_covariant_lyapunov_exponents, TRUE);
}

static void cmd_lyapunov_analysis_execute_ftcle_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  LyapunovVectorSet *covariant_lyapunov_vectors;
  GPtrArray *finite_time_covariant_lyapunov_exponents;
  if ((self->option_data->clv_initial_iterate <= 0) && (self->option_data->clv_last_iterate <= 0)) {
    covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->analysis_parameter[1], self->option_data->analysis_parameter[2], self->option_data->initial_point);
  } else {
    covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors_with_iteration(lyapunov_analysis, self->option_data->analysis_parameter[0], self->option_data->analysis_parameter[1], self->option_data->analysis_parameter[2], self->option_data->initial_point, self->option_data->clv_initial_iterate, self->option_data->clv_last_iterate);
  }
  finite_time_covariant_lyapunov_exponents = lyapunov_analysis_finite_time_covariant_lyapunov_exponents(lyapunov_analysis, covariant_lyapunov_vectors);
  cmd_lyapunov_analysis_finite_time_covariant_lyapunov_exponents_printf_free(self, lyapunov_analysis, finite_time_covariant_lyapunov_exponents);
  lyapunov_vector_set_free(covariant_lyapunov_vectors);
}

static void cmd_lyapunov_analysis_execute_ftcle_from_clv_output (LyapunovAnalysis *lyapunov_analysis, CmdLyapunovAnalysis *self)
{
  GPtrArray *finite_time_covariant_lyapunov_exponents;
  finite_time_covariant_lyapunov_exponents = lyapunov_analysis_finite_time_covariant_lyapunov_exponents2(lyapunov_analysis, self->option_data->string_finite_time_covariant_exponent_from_clv);
  cmd_lyapunov_analysis_finite_time_covariant_lyapunov_exponents_printf_free(self, lyapunov_analysis, finite_time_covariant_lyapunov_exponents);
}

int cmd_lyapunov_analysis_target_number (gpointer ptr_self)
{
  CmdLyapunovAnalysis *self;
  self = CMD_LYAPUNOV_ANALYSIS(ptr_self);
  if (self->option_data->target_number <= 0 || self->option_data->target_number > self->option_data->dimension) {
    return self->option_data->dimension;
  } else {
    return self->option_data->target_number;
  }
}

static LyapunovAnalysis *cmd_lyapunov_analysis_create (CmdLyapunovAnalysis *self)
{
  LyapunovAnalysis *lyapunov_analysis;
  lyapunov_analysis = lyapunov_analysis_alloc(COMMAND(self)->dynamical_system, cmd_lyapunov_analysis_target_number(self), self->option_data->time_step);
  if (self->option_data->space_derivative_perturbation > 0.0) {
    lyapunov_analysis_set_space_derivative_perturbation(lyapunov_analysis, self->option_data->space_derivative_perturbation);
  }
  if (self->option_data->cache_directory) {
    lyapunov_analysis_set_cache_directory_path(lyapunov_analysis, self->option_data->cache_directory);
  }
  if (mathfluid_thread_pool_max_thread_number() > 0) {
    lyapunov_analysis_set_use_thread(lyapunov_analysis, TRUE);
  }
  return lyapunov_analysis;
}

static void cmd_lyapunov_analysis_execute (gpointer ptr_self)
{
  CmdLyapunovAnalysis *self;
  LyapunovAnalysis *lyapunov_analysis;
  self = CMD_LYAPUNOV_ANALYSIS(ptr_self);
  command_base_header_json_fprint(stdout, ptr_self);
  lyapunov_analysis = cmd_lyapunov_analysis_create(self);
  if (self->option_data->analysis_type == LYAPUNOV_EXPONENT) {
    cmd_lyapunov_analysis_execute_le_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == FINITE_TIME_LYAPUNOV_EXPONENT) {
    cmd_lyapunov_analysis_execute_ftle_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == BACKWARD_LYAPUNOV_VECTOR) {
    cmd_lyapunov_analysis_execute_blv_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == COVARIANT_LYAPUNOV_VECTOR) {
    cmd_lyapunov_analysis_execute_clv_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT) {
    cmd_lyapunov_analysis_execute_ftcle_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT_FROM_CLV) {
    cmd_lyapunov_analysis_execute_ftcle_from_clv_output(lyapunov_analysis, self);
  } else if (self->option_data->analysis_type == COVARIANT_LYAPUNOV_VECTOR_TEST) {
    cmd_lyapunov_analysis_execute_clv_test(lyapunov_analysis, self);
  } else {
    printf("No calculation\n");
  }
  lyapunov_analysis_free(lyapunov_analysis);
  mathfluid_utils_tmpdir_clear();
}

static void cmd_lyapunov_analysis_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdLyapunovAnalysis *self;
  self = CMD_LYAPUNOV_ANALYSIS(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Lyapunov analysis");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "target_number");
  json_builder_add_int_value(builder, self->option_data->target_number > 0 ? self->option_data->target_number : self->option_data->dimension);
  json_builder_set_member_name(builder, "time_step");
  json_builder_add_double_value(builder, self->option_data->time_step);
  json_builder_set_member_name(builder, "space_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->space_derivative_perturbation);
  json_builder_set_member_name(builder, "use_database");
  json_builder_add_boolean_value(builder, self->option_data->use_database);
  json_builder_set_member_name(builder, "analysis_type");
  if (self->option_data->analysis_type == LYAPUNOV_EXPONENT) {
    json_builder_add_string_value(builder, "LE");
  } else if (self->option_data->analysis_type == FINITE_TIME_LYAPUNOV_EXPONENT) {
    json_builder_add_string_value(builder, "FTLE");
  } else if (self->option_data->analysis_type == BACKWARD_LYAPUNOV_VECTOR) {
    json_builder_add_string_value(builder, "BLV");
  } else if (self->option_data->analysis_type == COVARIANT_LYAPUNOV_VECTOR) {
    json_builder_add_string_value(builder, "CLV");
  } else if ((self->option_data->analysis_type == FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT) || (self->option_data->analysis_type == FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT_FROM_CLV)) {
    json_builder_add_string_value(builder, "FTCLE");
  } else if (self->option_data->analysis_type == COVARIANT_LYAPUNOV_VECTOR_TEST) {
    json_builder_add_string_value(builder, "CLV TEST");
  } else {
    json_builder_add_string_value(builder, "NONE");
  }
  if (self->option_data->analysis_parameter) {
    int i;
    json_builder_set_member_name(builder, "analysis_parameter");
    json_builder_begin_array(builder);
    for (i = 0; i < self->option_data->analysis_parameter_dim; i++) {
      json_builder_add_double_value(builder, self->option_data->analysis_parameter[i]);
    }
    if ((self->option_data->analysis_type == COVARIANT_LYAPUNOV_VECTOR) || (self->option_data->analysis_type == FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT)) {
      json_builder_add_int_value(builder, self->option_data->clv_initial_iterate);
      json_builder_add_int_value(builder, self->option_data->clv_last_iterate);
    }
    json_builder_end_array(builder);
  }
}

static void cmd_lyapunov_analysis_class_init (CmdLyapunovAnalysisClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_lyapunov_analysis_dispose;
  gobject_class->finalize = cmd_lyapunov_analysis_finalize;
  gobject_class->set_property = cmd_lyapunov_analysis_set_property;
  gobject_class->get_property = cmd_lyapunov_analysis_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_lyapunov_analysis_command_line_arguments_define;
  command_class->process_data = cmd_lyapunov_analysis_process_data;
  command_class->execute = cmd_lyapunov_analysis_execute;

  command_base_class->header_json_save = cmd_lyapunov_analysis_header_json_save;
}
