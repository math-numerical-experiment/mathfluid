#ifndef _CMD_LYAPUNOV_ANALYSIS_H_
#define _CMD_LYAPUNOV_ANALYSIS_H_

typedef struct _CmdLyapunovAnalysisClass CmdLyapunovAnalysisClass;
typedef struct _CmdLyapunovAnalysis CmdLyapunovAnalysis;
typedef struct _CmdLyapunovAnalysisOptionData CmdLyapunovAnalysisOptionData;

typedef enum {
  LYAPUNOV_ANALYSIS_OPTION_VERBOSE,
  LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT,
  LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT_WITH_DIM,
  LYAPUNOV_ANALYSIS_OPTION_NUMBER,
  LYAPUNOV_ANALYSIS_OPTION_EXPONENT,
  LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT,
  LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT,
  LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR,
  LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR,
  LYAPUNOV_ANALYSIS_OPTION_SPACE_DERIVATIVE_PERTURBATION,
  LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST,
  LYAPUNOV_ANALYSIS_OPTION_TIME_STEP,
  LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE,
  LYAPUNOV_ANALYSIS_OPTION_NUMBER_FORMAT,
  LYAPUNOV_ANALYSIS_OPTION_NUMBER_SPLITTER,
  LYAPUNOV_ANALYSIS_OPTION_LOAD_ARGUMENTS,
  N_LYAPUNOV_ANALYSIS_OPTIONS
} CmdLyapunovAnalysisOptionType;

typedef enum {
  NOT_SPECIFIED,
  LYAPUNOV_EXPONENT,
  FINITE_TIME_LYAPUNOV_EXPONENT,
  FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT,
  FINITE_TIME_COVARIANT_LYAPUNOV_EXPONENT_FROM_CLV,
  COVARIANT_LYAPUNOV_VECTOR,
  BACKWARD_LYAPUNOV_VECTOR,
  COVARIANT_LYAPUNOV_VECTOR_TEST
} LyapunovAnalysisType;

struct _CmdLyapunovAnalysisOptionData {
  double time_step;
  double space_derivative_perturbation;
  double *analysis_parameter;
  double *initial_point;
  int dimension;
  int target_number;
  int analysis_parameter_dim;
  int clv_initial_iterate;
  int clv_last_iterate;
  char *string_initial_point;
  char *string_exponent;
  char *string_finite_time_exponent;
  char *string_finite_time_covariant_exponent;
  char *string_finite_time_covariant_exponent_from_clv;
  char *string_clv;
  char *string_blv;
  char *string_clv_test;
  char *cache_directory;
  LyapunovAnalysisType analysis_type;
  gboolean use_database;
  gboolean active_options[N_LYAPUNOV_ANALYSIS_OPTIONS];
};

struct _CmdLyapunovAnalysisClass {
  CommandBaseClass parent;
};

struct _CmdLyapunovAnalysis {
  CommandBase parent;

  CmdLyapunovAnalysisOptionData *option_data;
};

#define TYPE_CMD_LYAPUNOV_ANALYSIS (cmd_lyapunov_analysis_get_type ())
#define CMD_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_LYAPUNOV_ANALYSIS, CmdLyapunovAnalysis))
#define CMD_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_LYAPUNOV_ANALYSIS, CmdLyapunovAnalysisClass))
#define IS_CMD_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_LYAPUNOV_ANALYSIS))
#define IS_CMD_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_LYAPUNOV_ANALYSIS))
#define CMD_LYAPUNOV_ANALYSIS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_LYAPUNOV_ANALYSIS, CmdLyapunovAnalysisClass))

GType cmd_lyapunov_analysis_get_type ();
#define cmd_lyapunov_analysis_dimension(self) (CMD_LYAPUNOV_ANALYSIS(self)->option_data->dimension)
int cmd_lyapunov_analysis_target_number (gpointer ptr_self);
void cmd_lyapunov_analysis_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_LYAPUNOV_ANALYSIS_H_ */
