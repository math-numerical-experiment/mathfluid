#ifndef _CMD_RAYLEIGH_RITZ_H_
#define _CMD_RAYLEIGH_RITZ_H_

typedef struct _CmdRayleighRitzClass CmdRayleighRitzClass;
typedef struct _CmdRayleighRitz CmdRayleighRitz;
typedef struct _CmdRayleighRitzOptionData CmdRayleighRitzOptionData;

typedef enum {
  RAYLEIGH_RITZ_OPTION_VERBOSE,
  RAYLEIGH_RITZ_OPTION_INITIAL_POINT,
  RAYLEIGH_RITZ_OPTION_INITIAL_POINT_WITH_DIM,
  RAYLEIGH_RITZ_OPTION_DIMENSION_INVARIANT_SPACE,
  RAYLEIGH_RITZ_OPTION_SIMULTANEOUS_ITERATION,
  RAYLEIGH_RITZ_OPTION_ARNOLDI_METHOD,
  RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV,
  RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION,
  RAYLEIGH_RITZ_OPTION_FROM_JACOBIAN,
  RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV,
  RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE,
  RAYLEIGH_RITZ_OPTION_SORT,
  RAYLEIGH_RITZ_OPTION_TIME_DERIVATIVE_PERTURBATION,
  RAYLEIGH_RITZ_OPTION_SPACE_DERIVATIVE_PERTURBATION,
  RAYLEIGH_RITZ_OPTION_NUMBER_FORMAT,
  RAYLEIGH_RITZ_OPTION_NUMBER_SPLITTER,
  RAYLEIGH_RITZ_OPTION_LOG_LEVEL,
  RAYLEIGH_RITZ_OPTION_LOAD_ARGUMENTS,
  N_RAYLEIGH_RITZ_OPTIONS
} CmdRayleighRitzOptionType;

enum {
  CMD_RAYLEIGH_RITZ_N_LOG_LEVELS,
};

typedef enum {
  RAYLEIGH_RITZ_INVARIANT_SPACE_SIMULTANEOUS_ITERATION,
  RAYLEIGH_RITZ_INVARIANT_SPACE_ARNOLDI_METHOD,
  RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV,
  RAYLEIGH_RITZ_INVARIANT_SPACE_UNDEFINED
} RayleighRitzInvariantSpaceType;

struct _CmdRayleighRitzOptionData {
  RayleighRitzInvariantSpaceType invariant_space_type;
  double time_derivative_perturbation;
  double space_derivative_perturbation;
  double number_shift;
  double gmres_error_ratio;
  double lyapunov_vector_time_step;
  int dimension;
  int dimension_invariant_space;
  int iteration_of_simultaneous_iteration;
  int gmres_max_iteration;
  int lyapunov_vector_iterate1;
  int lyapunov_vector_iterate2;
  char *string_initial_point;
  char *string_initial_invariant_space;
  char *string_backward_lyapunov;
  char *string_covariant_lyapunov;
  char *string_inverse_iteration;
  double *initial_point;
  double *initial_invariant_space;
  gboolean sort;
  gboolean inverse_p;
  gboolean arnoldi_method;
  gboolean from_jacobian;
  gboolean covariant_lyapunov_vector;
  gboolean active_options[N_RAYLEIGH_RITZ_OPTIONS];
};

struct _CmdRayleighRitzClass {
  CommandBaseClass parent;
};

struct _CmdRayleighRitz {
  CommandBase parent;

  CmdRayleighRitzOptionData *option_data;
};

#define TYPE_CMD_RAYLEIGH_RITZ (cmd_rayleigh_ritz_get_type ())
#define CMD_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_RAYLEIGH_RITZ, CmdRayleighRitz))
#define CMD_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_RAYLEIGH_RITZ, CmdRayleighRitzClass))
#define IS_CMD_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_RAYLEIGH_RITZ))
#define IS_CMD_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_RAYLEIGH_RITZ))
#define CMD_RAYLEIGH_RITZ_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_RAYLEIGH_RITZ, CmdRayleighRitzClass))

GType cmd_rayleigh_ritz_get_type ();
#define cmd_rayleigh_ritz_dimension(self) (CMD_RAYLEIGH_RITZ(self)->option_data->dimension)
void cmd_rayleigh_ritz_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_RAYLEIGH_RITZ_H_ */
