#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdSearchEquilibrium, cmd_search_equilibrium, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_search_equilibrium_option_range_set (CmdSearchEquilibrium *self, int ind, const char *range_string)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = self->option_data;
  if (!option_data->search_range) {
    option_data->search_range = command_utils_search_range_alloc(option_data->dimension);
  }
  command_utils_search_range_option_parse(g_ptr_array_index(option_data->search_range, ind), range_string);
}

void cmd_search_equilibrium_set_dimension (gpointer ptr_self, int dim)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  option_data->dimension = dim;
  if (option_data->search_range) {
    g_ptr_array_free(option_data->search_range, TRUE);
    option_data->search_range = NULL;
  }
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_search_equilibrium_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_search_equilibrium_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_search_equilibrium_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdSearchEquilibrium *self = CMD_SEARCH_EQUILIBRIUM(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, self->option_data->dimension);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_search_equilibrium_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_search_equilibrium_parent_class)->dispose(gobject);
}

static CmdSearchEquilibriumOptionData *cmd_search_equilibrium_option_data_alloc ()
{
  int i;
  CmdSearchEquilibriumOptionData *option_data;
  option_data = (CmdSearchEquilibriumOptionData *) g_malloc(sizeof(CmdSearchEquilibriumOptionData));
  option_data->remove_duplication = FALSE;
  option_data->sort_equilibria = FALSE;
  option_data->error_equilibria = 1.0e-6;
  option_data->dimension = 0;
  option_data->max_iteration = 1000;
  option_data->damping_parameter = 0;
  option_data->time_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION;
  option_data->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  option_data->search_range = NULL;
  option_data->initial_point = NULL;
  option_data->string_initial_point = NULL;
  for (i = 0; i < N_SEARCH_EQUILIBRIUM_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_search_equilibrium_option_data_free (CmdSearchEquilibriumOptionData *option_data)
{
  if (option_data) {
    if (option_data->search_range) {
      g_ptr_array_free(option_data->search_range, TRUE);
    }
    if (option_data->initial_point) {
      g_free(option_data->initial_point);
    }
    if (option_data->string_initial_point) {
      g_free(option_data->string_initial_point);
    }
    g_free(option_data);
  }
}

static void cmd_search_equilibrium_finalize (GObject *gobject)
{
  CmdSearchEquilibrium *self;
  self = CMD_SEARCH_EQUILIBRIUM(gobject);
  cmd_search_equilibrium_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_search_equilibrium_parent_class)->finalize(gobject);
}

static void cmd_search_equilibrium_init (CmdSearchEquilibrium *self)
{
  self->option_data = cmd_search_equilibrium_option_data_alloc();
}

static void cmd_search_equilibrium_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT] || option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "point", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_point, "Initial point specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "damping-parameter", 0, 0, G_OPTION_ARG_INT, &option_data->damping_parameter, "Damping parameter", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "remove-duplication", 'R', 0, G_OPTION_ARG_NONE, &option_data->remove_duplication, "Remove duplicated equilibria", NULL);
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "sort", 'S', 0, G_OPTION_ARG_NONE, &option_data->sort_equilibria, "Sort obtained equilibria", NULL);
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "error", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_equilibria, "Acceptable error of equilibria", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "max-iteration", 0, 0, G_OPTION_ARG_INT, &option_data->max_iteration, "Max number of iterations", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_derivative_perturbation, "Time evolution to calculate numerical derivative", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "space-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->space_derivative_perturbation, "Perturbation to calculate Jacobian free product", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_VERBOSE]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_VERBOSE] = TRUE;
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] = TRUE;
    COMMAND_BASE(ptr_self)->option_data->number_log_level = CMD_SEARCH_EQUILIBRIUM_N_LOG_LEVELS;
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_search_equilibrium_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void search_equilibrium_initial_point_alloc (CmdSearchEquilibriumOptionData *option_data)
{
  int i;
  if (option_data->initial_point) {
    g_free(option_data->initial_point);
  }
  option_data->initial_point = (double *) g_malloc(sizeof(double) * option_data->dimension);
  for (i = 0; i < option_data->dimension; i++) {
    option_data->initial_point[i] = 0.0;
  }
}

static void cmd_search_equilibrium_process_data (gpointer ptr_self)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_point);
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT]) {
    search_equilibrium_initial_point_alloc(option_data);
    command_utils_point_data_load(option_data->initial_point, option_data->string_initial_point, option_data->dimension);
  } else if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM]) {
    if (option_data->string_initial_point) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_point);
      cmd_search_equilibrium_set_dimension(ptr_self, dim);
      search_equilibrium_initial_point_alloc(option_data);
      memcpy(option_data->initial_point, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_point) {
    g_free(option_data->string_initial_point);
    option_data->string_initial_point = NULL;
  }
  COMMAND_CLASS(cmd_search_equilibrium_parent_class)->process_data(ptr_self);
}

static void search_equilibrium_verbose_print (gpointer ptr_self, gboolean found, double *pt_init, double *pt_result, int iter)
{
  if (command_base_verbose(ptr_self)) {
    int dim;
    const char *number_format, *number_splitter;
    dim = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data->dimension;
    number_format = command_base_number_format(ptr_self);
    number_splitter = command_base_number_splitter(ptr_self);
    if (found) {
      printf("[Found %d]\t", abs(iter));
    } else {
      printf("[Not found %d]\t", abs(iter));
    }
    mathfluid_utils_array_of_double_printf(dim, pt_init, number_format, number_splitter);
    printf("%s", number_splitter);
    mathfluid_utils_array_of_double_printf(dim, pt_result, number_format, number_splitter);
    printf("\n");
  }
}

static void search_equilibrium_append (SearchEquilibrium *search, gpointer ptr_self, GPtrArray *equilibria, double *pt) {
  CmdSearchEquilibriumOptionData *option_data;
  int iter;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  search_equilibrium_set(search, pt);
  iter = search_equilibrium_iterate_successively(search, option_data->max_iteration);
  if (iter > 0) {
    gsl_vector *vec;
    gboolean already_stored = FALSE;
    vec = gsl_vector_alloc(search_equilibrium_dimension(search));
    search_equilibrium_solution_copy(vec->data, search);
    search_equilibrium_verbose_print(ptr_self, TRUE, pt, vec->data, iter);
    if (option_data->remove_duplication) {
      gsl_vector *vec2;
      int i, j;
      gboolean duplicate;
      for (i = 0; i < equilibria->len; i++) {
        duplicate = TRUE;
        vec2 = g_ptr_array_index(equilibria, i);
        for (j = 0; j < search_equilibrium_dimension(search); j++) {
          if (fabs(gsl_vector_get(vec, j) - gsl_vector_get(vec2, j)) > search_periodic_orbit_newton_practical_error((search))) {
            duplicate = FALSE;
            break;
          }
        }
        if (duplicate) {
          already_stored = TRUE;
          break;
        }
      }
    }
    if (already_stored) {
      gsl_vector_free(vec);
    } else {
      g_ptr_array_add(equilibria, (gpointer) vec);
    }
  } else {
    search_equilibrium_verbose_print(ptr_self, FALSE,  pt, search_equilibrium_solution_ptr(search), iter);
  }
}

static GPtrArray *equilibria_search (CmdSearchEquilibrium *self)
{
  CmdSearchEquilibriumOptionData *option_data;
  GPtrArray *equilibria;
  SearchEquilibrium *search;

  option_data = self->option_data;
  if (option_data->dimension <= 0) {
    fprintf(stderr, "Dimension has not been set properly\n");
    abort();
  }
  search = search_equilibrium_alloc(COMMAND(self)->dynamical_system, option_data->damping_parameter, option_data->error_equilibria);
  if (option_data->time_derivative_perturbation > 0.0) {
    search_equilibrium_set_time_derivative_perturbation(search, option_data->time_derivative_perturbation);
  }
  if (option_data->space_derivative_perturbation > 0.0) {
    search_equilibrium_set_space_derivative_perturbation(search, option_data->space_derivative_perturbation);
  }
  search_equilibrium_set_log_level(search, COMMAND_BASE(self)->option_data->log_level[CMD_SEARCH_EQUILIBRIUM_LOG_LEVEL_NEWTON], COMMAND_BASE(self)->option_data->log_level[CMD_SEARCH_EQUILIBRIUM_LOG_LEVEL_GMRES]);
  equilibria = g_ptr_array_new_with_free_func((void (*) (gpointer)) gsl_vector_free);
  if (option_data->search_range) {
    double *pt;
    CommandUtilsPointsInRectangle *points_in_rectangle;
    points_in_rectangle = command_utils_points_in_rectangle_alloc(option_data->search_range);
    while ((pt = command_utils_points_in_rectangle_next(points_in_rectangle))) {
      search_equilibrium_append(search, self, equilibria, pt);
    }
    command_utils_points_in_rectangle_free(points_in_rectangle);
  } else {
    if (!option_data->initial_point) {
      search_equilibrium_initial_point_alloc(option_data);
    }
    search_equilibrium_append(search, self, equilibria, option_data->initial_point);
  }
  search_equilibrium_free(search);
  return equilibria;
}

static int equilibria_compare (gconstpointer a, gconstpointer b)
{
  double x, y;
  x = gsl_vector_get(*(gsl_vector **) a, 0);
  y = gsl_vector_get(*(gsl_vector **) b, 0);
  if (x < y) {
    return -1;
  } else if (x > y) {
    return 1;
  } else {
    return 0;
  }
}

static void equilibria_sort (GPtrArray *equilibria)
{
  g_ptr_array_sort(equilibria, equilibria_compare);
}

static void equilibria_print (CmdSearchEquilibrium *self, GPtrArray *equilibria)
{
  int i, dim;
  const char *number_format, *number_splitter;
  dim = cmd_search_equilibrium_dimension(self);
  number_format = command_base_number_format(self);
  number_splitter = command_base_number_splitter(self);
  for (i = 0; i < equilibria->len; i++) {
    gsl_vector *vec;
    vec = g_ptr_array_index(equilibria, i);
    mathfluid_utils_array_of_double_printf(dim, vec->data, number_format, number_splitter);
    printf("\n");
  }
}

static void cmd_search_equilibrium_search (gpointer ptr_self)
{
  GPtrArray *equilibria;
  CmdSearchEquilibrium *self;
  CmdSearchEquilibriumOptionData *option_data;
  self = CMD_SEARCH_EQUILIBRIUM(ptr_self);
  option_data = self->option_data;

  equilibria = equilibria_search(self);
  if (option_data->sort_equilibria) {
    equilibria_sort(equilibria);
  }
  equilibria_print(self, equilibria);
  g_ptr_array_free(equilibria, TRUE);
}

static void cmd_search_equilibrium_execute (gpointer ptr_self)
{
  command_base_header_json_fprint(stdout, ptr_self);
  cmd_search_equilibrium_search(ptr_self);
}

static void cmd_search_equilibrium_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdSearchEquilibrium *self;
  self = CMD_SEARCH_EQUILIBRIUM(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Search equilibrium");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "damping_parameter");
  json_builder_add_int_value(builder, self->option_data->damping_parameter);
  json_builder_set_member_name(builder, "max_iteration");
  json_builder_add_int_value(builder, self->option_data->max_iteration);
  json_builder_set_member_name(builder, "error_equilibria");
  json_builder_add_double_value(builder, self->option_data->error_equilibria);
  json_builder_set_member_name(builder, "time_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->time_derivative_perturbation);
  json_builder_set_member_name(builder, "space_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->space_derivative_perturbation);
  json_builder_set_member_name(builder, "remove_duplication");
  json_builder_add_boolean_value(builder, self->option_data->remove_duplication);
  json_builder_set_member_name(builder, "sort_equilibria");
  json_builder_add_boolean_value(builder, self->option_data->sort_equilibria);
  if (self->option_data->search_range) {
    json_builder_set_member_name(builder, "search_range");
    command_utils_search_range_json_output(builder, self->option_data->search_range);
  }
}

static void cmd_search_equilibrium_class_init (CmdSearchEquilibriumClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_search_equilibrium_dispose;
  gobject_class->finalize = cmd_search_equilibrium_finalize;
  gobject_class->set_property = cmd_search_equilibrium_set_property;
  gobject_class->get_property = cmd_search_equilibrium_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_search_equilibrium_command_line_arguments_define;
  command_class->process_data = cmd_search_equilibrium_process_data;
  command_class->execute = cmd_search_equilibrium_execute;

  command_base_class->header_json_save = cmd_search_equilibrium_header_json_save;
}
