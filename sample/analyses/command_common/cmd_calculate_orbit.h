#ifndef _CMD_CALCULATE_ORBIT_H_
#define _CMD_CALCULATE_ORBIT_H_

typedef struct _CmdCalcOrbitClass CmdCalcOrbitClass;
typedef struct _CmdCalcOrbit CmdCalcOrbit;
typedef struct _CmdCalcOrbitOptionData CmdCalcOrbitOptionData;

typedef enum {
  CALC_ORBIT_OPTION_VECTOR_FIELD,
  CALC_ORBIT_OPTION_TIME_MAX,
  CALC_ORBIT_OPTION_TIME_STEP,
  CALC_ORBIT_OPTION_INITIAL_POINT,
  CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM,
  CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL,
  CALC_ORBIT_OPTION_PERIODIC_ORBIT,
  CALC_ORBIT_OPTION_NUMBER_FORMAT,
  CALC_ORBIT_OPTION_NUMBER_SPLITTER,
  CALC_ORBIT_OPTION_LOAD_ARGUMENTS,
  N_CALC_ORBIT_OPTIONS
} CmdCalcOrbitOptionType;

struct _CmdCalcOrbitOptionData {
  gboolean vector_field;
  gboolean periodic_orbit;
  int dimension;
  double time_max;
  double time_step;
  double output_time_interval;
  double *initial_point;
  char *string_initial_point;
  gboolean active_options[N_CALC_ORBIT_OPTIONS];
};

struct _CmdCalcOrbitClass {
  CommandBaseClass parent;

  void (*print_orbit) (gpointer ptr_self, SystemState *state, double *vector_field);
};

struct _CmdCalcOrbit {
  CommandBase parent;

  double last_output;
  CmdCalcOrbitOptionData *option_data;
};

#define TYPE_CMD_CALC_ORBIT (cmd_calc_orbit_get_type ())
#define CMD_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_CALC_ORBIT, CmdCalcOrbit))
#define CMD_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_CALC_ORBIT, CmdCalcOrbitClass))
#define IS_CMD_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_CALC_ORBIT))
#define IS_CMD_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_CALC_ORBIT))
#define CMD_CALC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_CALC_ORBIT, CmdCalcOrbitClass))

GType cmd_calc_orbit_get_type ();
#define cmd_calc_orbit_dimension(self) (CMD_CALC_ORBIT(self)->option_data->dimension)
void cmd_calc_orbit_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_CALCULATE_ORBIT_H_ */
