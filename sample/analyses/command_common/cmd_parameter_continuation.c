#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdParameterContinuation, cmd_parameter_continuation, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_parameter_continuation_set_dimension (gpointer ptr_self, int dim)
{
  CmdParameterContinuationOptionData *option_data;
  option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  option_data->dimension = dim;
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_parameter_continuation_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_parameter_continuation_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_parameter_continuation_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdParameterContinuation *self = CMD_PARAMETER_CONTINUATION(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, self->option_data->dimension);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_parameter_continuation_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_parameter_continuation_parent_class)->dispose(gobject);
}

static CmdParameterContinuationOptionData *cmd_parameter_continuation_option_data_alloc ()
{
  int i;
  CmdParameterContinuationOptionData *option_data;
  option_data = (CmdParameterContinuationOptionData *) g_malloc(sizeof(CmdParameterContinuationOptionData));
  option_data->error_equilibria = 1.0e-6;
  option_data->dimension = 0;
  option_data->newton_max_iteration = 1000;
  option_data->newton_damping_parameter = 0;
  option_data->direction = PARAMETER_CONTINUATION_DIRECTION_POSITIVE;
  option_data->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  option_data->step_length = PARAMETER_CONTINUATION_DEFAULT_STEP_LENGTH;
  option_data->min_step_length = -1.0;
  option_data->max_arc_length = 10.0;
  option_data->time_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION;
  option_data->parameter_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_PARAMETER_DERIVATIVE_PERTURBATION;
  option_data->gmres_corrector_max_iteration = -1;
  option_data->initial_equilibrium = NULL;
  option_data->parameter_key = NULL;
  option_data->string_direction = NULL;
  option_data->string_initial_equilibrium = NULL;
  option_data->string_step_length_adjusted = NULL;
  option_data->newton_method = NULL;
  for (i = 0; i < N_PARAMETER_CONTINUATION_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_parameter_continuation_option_data_free (CmdParameterContinuationOptionData *option_data)
{
  if (option_data) {
    if (option_data->initial_equilibrium) {
      g_free(option_data->initial_equilibrium);
    }
    if (option_data->string_initial_equilibrium) {
      g_free(option_data->string_initial_equilibrium);
    }
    if (option_data->parameter_key) {
      g_free(option_data->parameter_key);
    }
    if (option_data->string_direction) {
      g_free(option_data->string_direction);
    }
    if (option_data->newton_method) {
      g_free(option_data->newton_method);
    }
    g_free(option_data);
  }
}

static void cmd_parameter_continuation_finalize (GObject *gobject)
{
  CmdParameterContinuation *self;
  self = CMD_PARAMETER_CONTINUATION(gobject);
  cmd_parameter_continuation_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_parameter_continuation_parent_class)->finalize(gobject);
}

static void cmd_parameter_continuation_init (CmdParameterContinuation *self)
{
  self->option_data = cmd_parameter_continuation_option_data_alloc();
}

static void cmd_parameter_continuation_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdParameterContinuationOptionData *option_data;
  option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM] || option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "equilibrium", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_equilibrium, "Initial equilibrium specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_KEY]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "parameter", 0, 0, G_OPTION_ARG_STRING, &option_data->parameter_key, "Parameter name to be continued", "STR");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DIRECTION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "direction", 0, 0, G_OPTION_ARG_STRING, &option_data->string_direction, "Direction of continuation: 'positive' or 'negative'", "STR");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_FIXED]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "step-length-fixed", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->step_length, "Fixed step length", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_ADJUSTED]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "step-length-adjusted", 0, 0, G_OPTION_ARG_STRING, &option_data->string_step_length_adjusted, "Adjusted step length: initial and minimum step length", "INIT,MIN");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_MAX_ARC_LENGTH]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "max-arc-length", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->max_arc_length, "Maximum number of arc length", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_METHOD]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "newton-method", 0, 0, G_OPTION_ARG_STRING, &option_data->newton_method, "Type of Newton method: \"Keller\" or \"Gauss-Newton\"", "STR");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_DAMPING_PARAMETER]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "newton-damping-parameter", 0, 0, G_OPTION_ARG_INT, &option_data->newton_damping_parameter, "Damping parameter of Newton method", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_ERROR_EQUILIBRIA]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "newton-error", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_equilibria, "Acceptable error of equilibria", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_MAX_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "newton-max-iteration", 0, 0, G_OPTION_ARG_INT, &option_data->newton_max_iteration, "Max number of iterations of Newton method", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_GMRES_CORRECTOR_MAX_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "gmres-corrector-max-iteration", 0, 0, G_OPTION_ARG_INT, &option_data->gmres_corrector_max_iteration, "Max number of iterations of GMRES solver of corrector", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_TIME_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_derivative_perturbation, "Time evolution to calculate numerical derivative on corrector steps", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "parameter-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->parameter_derivative_perturbation, "Size of parameter perturbation on corrector steps", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_SPACE_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "space-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->space_derivative_perturbation, "Perturbation to calculate Jacobian free product", "NUM");
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_VERBOSE]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_VERBOSE] = TRUE;
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOG_LEVEL]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] = TRUE;
    COMMAND_BASE(ptr_self)->option_data->number_log_level = CMD_PARAMETER_CONTINUATION_N_LOG_LEVELS;
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_parameter_continuation_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void parameter_continuation_initial_equilibrium_alloc (CmdParameterContinuationOptionData *option_data)
{
  int i;
  if (option_data->initial_equilibrium) {
    g_free(option_data->initial_equilibrium);
  }
  option_data->initial_equilibrium = (double *) g_malloc(sizeof(double) * option_data->dimension);
  for (i = 0; i < option_data->dimension; i++) {
    option_data->initial_equilibrium[i] = 0.0;
  }
}

static void cmd_parameter_continuation_process_data (gpointer ptr_self)
{
  CmdParameterContinuationOptionData *option_data;
  option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_equilibrium);
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_KEY] && option_data->string_direction) {
    if (option_data->string_direction[0] == 'p') {
      option_data->direction = PARAMETER_CONTINUATION_DIRECTION_POSITIVE;
    } else if (option_data->string_direction[0] == 'n') {
      option_data->direction = PARAMETER_CONTINUATION_DIRECTION_NEGATIVE;
    }
  }
  if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM]) {
    parameter_continuation_initial_equilibrium_alloc(option_data);
    command_utils_point_data_load(option_data->initial_equilibrium, option_data->string_initial_equilibrium, option_data->dimension);
  } else if (option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM_WITH_DIM]) {
    if (option_data->string_initial_equilibrium) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_equilibrium);
      cmd_parameter_continuation_set_dimension(ptr_self, dim);
      parameter_continuation_initial_equilibrium_alloc(option_data);
      memcpy(option_data->initial_equilibrium, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_equilibrium) {
    g_free(option_data->string_initial_equilibrium);
    option_data->string_initial_equilibrium = NULL;
  }
  if (option_data->string_step_length_adjusted) {
    double nums[2];
    mathfluid_utils_string_numbers_to_array_of_double(nums, 2, option_data->string_step_length_adjusted, "Two numbers are required\n");
    option_data->step_length = nums[0];
    option_data->min_step_length = nums[1];
  }
  COMMAND_CLASS(cmd_parameter_continuation_parent_class)->process_data(ptr_self);
}

static ParameterContinuation *cmd_parameter_continuation_alloc (CmdParameterContinuation *self)
{
  CmdParameterContinuationOptionData *option_data;
  ParameterContinuation *continuation;
  ParameterContinuationCorrection correction;
  option_data = self->option_data;
  if (option_data->dimension <= 0) {
    fprintf(stderr, "Dimension has not been set properly\n");
    abort();
  } else if (!option_data->parameter_key) {
    fprintf(stderr, "Key of parameter is not specified\n");
    abort();
  } else if (!option_data->newton_method) {
    option_data->newton_method = g_strdup("keller");
  }
  if ((option_data->newton_method[0] == 'k') || (option_data->newton_method[0] == 'K')) {
    correction = PARAMETER_CONTINUATION_CORRECTION_KELLER;
  } else if ((option_data->newton_method[0] == 'g') || (option_data->newton_method[0] == 'G')) {
    correction = PARAMETER_CONTINUATION_CORRECTION_GAUSS_NEWTON;
  } else {
    fprintf(stderr, "Invalid type of correction: %s\n", option_data->newton_method);
    abort();
  }
  continuation = parameter_continuation_alloc(correction, COMMAND(self)->dynamical_system, option_data->parameter_key, option_data->direction, option_data->newton_damping_parameter, option_data->error_equilibria);
  if (option_data->string_step_length_adjusted) {
    parameter_continuation_set_step_length_control(continuation, PARAMETER_CONTINUATION_STEP_LENGTH_ADJUSTED, option_data->step_length, option_data->min_step_length);
  } else {
    parameter_continuation_set_step_length_control(continuation, PARAMETER_CONTINUATION_STEP_LENGTH_FIXED, option_data->step_length);
  }
  parameter_continuation_set_equilibrium(continuation, option_data->initial_equilibrium);
  parameter_continuation_set_newton_max_iteration(continuation, option_data->newton_max_iteration);
  if (option_data->gmres_corrector_max_iteration > 0) {
    parameter_continuation_set_gmres_corrector_max_iteration(continuation, option_data->gmres_corrector_max_iteration);
  }
  if (option_data->space_derivative_perturbation > 0.0) {
    parameter_continuation_set_space_derivative_perturbation(continuation,  option_data->space_derivative_perturbation);
  }
  if (option_data->time_derivative_perturbation > 0.0) {
    parameter_continuation_set_time_derivative_perturbation(continuation, option_data->time_derivative_perturbation);
  }
  if (option_data->parameter_derivative_perturbation > 0.0) {
    parameter_continuation_set_parameter_derivative_perturbation(continuation, option_data->parameter_derivative_perturbation);
  }
  return continuation;
}

static void cmd_parameter_continuation_set_log_level (ParameterContinuation *continuation, CommandBaseOptionData *option_data)
{
  parameter_continuation_set_log_level(continuation, option_data->log_level[CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION], option_data->log_level[CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION_NEWTON], option_data->log_level[CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION_GMRES]);
}

static void cmd_parameter_continuation_print (ParameterContinuation *continuation, void *data)
{
  CmdParameterContinuation *self;
  int dim;
  double param, *eq;
  const char *number_format, *number_splitter;
  self = (CmdParameterContinuation *) data;
  number_format = command_base_number_format(self);
  number_splitter = command_base_number_splitter(self);

  dim = parameter_continuation_system_dimension(continuation);
  eq = (double *) g_malloc(sizeof(double) * dim);
  parameter_continuation_get_equilibrium(eq, &param, continuation);
  printf(number_format, param);
  printf("%s", number_splitter);
  mathfluid_utils_array_of_double_printf(dim, eq, number_format, number_splitter);
  printf("\n");
  g_free(eq);
}

static void cmd_parameter_continuation_search (gpointer ptr_self)
{
  CmdParameterContinuation *self;
  ParameterContinuation *continuation;
  gboolean success;

  self = CMD_PARAMETER_CONTINUATION(ptr_self);
  continuation = cmd_parameter_continuation_alloc(self);
  cmd_parameter_continuation_set_log_level(continuation, COMMAND_BASE(self)->option_data);

  success = parameter_continuation_step_forward_until(continuation, self->option_data->max_arc_length, cmd_parameter_continuation_print, (void *) self);
  if (!success) {
    fprintf(stderr, "Continuation has stopped in mid-course\n");
  }

  parameter_continuation_free(continuation);
}

static void cmd_parameter_continuation_execute (gpointer ptr_self)
{
  command_base_header_json_fprint(stdout, ptr_self);
  cmd_parameter_continuation_search(ptr_self);
}

static void cmd_parameter_continuation_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdParameterContinuation *self;
  self = CMD_PARAMETER_CONTINUATION(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Parameter continuation");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "parameter_key");
  json_builder_add_string_value(builder, self->option_data->parameter_key);
  json_builder_set_member_name(builder, "newton_method");
  if (self->option_data->newton_method) {
    json_builder_add_string_value(builder, self->option_data->newton_method);
  } else {
    json_builder_add_string_value(builder, "Keller");
  }
  json_builder_set_member_name(builder, "direction");
  if (self->option_data->direction == PARAMETER_CONTINUATION_DIRECTION_NEGATIVE) {
    json_builder_add_string_value(builder, "negative");
  } else {
    json_builder_add_string_value(builder, "positive");
  }
  json_builder_set_member_name(builder, "step_length_control");
  json_builder_add_string_value(builder, (self->option_data->string_step_length_adjusted ? "adjusted" : "fixed"));
  json_builder_set_member_name(builder, "step_length");
  json_builder_add_double_value(builder, self->option_data->step_length);
  json_builder_set_member_name(builder, "min_step_length");
  json_builder_add_double_value(builder, self->option_data->min_step_length);
  json_builder_set_member_name(builder, "max_arc_length");
  json_builder_add_double_value(builder, self->option_data->max_arc_length);
  json_builder_set_member_name(builder, "search_equilibrium");
  json_builder_begin_object(builder);
  json_builder_set_member_name(builder, "gmres_corrector_max_iteration");
  json_builder_add_int_value(builder, self->option_data->gmres_corrector_max_iteration);
  json_builder_set_member_name(builder, "newton_max_iteration");
  json_builder_add_int_value(builder, self->option_data->newton_max_iteration);
  json_builder_set_member_name(builder, "error_equilibria");
  json_builder_add_double_value(builder, self->option_data->error_equilibria);
  json_builder_set_member_name(builder, "time_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->time_derivative_perturbation);
  json_builder_set_member_name(builder, "parameter_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->parameter_derivative_perturbation);
  json_builder_set_member_name(builder, "space_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->space_derivative_perturbation);
  json_builder_set_member_name(builder, "newton_damping_parameter");
  json_builder_add_int_value(builder, self->option_data->newton_damping_parameter);
  json_builder_end_object(builder);
}

static void cmd_parameter_continuation_class_init (CmdParameterContinuationClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_parameter_continuation_dispose;
  gobject_class->finalize = cmd_parameter_continuation_finalize;
  gobject_class->set_property = cmd_parameter_continuation_set_property;
  gobject_class->get_property = cmd_parameter_continuation_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_parameter_continuation_command_line_arguments_define;
  command_class->process_data = cmd_parameter_continuation_process_data;
  command_class->execute = cmd_parameter_continuation_execute;

  command_base_class->header_json_save = cmd_parameter_continuation_header_json_save;
}
