#include "command_common.h"

G_DEFINE_INTERFACE (SystemDefine, system_define, 0);

static void system_define_default_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = NULL;
  iface->option_data_alloc = NULL;
  iface->option_data_set_default = NULL;
  iface->option_data_process = NULL;
  iface->option_data_free = NULL;
  iface->command_line_arguments_define = NULL;
  iface->header_json_save = NULL;
}

DynamicalSystem *system_define_dynamical_system_create (gpointer ptr_command)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->dynamical_system_create) {
    return iface->dynamical_system_create(COMMAND(ptr_command)->system_option_data);
  } else {
    return NULL;
  }
}

gpointer system_define_option_data_alloc (gpointer ptr_command, gpointer cmdinit_optional_args)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->option_data_alloc) {
    return iface->option_data_alloc(cmdinit_optional_args);
  } else {
    return NULL;
  }
}

void system_define_option_data_set_default (gpointer ptr_command, gpointer system_option_data, gpointer cmdinit_optional_args)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->option_data_set_default) {
    iface->option_data_set_default(system_option_data, cmdinit_optional_args);
  }
}

void system_define_option_data_process (gpointer ptr_command, gpointer system_option_data)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->option_data_process) {
    iface->option_data_process(system_option_data);
  }
}

void system_define_option_data_free (gpointer ptr_command, gpointer system_option_data)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->option_data_free) {
    iface->option_data_free(system_option_data);
  }
}

void system_define_command_line_arguments_define (gpointer ptr_command, CmdOpts *cmd_opts, gpointer system_option_data)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  if (iface->command_line_arguments_define) {
    iface->command_line_arguments_define(cmd_opts, system_option_data);
  }
}

gboolean system_define_command_header_json_defined_p (gpointer ptr_command)
{
  SystemDefineInterface *iface;
  iface = SYSTEM_DEFINE_GET_INTERFACE(ptr_command);
  return (iface->header_json_save ? TRUE : FALSE);
}

void system_define_command_header_json_save (JsonBuilder *builder, gpointer ptr_command, gpointer system_option_data)
{
  if (system_define_command_header_json_defined_p(ptr_command)) {
    SYSTEM_DEFINE_GET_INTERFACE(ptr_command)->header_json_save(builder, system_option_data);
  }
}
