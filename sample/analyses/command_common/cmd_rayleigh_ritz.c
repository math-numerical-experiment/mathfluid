#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdRayleighRitz, cmd_rayleigh_ritz, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_rayleigh_ritz_set_dimension (gpointer ptr_self, int dim)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  option_data->dimension = dim;
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_rayleigh_ritz_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_rayleigh_ritz_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_rayleigh_ritz_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdRayleighRitz *self = CMD_RAYLEIGH_RITZ(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, self->option_data->dimension);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_rayleigh_ritz_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_rayleigh_ritz_parent_class)->dispose(gobject);
}

static CmdRayleighRitzOptionData *cmd_rayleigh_ritz_option_data_alloc ()
{
  int i;
  CmdRayleighRitzOptionData *option_data;
  option_data = (CmdRayleighRitzOptionData *) g_malloc(sizeof(CmdRayleighRitzOptionData));
  option_data->invariant_space_type = RAYLEIGH_RITZ_INVARIANT_SPACE_UNDEFINED;
  option_data->time_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION;
  option_data->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  option_data->number_shift = 0.0;
  option_data->gmres_error_ratio = 0.0;
  option_data->lyapunov_vector_time_step = 0.0;
  option_data->dimension = 0;
  option_data->dimension_invariant_space = 0;
  option_data->iteration_of_simultaneous_iteration = 0;
  option_data->gmres_max_iteration = 0;
  option_data->lyapunov_vector_iterate1 = 0;
  option_data->lyapunov_vector_iterate2 = 0;
  option_data->string_initial_point = NULL;
  option_data->string_initial_invariant_space = NULL;
  option_data->string_backward_lyapunov = NULL;
  option_data->string_covariant_lyapunov = NULL;
  option_data->string_inverse_iteration = NULL;
  option_data->initial_point = NULL;
  option_data->initial_invariant_space = NULL;
  option_data->sort = FALSE;
  option_data->inverse_p = FALSE;
  option_data->arnoldi_method = FALSE;
  option_data->from_jacobian = FALSE;
  option_data->covariant_lyapunov_vector = FALSE;
  for (i = 0; i < N_RAYLEIGH_RITZ_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_rayleigh_ritz_option_data_free (CmdRayleighRitzOptionData *option_data)
{
  if (option_data) {
    if (option_data->initial_point) {
      g_free(option_data->initial_point);
    }
    if (option_data->initial_invariant_space) {
      g_free(option_data->initial_invariant_space);
    }
    if (option_data->string_initial_point) {
      g_free(option_data->string_initial_point);
    }
    if (option_data->string_initial_invariant_space) {
      g_free(option_data->string_initial_invariant_space);
    }
    if (option_data->string_backward_lyapunov) {
      g_free(option_data->string_backward_lyapunov);
    }
    if (option_data->string_covariant_lyapunov) {
      g_free(option_data->string_covariant_lyapunov);
    }
    if (option_data->string_inverse_iteration) {
      g_free(option_data->string_inverse_iteration);
    }
    g_free(option_data);
  }
}

static void cmd_rayleigh_ritz_finalize (GObject *gobject)
{
  CmdRayleighRitz *self;
  self = CMD_RAYLEIGH_RITZ(gobject);
  cmd_rayleigh_ritz_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_rayleigh_ritz_parent_class)->finalize(gobject);
}

static void cmd_rayleigh_ritz_init (CmdRayleighRitz *self)
{
  self->option_data = cmd_rayleigh_ritz_option_data_alloc();
}

static void cmd_rayleigh_ritz_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT] || option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "point", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_point, "Initial point specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_SIMULTANEOUS_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "simultaneous-iteration", 0, 0, G_OPTION_ARG_INT, &option_data->iteration_of_simultaneous_iteration, "Iteration number of simultaneous iteration to calculate approximate invariant space", "NUM");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_ARNOLDI_METHOD]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "arnoldi-method", 0, 0, G_OPTION_ARG_NONE, &option_data->arnoldi_method, "Use Arnoldi method to calculate approximate invariant space", NULL);
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "backward-lyapunov", 0, 0, G_OPTION_ARG_STRING, &option_data->string_backward_lyapunov, "Use backward Lyapunov vector as an invariant space: time step, iteration for forward process", "TIME_STEP,ITER");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "inverse", 0, 0, G_OPTION_ARG_STRING, &option_data->string_inverse_iteration, "Use inverse iteration: number of shift, maximum iteration of GMRES, ratio of error of GMRES", "SHIFT,MAX_ITER,ERR");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "covariant-lyapunov", 0, 0, G_OPTION_ARG_STRING, &option_data->string_covariant_lyapunov, "Calculate covariant Lyapunov vectors: time step, iteration for forward Lyapunov vector, iteration of backward process", "TIME_STEP,ITER1,ITER2");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_FROM_JACOBIAN]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "from-jacobian", 0, 0, G_OPTION_ARG_NONE, &option_data->from_jacobian, "Calculate eigenvectors from Jacobian matrix for low dimensional case", NULL);
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_DIMENSION_INVARIANT_SPACE]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "dimension-invariant-space", 0, 0, G_OPTION_ARG_INT, &option_data->dimension_invariant_space, "Dimension of invariant space; default value is same as system dimension", "NUM");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_SORT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "sort", 0, 0, G_OPTION_ARG_NONE, &option_data->sort, "Sort by absolute values of eigenvalues", NULL);
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "initial-invariant-space", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_invariant_space, "Initial invariant space whose data size depends on type of calculation method", "NUMS");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_TIME_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_derivative_perturbation, "Time evolution to calculate numerical derivative", "NUM");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_SPACE_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "space-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->space_derivative_perturbation, "Perturbation to calculate Jacobian free product", "NUM");
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_VERBOSE]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_VERBOSE] = TRUE;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_LOG_LEVEL]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] = TRUE;
    COMMAND_BASE(ptr_self)->option_data->number_log_level = CMD_RAYLEIGH_RITZ_N_LOG_LEVELS;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_rayleigh_ritz_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void rayleigh_ritz_initial_point_alloc (CmdRayleighRitzOptionData *option_data)
{
  int i;
  if (option_data->initial_point) {
    g_free(option_data->initial_point);
  }
  option_data->initial_point = (double *) g_malloc(sizeof(double) * option_data->dimension);
  for (i = 0; i < option_data->dimension; i++) {
    option_data->initial_point[i] = 0.0;
  }
}

static void cmd_rayleigh_ritz_process_data (gpointer ptr_self)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_point);
  if (option_data->iteration_of_simultaneous_iteration > 0) {
    option_data->invariant_space_type = RAYLEIGH_RITZ_INVARIANT_SPACE_SIMULTANEOUS_ITERATION;
  } else if (option_data->arnoldi_method) {
    option_data->invariant_space_type = RAYLEIGH_RITZ_INVARIANT_SPACE_ARNOLDI_METHOD;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT]) {
    rayleigh_ritz_initial_point_alloc(option_data);
    command_utils_point_data_load(option_data->initial_point, option_data->string_initial_point, option_data->dimension);
  } else if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT_WITH_DIM]) {
    if (option_data->string_initial_point) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_point);
      cmd_rayleigh_ritz_set_dimension(ptr_self, dim);
      rayleigh_ritz_initial_point_alloc(option_data);
      memcpy(option_data->initial_point, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_point) {
    g_free(option_data->string_initial_point);
    option_data->string_initial_point = NULL;
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV] && option_data->string_backward_lyapunov) {
    int num;
    gchar **num_strings;
    num_strings = mathfluid_utils_split_string_numbers(&num, option_data->string_backward_lyapunov);
    if (num != 2) {
      fprintf(stderr, "Invalid argument of --backward-lyapunov\n");
      exit(1);
    }
    option_data->invariant_space_type = RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV;
    option_data->lyapunov_vector_time_step = mathfluid_utils_string_to_double(num_strings[0]);
    option_data->lyapunov_vector_iterate1 = mathfluid_utils_string_to_long(num_strings[1]);
    g_strfreev(num_strings);
  } else if (option_data->active_options[RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV] && option_data->string_covariant_lyapunov) {
    int num;
    gchar **num_strings;
    num_strings = mathfluid_utils_split_string_numbers(&num, option_data->string_covariant_lyapunov);
    if (num != 3) {
      fprintf(stderr, "Invalid argument of --covariant-lyapunov\n");
      exit(1);
    }
    option_data->covariant_lyapunov_vector = TRUE;
    option_data->lyapunov_vector_time_step = mathfluid_utils_string_to_double(num_strings[0]);
    option_data->lyapunov_vector_iterate1 = mathfluid_utils_string_to_long(num_strings[1]);
    option_data->lyapunov_vector_iterate2 = mathfluid_utils_string_to_double(num_strings[2]);
    g_strfreev(num_strings);
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION] && option_data->string_inverse_iteration) {
    int num;
    gchar **num_strings;
    num_strings = mathfluid_utils_split_string_numbers(&num, option_data->string_inverse_iteration);
    if (num != 3) {
      fprintf(stderr, "Invalid argument of --inverse\n");
      exit(1);
    }
    option_data->number_shift = mathfluid_utils_string_to_double(num_strings[0]);
    option_data->gmres_max_iteration = mathfluid_utils_string_to_long(num_strings[1]);
    option_data->gmres_error_ratio = mathfluid_utils_string_to_double(num_strings[2]);
    option_data->inverse_p = TRUE;
    if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV || option_data->covariant_lyapunov_vector || option_data->from_jacobian) {
      fprintf(stderr, "The option --invariant is invalid when we set --backward-lyapunov, --covariant-lyapunov, or --from-jacobian\n");
      exit(1);
    }
  }
  if (option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE]) {
    if (option_data->string_initial_invariant_space) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_invariant_space);
      if ((option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_SIMULTANEOUS_ITERATION) || (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV)) {
        if (option_data->dimension_invariant_space <= 0) {
          div_t d;
          d = div(option_data->dimension, dim);
          if (d.rem == 0) {
            option_data->dimension_invariant_space = d.quot;
          } else {
            fprintf(stderr, "Data size of inital invariant space must be divided by (system dimension)\n. Actual size is %d\n", dim);
            exit(1);
          }
        } else {
          if (dim != (option_data->dimension * option_data->dimension_invariant_space)) {
            fprintf(stderr, "Data size of inital invariant space must be (system dimension) * (invariant space dimension)\n. Actual size is %d\n", dim);
            exit(1);
          }
        }
      } else if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_ARNOLDI_METHOD) {
        if (option_data->dimension_invariant_space > 0) {
          if (dim != option_data->dimension) {
            fprintf(stderr, "Data size of inital invariant space must be (system dimension)\n. Actual size is %d\n", dim);
          }
        }
      }
      option_data->initial_invariant_space = (double *) g_malloc(sizeof(double) * dim);
      memcpy(option_data->initial_invariant_space, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_invariant_space) {
    g_free(option_data->string_initial_invariant_space);
    option_data->string_initial_invariant_space = NULL;
  }
  if (option_data->dimension_invariant_space <= 0) {
    option_data->dimension_invariant_space = option_data->dimension;
  }
  COMMAND_CLASS(cmd_rayleigh_ritz_parent_class)->process_data(ptr_self);
}

static void simultaneous_iteration_invariant_space_set (RayleighRitz *rayleigh_ritz, CmdRayleighRitz *self, CmdRayleighRitzOptionData *option_data, MatrixVectorProduct *matrix_vector_product)
{
  double *invariant_space;
  SimultaneousIteration *simultaneous_iteration;
  simultaneous_iteration = simultaneous_iteration_alloc(option_data->dimension, option_data->dimension_invariant_space, matrix_vector_product);
  if (option_data->initial_invariant_space) {
    simultaneous_iteration_set_qr(simultaneous_iteration, option_data->initial_invariant_space);
  } else {
    simultaneous_iteration_set_qr_randomly(simultaneous_iteration);
  }
  simultaneous_iteration_iterate_successively(simultaneous_iteration, option_data->iteration_of_simultaneous_iteration);
  invariant_space = gsl_matrix_alloc_copy_elements(simultaneous_iteration->q);
  rayleigh_ritz_set_basis(rayleigh_ritz, option_data->dimension_invariant_space, invariant_space);
  simultaneous_iteration_free(simultaneous_iteration);
  g_free(invariant_space);
}

static void arnoldi_method_invariant_space_set (RayleighRitz *rayleigh_ritz, CmdRayleighRitz *self, CmdRayleighRitzOptionData *option_data, MatrixVectorProduct *matrix_vector_product)
{
  ArnoldiMethod *arnoldi_method;
  double *hessenberg, *invariant_space;
  int row_number, column_number;
  arnoldi_method = arnoldi_method_alloc(option_data->dimension, option_data->dimension_invariant_space, matrix_vector_product);
  if (option_data->initial_invariant_space) {
    arnoldi_method_set(arnoldi_method, option_data->initial_invariant_space);
  } else {
    arnoldi_method_set_randomly(arnoldi_method);
  }
  hessenberg = arnoldi_method_iterate_successively(&row_number, &column_number, arnoldi_method);
  invariant_space = arnoldi_method_basis_matrix(arnoldi_method);
  rayleigh_ritz_set_basis(rayleigh_ritz, column_number, invariant_space);
  rayleigh_ritz_set_matrix(rayleigh_ritz, hessenberg);
  arnoldi_method_free(arnoldi_method);
  g_free(hessenberg);
  g_free(invariant_space);
}

static void backward_lyapunov_vector_invariant_space_set (RayleighRitz *rayleigh_ritz, CmdRayleighRitz *self, CmdRayleighRitzOptionData *option_data)
{
  LyapunovAnalysis *lyapunov_analysis;
  LyapunovVector *lyapunov_vector;
  double *invariant_space;
  lyapunov_analysis = lyapunov_analysis_alloc(COMMAND(self)->dynamical_system, option_data->dimension_invariant_space, option_data->lyapunov_vector_time_step);
  if (option_data->initial_invariant_space) {
    fprintf(stderr, "Not implemented\n");
    exit(1);
  }
  lyapunov_vector = lyapunov_analysis_backward_lyapunov_vectors_of_equilibrium(lyapunov_analysis, option_data->lyapunov_vector_iterate1, option_data->initial_point);
  invariant_space = lyapunov_vector_double_array_row_major_order(lyapunov_vector);
  rayleigh_ritz_set_basis(rayleigh_ritz, option_data->dimension_invariant_space, invariant_space);
  g_free(invariant_space);
  lyapunov_analysis_free(lyapunov_analysis);
  lyapunov_vector_free(lyapunov_vector);
}

/**
 * Print eigenvalues and eigenvectors.
 * Each line includes a pair of eigenvalue and eigenvector whose values are considered as complex numbers.
 */
static void cmd_rayleigh_ritz_print_eigen (CmdRayleighRitz *self, double complex *eigenvalue, double complex *eigenvector, int number_eigenvalues)
{
  int i;
  for (i = 0; i < number_eigenvalues; i++) {
    mathfluid_utils_complex_double_printf(eigenvalue[i], command_base_number_format(self), command_base_number_splitter(self));
    printf("%s", command_base_number_splitter(self));
    mathfluid_utils_array_of_complex_double_printf(self->option_data->dimension, (eigenvector + i * self->option_data->dimension), command_base_number_format(self), command_base_number_splitter(self));
    printf("\n");
  }
}

static void cmd_rayleigh_ritz_calculate_print (CmdRayleighRitz *self, RayleighRitz *rayleigh_ritz)
{
  double complex *eigenvalue, *eigenvector;
  int number_eigenvalues;
  number_eigenvalues = rayleigh_ritz_eigen(&eigenvalue, &eigenvector, rayleigh_ritz);
  if (self->option_data->inverse_p) {
    rayleigh_ritz_convert_inverse_eigenvalues(eigenvalue, number_eigenvalues, self->option_data->number_shift);
  }
  if (self->option_data->sort) {
    rayleigh_ritz_eigen_sort_by_absolute_value(eigenvalue, eigenvector, number_eigenvalues, self->option_data->dimension);
  }
  cmd_rayleigh_ritz_print_eigen(self, eigenvalue, eigenvector, number_eigenvalues);
  g_free(eigenvalue);
  g_free(eigenvector);
}

static MatrixVectorProduct *cmd_rayleigh_ritz_matrix_vector_product_alloc (CmdRayleighRitz *self, CmdRayleighRitzOptionData *option_data)
{
  MatrixVectorProduct *matrix_vector_product;
  DerivativeOfSystem *derivative;
  matrix_vector_product = jacobian_of_vector_field_product_alloc(COMMAND(self)->dynamical_system, option_data->initial_point);
  derivative = jacobian_of_vector_field_product_derivative_of_system_ptr(matrix_vector_product);
  derivative_of_system_set_time_derivative_perturbation(derivative, option_data->time_derivative_perturbation);
  derivative_of_system_set_space_derivative_perturbation(derivative, option_data->space_derivative_perturbation);
  if (option_data->inverse_p) {
    return inverse_matrix_vector_product_alloc(option_data->dimension, option_data->gmres_error_ratio, option_data->gmres_max_iteration, matrix_vector_product, option_data->number_shift);
  } else {
    return matrix_vector_product;
  }
}

static void cmd_rayleigh_ritz_calculate (gpointer ptr_self)
{
  MatrixVectorProduct *matrix_vector_product;
  RayleighRitz *rayleigh_ritz;
  CmdRayleighRitz *self;
  CmdRayleighRitzOptionData *option_data;
  self = CMD_RAYLEIGH_RITZ(ptr_self);
  option_data = self->option_data;
  if (!option_data->initial_point) {
    fprintf(stderr, "Point of orbit is not set\n");
    exit(1);
  }
  if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_UNDEFINED) {
    fprintf(stderr, "Type of invariant space is not defined\n");
    exit(1);
  }
  matrix_vector_product = cmd_rayleigh_ritz_matrix_vector_product_alloc(self, option_data);
  rayleigh_ritz = rayleigh_ritz_alloc(option_data->dimension, matrix_vector_product_duplicate(matrix_vector_product));
  if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_SIMULTANEOUS_ITERATION) {
    simultaneous_iteration_invariant_space_set(rayleigh_ritz, self, option_data, matrix_vector_product_duplicate(matrix_vector_product));
  } else if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_ARNOLDI_METHOD) {
    arnoldi_method_invariant_space_set(rayleigh_ritz, self, option_data, matrix_vector_product_duplicate(matrix_vector_product));
  } else if (option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV) {
    backward_lyapunov_vector_invariant_space_set(rayleigh_ritz, self, option_data);
  } else {
    fprintf(stderr, "Calculation of invariant space is not implemented\n");
    exit(1);
  }
  cmd_rayleigh_ritz_calculate_print(self, rayleigh_ritz);
  matrix_vector_product_free(matrix_vector_product);
  rayleigh_ritz_free(rayleigh_ritz);
}

static void cmd_rayleigh_ritz_eigen_from_jacobian (gpointer ptr_self)
{
  CmdRayleighRitz *self;
  DynamicalSystem *dynamical_system;
  double complex *eigenvalue, *eigenvector;
  int number_eigenvalues;
  self = CMD_RAYLEIGH_RITZ(ptr_self);
  dynamical_system = COMMAND(self)->dynamical_system;
  if (!self->option_data->initial_point) {
    fprintf(stderr, "Point of orbit is not set\n");
    exit(1);
  }
  number_eigenvalues = dynamical_system_eigen_of_linearized_system(&eigenvalue, &eigenvector, dynamical_system, self->option_data->initial_point);
  if (number_eigenvalues < 0) {
    fprintf(stderr, "Jacobian is not defined\n");
    exit(1);
  } else if (number_eigenvalues == 0) {
    fprintf(stderr, "Can not calculate eigenvalues and eigenvectors\n");
    exit(1);
  }
  cmd_rayleigh_ritz_print_eigen(self, eigenvalue, eigenvector, number_eigenvalues);
  g_free(eigenvalue);
  g_free(eigenvector);
}

static void cmd_rayleigh_ritz_covariant_lyapunov_vector (gpointer ptr_self)
{
  CmdRayleighRitz *self;
  double complex *eigenvalue, *eigenvector;
  int i, j;
  CmdRayleighRitzOptionData *option_data;
  LyapunovAnalysis *lyapunov_analysis;
  LyapunovVector *lyapunov_vector;
  double *lyapunov_exponents;
  self = CMD_RAYLEIGH_RITZ(ptr_self);
  option_data = self->option_data;
  lyapunov_analysis = lyapunov_analysis_alloc(COMMAND(self)->dynamical_system, option_data->dimension_invariant_space, option_data->lyapunov_vector_time_step);
  if (option_data->initial_invariant_space) {
    fprintf(stderr, "Not implemented\n");
    exit(1);
  }
  lyapunov_vector = lyapunov_analysis_covariant_lyapunov_vectors_of_equilibrium(&lyapunov_exponents, lyapunov_analysis, option_data->lyapunov_vector_iterate1, option_data->lyapunov_vector_iterate2, option_data->initial_point);
  eigenvalue = (double complex *) g_malloc(sizeof(double complex) * option_data->dimension_invariant_space);
  eigenvector = (double complex *) g_malloc(sizeof(double complex) * option_data->dimension * option_data->dimension_invariant_space);
  for (i = 0; i < option_data->dimension_invariant_space; i++) {
    eigenvalue[i] = lyapunov_exponents[i];
    for (j = 0; j < option_data->dimension; j++) {
      eigenvector[i * option_data->dimension + j] = lyapunov_vector->vector[i][j];
    }
  }
  cmd_rayleigh_ritz_print_eigen(self, eigenvalue, eigenvector, option_data->dimension_invariant_space);
  g_free(eigenvalue);
  g_free(eigenvector);
  g_free(lyapunov_exponents);
  lyapunov_analysis_free(lyapunov_analysis);
  lyapunov_vector_free(lyapunov_vector);
}

static void cmd_rayleigh_ritz_execute (gpointer ptr_self)
{
  CmdRayleighRitz *self;
  command_base_header_json_fprint(stdout, ptr_self);
  self = CMD_RAYLEIGH_RITZ(ptr_self);
  if (self->option_data->from_jacobian) {
    cmd_rayleigh_ritz_eigen_from_jacobian(ptr_self);
  } else if (self->option_data->covariant_lyapunov_vector){
    cmd_rayleigh_ritz_covariant_lyapunov_vector(ptr_self);
  } else {
    cmd_rayleigh_ritz_calculate(ptr_self);
  }
}

static void cmd_rayleigh_ritz_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdRayleighRitz *self;
  self = CMD_RAYLEIGH_RITZ(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Rayleigh-Ritz method");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "dimension_invariant_space");
  json_builder_add_int_value(builder, self->option_data->dimension_invariant_space);
  json_builder_set_member_name(builder, "sort");
  json_builder_add_boolean_value(builder, self->option_data->sort);
  json_builder_set_member_name(builder, "time_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->time_derivative_perturbation);
  json_builder_set_member_name(builder, "space_derivative_perturbation");
  json_builder_add_double_value(builder, self->option_data->space_derivative_perturbation);
  json_builder_set_member_name(builder, "invariant_space_type");
  if (self->option_data->from_jacobian) {
    json_builder_add_string_value(builder, "none");
  } else if (self->option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_SIMULTANEOUS_ITERATION) {
    json_builder_add_string_value(builder, "simultaneous iteration");
  } else if (self->option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_ARNOLDI_METHOD) {
    json_builder_add_string_value(builder, "Arnoldi method");
  } else if (self->option_data->invariant_space_type == RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV) {
    json_builder_add_string_value(builder, "Lyapunov vector");
  } else {
    json_builder_add_string_value(builder, "undefined");
  }
  if (!self->option_data->from_jacobian && !self->option_data->covariant_lyapunov_vector
      && (self->option_data->invariant_space_type != RAYLEIGH_RITZ_INVARIANT_SPACE_BACKWARD_LYAPUNOV)) {
    json_builder_set_member_name(builder, "inverse");
    json_builder_add_boolean_value(builder, self->option_data->inverse_p);
    if (self->option_data->inverse_p) {
      json_builder_set_member_name(builder, "inverse-arguments");
      json_builder_begin_object(builder);
      json_builder_set_member_name(builder, "number_shift");
      json_builder_add_double_value(builder, self->option_data->number_shift);
      json_builder_set_member_name(builder, "gmres_max_iteration");
      json_builder_add_int_value(builder, self->option_data->gmres_max_iteration);
      json_builder_set_member_name(builder, "gmres_error_ratio");
      json_builder_add_double_value(builder, self->option_data->gmres_error_ratio);
      json_builder_end_object(builder);
    }
  }
}

static void cmd_rayleigh_ritz_class_init (CmdRayleighRitzClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_rayleigh_ritz_dispose;
  gobject_class->finalize = cmd_rayleigh_ritz_finalize;
  gobject_class->set_property = cmd_rayleigh_ritz_set_property;
  gobject_class->get_property = cmd_rayleigh_ritz_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_rayleigh_ritz_command_line_arguments_define;
  command_class->process_data = cmd_rayleigh_ritz_process_data;
  command_class->execute = cmd_rayleigh_ritz_execute;

  command_base_class->header_json_save = cmd_rayleigh_ritz_header_json_save;
}
