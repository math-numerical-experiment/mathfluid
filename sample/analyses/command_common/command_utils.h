#ifndef _COMMAND_UTILS_H_
#define _COMMAND_UTILS_H_

MathFluidLogLevel command_utils_parse_log_level_string (const char *string_log_level);
void command_utils_parse_multiple_log_level_string (MathFluidLogLevel *level, int n, const char *string_log_level);
GPtrArray *command_utils_search_range_alloc (int dimension);
void command_utils_search_range_option_parse (double *nums, const char *range_string);
void command_utils_search_range_json_output (JsonBuilder *builder, GPtrArray *search_range);
void command_utils_search_range_json_output2 (JsonBuilder *builder, GPtrArray *search_range, int index_min, int index_max);

typedef struct {
  GPtrArray *region_data;
  double *pt;
  int ind;
  gboolean finish_creating_pt;
} CommandUtilsPointsInRectangle;

CommandUtilsPointsInRectangle *command_utils_points_in_rectangle_alloc (GPtrArray *region_data);
void command_utils_points_in_rectangle_free (CommandUtilsPointsInRectangle *points_in_rectangle);
#define command_utils_points_in_rectangle_dimension(points_in_rectangle) (points_in_rectangle->region_data->len)
double *command_utils_points_in_rectangle_next (CommandUtilsPointsInRectangle *points_in_rectangle);
double *command_utils_points_in_rectangle_next_allocated (CommandUtilsPointsInRectangle *points_in_rectangle);
void command_utils_load_file_data (char **string_input);
void command_utils_load_file_data_without_header (char **string_input);
void command_utils_point_data_load (double *nums_output, char *string_input, int dim);
JsonParser *command_utils_get_json_header_parser (const char *path);

#endif /* _COMMAND_UTILS_H_ */
