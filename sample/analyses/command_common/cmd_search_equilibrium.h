#ifndef _CMD_SEARCH_EQUILIBRIUM_H_
#define _CMD_SEARCH_EQUILIBRIUM_H_

typedef struct _CmdSearchEquilibriumClass CmdSearchEquilibriumClass;
typedef struct _CmdSearchEquilibrium CmdSearchEquilibrium;
typedef struct _CmdSearchEquilibriumOptionData CmdSearchEquilibriumOptionData;

typedef enum {
  SEARCH_EQUILIBRIUM_OPTION_VERBOSE,
  SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION,
  SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA,
  SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA,
  SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION,
  SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT,
  SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM,
  SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER,
  SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION,
  SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION,
  SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT,
  SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER,
  SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL,
  SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS,
  N_SEARCH_EQUILIBRIUM_OPTIONS
} CmdSearchEquilibriumOptionType;

enum {
  CMD_SEARCH_EQUILIBRIUM_LOG_LEVEL_NEWTON,
  CMD_SEARCH_EQUILIBRIUM_LOG_LEVEL_GMRES,
  CMD_SEARCH_EQUILIBRIUM_N_LOG_LEVELS,
};

struct _CmdSearchEquilibriumOptionData {
  gboolean remove_duplication;
  gboolean sort_equilibria;
  double error_equilibria;
  double time_derivative_perturbation;
  double space_derivative_perturbation;
  int dimension;
  int max_iteration;
  int damping_parameter;
  char *string_initial_point;
  GPtrArray *search_range;
  double *initial_point;
  gboolean active_options[N_SEARCH_EQUILIBRIUM_OPTIONS];
};

struct _CmdSearchEquilibriumClass {
  CommandBaseClass parent;
};

struct _CmdSearchEquilibrium {
  CommandBase parent;

  CmdSearchEquilibriumOptionData *option_data;
};

#define TYPE_CMD_SEARCH_EQUILIBRIUM (cmd_search_equilibrium_get_type ())
#define CMD_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_SEARCH_EQUILIBRIUM, CmdSearchEquilibrium))
#define CMD_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_SEARCH_EQUILIBRIUM, CmdSearchEquilibriumClass))
#define IS_CMD_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_SEARCH_EQUILIBRIUM))
#define IS_CMD_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_SEARCH_EQUILIBRIUM))
#define CMD_SEARCH_EQUILIBRIUM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_SEARCH_EQUILIBRIUM, CmdSearchEquilibriumClass))

GType cmd_search_equilibrium_get_type ();
#define cmd_search_equilibrium_dimension(self) (CMD_SEARCH_EQUILIBRIUM(self)->option_data->dimension)
void cmd_search_equilibrium_option_range_set (CmdSearchEquilibrium *self, int ind, const char *range_string);
void cmd_search_equilibrium_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_SEARCH_EQUILIBRIUM_H_ */
