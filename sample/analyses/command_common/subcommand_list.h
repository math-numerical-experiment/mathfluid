#ifndef _SUBCOMMAND_LIST_H_
#define _SUBCOMMAND_LIST_H_

typedef struct {
  gboolean initialized;
  GPtrArray *function_initialize;
  GPtrArray *function_finalize;
  GHashTable *table_subcommand;
} SubcommandList;

SubcommandList *subcommand_list_alloc (void);
void subcommand_list_free (SubcommandList *subcommand_list);
gchar *subcommand_list_get_basename (const char *path);
void subcommand_list_register_function_initialize (SubcommandList *subcommand_list, void (* func)());
void subcommand_list_register_function_finalize (SubcommandList *subcommand_list, void (* func)());
void subcommand_list_register_subcommand (SubcommandList *subcommand_list, const char *program_basename, const char *subcommand_name, int (* command_execute)(int argc, char **argv));
int subcommand_list_execute_command (SubcommandList *subcommand_list, int argc, char **argv);
void subcommand_list_global_register_subcommand (const char *program_basename, const char *subcommand_name, int (* command_execute)(int argc, char **argv));
int subcommand_list_global_execute_command (int argc, char **argv);

#endif /* _SUBCOMMAND_LIST_H_ */
