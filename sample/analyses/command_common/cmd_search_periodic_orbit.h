#ifndef _CMD_SEARCH_PERIODIC_ORBIT_H_
#define _CMD_SEARCH_PERIODIC_ORBIT_H_

typedef struct _CmdSearchPeriodicOrbitClass CmdSearchPeriodicOrbitClass;
typedef struct _CmdSearchPeriodicOrbit CmdSearchPeriodicOrbit;
typedef struct _CmdSearchPeriodicOrbitOptionData CmdSearchPeriodicOrbitOptionData;

typedef enum {
  SEARCH_PERIODIC_ORBIT_OPTION_VERBOSE,
  /* SEARCH_PERIODIC_ORBIT_OPTION_REMOVE_DUPLICATION, */
  SEARCH_PERIODIC_ORBIT_OPTION_SORT_PERIODIC_ORBITS,
  SEARCH_PERIODIC_ORBIT_OPTION_NEWTON_ERROR_PERIODIC_ORBIT,
  SEARCH_PERIODIC_ORBIT_OPTION_PRACTICAL_ERROR_PERIODIC_ORBIT,
  SEARCH_PERIODIC_ORBIT_OPTION_MAX_ITERATION,
  SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT,
  SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM,
  SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_PERIOD,
  SEARCH_PERIODIC_ORBIT_OPTION_PERIOD_RANGE,
  SEARCH_PERIODIC_ORBIT_OPTION_DAMPING_PARAMETER,
  SEARCH_PERIODIC_ORBIT_OPTION_TIME_DERIVATIVE_PERTURBATION,
  SEARCH_PERIODIC_ORBIT_OPTION_SPACE_DERIVATIVE_PERTURBATION,
  SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_FORMAT,
  SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_SPLITTER,
  SEARCH_PERIODIC_ORBIT_OPTION_LOG_LEVEL,
  SEARCH_PERIODIC_ORBIT_OPTION_LOAD_ARGUMENTS,
  N_SEARCH_PERIODIC_ORBIT_OPTIONS
} CmdSearchPeriodicOrbitOptionType;

enum {
  CMD_SEARCH_PERIODIC_ORBIT_LOG_LEVEL_NEWTON,
  CMD_SEARCH_PERIODIC_ORBIT_LOG_LEVEL_GMRES,
  CMD_SEARCH_PERIODIC_ORBIT_N_LOG_LEVELS,
};

struct _CmdSearchPeriodicOrbitOptionData {
  /* gboolean remove_duplication; */
  gboolean sort_periodic_orbits;
  double newton_error;
  double practical_error;
  double time_derivative_perturbation;
  double space_derivative_perturbation;
  double initial_period;
  int dimension;
  int max_iteration;
  int damping_parameter;
  char *string_initial_point;
  char *string_period_range;
  GPtrArray *search_range;
  double *initial_point;
  double *period_range;
  gboolean active_options[N_SEARCH_PERIODIC_ORBIT_OPTIONS];
};

struct _CmdSearchPeriodicOrbitClass {
  CommandBaseClass parent;
};

struct _CmdSearchPeriodicOrbit {
  CommandBase parent;

  CmdSearchPeriodicOrbitOptionData *option_data;
};

#define TYPE_CMD_SEARCH_PERIODIC_ORBIT (cmd_search_periodic_orbit_get_type ())
#define CMD_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_SEARCH_PERIODIC_ORBIT, CmdSearchPeriodicOrbit))
#define CMD_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_SEARCH_PERIODIC_ORBIT, CmdSearchPeriodicOrbitClass))
#define IS_CMD_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_SEARCH_PERIODIC_ORBIT))
#define IS_CMD_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_SEARCH_PERIODIC_ORBIT))
#define CMD_SEARCH_PERIODIC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_SEARCH_PERIODIC_ORBIT, CmdSearchPeriodicOrbitClass))

GType cmd_search_periodic_orbit_get_type ();
#define cmd_search_periodic_orbit_dimension(self) (CMD_SEARCH_PERIODIC_ORBIT(self)->option_data->dimension)
void cmd_search_periodic_orbit_option_range_set (CmdSearchPeriodicOrbit *self, int ind, const char *range_string);
void cmd_search_periodic_orbit_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_SEARCH_PERIODIC_ORBIT_H_ */
