#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = NULL;
  iface->option_data_alloc = NULL;
  iface->option_data_set_default = NULL;
  iface->option_data_free = NULL;
  iface->command_line_arguments_define = NULL;
}

G_DEFINE_TYPE_WITH_CODE(Command, command, G_TYPE_OBJECT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

static void command_dispose (GObject *gobject)
{
  Command *self = COMMAND(gobject);
  g_object_unref(self->cmd_opts);
  G_OBJECT_CLASS(command_parent_class)->dispose(gobject);
}

static void command_system_option_data_free (Command *self)
{
  if (self->system_option_data) {
    system_define_option_data_free(self, self->system_option_data);
  }
  self->system_option_data = NULL;
}

static void command_finalize (GObject *gobject)
{
  Command *self = COMMAND(gobject);
  if (self->dynamical_system) {
    dynamical_system_free(self->dynamical_system);
  }
  command_system_option_data_free(self);
  G_OBJECT_CLASS(command_parent_class)->finalize(gobject);
}

static void command_init (Command *self)
{
  self->cmd_opts = g_object_new(TYPE_CMD_OPTS, NULL);
  self->dynamical_system = NULL;
  self->system_option_data = NULL;
}

static void command_class_init (CommandClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  gobject_class->dispose = command_dispose;
  gobject_class->finalize = command_finalize;

  klass->set_default_value = NULL;
  klass->process_remained_arguments = NULL;
  klass->command_line_arguments_define = NULL;
  klass->process_data = NULL;
  klass->execute = NULL;
}

CmdOpts *command_cmd_opts (Command *self)
{
  return self->cmd_opts;
}

void command_system_command_line_arguments_define (gpointer ptr_self)
{
  Command *self;
  self = COMMAND(ptr_self);
  system_define_command_line_arguments_define(self, self->cmd_opts, self->system_option_data);
}

void command_application_command_line_arguments_define (gpointer ptr_self)
{
  Command *self;
  self = COMMAND(ptr_self);
  if (COMMAND_GET_CLASS(self)->command_line_arguments_define) {
    COMMAND_GET_CLASS(self)->command_line_arguments_define(self->cmd_opts, ptr_self);
  }
}

gpointer command_system_option_data (gpointer ptr_self)
{
  return COMMAND(ptr_self)->system_option_data;
}

static void command_application_set_default_value (gpointer ptr_self, gpointer cmdinit_optional_args)
{
  if (COMMAND_GET_CLASS(ptr_self)->set_default_value) {
    COMMAND_GET_CLASS(ptr_self)->set_default_value(ptr_self, cmdinit_optional_args);
  }
}

/**
 * Note that properties are set after allocation of option data.
 * Therefore, the option of property G_PARAM_CONSTRUCT_ONLY can not be used.
 */
gpointer command_new (GType object_type, gpointer cmdinit_optional_args, const gchar *first_property_name, ...)
{
  gpointer ptr_self;
  va_list var_args;
  Command *self;
  /* It is better implementation that we test flags of properties and
     set values of properties so that flags include G_PARAM_CONSTRUCT_ONLY
     at instantiation. */
  ptr_self = g_object_new(object_type, NULL);
  self = COMMAND(ptr_self);
  self->system_option_data = system_define_option_data_alloc(self, cmdinit_optional_args);
  if (first_property_name) {
    va_start(var_args, first_property_name);
    g_object_set_valist(ptr_self, first_property_name, var_args);
    va_end(var_args);
  }
  if (self->system_option_data) {
    system_define_option_data_set_default(self, self->system_option_data, cmdinit_optional_args);
  }
  command_application_set_default_value(self, cmdinit_optional_args);
  return ptr_self;
}

static void command_process_data (gpointer ptr_self)
{
  system_define_option_data_process(ptr_self, COMMAND(ptr_self)->system_option_data);
  if (COMMAND_GET_CLASS(ptr_self)->process_data) {
    COMMAND_GET_CLASS(ptr_self)->process_data(ptr_self);
  }
}

static void command_process_remained_arguments (gpointer ptr_self, MathFluidARGV *mathfluid_argv)
{
  if (COMMAND_GET_CLASS(ptr_self)->process_remained_arguments) {
    COMMAND_GET_CLASS(ptr_self)->process_remained_arguments(ptr_self, mathfluid_argv);
  }
}

void command_process_command_line_arguments (gpointer ptr_self, MathFluidARGV *mathfluid_argv)
{
  command_system_command_line_arguments_define(ptr_self);
  command_application_command_line_arguments_define(ptr_self);
  cmd_opts_arguments_parse(COMMAND(ptr_self)->cmd_opts, mathfluid_argv);
  command_process_remained_arguments(ptr_self, mathfluid_argv);
  command_process_data(ptr_self);
}

void command_dynamical_system_create (gpointer ptr_self)
{
  Command *command = COMMAND(ptr_self);
  if (command->dynamical_system) {
    dynamical_system_free(command->dynamical_system);
  }
  command->dynamical_system = system_define_dynamical_system_create(ptr_self);
  if (!command->dynamical_system) {
    fprintf(stderr, "No definition of dynamical system\n");
    abort();
  }
}

void command_short_description_set (gpointer ptr_self, const char *short_description)
{
  cmd_opts_short_description_set(COMMAND(ptr_self)->cmd_opts, short_description);
}

void command_summary_set (gpointer ptr_self, const char *summary)
{
  cmd_opts_summary_set(COMMAND(ptr_self)->cmd_opts, summary);
}

void command_description_set (gpointer ptr_self, const char *description)
{
  cmd_opts_description_set(COMMAND(ptr_self)->cmd_opts, description);
}

void command_execute (gpointer ptr_self)
{
  if (COMMAND_GET_CLASS(ptr_self)->execute) {
    COMMAND_GET_CLASS(ptr_self)->execute(ptr_self);
  }
}
