#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdSearchPeriodicOrbit, cmd_search_periodic_orbit, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_search_periodic_orbit_option_range_set (CmdSearchPeriodicOrbit *self, int ind, const char *range_string)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = self->option_data;
  if (!option_data->search_range) {
    option_data->search_range = command_utils_search_range_alloc(option_data->dimension + 1);
  }
  command_utils_search_range_option_parse(g_ptr_array_index(option_data->search_range, ind), range_string);
}

void cmd_search_periodic_orbit_set_dimension (gpointer ptr_self, int dim)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  option_data->dimension = dim;
  if (option_data->search_range) {
    g_ptr_array_free(option_data->search_range, TRUE);
    option_data->search_range = NULL;
  }
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_search_periodic_orbit_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_search_periodic_orbit_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_search_periodic_orbit_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdSearchPeriodicOrbit *self = CMD_SEARCH_PERIODIC_ORBIT(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, cmd_search_periodic_orbit_dimension(self));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_search_periodic_orbit_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_search_periodic_orbit_parent_class)->dispose(gobject);
}

static CmdSearchPeriodicOrbitOptionData *cmd_search_periodic_orbit_option_data_alloc ()
{
  int i;
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = (CmdSearchPeriodicOrbitOptionData *) g_malloc(sizeof(CmdSearchPeriodicOrbitOptionData));
  /* option_data->remove_duplication = FALSE; */
  option_data->sort_periodic_orbits = FALSE;
  option_data->newton_error = 1.0e-6;
  option_data->practical_error = 1.0e-6;
  option_data->dimension = 0;
  option_data->max_iteration = 1000;
  option_data->damping_parameter = 0;
  option_data->time_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION;
  option_data->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  option_data->search_range = NULL;
  option_data->initial_point = NULL;
  option_data->string_initial_point = NULL;
  option_data->string_period_range = NULL;
  option_data->period_range = NULL;
  option_data->initial_period = 1.0;
  for (i = 0; i < N_SEARCH_PERIODIC_ORBIT_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_search_periodic_orbit_option_data_free (CmdSearchPeriodicOrbitOptionData *option_data)
{
  if (option_data) {
    if (option_data->search_range) {
      g_ptr_array_free(option_data->search_range, TRUE);
    }
    if (option_data->initial_point) {
      g_free(option_data->initial_point);
    }
    if (option_data->string_initial_point) {
      g_free(option_data->string_initial_point);
    }
    if (option_data->string_period_range) {
      g_free(option_data->string_period_range);
    }
    if (option_data->period_range) {
      g_free(option_data->period_range);
    }
    g_free(option_data);
  }
}

static void cmd_search_periodic_orbit_finalize (GObject *gobject)
{
  CmdSearchPeriodicOrbit *self;
  self = CMD_SEARCH_PERIODIC_ORBIT(gobject);
  cmd_search_periodic_orbit_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_search_periodic_orbit_parent_class)->finalize(gobject);
}

static void cmd_search_periodic_orbit_init (CmdSearchPeriodicOrbit *self)
{
  self->option_data = cmd_search_periodic_orbit_option_data_alloc();
}

static void cmd_search_periodic_orbit_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT] || option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "point", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_point, "Initial point specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_PERIOD]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "period", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->initial_period, "Initial period", "NUM");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PERIOD_RANGE]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "period-range", 'p', 0, G_OPTION_ARG_STRING, &option_data->string_period_range, "Search range of period: STEP,MIN,MAX or VAL", "NUMS");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_DAMPING_PARAMETER]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "damping-parameter", 'd', 0, G_OPTION_ARG_INT, &option_data->damping_parameter, "Damping parameter", "NUM");
  }
  /* if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_REMOVE_DUPLICATION]) { */
  /*   cmd_opts_application_option_alloc_add(cmd_opts, "remove-duplication", 'R', 0, G_OPTION_ARG_NONE, &option_data->remove_duplication, "Remove duplicated equilibria", NULL); */
  /* } */
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_SORT_PERIODIC_ORBITS]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "sort", 'S', 0, G_OPTION_ARG_NONE, &option_data->sort_periodic_orbits, "Sort obtained periodic orbit", NULL);
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NEWTON_ERROR_PERIODIC_ORBIT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "newton-error", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->newton_error, "Acceptable error of Newton method", "NUM");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PRACTICAL_ERROR_PERIODIC_ORBIT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "practical-error", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->practical_error, "Acceptable practical error", "NUM");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_MAX_ITERATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "max-iteration", 0, 0, G_OPTION_ARG_INT, &option_data->max_iteration, "Max number of iterations", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_derivative_perturbation, "Time evolution to calculate numerical derivative", "NUM");
  }
  if (option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "space-derivative-perturbation", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->space_derivative_perturbation, "Perturbation to calculate Jacobian free product", "NUM");
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_VERBOSE]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_VERBOSE] = TRUE;
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOG_LEVEL]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] = TRUE;
    COMMAND_BASE(ptr_self)->option_data->number_log_level = CMD_SEARCH_PERIODIC_ORBIT_N_LOG_LEVELS;
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_search_periodic_orbit_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void search_periodic_orbit_initial_point_alloc (CmdSearchPeriodicOrbitOptionData *option_data)
{
  int i;
  if (option_data->initial_point) {
    g_free(option_data->initial_point);
  }
  option_data->initial_point = (double *) g_malloc(sizeof(double) * option_data->dimension);
  for (i = 0; i < option_data->dimension; i++) {
    option_data->initial_point[i] = 0.0;
  }
}

static void cmd_search_periodic_orbit_process_data (gpointer ptr_self)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_point);
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT]) {
    search_periodic_orbit_initial_point_alloc(option_data);
    command_utils_point_data_load(option_data->initial_point, option_data->string_initial_point, option_data->dimension);
  } else if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM]) {
    if (option_data->string_initial_point) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_point);
      cmd_search_periodic_orbit_set_dimension(ptr_self, dim);
      search_periodic_orbit_initial_point_alloc(option_data);
      memcpy(option_data->initial_point, nums, sizeof(double) * dim);
      g_free(nums);
    }
  }
  if (option_data->string_initial_point) {
    g_free(option_data->string_initial_point);
    option_data->string_initial_point = NULL;
  }
  if (option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PERIOD_RANGE]) {
    if (option_data->string_period_range) {
      option_data->period_range = (double *) g_malloc(sizeof(double) * 3);
      command_utils_search_range_option_parse(option_data->period_range, option_data->string_period_range);
      if (option_data->period_range[0] <= 0.0 || option_data->period_range[1] >= option_data->period_range[2]) {
        fprintf(stderr, "Invalid argument of search range of period\nComma separated numbers a,b,c must satisfy the following conditions: a > 0, b < c\n");
        exit(1);
      }
    }
  }
  COMMAND_CLASS(cmd_search_periodic_orbit_parent_class)->process_data(ptr_self);
}

static void search_periodic_orbit_verbose_print (gpointer ptr_self, gboolean found, double *pt_init, double *period, double *pt_result, int iter)
{
  if (command_base_verbose(ptr_self)) {
    const char *number_format, *number_splitter;
    number_format = command_base_number_format(ptr_self);
    number_splitter = command_base_number_splitter(ptr_self);
    if (found) {
      printf("[Found %d]\t", abs(iter));
    } else {
      printf("[Not found %d]\t", abs(iter));
    }
    mathfluid_utils_array_of_double_printf(cmd_search_periodic_orbit_dimension(ptr_self), pt_init, number_format, number_splitter);
    printf("%s", number_splitter);
    printf(number_format, *period);
    printf("%s", number_splitter);
    mathfluid_utils_array_of_double_printf(cmd_search_periodic_orbit_dimension(ptr_self) + 1, pt_result, number_format, number_splitter);
    printf("\n");
  }
}

/**
 * @param[in]    pt        A pointer of an array whose size is (dim + 1) if the array includes number of period
 *                         as the last element. Otherwise a pointer of the coordinate of a point whose size is dim
 * @param[in]    period    Initial period. If pt includes initial period then the value should be negative.
 */
static void search_periodic_orbit_append (SearchPeriodicOrbit *search, CmdSearchPeriodicOrbit *self, GPtrArray *periodic_orbits, double *pt, double period) {
  CmdSearchPeriodicOrbitOptionData *option_data;
  int iter;
  option_data = self->option_data;
  if (period > 0.0) {
    search_periodic_orbit_set2(search, pt, period);
  } else {
    search_periodic_orbit_set(search, pt);
    period = pt[search_periodic_orbit_system_dimension(search)];
  }
  iter = search_periodic_orbit_iterate_successively(search, option_data->max_iteration);
  if (iter > 0) {
    gsl_vector *vec;
    gboolean already_stored = FALSE;
    vec = gsl_vector_alloc(option_data->dimension + 1);
    search_periodic_orbit_solution_copy(vec->data, search);
    search_periodic_orbit_verbose_print(self, TRUE, pt, &period, vec->data, iter);
    /* if (option_data->remove_duplication) { */
    /*   gsl_vector *vec2; */
    /*   int i, j; */
    /*   gboolean duplicate; */
    /*   for (i = 0; i < periodic_orbits->len; i++) { */
    /*     duplicate = TRUE; */
    /*     vec2 = g_ptr_array_index(periodic_orbits, i); */
    /*     for (j = 0; j < search_periodic_orbit_total_dimension(search); j++) { */
    /*       if (fabs(gsl_vector_get(vec, j) - gsl_vector_get(vec2, j)) > search->error) { */
    /*         duplicate = FALSE; */
    /*         break; */
    /*       } */
    /*     } */
    /*     if (duplicate) { */
    /*       already_stored = TRUE; */
    /*       break; */
    /*     } */
    /*   } */
    /* } */
    if (already_stored) {
      gsl_vector_free(vec);
    } else {
      g_ptr_array_add(periodic_orbits, (gpointer) vec);
    }
  } else {
    search_periodic_orbit_verbose_print(self, FALSE,  pt, &period, search_periodic_orbit_solution_ptr(search), iter);
  }
}

static GPtrArray *periodic_orbits_search (CmdSearchPeriodicOrbit *self)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  GPtrArray *periodic_orbits;
  SearchPeriodicOrbit *search;

  option_data = self->option_data;
  if (option_data->dimension <= 0) {
    fprintf(stderr, "Dimension has not been set properly\n");
    abort();
  }
  search = search_periodic_orbit_alloc(COMMAND(self)->dynamical_system, option_data->damping_parameter, option_data->newton_error, option_data->practical_error);
  if (option_data->time_derivative_perturbation > 0.0) {
    search_periodic_orbit_set_time_derivative_perturbation(search, option_data->time_derivative_perturbation);
  }
  if (option_data->space_derivative_perturbation > 0.0) {
    search_periodic_orbit_set_space_derivative_perturbation(search, option_data->space_derivative_perturbation);
  }
  search_periodic_orbit_set_log_level(search, COMMAND_BASE(self)->option_data->log_level[CMD_SEARCH_PERIODIC_ORBIT_LOG_LEVEL_NEWTON], COMMAND_BASE(self)->option_data->log_level[CMD_SEARCH_PERIODIC_ORBIT_LOG_LEVEL_GMRES]);
  periodic_orbits = g_ptr_array_new_with_free_func((void (*) (gpointer)) gsl_vector_free);
  if (option_data->search_range) {
    double *pt;
    CommandUtilsPointsInRectangle *points_in_rectangle;
    if (option_data->period_range) {
      memcpy(g_ptr_array_index(option_data->search_range, search_periodic_orbit_system_dimension(search)), option_data->period_range, sizeof(double) * 3);
    } else {
      double *ary;
      ary = (double *) g_ptr_array_index(option_data->search_range, search_periodic_orbit_system_dimension(search));
      ary[1] = option_data->initial_period;
      ary[2] = option_data->initial_period;
    }
    points_in_rectangle = command_utils_points_in_rectangle_alloc(option_data->search_range);
    while ((pt = command_utils_points_in_rectangle_next(points_in_rectangle))) {
      search_periodic_orbit_append(search, self, periodic_orbits, pt, -1.0);
    }
    command_utils_points_in_rectangle_free(points_in_rectangle);
  } else if (option_data->period_range) {
    double period;
    period = option_data->period_range[1];
    while (period <= option_data->period_range[2]) {
      search_periodic_orbit_append(search, self, periodic_orbits, option_data->initial_point, period);
      period += option_data->period_range[0];
    }
  } else {
    search_periodic_orbit_append(search, self, periodic_orbits, option_data->initial_point, option_data->initial_period);
  }
  search_periodic_orbit_free(search);
  return periodic_orbits;
}

static int periodic_orbits_compare (gconstpointer a, gconstpointer b)
{
  double x, y;
  gsl_vector *v1;
  int time_index;
  v1 = *(gsl_vector **) a;
  time_index = v1->size - 1;
  x = gsl_vector_get(v1, time_index);
  y = gsl_vector_get(*(gsl_vector **) b, time_index);
  if (x < y) {
    return -1;
  } else if (x > y) {
    return 1;
  } else {
    return 0;
  }
}

static void periodic_orbits_sort (GPtrArray *periodic_orbits)
{
  g_ptr_array_sort(periodic_orbits, periodic_orbits_compare);
}

static void periodic_orbits_print (CmdSearchPeriodicOrbit *self, GPtrArray *periodic_orbits)
{
  if (periodic_orbits->len > 0) {
    int i, data_dim;
    gsl_vector *vec;
    const char *number_format, *number_splitter;
    data_dim = cmd_search_periodic_orbit_dimension(self) + 1;
    number_format = command_base_number_format(self);
    number_splitter = command_base_number_splitter(self);
    for (i = 0; i < periodic_orbits->len; i++) {
      vec = g_ptr_array_index(periodic_orbits, i);
      mathfluid_utils_array_of_double_printf(data_dim, vec->data, number_format, number_splitter);
      printf("\n");
    }
  }
}

static void cmd_search_periodic_orbit_search (gpointer ptr_self)
{
  GPtrArray *periodic_orbits;
  CmdSearchPeriodicOrbit *self;
  CmdSearchPeriodicOrbitOptionData *option_data;
  self = CMD_SEARCH_PERIODIC_ORBIT(ptr_self);
  option_data = self->option_data;

  periodic_orbits = periodic_orbits_search(self);
  if (option_data->sort_periodic_orbits) {
    periodic_orbits_sort(periodic_orbits);
  }
  periodic_orbits_print(self, periodic_orbits);
}

static void cmd_search_periodic_orbit_execute (gpointer ptr_self)
{
  command_base_header_json_fprint(stdout, ptr_self);
  cmd_search_periodic_orbit_search(ptr_self);
}

static void cmd_search_periodic_orbit_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdSearchPeriodicOrbit *self;
  self = CMD_SEARCH_PERIODIC_ORBIT(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Search periodic orbit");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "damping_parameter");
  json_builder_add_int_value(builder, self->option_data->damping_parameter);
  json_builder_set_member_name(builder, "max_iteration");
  json_builder_add_int_value(builder, self->option_data->max_iteration);
  json_builder_set_member_name(builder, "newton_error");
  json_builder_add_double_value(builder, self->option_data->newton_error);
  json_builder_set_member_name(builder, "practical_error");
  json_builder_add_double_value(builder, self->option_data->practical_error);
  json_builder_set_member_name(builder, "sort_periodic_orbits");
  json_builder_add_boolean_value(builder, self->option_data->sort_periodic_orbits);
  if (self->option_data->search_range) {
    json_builder_set_member_name(builder, "search_range");
    command_utils_search_range_json_output2(builder, self->option_data->search_range, 0, self->option_data->dimension - 1);
  }
  if (self->option_data->period_range) {
    /* json_builder_set_member_name(builder, "period_range"); */
    /* json_builder_add_double_value(builder, self->option_data->initial_period);     */
  } else {
    json_builder_set_member_name(builder, "initial_period");
    json_builder_add_double_value(builder, self->option_data->initial_period);
  }
}

static void cmd_search_periodic_orbit_class_init (CmdSearchPeriodicOrbitClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_search_periodic_orbit_dispose;
  gobject_class->finalize = cmd_search_periodic_orbit_finalize;
  gobject_class->set_property = cmd_search_periodic_orbit_set_property;
  gobject_class->get_property = cmd_search_periodic_orbit_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_search_periodic_orbit_command_line_arguments_define;
  command_class->process_data = cmd_search_periodic_orbit_process_data;
  command_class->execute = cmd_search_periodic_orbit_execute;

  command_base_class->header_json_save = cmd_search_periodic_orbit_header_json_save;
}
