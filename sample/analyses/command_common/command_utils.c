#include "command_common.h"

#define PATTERN_SPLIT_STRING_LOG_LEVELS "[ \t\r\n\f]*[ \t,][ \t\r\n\f]*"

MathFluidLogLevel command_utils_parse_log_level_string (const char *string_log_level)
{
  MathFluidLogLevel level;
  char *str;
  str = g_ascii_strup(string_log_level, -1);
  if (strcmp(str, "DEBUG") == 0) {
    level =  MATH_FLUID_LOG_DEBUG;
  } else if (strcmp(str, "INFO") == 0) {
    level = MATH_FLUID_LOG_INFO;
  } else if (strcmp(str, "WARN") == 0) {
    level = MATH_FLUID_LOG_WARN;
  } else if (strcmp(str, "ERROR") == 0) {
    level = MATH_FLUID_LOG_ERROR;
  } else if (strcmp(str, "FATAL") == 0) {
    level = MATH_FLUID_LOG_FATAL;
  } else if (strcmp(str, "NONE") == 0) {
    level = MATH_FLUID_NO_LOG;
  } else {
    fprintf(stderr, "Invalid log level: %s\n", string_log_level);
    abort();
  }
  g_free(str);
  return level;
}

void command_utils_parse_multiple_log_level_string (MathFluidLogLevel *level, int n, const char *string_log_level)
{
  gchar **level_string_array;
  int count, i;
  count = 0;
  level_string_array = g_regex_split_simple(PATTERN_SPLIT_STRING_LOG_LEVELS, string_log_level, G_REGEX_OPTIMIZE, 0);
  while (level_string_array[count]) {
    count += 1;
  }
  for (i = 0; i < n; i++) {
    level[i] = command_utils_parse_log_level_string(level_string_array[(i < count ? i : count - 1)]);
  }
  g_strfreev(level_string_array);
}

GPtrArray *command_utils_search_range_alloc (int dimension)
{
  if (dimension > 0) {
    GPtrArray *search_range;
    int i;
    double *range;
    search_range = g_ptr_array_new_full(dimension, g_free);
    for (i = 0; i < dimension; i++) {
      range = (double *) g_malloc(sizeof(double) * 3);
      range[0] = 1.0;
      range[1] = 0.0;
      range[2] = 0.0;
      g_ptr_array_add(search_range, range);
    }
    return search_range;
  } else {
    return NULL;
  }
}

void command_utils_search_range_option_parse (double *nums, const char *range_string)
{
  if (range_string) {
    gchar **coordinate_string_array;
    int count_string_array, i;
    coordinate_string_array = mathfluid_utils_split_string_numbers(&count_string_array, range_string);
    if (count_string_array == 1) {
      nums[0] = 1.0;
      nums[1] = mathfluid_utils_string_to_double(coordinate_string_array[0]);
      nums[2] = mathfluid_utils_string_to_double(coordinate_string_array[0]);
    } else if (count_string_array == 3){
      for (i = 0; i < count_string_array; i++) {
        nums[i] = mathfluid_utils_string_to_double(coordinate_string_array[i]);
      }
      if (nums[0] <= 0.0 || nums[1] >= nums[2]) {
        fprintf(stderr, "Invalid argument of search range\nComma separated numbers a,b,c must satisfy the following conditions: a > 0, b < c\n");
        exit(1);
      }
    } else {
      fprintf(stderr, "Invalid number of comma separated numbers for search range\n");
      exit(1);
    }
    g_strfreev(coordinate_string_array);
  }
}

void command_utils_search_range_json_output (JsonBuilder *builder, GPtrArray *search_range)
{
  command_utils_search_range_json_output2(builder, search_range, 0, search_range->len - 1);
}

void command_utils_search_range_json_output2 (JsonBuilder *builder, GPtrArray *search_range, int index_min, int index_max)
{
  int i, j;
  double *ary;
  json_builder_begin_array(builder);
  for (i = index_min; i <= index_max; i++) {
    ary = g_ptr_array_index(search_range, i);
    json_builder_begin_array(builder);
    for (j = 0; j < 3; j++) {
      json_builder_add_int_value(builder, ary[j]);
    }
    json_builder_end_array(builder);
  }
  json_builder_end_array(builder);
}

CommandUtilsPointsInRectangle *command_utils_points_in_rectangle_alloc (GPtrArray *region_data)
{
  CommandUtilsPointsInRectangle *points_in_rectangle;
  points_in_rectangle = (CommandUtilsPointsInRectangle *) g_malloc(sizeof(CommandUtilsPointsInRectangle));
  points_in_rectangle->region_data = region_data;
  points_in_rectangle->pt = NULL;
  points_in_rectangle->finish_creating_pt = FALSE;
  return points_in_rectangle;
}

void command_utils_points_in_rectangle_free (CommandUtilsPointsInRectangle *points_in_rectangle)
{
  if (points_in_rectangle->pt) {
    g_free(points_in_rectangle->pt);
  }
  g_free(points_in_rectangle);
}

/**
 * @return    A pointer of array which must not be changed. If we change the value of the array,
 *            next point become invalid.
 */
double *command_utils_points_in_rectangle_next (CommandUtilsPointsInRectangle *points_in_rectangle)
{
  if (points_in_rectangle->finish_creating_pt) {
    return NULL;
  }
  if (points_in_rectangle->pt) {
    int i;
    double *range;
    /* Note that range = { step_size, min_number, max_number } */
    while (TRUE) {
      range = (double *) g_ptr_array_index(points_in_rectangle->region_data, points_in_rectangle->ind);
      points_in_rectangle->pt[points_in_rectangle->ind] += range[0];
      if (points_in_rectangle->pt[points_in_rectangle->ind] <= range[2]) {
        break;
      }
      if (points_in_rectangle->ind == 0) {
        points_in_rectangle->finish_creating_pt = TRUE;
        break;
      }
      points_in_rectangle->ind -= 1;
    }
    if (points_in_rectangle->finish_creating_pt) {
      return NULL;
    } else {
      for (i = points_in_rectangle->ind + 1; i < command_utils_points_in_rectangle_dimension(points_in_rectangle); i++) {
        points_in_rectangle->pt[i] = ((double *) g_ptr_array_index(points_in_rectangle->region_data, i))[1];
      }
      points_in_rectangle->ind = command_utils_points_in_rectangle_dimension(points_in_rectangle) - 1;
    }
  } else {
    int i;
    points_in_rectangle->ind = command_utils_points_in_rectangle_dimension(points_in_rectangle) - 1;
    points_in_rectangle->pt = (double *) g_malloc(sizeof(double) * command_utils_points_in_rectangle_dimension(points_in_rectangle));
    for (i = 0; i < command_utils_points_in_rectangle_dimension(points_in_rectangle); i++) {
      points_in_rectangle->pt[i] = ((double *) g_ptr_array_index(points_in_rectangle->region_data, i))[1];
    }
  }
  return points_in_rectangle->pt;
}

double *command_utils_points_in_rectangle_next_allocated (CommandUtilsPointsInRectangle *points_in_rectangle)
{
  if (command_utils_points_in_rectangle_next(points_in_rectangle)) {
    double *pt;
    int ary_size;
    ary_size = sizeof(double) * command_utils_points_in_rectangle_dimension(points_in_rectangle);
    pt = (double *) g_malloc(ary_size);
    memcpy(pt, points_in_rectangle->pt, ary_size);
    return pt;
  } else {
    return NULL;
  }
}


/**
 * If *string_input means a file path then the file contents are set to *string_input.
 * @param[in,out]    string_input    A pointer of data or file path
 */
void command_utils_load_file_data (char **string_input)
{
  if (string_input && *string_input) {
    if (g_file_test(*string_input, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)) {
      char *file_contents;
      gsize len;
      if (!g_file_get_contents(*string_input, &file_contents, &len, NULL)) {
        fprintf(stderr, "Can not read file %s\n", *string_input);
        exit(0);
      }
      g_free(*string_input);
      *string_input = file_contents;
    }
  }
}

/**
 * If *string_input means a file path then the file contents are set to *string_input.
 * The header of file is removed.
 * @param[in,out]    string_input    A pointer of data or file path
 */
void command_utils_load_file_data_without_header (char **string_input)
{
  command_utils_load_file_data(string_input);
  if (string_input && *string_input) {
    char *string_new, *string_current;
    string_current = *string_input;
    while (TRUE) {
      if (string_current[0] != '#') {
        break;
      }
      while ((string_current[0] != '\n') && (string_current[0] != '\r')) {
        string_current++;
      }
      while ((string_current[0] == '\n') || (string_current[0] == '\r')) {
        string_current++;
      }
    }
    string_new = g_strdup(string_current);
    g_free(*string_input);
    *string_input = string_new;
  }
}

/**
 * Load point data from *string_input. If data is invalid then the program exits.
 * @param[out]    nums_output    A pointer of an array of double
 * @param[in]     string_input   String of numbers or path of data file
 * @param[in]     dim            Dimension of data
 */
void command_utils_point_data_load (double *nums_output, char *string_input, int dim)
{
  if (dim <= 0) {
    fprintf(stderr, "Dimension is invalid: %d\n", dim);
    exit(0);
  }
  if (string_input && *string_input) {
    mathfluid_utils_string_numbers_to_array_of_double(nums_output, dim, string_input, "Invalid dimension of initial point\n");
  }
}

static char *command_utils_get_json_header_string (const char *path)
{
  FILE *input;
  char *header, *buf;
  int num_get;
  size_t num_allocated;
  GPtrArray *lines;
  input = fopen(path, "r");
  if (!input) {
    fprintf(stderr, "Can not open file %s\n", path);
    abort();
  }
  lines = g_ptr_array_new_with_free_func(g_free);
  buf = NULL;
  num_allocated = 0;
  while (TRUE) {
    num_get = getline(&buf, &num_allocated, input);
    if (num_get <= 0 || buf[0] != '#') {
      break;
    }
    g_ptr_array_add(lines, g_strdup(buf + 1));
  }
  g_ptr_array_add(lines, NULL);
  header = g_strjoinv(NULL, (char **) lines->pdata);
  g_free(buf);
  g_ptr_array_free(lines, TRUE);
  fclose(input);
  return header;
}

JsonParser *command_utils_get_json_header_parser (const char *path)
{
  JsonParser *parser;
  char *json_header_string;
  gboolean success;
  parser = json_parser_new();
  json_header_string = command_utils_get_json_header_string(path);
  success = json_parser_load_from_data(parser, json_header_string, strlen(json_header_string), NULL);
  g_free(json_header_string);
  if (!success) {
    g_object_unref(parser);
    parser = NULL;
  }
  return parser;
}
