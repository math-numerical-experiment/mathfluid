#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CmdCalcOrbit, cmd_calc_orbit, TYPE_COMMAND_BASE,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

void cmd_calc_orbit_set_dimension (gpointer ptr_self, int dim)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  option_data->dimension = dim;
  if (option_data->initial_point) {
    g_free(option_data->initial_point);
    option_data->initial_point = NULL;
  }
  if (option_data->dimension > 0) {
    int i;
    option_data->initial_point = (double *) g_malloc(sizeof(double) * option_data->dimension);
    for (i = 0; i < option_data->dimension; i++) {
      option_data->initial_point[i] = 0.0;
    }
  }
}

enum {
  PROP_0,

  PROP_SYSTEM_DIM,

  N_PROPERTIES
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static void cmd_calc_orbit_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
  case PROP_SYSTEM_DIM:
    cmd_calc_orbit_set_dimension(object, g_value_get_int(value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_calc_orbit_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
  CmdCalcOrbit *self = CMD_CALC_ORBIT(object);

  switch (property_id) {
  case PROP_SYSTEM_DIM:
    g_value_set_int(value, self->option_data->dimension);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
    break;
  }
}

static void cmd_calc_orbit_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_calc_orbit_parent_class)->dispose(gobject);
}

static CmdCalcOrbitOptionData *cmd_calc_orbit_option_data_alloc ()
{
  int i;
  CmdCalcOrbitOptionData *option_data;
  option_data = (CmdCalcOrbitOptionData *) g_malloc(sizeof(CmdCalcOrbitOptionData));
  option_data->vector_field = FALSE;
  option_data->periodic_orbit = FALSE;
  option_data->dimension = 0;
  option_data->time_max = 1.0;
  option_data->time_step = 0.1;
  option_data->output_time_interval = 0.0;
  option_data->initial_point = NULL;
  option_data->string_initial_point = NULL;
  for (i = 0; i < N_CALC_ORBIT_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void cmd_calc_orbit_option_data_free (CmdCalcOrbitOptionData *option_data)
{
  if (option_data) {
    if (option_data->initial_point) {
      g_free(option_data->initial_point);
    }
    if (option_data->string_initial_point) {
      g_free(option_data->string_initial_point);
    }
    g_free(option_data);
  }
}

static void cmd_calc_orbit_finalize (GObject *gobject)
{
  CmdCalcOrbit *self;
  self = CMD_CALC_ORBIT(gobject);
  cmd_calc_orbit_option_data_free(self->option_data);
  G_OBJECT_CLASS(cmd_calc_orbit_parent_class)->finalize(gobject);
}

static void cmd_calc_orbit_init (CmdCalcOrbit *self)
{
  self->last_output = -10000.0;
  self->option_data = cmd_calc_orbit_option_data_alloc();
}

static void cmd_calc_orbit_default_print (gpointer ptr_self, SystemState *state, double *vector_field)
{
  int dim;
  const char *number_format, *number_splitter;
  dim = cmd_calc_orbit_dimension(ptr_self);
  number_format = command_base_number_format(ptr_self);
  number_splitter = command_base_number_splitter(ptr_self);
  printf(number_format, state->time);
  printf("%s", number_splitter);
  mathfluid_utils_array_of_double_printf(dim, state->coordinate, number_format, number_splitter);
  if (vector_field) {
    printf("%s", number_splitter);
    mathfluid_utils_array_of_double_printf(dim, vector_field, number_format, number_splitter);
  }
  printf("\n");
}

static void cmd_calc_orbit_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  if (option_data->active_options[CALC_ORBIT_OPTION_VECTOR_FIELD]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "vector-field", 0, 0, G_OPTION_ARG_NONE, &option_data->vector_field, "Show tangent vector at each step", NULL);
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_PERIODIC_ORBIT]) {
    if (!(option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT] || option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM])) {
      fprintf(stderr, "Option --periodic-orbit must be together with option --point\n");
      abort();
    }
    cmd_opts_application_option_alloc_add(cmd_opts, "periodic-orbit", 0, 0, G_OPTION_ARG_NONE, &option_data->periodic_orbit, "Specify period by option --point whose last number is period", NULL);
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_TIME_MAX]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-max", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_max, "Maximum time of evolution of orbit", "NUM");
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_TIME_STEP]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "time-step", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->time_step, "Time step of evolution of orbit", "NUM");
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT] || option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "point", 0, 0, G_OPTION_ARG_STRING, &option_data->string_initial_point, "Initial point specified by comma separated numbers or file path", "NUMS");
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "output-time-interval", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->output_time_interval, "Time interval of output of orbit", NULL);
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_NUMBER_FORMAT]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] = TRUE;
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_NUMBER_SPLITTER]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] = TRUE;
  }
  if (option_data->active_options[CALC_ORBIT_OPTION_LOAD_ARGUMENTS]) {
    COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS] = TRUE;
  }
  COMMAND_CLASS(cmd_calc_orbit_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void cmd_calc_orbit_process_data (gpointer ptr_self)
{
  CmdCalcOrbit *self;
  CmdCalcOrbitOptionData *option_data;
  self = CMD_CALC_ORBIT(ptr_self);
  option_data = self->option_data;
  command_utils_load_file_data_without_header(&option_data->string_initial_point);
  if (option_data->string_initial_point) {
    if (option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT]) {
      command_utils_point_data_load(option_data->initial_point, option_data->string_initial_point,
                                    (option_data->periodic_orbit ? option_data->dimension + 1 : option_data->dimension));
      if (option_data->periodic_orbit) {
        option_data->time_max = option_data->initial_point[option_data->dimension];
      }
    } else if (option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM]) {
      int dim;
      double *nums;
      nums = mathfluid_utils_split_convert_string_numbers(&dim, option_data->string_initial_point);
      if (option_data->periodic_orbit) {
        dim -= 1;
        option_data->time_max = nums[dim];
      }
      cmd_calc_orbit_set_dimension(ptr_self, dim);
      memcpy(option_data->initial_point, nums, sizeof(double) * dim);
      g_free(nums);
    }
    g_free(option_data->string_initial_point);
    option_data->string_initial_point = NULL;
  }
  COMMAND_CLASS(cmd_calc_orbit_parent_class)->process_data(ptr_self);
}

static void cmd_calc_orbit_print (gpointer ptr_self, SystemState *state, double *vector_field)
{
  CmdCalcOrbit *self;
  CmdCalcOrbitOptionData *option_data;
  self = CMD_CALC_ORBIT(ptr_self);
  option_data = self->option_data;
  if (state->time - self->last_output >= option_data->output_time_interval) {
    CMD_CALC_ORBIT_GET_CLASS(ptr_self)->print_orbit(ptr_self, state, vector_field);
    self->last_output = state->time;
  }
}

static void cmd_calc_orbit_init_last_output (CmdCalcOrbit *self)
{
  self->last_output = -self->option_data->output_time_interval * 2.0;
}

static void cmd_calc_orbit_print_orbit (gpointer ptr_self)
{
  CmdCalcOrbit *self;
  SystemState *state;
  double *vector_field;
  double time_next;
  int i;
  CmdCalcOrbitOptionData *option_data;
  self = CMD_CALC_ORBIT(ptr_self);
  option_data = self->option_data;

  if (option_data->time_step <= 0.0) {
    fprintf(stderr, "Time step must be positive: %.8lf\n", option_data->time_step);
    abort();
  }

  state = system_state_alloc2(cmd_calc_orbit_dimension(self), 0.0, option_data->initial_point);
  cmd_calc_orbit_init_last_output(self);
  if (option_data->vector_field) {
    vector_field = (double *) g_malloc(sizeof(double) * cmd_calc_orbit_dimension(self));
    dynamical_system_time_derivative(vector_field, COMMAND(self)->dynamical_system, state->coordinate);
  } else {
    vector_field = NULL;
  }
  cmd_calc_orbit_print(ptr_self, state, vector_field);

  i = 1;
  while (state->time < option_data->time_max) {
    time_next = i * option_data->time_step;
    i += 1;
    if (!dynamical_system_evolve(state, COMMAND(self)->dynamical_system, time_next)) {
      fprintf(stderr, "Can not evolve the solution\n");
      abort();
    }
    if (option_data->vector_field) {
      dynamical_system_time_derivative(vector_field, COMMAND(self)->dynamical_system, state->coordinate);
    }
    cmd_calc_orbit_print(ptr_self, state, vector_field);
  }
  if (vector_field) {
    g_free(vector_field);
  }
  system_state_free(state);
}

static void cmd_calc_orbit_execute (gpointer ptr_self)
{
  command_base_header_json_fprint(stdout, ptr_self);
  cmd_calc_orbit_print_orbit(ptr_self);
}

static void cmd_calc_orbit_header_json_save (JsonBuilder *builder, gpointer ptr_self)
{
  CmdCalcOrbit *self;
  self = CMD_CALC_ORBIT(ptr_self);
  json_builder_set_member_name(builder, "description");
  json_builder_add_string_value(builder, "Calculate orbit");
  json_builder_set_member_name(builder, "dimension");
  json_builder_add_int_value(builder, self->option_data->dimension);
  json_builder_set_member_name(builder, "output_time_interval");
  json_builder_add_double_value(builder, self->option_data->output_time_interval);
  json_builder_set_member_name(builder, "time_step");
  json_builder_add_double_value(builder, self->option_data->time_step);
  json_builder_set_member_name(builder, "time_max");
  json_builder_add_double_value(builder, self->option_data->time_max);
  json_builder_set_member_name(builder, "vector_field");
  json_builder_add_boolean_value(builder, self->option_data->vector_field);
}

static void cmd_calc_orbit_class_init (CmdCalcOrbitClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CommandBaseClass  *command_base_class = COMMAND_BASE_CLASS(klass);
  gobject_class->dispose = cmd_calc_orbit_dispose;
  gobject_class->finalize = cmd_calc_orbit_finalize;
  gobject_class->set_property = cmd_calc_orbit_set_property;
  gobject_class->get_property = cmd_calc_orbit_get_property;

  obj_properties[PROP_SYSTEM_DIM] = g_param_spec_int("system-dimension", "System dimension", "Set system dimension and alloc memory of initial point", 0, INT_MAX, 0, G_PARAM_READWRITE);

  g_object_class_install_properties(gobject_class, N_PROPERTIES, obj_properties);

  command_class->command_line_arguments_define = cmd_calc_orbit_command_line_arguments_define;
  command_class->process_data = cmd_calc_orbit_process_data;
  command_class->execute = cmd_calc_orbit_execute;

  command_base_class->header_json_save = cmd_calc_orbit_header_json_save;

  klass->print_orbit = cmd_calc_orbit_default_print;
}
