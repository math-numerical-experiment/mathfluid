#ifndef _COMMAND_H_
#define _COMMAND_H_

typedef struct _CommandClass CommandClass;
typedef struct _Command Command;

struct _CommandClass {
  GObjectClass parent;

  /**
   * Set default value
   */
  void (*set_default_value) (gpointer ptr_self, gpointer cmdinit_optional_args);
  /**
   * Add GOptionEntry to Command#cmd_opts
   */
  void (*command_line_arguments_define) (CmdOpts *cmd_opts, gpointer ptr_self);
  /**
   * Process remained arguments
   */
  void (*process_remained_arguments) (gpointer ptr_self, MathFluidARGV *mathfluid_argv);
  /**
   * Process data set by command line arguments.
   */
  void (*process_data) (gpointer ptr_self);
  /**
   * Execute command
   */
  void (*execute) (gpointer ptr_self);
};

struct _Command {
  GObject parent;

  /**
   * Options of command behaviors that are defined by child classes of CommandClass
   */
  CmdOpts *cmd_opts;
  /**
   * Pointer of target dynamical system
   */
  DynamicalSystem *dynamical_system;
  /**
   * Options of system parameters that are defined by child interface of SystemDefineInterface
   */
  gpointer system_option_data;
};

#define TYPE_COMMAND (command_get_type ())
#define COMMAND(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_COMMAND, Command))
#define COMMAND_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_COMMAND, CommandClass))
#define IS_COMMAND(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_COMMAND))
#define IS_COMMAND_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_COMMAND))
#define COMMAND_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_COMMAND, CommandClass))

GType command_get_type ();
CmdOpts *command_cmd_opts (Command *command);
void command_system_command_line_arguments_define (gpointer ptr_self);
void command_application_command_line_arguments_define (gpointer ptr_self);
void command_short_description_set (gpointer ptr_self, const char *short_description);
void command_summary_set (gpointer ptr_self, const char *summary);
void command_description_set (gpointer ptr_self, const char *description);
gpointer command_system_option_data (gpointer ptr_self);
void command_process_command_line_arguments (gpointer ptr_self, MathFluidARGV *mathfluid_argv);
gpointer command_new (GType object_type, gpointer cmdinit_optional_args, const gchar *first_property_name, ...);
void command_dynamical_system_create (gpointer ptr_self);
void command_execute (gpointer ptr_self);

#endif /* _COMMAND_H_ */
