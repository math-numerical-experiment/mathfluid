#ifndef _SYSTEM_DEFINE_INTERFACE_H_
#define _SYSTEM_DEFINE_INTERFACE_H_

#define TYPE_SYSTEM_DEFINE (system_define_get_type ())
#define SYSTEM_DEFINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SYSTEM_DEFINE, SystemDefine))
#define IS_SYSTEM_DEFINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SYSTEM_DEFINE))
#define SYSTEM_DEFINE_GET_INTERFACE(inst)  (G_TYPE_INSTANCE_GET_INTERFACE ((inst), TYPE_SYSTEM_DEFINE, SystemDefineInterface))

typedef struct _SystemDefine SystemDefine;
typedef struct _SystemDefineInterface SystemDefineInterface;

struct _SystemDefineInterface
{
  GTypeInterface parent_iface;

  DynamicalSystem *(*dynamical_system_create) (gpointer system_option_data);
  gpointer (*option_data_alloc) (gpointer cmdinit_optional_args);
  void (*option_data_set_default) (gpointer system_option_data, gpointer cmdinit_optional_args);
  void (*option_data_process) (gpointer system_option_data);
  void (*option_data_free) (gpointer system_option_data);
  void (*command_line_arguments_define) (CmdOpts *cmd_opts, gpointer system_option_data);
  void (*header_json_save) (JsonBuilder *builder, gpointer system_option_data);
};

GType system_define_get_type (void);
DynamicalSystem *system_define_dynamical_system_create (gpointer ptr_command);
gpointer system_define_option_data_alloc (gpointer ptr_command, gpointer cmdinit_optional_args);
void system_define_option_data_set_default (gpointer ptr_command, gpointer system_option_data, gpointer cmdinit_optional_args);
void system_define_option_data_process (gpointer ptr_command, gpointer system_option_data);
void system_define_option_data_free (gpointer ptr_command, gpointer system_option_data);
void system_define_command_line_arguments_define (gpointer ptr_command, CmdOpts *cmd_opts, gpointer system_option_data);
gboolean system_define_command_header_json_defined_p (gpointer ptr_command);
void system_define_command_header_json_save (JsonBuilder *builder, gpointer ptr_command, gpointer system_option_data);

#endif /* _SYSTEM_DEFINE_INTERFACE_H_ */
