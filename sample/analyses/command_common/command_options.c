#include "command_common.h"

#define NUMBER_RESERVED_CLI_ARGS 10

G_DEFINE_TYPE(CmdOpts, cmd_opts, G_TYPE_OBJECT)

static void cmd_opts_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(cmd_opts_parent_class)->dispose(gobject);
}

static void cmd_opts_finalize (GObject *gobject)
{
  CmdOpts *self = CMD_OPTS(gobject);
  G_OBJECT_CLASS(cmd_opts_parent_class)->finalize(gobject);
  if (self->application_options) {
    g_ptr_array_free(self->application_options, TRUE);
  }
  if (self->system_options) {
    g_ptr_array_free(self->system_options, TRUE);
  }
  if (self->storage_entries) {
    g_ptr_array_free(self->storage_entries, TRUE);
  }
  if (self->short_description) {
    g_free(self->short_description);
  }
  if (self->summary) {
    g_free(self->summary);
  }
  if (self->description) {
    g_free(self->description);
  }
  if (self->option_file_read) {
    g_free(self->option_file_read);
  }
}

static void option_entry_free (gpointer ptr)
{
  GOptionEntry *entry;
  entry = (GOptionEntry *) ptr;
  g_free((gpointer) entry->long_name);
  g_free((gpointer) entry->description);
  g_free((gpointer) entry->arg_description);
  g_free(ptr);
}

static void cmd_opts_init (CmdOpts *self)
{
  self->short_description = NULL;
  self->summary = NULL;
  self->description = NULL;
  self->option_file_read = NULL;
  self->application_options = g_ptr_array_sized_new(NUMBER_RESERVED_CLI_ARGS);
  self->system_options = g_ptr_array_sized_new(NUMBER_RESERVED_CLI_ARGS);
  self->storage_entries = g_ptr_array_new_full(NUMBER_RESERVED_CLI_ARGS, option_entry_free);
}

static void application_option_add (CmdOpts *self, GOptionEntry *entry)
{
  g_ptr_array_add(self->application_options, entry);
}

static void application_option_insert (CmdOpts *self, int ind, GOptionEntry *entry)
{
  g_ptr_array_insert(self->application_options, ind, entry);
}

static void system_option_add (CmdOpts *self, GOptionEntry *entry)
{
  g_ptr_array_add(self->system_options, entry);
}

static void system_option_insert (CmdOpts *self, int ind, GOptionEntry *entry)
{
  g_ptr_array_insert(self->system_options, ind, entry);
}

static GOptionEntry *option_entries_dump (GPtrArray *ary)
{
  int i;
  GOptionEntry *entries;
  entries = (GOptionEntry *) g_malloc(sizeof(GOptionEntry) * (ary->len + 1));
  for (i = 0; i < ary->len; i++) {
    memcpy(&entries[i], g_ptr_array_index(ary, i), sizeof(GOptionEntry));
  }
  entries[ary->len].long_name = NULL;
  entries[ary->len].short_name = 0;
  entries[ary->len].flags = 0;
  entries[ary->len].arg = 0;
  entries[ary->len].arg_data = NULL;
  entries[ary->len].description = NULL;
  entries[ary->len].arg_description = NULL;
  return entries;
}

static void arguments_parse (CmdOpts *self, MathFluidARGV *mathfluid_argv)
{
  GError *error = NULL;
  GOptionContext *context;
  GOptionGroup *group_system;
  GOptionEntry *entries_system = NULL, *entries_application = NULL;
  gboolean show_help_message = FALSE;
  int argc;
  char **argv;
  group_system = g_option_group_new("system", "System Options:", "help", NULL, NULL);
  context = g_option_context_new (self->short_description);
  g_option_context_set_help_enabled(context, FALSE);
  if (self->summary) {
    g_option_context_set_summary(context, self->summary);
  }
  if (self->description) {
    g_option_context_set_description(context, self->description);
  }
  if (self->application_options->len + self->system_options->len > 0) {
    GOptionEntry entry_help = { "help", 'h', 0, G_OPTION_ARG_NONE, &show_help_message, "Show help message", NULL };
    cmd_opts_application_option_add(self, &entry_help);
    entries_application = option_entries_dump(self->application_options);
    g_option_context_add_main_entries(context, entries_application, NULL);
    g_free(entries_application);
  }
  if (self->system_options->len > 0) {
    entries_system = option_entries_dump(self->system_options);
    g_option_group_add_entries(group_system, entries_system);
    g_option_context_add_group(context, group_system);
    g_free(entries_system);
  }
  if (self->option_file_read) {
    mathfluid_argv_parse_file_option(mathfluid_argv, self->option_file_read);
  }
  argc = mathfluid_argv_argc(mathfluid_argv);
  argv = mathfluid_argv_pointer(mathfluid_argv);
  if (!g_option_context_parse(context, &argc, &argv, &error)) {
    fprintf(stderr, "Option parsing failed: %s\n", error->message);
    exit(1);
  }
  mathfluid_argv_reset(mathfluid_argv);
  mathfluid_argv_append(mathfluid_argv, argc, argv);
  if (show_help_message) {
    gchar *mes;
    mes = g_option_context_get_help(context, FALSE, NULL);
    printf("%s", mes);
    g_free(mes);
    exit(0);
  }
  g_option_context_free(context);
}

static void cmd_opts_class_init (CmdOptsClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  gobject_class->dispose = cmd_opts_dispose;
  gobject_class->finalize = cmd_opts_finalize;

  klass->application_option_add = application_option_add;
  klass->application_option_insert = application_option_insert;
  klass->system_option_add = system_option_add;
  klass->system_option_insert = system_option_insert;
  klass->arguments_parse = arguments_parse;
}

void cmd_opts_application_option_add (CmdOpts *self, GOptionEntry *entry)
{
  CMD_OPTS_GET_CLASS(self)->application_option_add(self, entry);
}

void cmd_opts_application_option_insert (CmdOpts *self, int ind, GOptionEntry *entry)
{
  CMD_OPTS_GET_CLASS(self)->application_option_insert(self, ind, entry);
}

static void cmd_opts_option_array_add (CmdOpts *self, GOptionEntry *entries, void func (CmdOpts *self, GOptionEntry *entry))
{
  GOptionEntry *entry;
  int i = 0;
  while (TRUE) {
    entry = &entries[i];
    if (!entry->long_name && entry->short_name == 0) {
      break;
    } else {
      func(self, entry);
    }
    i += 1;
  }
}

static void cmd_opts_option_array_insert (CmdOpts *self, int ind, GOptionEntry *entries, void func (CmdOpts *self, int ind, GOptionEntry *entry))
{
  GOptionEntry *entry;
  int i = 0;
  while (TRUE) {
    entry = &entries[i];
    if (!entry->long_name && entry->short_name == 0) {
      break;
    } else {
      func(self, ind + i, entry);
    }
    i += 1;
  }
}

/**
 * Add option entries to application option group.
 * @param[in]    self        An object of CmdOpts
 * @param[in]    entries     A pointer of an array of entries whose last element is { NULL }.
 */
void cmd_opts_application_option_array_add (CmdOpts *self, GOptionEntry *entries)
{
  cmd_opts_option_array_add(self, entries, cmd_opts_application_option_add);
}

/**
 * Insert option entries to application option group.
 * @param[in]    self        An object of CmdOpts
 * @param[in]    ind         Index to insert entries
 * @param[in]    entries     A pointer of an array of entries whose last element is { NULL }.
 */
void cmd_opts_application_option_array_insert (CmdOpts *self, int ind, GOptionEntry *entries)
{
  cmd_opts_option_array_insert(self, ind, entries, cmd_opts_application_option_insert);
}

void cmd_opts_system_option_add (CmdOpts *self, GOptionEntry *entry)
{
  CMD_OPTS_GET_CLASS(self)->system_option_add(self, entry);
}

void cmd_opts_system_option_insert (CmdOpts *self, int ind, GOptionEntry *entry)
{
  CMD_OPTS_GET_CLASS(self)->system_option_insert(self, ind, entry);
}

/**
 * Add option entries to system option group.
 * @param[in]    self        An object of CmdOpts
 * @param[in]    entries     A pointer of an array of entries whose last element is { NULL }.
 */
void cmd_opts_system_option_array_add (CmdOpts *self, GOptionEntry *entries)
{
  cmd_opts_option_array_add(self, entries, cmd_opts_system_option_add);
}

/**
 * Insert option entries to system option group.
 * @param[in]    self        An object of CmdOpts
 * @param[in]    ind         Index to insert entries
 * @param[in]    entries     A pointer of an array of entries whose last element is { NULL }.
 */
void cmd_opts_system_option_array_insert (CmdOpts *self, int ind, GOptionEntry *entries)
{
  cmd_opts_option_array_insert(self, ind, entries, cmd_opts_system_option_insert);
}

void cmd_opts_arguments_parse (CmdOpts *self, MathFluidARGV *mathfluid_argv)
{
  CMD_OPTS_GET_CLASS(self)->arguments_parse(self, mathfluid_argv);
}

void cmd_opts_short_description_set (CmdOpts *self, const gchar *short_description)
{
  if (self->short_description) {
    g_free(self->short_description);
  }
  self->short_description = g_strdup(short_description);
}

void cmd_opts_summary_set (CmdOpts *self, const gchar *summary)
{
  if (self->summary) {
    g_free(self->summary);
  }
  self->summary = g_strdup(summary);
}

void cmd_opts_description_set (CmdOpts *self, const gchar *description)
{
  if (self->description) {
    g_free(self->description);
  }
  self->description = g_strdup(description);
}

void cmd_opts_option_file_read_set (CmdOpts *self, const gchar *option_file_read)
{
  if (self->option_file_read) {
    g_free(self->option_file_read);
  }
  self->option_file_read = g_strdup(option_file_read);
}
