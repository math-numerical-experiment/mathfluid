#ifndef _COMMAND_BASE_H_
#define _COMMAND_BASE_H_

typedef struct _CommandBaseClass CommandBaseClass;
typedef struct _CommandBase CommandBase;
typedef struct _CommandBaseOptionData CommandBaseOptionData;

typedef enum {
  COMMAND_BASE_OPTION_LOAD_ARGUMENTS,
  COMMAND_BASE_OPTION_THREAD,
  COMMAND_BASE_OPTION_NUMBER_FORMAT,
  COMMAND_BASE_OPTION_NUMBER_SPLITTER,
  COMMAND_BASE_OPTION_LOG_LEVEL,
  COMMAND_BASE_OPTION_VERBOSE,
  N_COMMAND_BASE_OPTIONS
} CommandBaseOptionType;

struct _CommandBaseOptionData {
  gboolean verbose;
  char *load_arguments;
  char *string_number_format;
  char *string_number_splitter;
  char *string_log_level;
  char *number_format;
  char *number_splitter;
  int number_thread;
  int number_log_level;
  MathFluidLogLevel *log_level;
  gboolean active_options[N_COMMAND_BASE_OPTIONS];
};

struct _CommandBaseClass {
  CommandClass parent;

  void (*header_json_save) (JsonBuilder *builder, gpointer ptr_self);
};

struct _CommandBase {
  Command parent;

  CommandBaseOptionData *option_data;
};

#define TYPE_COMMAND_BASE (command_base_get_type ())
#define COMMAND_BASE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_COMMAND_BASE, CommandBase))
#define COMMAND_BASE_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_COMMAND_BASE, CommandBaseClass))
#define IS_COMMAND_BASE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_COMMAND_BASE))
#define IS_COMMAND_BASE_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_COMMAND_BASE))
#define COMMAND_BASE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_COMMAND_BASE, CommandBaseClass))

GType command_base_get_type ();
#define command_base_dimension(self) (COMMAND_BASE(self)->option_data->dimension)
void command_base_log_level_set (gpointer ptr_self, int num, ...);
void command_base_log_level_vset (gpointer ptr_self, int num, va_list args);
gboolean command_base_verbose (gpointer ptr_self);
const char *command_base_number_format (gpointer ptr_self);
const char *command_base_number_splitter (gpointer ptr_self);
MathFluidLogLevel *command_base_number_log_level (gpointer ptr_self);
void command_base_header_json_fprint (FILE *out, gpointer ptr_self);

#endif /* _COMMAND_BASE_H_ */
