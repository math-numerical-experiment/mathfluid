#ifndef _CMD_PARAMETER_CONTINUATION_H_
#define _CMD_PARAMETER_CONTINUATION_H_

typedef struct _CmdParameterContinuationClass CmdParameterContinuationClass;
typedef struct _CmdParameterContinuation CmdParameterContinuation;
typedef struct _CmdParameterContinuationOptionData CmdParameterContinuationOptionData;

typedef enum {
  PARAMETER_CONTINUATION_OPTION_VERBOSE,
  PARAMETER_CONTINUATION_OPTION_PARAMETER_KEY,
  PARAMETER_CONTINUATION_OPTION_PARAMETER_DIRECTION,
  PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_FIXED,
  PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_ADJUSTED,
  PARAMETER_CONTINUATION_OPTION_MAX_ARC_LENGTH,
  PARAMETER_CONTINUATION_OPTION_GMRES_CORRECTOR_MAX_ITERATION,
  PARAMETER_CONTINUATION_OPTION_TIME_DERIVATIVE_PERTURBATION,
  PARAMETER_CONTINUATION_OPTION_PARAMETER_DERIVATIVE_PERTURBATION,
  PARAMETER_CONTINUATION_OPTION_SPACE_DERIVATIVE_PERTURBATION,
  PARAMETER_CONTINUATION_OPTION_NEWTON_ERROR_EQUILIBRIA,
  PARAMETER_CONTINUATION_OPTION_NEWTON_MAX_ITERATION,
  PARAMETER_CONTINUATION_OPTION_NEWTON_DAMPING_PARAMETER,
  PARAMETER_CONTINUATION_OPTION_NEWTON_METHOD,
  PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM,
  PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM_WITH_DIM,

  PARAMETER_CONTINUATION_OPTION_NUMBER_FORMAT,
  PARAMETER_CONTINUATION_OPTION_NUMBER_SPLITTER,
  PARAMETER_CONTINUATION_OPTION_LOG_LEVEL,
  PARAMETER_CONTINUATION_OPTION_LOAD_ARGUMENTS,

  N_PARAMETER_CONTINUATION_OPTIONS
} CmdParameterContinuationOptionType;

enum {
  CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION,
  CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION_NEWTON,
  CMD_PARAMETER_CONTINUATION_LOG_LEVEL_CONTINUATION_GMRES,
  CMD_PARAMETER_CONTINUATION_N_LOG_LEVELS,
};

struct _CmdParameterContinuationOptionData {
  int dimension;
  int newton_max_iteration;
  int newton_damping_parameter;
  int gmres_corrector_max_iteration;
  char *parameter_key;
  char *string_initial_equilibrium;
  char *string_direction;
  char *string_step_length_adjusted;
  char *newton_method;
  ParameterContinuationDirection direction;
  double time_derivative_perturbation;
  double parameter_derivative_perturbation;
  double error_equilibria;
  double space_derivative_perturbation;
  double step_length;
  double min_step_length;
  double max_arc_length;
  double *initial_equilibrium;
  gboolean active_options[N_PARAMETER_CONTINUATION_OPTIONS];
};

struct _CmdParameterContinuationClass {
  CommandBaseClass parent;
};

struct _CmdParameterContinuation {
  CommandBase parent;

  CmdParameterContinuationOptionData *option_data;
};

#define TYPE_CMD_PARAMETER_CONTINUATION (cmd_parameter_continuation_get_type ())
#define CMD_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_PARAMETER_CONTINUATION, CmdParameterContinuation))
#define CMD_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_PARAMETER_CONTINUATION, CmdParameterContinuationClass))
#define IS_CMD_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_PARAMETER_CONTINUATION))
#define IS_CMD_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_PARAMETER_CONTINUATION))
#define CMD_PARAMETER_CONTINUATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_PARAMETER_CONTINUATION, CmdParameterContinuationClass))

GType cmd_parameter_continuation_get_type ();
#define cmd_parameter_continuation_dimension(self) (CMD_PARAMETER_CONTINUATION(self)->option_data->dimension)
void cmd_parameter_continuation_option_range_set (CmdParameterContinuation *self, int ind, const char *range_string);
void cmd_parameter_continuation_set_dimension (gpointer ptr_self, int dim);

#endif /* _CMD_PARAMETER_CONTINUATION_H_ */
