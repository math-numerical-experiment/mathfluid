#ifndef _COMMAND_OPTIONS_H_
#define _COMMAND_OPTIONS_H_

typedef struct _CmdOptsClass CmdOptsClass;
typedef struct _CmdOpts CmdOpts;

struct _CmdOptsClass {
  GObjectClass parent;

  void (*application_option_add) (CmdOpts *self, GOptionEntry *entry);
  void (*application_option_insert) (CmdOpts *self, int ind, GOptionEntry *entry);
  void (*system_option_add) (CmdOpts *self, GOptionEntry *entry);
  void (*system_option_insert) (CmdOpts *self, int ind, GOptionEntry *entry);
  void (*arguments_parse) (CmdOpts *self, MathFluidARGV *mathfluid_argv);
};

struct _CmdOpts {
  GObject parent;

  GPtrArray *application_options;
  GPtrArray *system_options;
  GPtrArray *storage_entries;
  gchar *short_description;
  gchar *summary;
  gchar *description;
  gchar *option_file_read;
};

#define TYPE_CMD_OPTS (cmd_opts_get_type ())
#define CMD_OPTS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_CMD_OPTS, CmdOpts))
#define CMD_OPTS_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_CMD_OPTS, CmdOptsClass))
#define IS_CMD_OPTS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_CMD_OPTS))
#define IS_CMD_OPTS_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_CMD_OPTS))
#define CMD_OPTS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_CMD_OPTS, CmdOptsClass))

GType cmd_opts_get_type ();

void cmd_opts_short_description_set (CmdOpts *self, const gchar *short_description);
void cmd_opts_summary_set (CmdOpts *self, const gchar *summary);
void cmd_opts_description_set (CmdOpts *self, const gchar *description);
void cmd_opts_option_file_read_set (CmdOpts *self, const gchar *option_file_read);
void cmd_opts_application_option_add (CmdOpts *self, GOptionEntry *entry);
void cmd_opts_application_option_insert (CmdOpts *self, int ind, GOptionEntry *entry);
void cmd_opts_application_option_array_add (CmdOpts *self, GOptionEntry *entries);
void cmd_opts_application_option_array_insert (CmdOpts *self, int ind, GOptionEntry *entries);
void cmd_opts_system_option_add (CmdOpts *self, GOptionEntry *entry);
void cmd_opts_system_option_insert (CmdOpts *self, int ind, GOptionEntry *entry);
void cmd_opts_system_option_array_add (CmdOpts *self, GOptionEntry *entries);
void cmd_opts_system_option_array_insert (CmdOpts *self, int ind, GOptionEntry *entries);
void cmd_opts_arguments_parse (CmdOpts *self, MathFluidARGV *mathfluid_argv);

#define cmd_opts_option_entry_alloc_set(ptr, lname, sname, f, ar, adata, desc, arg_desc) { \
    ptr = (GOptionEntry *) g_malloc(sizeof(GOptionEntry));              \
    ptr->long_name = g_strdup(lname);                                   \
    ptr->short_name = sname;                                            \
    ptr->flags = f;                                                     \
    ptr->arg = ar;                                                      \
    ptr->arg_data = adata;                                              \
    ptr->description = g_strdup(desc);                                  \
    ptr->arg_description = g_strdup(arg_desc);                          \
  }

#define cmd_opts_application_option_alloc_add(cmd_opts, lname, sname, f, ar, adata, desc, arg_desc) { \
    GOptionEntry *__entry_ptr;                                          \
    cmd_opts_option_entry_alloc_set(__entry_ptr, lname, sname, f, ar, adata, desc, arg_desc); \
    g_ptr_array_add(cmd_opts->storage_entries, __entry_ptr);            \
    cmd_opts_application_option_add(cmd_opts, __entry_ptr);             \
  }

#define cmd_opts_application_option_alloc_insert(cmd_opts, i, lname, sname, f, ar, adata, desc, arg_desc) { \
    GOptionEntry *__entry_ptr;                                          \
    cmd_opts_option_entry_alloc_set(__entry_ptr, lname, sname, f, ar, adata, desc, arg_desc); \
    g_ptr_array_add(cmd_opts->storage_entries, __entry_ptr);            \
    cmd_opts_application_option_insert(cmd_opts, i, __entry_ptr);       \
  }

#define cmd_opts_system_option_alloc_add(cmd_opts, lname, sname, f, ar, adata, desc, arg_desc) { \
    GOptionEntry *__entry_ptr;                                          \
    cmd_opts_option_entry_alloc_set(__entry_ptr, lname, sname, f, ar, adata, desc, arg_desc); \
    g_ptr_array_add(cmd_opts->storage_entries, __entry_ptr);            \
    cmd_opts_system_option_add(cmd_opts, __entry_ptr);                  \
  }

#define cmd_opts_system_option_alloc_insert(cmd_opts, i, lname, sname, f, ar, adata, desc, arg_desc) { \
    GOptionEntry *__entry_ptr;                                          \
    cmd_opts_option_entry_alloc_set(__entry_ptr, lname, sname, f, ar, adata, desc, arg_desc); \
    g_ptr_array_add(cmd_opts->storage_entries, __entry_ptr);            \
    cmd_opts_system_option_add(cmd_opts, i, __entry_ptr);               \
  }

#endif /* _COMMAND_OPTIONS_H_ */
