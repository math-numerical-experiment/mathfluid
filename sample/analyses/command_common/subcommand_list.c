#include "command_common.h"

static SubcommandList *subcommand_list_global = NULL;

typedef struct {
  gchar *key;
  int (* command)(int argc, char **argv);
} SubcommandListData;

static SubcommandListData *subcommand_list_data_alloc (gchar *key, int (* command)(int argc, char **argv))
{
  SubcommandListData *data;
  data = g_malloc(sizeof(SubcommandListData));
  data->key = g_strdup(key);
  data->command = command;
  return data;
}

static void subcommand_data_free (SubcommandListData *data)
{
  g_free(data->key);
  g_free(data);
}

SubcommandList *subcommand_list_alloc (void)
{
  SubcommandList *subcommand_list;
  subcommand_list = (SubcommandList *) g_malloc(sizeof(SubcommandList));
  subcommand_list->initialized = FALSE;
  subcommand_list->function_initialize = g_ptr_array_new();
  subcommand_list->function_finalize = g_ptr_array_new();
  subcommand_list->table_subcommand = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, (void (*) (gpointer data)) subcommand_data_free);
  return subcommand_list;
}

void subcommand_list_free (SubcommandList *subcommand_list)
{
  int i;
  void (* func)();
  for (i = 0; i < subcommand_list->function_finalize->len; i++) {
    func = (void (*)()) g_ptr_array_index(subcommand_list->function_finalize, i);
    (*func)();
  }
  if (subcommand_list->function_initialize) {
    g_ptr_array_free(subcommand_list->function_initialize, TRUE);
  }
  if (subcommand_list->function_finalize) {
    g_ptr_array_free(subcommand_list->function_finalize, TRUE);
  }
  if (subcommand_list->table_subcommand) {
    g_hash_table_destroy(subcommand_list->table_subcommand);
  }
  g_free(subcommand_list);
}

gchar *subcommand_list_get_basename (const char *path)
{
  gchar *basename;
  basename = g_path_get_basename(path);
  if (g_str_has_prefix(basename, "lt-")) {
    gchar *tmp;
    tmp = basename;
    basename = g_strdup(tmp + 3);
    g_free(tmp);
  }
  return basename;
}

void subcommand_list_register_function_initialize (SubcommandList *subcommand_list, void (* func)(void))
{
  g_ptr_array_add(subcommand_list->function_initialize, func);
}

void subcommand_list_register_function_finalize (SubcommandList *subcommand_list, void (* func)(void))
{
  g_ptr_array_add(subcommand_list->function_finalize, func);
}

static gchar *subcommand_list_generate_key (const char *program_basename, const char *subcommand_name)
{
  return g_strconcat(program_basename, "#", subcommand_name, NULL);
}

void subcommand_list_register_subcommand (SubcommandList *subcommand_list, const char *program_basename, const char *subcommand_name, int (* command_execute)(int argc, char **argv))
{
  if (subcommand_list->initialized) {
    fprintf(stderr, "Can not register subcommands after initialization");
    abort();
  }
  gchar *key = subcommand_list_generate_key(program_basename, subcommand_name);
  SubcommandListData *data = subcommand_list_data_alloc(key, command_execute);
  g_hash_table_insert(subcommand_list->table_subcommand, key, data);
}

static int (* subcommand_list_get_command (SubcommandList *subcommand_list, char *program_path, char *subcommand_name)) (int argc, char **argv)
{
  if (!subcommand_list->initialized) {
    int i;
    void (* func)();
    for (i = 0; i < subcommand_list->function_initialize->len; i++) {
      func = (void (*)()) g_ptr_array_index(subcommand_list->function_initialize, i);
      (*func)();
    }
    subcommand_list->initialized = TRUE;
  }
  gchar *program_basename = subcommand_list_get_basename(program_path);
  gchar *key = subcommand_list_generate_key(program_basename, subcommand_name);
  SubcommandListData *data = (SubcommandListData *) g_hash_table_lookup(subcommand_list->table_subcommand, key);
  g_free(program_basename);
  g_free(key);
  if (data != NULL) {
    return data->command;
  } else {
    return NULL;
  }
}

static void subcommand_list_global_finalize (void)
{
  if (subcommand_list_global) {
    subcommand_list_free(subcommand_list_global);
  }
}

static void subcommand_list_global_initialize (void)
{
  if (!subcommand_list_global) {
    subcommand_list_global = subcommand_list_alloc();
  }
}

static gint subcommand_list_strcmp (gconstpointer strptr1, gconstpointer strptr2)
{
  return g_strcmp0(*((char **) strptr1), *((char **) strptr2));
}

static void subcommand_list_print_list (SubcommandList *subcommand_list, char *program_path)
{
  GList *list, *ptr;
  SubcommandListData *data;
  GPtrArray *subcommand_array;
  gchar *program_basename, *prefix_string;
  printf("List of subcommands\n");
  list = g_hash_table_get_values(subcommand_list->table_subcommand);
  subcommand_array = g_ptr_array_new_with_free_func(g_free);
  program_basename = subcommand_list_get_basename(program_path);
  prefix_string = g_strconcat(program_basename, "#", NULL);
  for (ptr = list; ptr != NULL; ptr = ptr->next) {
    data = (SubcommandListData *) ptr->data;
    if (g_str_has_prefix(data->key, prefix_string)) {
      gchar *subcommand_name = g_strdup(data->key + strlen(prefix_string));
      g_ptr_array_add(subcommand_array, subcommand_name);
    }
  }
  g_ptr_array_sort(subcommand_array, subcommand_list_strcmp);
  int i;
  for (i = 0; i < subcommand_array->len; i++) {
    printf("  %s\n", (char *) g_ptr_array_index(subcommand_array, i));
  }
  g_free(program_basename);
  g_free(prefix_string);
  g_list_free(list);
  g_ptr_array_free(subcommand_array, TRUE);;
}

int subcommand_list_execute_command (SubcommandList *subcommand_list, int argc, char **argv)
{
  if (argc > 1) {
    int (* command_execute)(int argc, char **argv);
    command_execute = subcommand_list_get_command(subcommand_list, argv[0], argv[1]);
    if (command_execute != NULL) {
      return command_execute(argc, argv);
    } else {
      fprintf(stderr, "Subcommand is not found: %s\n", argv[1]);
    }
  } else {
    fprintf(stderr, "Subcommand is not specified\n");
  }
  subcommand_list_print_list(subcommand_list, argv[0]);
  return -1;
}

void subcommand_list_global_register_subcommand (const char *program_basename, const char *subcommand_name, int (* command_execute)(int argc, char **argv))
{
  subcommand_list_global_initialize();
  subcommand_list_register_subcommand(subcommand_list_global, program_basename, subcommand_name, command_execute);
}

/**
 * Search and execute a subcommand from global subcommand database.
 * Note that automatically this function finalize the global database of subcommands.
 * @param[in]    argc    Number of command line arguments
 * @param[in]    argv    Array of command line arguments
 */
int subcommand_list_global_execute_command (int argc, char **argv)
{
  int code;
  subcommand_list_global_initialize();
  code = subcommand_list_execute_command(subcommand_list_global, argc, argv);
  subcommand_list_global_finalize();
  return code;
}
