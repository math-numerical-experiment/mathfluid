#include "command_common.h"

static void command_system_define_interface_init (SystemDefineInterface *iface)
{
}

G_DEFINE_TYPE_WITH_CODE(CommandBase, command_base, TYPE_COMMAND,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, command_system_define_interface_init))

static void command_base_dispose (GObject *gobject)
{
  G_OBJECT_CLASS(command_base_parent_class)->dispose(gobject);
}

static CommandBaseOptionData *command_base_option_data_alloc ()
{
  int i;
  CommandBaseOptionData *option_data;
  option_data = (CommandBaseOptionData *) g_malloc(sizeof(CommandBaseOptionData));
  option_data->verbose = FALSE;
  option_data->load_arguments = NULL;
  option_data->string_number_format = NULL;
  option_data->string_number_splitter = NULL;
  option_data->string_log_level = NULL;
  option_data->number_format = g_strdup("%.14lf");
  option_data->number_splitter = g_strdup("\t");
  option_data->number_thread = 1;
  option_data->number_log_level = 0;
  option_data->log_level = NULL;
  for (i = 0; i < N_COMMAND_BASE_OPTIONS; i++) {
    option_data->active_options[i] = FALSE;
  }
  return option_data;
}

static void command_base_option_data_free (CommandBaseOptionData *option_data)
{
  if (option_data) {
    if (option_data->string_number_format) {
      g_free(option_data->string_number_format);
    }
    if (option_data->string_number_splitter) {
      g_free(option_data->string_number_splitter);
    }
    if (option_data->string_log_level) {
      g_free(option_data->string_log_level);
    }
    if (option_data->number_format) {
      g_free(option_data->number_format);
    }
    if (option_data->number_splitter) {
      g_free(option_data->number_splitter);
    }
    if (option_data->log_level) {
      g_free(option_data->log_level);
    }
    g_free(option_data);
  }
}

static void command_base_finalize (GObject *gobject)
{
  CommandBase *self;
  self = COMMAND_BASE(gobject);
  if (COMMAND_BASE(gobject)->option_data->number_thread > 1) {
    mathfluid_thread_pool_finalize();
  }
  command_base_option_data_free(self->option_data);
  G_OBJECT_CLASS(command_base_parent_class)->finalize(gobject);
}

static void command_base_init (CommandBase *self)
{
  self->option_data = command_base_option_data_alloc();
}

static void command_base_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  CommandBaseOptionData *option_data;
  option_data = COMMAND_BASE(ptr_self)->option_data;
  if (option_data->active_options[COMMAND_BASE_OPTION_LOAD_ARGUMENTS]) {
    cmd_opts_option_file_read_set(cmd_opts, "--load-arguments");
    cmd_opts_application_option_alloc_add(cmd_opts, "load-arguments", 0, 0, G_OPTION_ARG_STRING, &option_data->load_arguments, "Load command line arguments from file", "PATH");
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_THREAD]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "thread", 0, 0, G_OPTION_ARG_INT, &option_data->number_thread, "Set number of threads", "NUM");
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "number-format", 0, 0, G_OPTION_ARG_STRING, &option_data->string_number_format, "Number format; default is %.14lf", "STR");
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "number-splitter", 0, 0, G_OPTION_ARG_STRING, &option_data->string_number_splitter, "Number splitter; default is \\t", "STR");
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] && option_data->number_log_level > 0) {
    if (option_data->number_log_level == 1) {
      cmd_opts_application_option_alloc_add(cmd_opts, "log-level", 0, 0, G_OPTION_ARG_STRING, &option_data->string_log_level, "Log level: DEBUG, INFO, WARN, ERROR, FATAL, or NONE", "STR");
    } else {
      gchar *desc;
      desc = g_strdup_printf("Comma-separated %d log levels: DEBUG, INFO, WARN, ERROR, FATAL, or NONE", option_data->number_log_level);
      cmd_opts_application_option_alloc_add(cmd_opts, "log-level", 0, 0, G_OPTION_ARG_STRING, &option_data->string_log_level, desc, "STR");
      g_free(desc);
    }
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_VERBOSE]) {
    cmd_opts_application_option_alloc_add(cmd_opts, "verbose", 0, 0, G_OPTION_ARG_NONE, &option_data->verbose, "Provide verbose output", NULL);
  }
}

static void command_base_log_level_allocate (CommandBaseOptionData *option_data, int number_log_level)
{
  if (option_data->log_level) {
    g_free(option_data->log_level);
    option_data->log_level = NULL;
  }
  if (number_log_level > 0) {
    option_data->number_log_level = number_log_level;
    option_data->log_level = (MathFluidLogLevel *) g_malloc(sizeof(MathFluidLogLevel) * option_data->number_log_level);
  } else {
    option_data->number_log_level = 0;
  }
}

void command_base_log_level_set (gpointer ptr_self, int num, ...)
{
  va_list args;
  va_start(args, num);
  command_base_log_level_vset(ptr_self, num, args);
  va_end (args);
}

void command_base_log_level_vset (gpointer ptr_self, int num, va_list args)
{
  CommandBaseOptionData *option_data;
  int i;
  option_data = COMMAND_BASE(ptr_self)->option_data;
  command_base_log_level_allocate(option_data, num);
  for (i = 0; i < num; i++) {
    option_data->log_level[i] = va_arg(args, MathFluidLogLevel);
  }
}

static void command_base_process_data (gpointer ptr_self)
{
  CommandBase *self;
  CommandBaseOptionData *option_data;
  self = COMMAND_BASE(ptr_self);
  option_data = self->option_data;
  if (option_data->active_options[COMMAND_BASE_OPTION_NUMBER_FORMAT] && option_data->string_number_format) {
    if (option_data->number_format) {
      g_free(option_data->number_format);
    }
    option_data->number_format = option_data->string_number_format;
    option_data->string_number_format = NULL;
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_NUMBER_SPLITTER] && option_data->string_number_splitter) {
    if (option_data->number_splitter) {
      g_free(option_data->number_splitter);
    }
    option_data->number_splitter = option_data->string_number_splitter;
    option_data->string_number_splitter = NULL;
  }
  if (option_data->active_options[COMMAND_BASE_OPTION_LOG_LEVEL] && option_data->number_log_level > 0) {
    if (option_data->string_log_level) {
      command_base_log_level_allocate(option_data, option_data->number_log_level);
      command_utils_parse_multiple_log_level_string(option_data->log_level, option_data->number_log_level, option_data->string_log_level);
    } else {
      if (!option_data->log_level) {
        int i;
        command_base_log_level_allocate(option_data, option_data->number_log_level);
        for (i = 0; i < option_data->number_log_level; i++) {
          option_data->log_level[i] = MATH_FLUID_NO_LOG;
        }
      }
    }
  }
  if (option_data->number_thread > 1) {
    mathfluid_thread_pool_initialize(option_data->number_thread);
  }
}

static void command_base_class_init (CommandBaseClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  gobject_class->dispose = command_base_dispose;
  gobject_class->finalize = command_base_finalize;
  command_class->command_line_arguments_define = command_base_command_line_arguments_define;
  command_class->process_data = command_base_process_data;
  klass->header_json_save = NULL;
}

gboolean command_base_verbose (gpointer ptr_self)
{
  return COMMAND_BASE(ptr_self)->option_data->verbose;
}

const char *command_base_number_format (gpointer ptr_self)
{
  return COMMAND_BASE(ptr_self)->option_data->number_format;
}

const char *command_base_number_splitter (gpointer ptr_self)
{
  return COMMAND_BASE(ptr_self)->option_data->number_splitter;
}

MathFluidLogLevel *command_base_number_log_level (gpointer ptr_self)
{
  return COMMAND_BASE(ptr_self)->option_data->log_level;
}

static gchar *command_base_get_json_builder_root_string (JsonBuilder *builder, gsize *len_json)
{
  JsonGenerator *generator;
  JsonNode *node;
  gchar *str_json;

  generator = json_generator_new();
  node = json_builder_get_root(builder);
  json_generator_set_root(generator, node);
  json_generator_set_pretty(generator, TRUE);
  str_json = json_generator_to_data (generator, len_json);

  g_object_unref(generator);
  json_node_free(node);
  return str_json;
}

static void command_base_fprintf_json_builder_root_comment (FILE *out, JsonBuilder *builder)
{
  gchar *str_json, *str_comment;
  gsize len_json;
  GRegex *regex;

  str_json = command_base_get_json_builder_root_string(builder, &len_json);
  regex = g_regex_new("^", G_REGEX_MULTILINE, 0, NULL);
  str_comment = g_regex_replace(regex, str_json, len_json, 0, "# ", 0, NULL);
  g_regex_unref(regex);
  fprintf(out, "%s\n", str_comment);
  g_free(str_json);
  g_free(str_comment);
}

void command_base_header_json_fprint (FILE *out, gpointer ptr_self)
{
  CommandBaseClass *klass;
  klass = COMMAND_BASE_GET_CLASS(ptr_self);
  if (klass->header_json_save ||system_define_command_header_json_defined_p(ptr_self)) {
    JsonBuilder *builder;
    builder = json_builder_new();
    json_builder_begin_object(builder);
    if (klass->header_json_save) {
      json_builder_set_member_name(builder, "application");
      json_builder_begin_object(builder);
      klass->header_json_save(builder, ptr_self);
      json_builder_end_object(builder);
    }
    if (system_define_command_header_json_defined_p(ptr_self)) {
      json_builder_set_member_name(builder, "system");
      json_builder_begin_object(builder);
      system_define_command_header_json_save(builder, ptr_self, command_system_option_data(ptr_self));
      json_builder_end_object(builder);
    }
    json_builder_end_object(builder);
    command_base_fprintf_json_builder_root_comment(out, builder);
    g_object_unref (builder);
  }
}
