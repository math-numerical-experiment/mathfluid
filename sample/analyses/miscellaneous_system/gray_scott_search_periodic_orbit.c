#include "miscellaneous_system_interface.h"

typedef struct _GrayScottSearchPeriodicOrbitClass GrayScottSearchPeriodicOrbitClass;
typedef struct _GrayScottSearchPeriodicOrbit GrayScottSearchPeriodicOrbit;

struct _GrayScottSearchPeriodicOrbitClass {
  CmdSearchPeriodicOrbitClass parent;
};

struct _GrayScottSearchPeriodicOrbit {
  CmdSearchPeriodicOrbit parent;
};

#define TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT (gray_scott_search_periodic_orbit_get_type ())
#define GRAY_SCOTT_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT, GrayScottSearchPeriodicOrbit))
#define GRAY_SCOTT_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT, GrayScottSearchPeriodicOrbitClass))
#define IS_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT))
#define IS_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT))
#define GRAY_SCOTT_SEARCH_PERIODIC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT, GrayScottSearchPeriodicOrbitClass))

GType gray_scott_search_periodic_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottSearchPeriodicOrbit, gray_scott_search_periodic_orbit, TYPE_CMD_SEARCH_PERIODIC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_search_periodic_orbit_init (GrayScottSearchPeriodicOrbit *self)
{
}

static void gray_scott_search_periodic_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_VERBOSE] = TRUE;
  /* option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_REMOVE_DUPLICATION] = TRUE; */
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_SORT_PERIODIC_ORBITS] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NEWTON_ERROR_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PRACTICAL_ERROR_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_PERIOD] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PERIOD_RANGE] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void gray_scott_search_process_data (gpointer ptr_self)
{
  CmdSearchPeriodicOrbitOptionData *app_option_data;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(gray_scott_search_periodic_orbit_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_search_periodic_orbit_set_dimension(ptr_self, system_option_data->division * 2);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
}

static void gray_scott_search_periodic_orbit_class_init (GrayScottSearchPeriodicOrbitClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->process_data = gray_scott_search_process_data;
  command_class->set_default_value = gray_scott_search_periodic_orbit_set_default_value;
}

gpointer gray_scott_search_periodic_orbit_new (void)
{
  GrayScottSearchPeriodicOrbit *gray_scott_search_periodic_orbit;
  gray_scott_search_periodic_orbit = command_new(TYPE_GRAY_SCOTT_SEARCH_PERIODIC_ORBIT, NULL, NULL);
  command_short_description_set(gray_scott_search_periodic_orbit, "-- search for periodic orbits of Gray-Scott model");
  command_summary_set(gray_scott_search_periodic_orbit, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_search_periodic_orbit;
}
