#include "miscellaneous_system_interface.h"

typedef struct _GrayScottParameterContinuationClass GrayScottParameterContinuationClass;
typedef struct _GrayScottParameterContinuation GrayScottParameterContinuation;

struct _GrayScottParameterContinuationClass {
  CmdParameterContinuationClass parent;
};

struct _GrayScottParameterContinuation {
  CmdParameterContinuation parent;
};

#define TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION (gray_scott_parameter_continuation_get_type ())
#define GRAY_SCOTT_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION, GrayScottParameterContinuation))
#define GRAY_SCOTT_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION, GrayScottParameterContinuationClass))
#define IS_GRAY_SCOTT_PARAMETER_CONTINUATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION))
#define IS_GRAY_SCOTT_PARAMETER_CONTINUATION_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION))
#define GRAY_SCOTT_PARAMETER_CONTINUATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION, GrayScottParameterContinuationClass))

GType gray_scott_parameter_continuation_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottParameterContinuation, gray_scott_parameter_continuation, TYPE_CMD_PARAMETER_CONTINUATION,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_parameter_continuation_init (GrayScottParameterContinuation *self)
{
}

static void gray_scott_parameter_continuation_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdParameterContinuationOptionData *option_data;
  option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_VERBOSE] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_KEY] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DIRECTION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_FIXED] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_STEP_LENGTH_ADJUSTED] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_MAX_ARC_LENGTH] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_GMRES_CORRECTOR_MAX_ITERATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_PARAMETER_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_MAX_ITERATION] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NEWTON_METHOD] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_INITIAL_EQUILIBRIUM_WITH_DIM] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[PARAMETER_CONTINUATION_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void gray_scott_parameter_continuation_process_data (gpointer ptr_self)
{
  CmdParameterContinuationOptionData *app_option_data;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_PARAMETER_CONTINUATION(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(gray_scott_parameter_continuation_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_parameter_continuation_set_dimension(ptr_self, system_option_data->division * 2);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
}

static void gray_scott_parameter_continuation_class_init (GrayScottParameterContinuationClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = gray_scott_parameter_continuation_set_default_value;
  command_class->process_data = gray_scott_parameter_continuation_process_data;
}

gpointer gray_scott_parameter_continuation_new (void)
{
  GrayScottParameterContinuation *gray_scott_parameter_continuation;
  gray_scott_parameter_continuation = command_new(TYPE_GRAY_SCOTT_PARAMETER_CONTINUATION, NULL, NULL);
  command_short_description_set(gray_scott_parameter_continuation, "-- Parameter continuation of Gray-Scott model");
  command_summary_set(gray_scott_parameter_continuation, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_parameter_continuation;
}
