#include "miscellaneous_system_interface.h"

typedef struct _PointVortexInPlaneRayleighRitzClass PointVortexInPlaneRayleighRitzClass;
typedef struct _PointVortexInPlaneRayleighRitz PointVortexInPlaneRayleighRitz;

struct _PointVortexInPlaneRayleighRitzClass {
  CmdRayleighRitzClass parent;
};

struct _PointVortexInPlaneRayleighRitz {
  CmdRayleighRitz parent;
};

#define TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ (point_vortex_in_plane_rayleigh_ritz_get_type ())
#define POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ, PointVortexInPlaneRayleighRitz))
#define POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ, PointVortexInPlaneRayleighRitzClass))
#define IS_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ))
#define IS_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ))
#define POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ, PointVortexInPlaneRayleighRitzClass))

GType point_vortex_in_plane_rayleigh_ritz_get_type ();

G_DEFINE_TYPE_WITH_CODE(PointVortexInPlaneRayleighRitz, point_vortex_in_plane_rayleigh_ritz, TYPE_CMD_RAYLEIGH_RITZ,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, point_vortex_in_plane_interface_init))

static void point_vortex_in_plane_rayleigh_ritz_init (PointVortexInPlaneRayleighRitz *self)
{
}

static void point_vortex_in_plane_rayleigh_ritz_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_VERBOSE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_DIMENSION_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SIMULTANEOUS_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_ARNOLDI_METHOD] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_FROM_JACOBIAN] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SORT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void point_vortex_in_plane_rayleigh_ritz_process_data (gpointer ptr_self)
{
  CmdRayleighRitzOptionData *app_option_data;
  PointVortexInPlaneSystemOptionData *system_option_data;
  app_option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(point_vortex_in_plane_rayleigh_ritz_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension == 0 || (app_option_data->dimension != system_option_data->number_circulations * 2)) {
    fprintf(stderr, "Initial points or circulations are not valid\n");
    abort();
  }
}

static void point_vortex_in_plane_rayleigh_ritz_class_init (PointVortexInPlaneRayleighRitzClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = point_vortex_in_plane_rayleigh_ritz_set_default_value;
  command_class->process_data = point_vortex_in_plane_rayleigh_ritz_process_data;
}

gpointer point_vortex_in_plane_rayleigh_ritz_new (void)
{
  PointVortexInPlaneRayleighRitz *point_vortex_in_plane_rayleigh_ritz;
  point_vortex_in_plane_rayleigh_ritz = command_new(TYPE_POINT_VORTEX_IN_PLANE_RAYLEIGH_RITZ, NULL, NULL);
  command_short_description_set(point_vortex_in_plane_rayleigh_ritz, "-- Rayleigh-Ritz method of point vortex model");
  command_summary_set(point_vortex_in_plane_rayleigh_ritz, POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION);
  return point_vortex_in_plane_rayleigh_ritz;
}
