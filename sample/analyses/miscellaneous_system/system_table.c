#include "system_table.h"

static SystemTableElement *system_table_list = NULL;

static SystemTableElement *system_table_find (char *name, SystemTableElement *table)
{
  int i;
  SystemTableElement *table_element = NULL;
  if (name != NULL) {
    i = 0;
    while (table[i].name != NULL) {
      if (g_ascii_strcasecmp(name, table[i].name) == 0) {
        table_element = table + i;
        break;
      }
      i += 1;
    }
  }
  if (!table_element) {
    fprintf(stderr, "Invalid name of system: %s\n", name);
    exit(1);
  }
  return table_element;
}

static int system_table_object_create_index (char *basename)
{
  int index;
  if (g_strcmp0(basename, "calculate_orbit") == 0) {
    index = SYSTEM_OBJECT_CREATE_CALCULATE_ORBIT;
  } else if (g_strcmp0(basename, "search_equilibrium") == 0) {
    index = SYSTEM_OBJECT_CREATE_SEARCH_EQUILIBRIUM;
  } else if (g_strcmp0(basename, "search_periodic_orbit") == 0) {
    index = SYSTEM_OBJECT_CREATE_SEARCH_PERIODIC_POINT;
  } else if (g_strcmp0(basename, "lyapunov_analysis") == 0) {
    index = SYSTEM_OBJECT_CREATE_LYAPUNOV_ANALYSIS;
  } else if (g_strcmp0(basename, "rayleigh_ritz") == 0) {
    index = SYSTEM_OBJECT_CREATE_RAYLEIGH_RITZ;
  } else if (g_strcmp0(basename, "parameter_continuation") == 0) {
    index = SYSTEM_OBJECT_CREATE_PARAMETER_CONTINUATION;
  } else {
    index = -1;
  }
  return index;
}

static gpointer (* system_table_object_create (SystemTableElement *table_element, char *program_path)) ()
{
  gchar *basename = subcommand_list_get_basename(program_path);
  int index = system_table_object_create_index(basename);
  if (index < 0) {
    fprintf(stderr, "Not support program \"%s\"\n", basename);
    abort();
  }
  g_free(basename);
  return table_element->object_create[index];
}

static int system_table_command_execute (int argc, char **argv, SystemTableElement *table)
{
  gpointer ptr_object;
  MathFluidARGV *mathfluid_argv;
  SystemTableElement *table_element;
  char *name;
  mathfluid_argv = mathfluid_argv_alloc_parse(argc, argv, TRUE, 1);
  name = mathfluid_argv_take_out(mathfluid_argv, 1);
  table_element = system_table_find(name, table);
  ptr_object = (system_table_object_create(table_element, argv[0]))();
  command_process_command_line_arguments(ptr_object, mathfluid_argv);
  command_dynamical_system_create(ptr_object);
  command_execute(ptr_object);
  g_object_unref(ptr_object);
  mathfluid_argv_free(mathfluid_argv);
  return 0;
}

static int system_table_execute (int argc, char **argv)
{
  return system_table_command_execute(argc, argv, system_table_list);
}

void system_table_init (char *program_path, SystemTableElement *table)
{
  if (system_table_list != NULL) {
    fprintf(stderr, "Call system_table_init twice\n");
    abort();
  }
  system_table_list = table;
  gchar *basename = subcommand_list_get_basename(program_path);
  int index = system_table_object_create_index(basename);
  if (index < 0) {
    fprintf(stderr, "Not support program \"%s\"\n", basename);
    abort();
  }
  SystemTableElement *table_ptr;
  int i = 0;
  table_ptr = table;
  while ((table_ptr != NULL) && (table_ptr->name != NULL)) {
    if (table_ptr->object_create[index] != NULL) {
      subcommand_list_global_register_subcommand(basename, table_ptr->name, system_table_execute);
    }
    i += 1;
    table_ptr = table + i;
  }
  g_free(basename);
}
