#include "point_vortex_in_plane_interface.h"

#define POINT_VORTEX_IN_PLANE_ERROR_EVOLUTION 1e-8
#define POINT_VORTEX_IN_PLANE_INITIAL_STEP_SIZE 1e-10
#define M_1_2PI (M_1_PI / 2.0)

/* Prepare data that is used to calculate vector field and its jacobian:
   x_j - x_k, y_j - y_k, and (x_j - x_k)^2 + (y_j - y_k)^2 */
static void point_vortex_alloc_data_array (GArray **ptr_subtract, GArray **ptr_square, const double x[], GenericParameter *params)
{
  int j, k, jind, kind;
  *ptr_subtract = g_array_sized_new(FALSE, FALSE, sizeof(double), generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) * (generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) - 1));
  *ptr_square = g_array_sized_new(FALSE, FALSE, sizeof(double), generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) * (generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) - 1) / 2);
  for (j = 0; j < generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS); j++) {
    for (k = j + 1; k < generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS); k++) {
      double subtract_vals[2], square_val;
      jind = j * 2;
      kind = k * 2;
      subtract_vals[0] = x[jind] - x[kind];
      subtract_vals[1] = x[jind + 1] - x[kind + 1];
      g_array_append_vals(*ptr_subtract, subtract_vals, 2);
      square_val = subtract_vals[0] * subtract_vals[0] + subtract_vals[1] * subtract_vals[1];
      g_array_append_val(*ptr_square, square_val);
    }
  }
}

/* Return index number of ptr_square of point_vortex_alloc_data_array. */
static int point_vortex_data_square_index (int n, int j, int k)
{
  if (j > k) {
    int i;
    i = j;
    j = k;
    k = i;
  }
  return (k - j - 1) + (2 * n - j - 1) * j / 2;
}

static double point_vortex_data_subtract_get (GArray *subtract, int ind, int j, int k)
{
  if (j < k) {
    return g_array_index(subtract, double, ind);
  } else {
    return -g_array_index(subtract, double, ind);
  }
}

/* Calculate C_{jk} that is used to calculate vector field and its jacobian:
   \Gamma_k / (x_j - x_k)^2 + (y_j - y_k)^2 */
static double point_vortex_data_calculate_cjk (GenericParameter *params, GArray *square, int j, int k, int square_ind)
{
  return generic_parameter_double(params, k) / g_array_index(square, double, square_ind);
}

static int point_vortex_in_plane_function (double f[], const double x[], GenericParameter *params)
{
  int j, k, jind, num_circulations;
  GArray *subtract, *square;
  num_circulations = generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS);
  point_vortex_alloc_data_array(&subtract, &square, x, params);
  for (j = 0; j < generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS); j++) {
    jind = j * 2;
    f[jind] = 0.0;
    f[jind + 1] = 0.0;
    for (k = 0; k < generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS); k++) {
      if (j != k) {
        int ind = point_vortex_data_square_index(num_circulations, j, k);
        double val = point_vortex_data_calculate_cjk(params, square, j, k, ind);
        f[jind] += val * point_vortex_data_subtract_get(subtract, 2 * ind + 1, j, k);
        f[jind + 1] += val * point_vortex_data_subtract_get(subtract, 2 * ind, j, k);
      }
    }
    f[jind] = -f[jind] * M_1_2PI;
    f[jind + 1] = f[jind + 1] * M_1_2PI;
  }
  g_array_free(subtract, TRUE);
  g_array_free(square, TRUE);
  return GSL_SUCCESS;
}

static int point_vortex_in_plane_function_time_reversing (double f[], const double x[], GenericParameter *params)
{
  int n, i;
  point_vortex_in_plane_function(f, x, params);
  n = generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) * 2;
  for (i = 0; i < n; i++) {
    f[i] = -f[i];
  }
  return GSL_SUCCESS;
}

/* Return index number of cjk array. This function is used only in point_vortex_in_plane_jacobian. */
static int point_vortex_in_plane_cjk_index (int n, int j, int k)
{
  int index;
  index = (n - 1) * j + k;
  if (j < k) {
    index -= 1;
  }
  return index;
}

static int point_vortex_in_plane_jacobian (double *dfdx, const double x[], GenericParameter *params)
{
  int i, j, k, num_circulations, size_array_cjk;
  GArray *subtract, *square, *array_cjk, *array_cjk_differential;
  gsl_matrix_view dfdx_mat;
  gsl_matrix *m;

  point_vortex_alloc_data_array(&subtract, &square, x, params);

  num_circulations = generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS);
  size_array_cjk = num_circulations * (num_circulations - 1);
  array_cjk = g_array_sized_new(FALSE, FALSE, sizeof(double), size_array_cjk);
  array_cjk_differential = g_array_sized_new(FALSE, FALSE, sizeof(double), 2 * size_array_cjk);
  for (j = 0; j < num_circulations; j++) {
    for (k = 0; k < num_circulations; k++) {
      if (j != k) {
        double cjk, cjk_differential[2], val;
        int index_square = point_vortex_data_square_index(num_circulations, j, k);
        cjk = point_vortex_data_calculate_cjk(params, square, j, k, point_vortex_data_square_index(num_circulations, j, k));
        g_array_append_val(array_cjk, cjk);
        val = -2.0 * cjk / g_array_index(square, double, index_square);
        /* C_{jk} differentiated by x_j */
        cjk_differential[0] = val * point_vortex_data_subtract_get(subtract, index_square * 2, j, k) ;
        /* C_{jk} differentiated by y_j */
        cjk_differential[1] = val * point_vortex_data_subtract_get(subtract, index_square * 2 + 1, j, k);
        g_array_append_vals(array_cjk_differential, cjk_differential, 2);
      }
    }
  }

  dfdx_mat = gsl_matrix_view_array(dfdx, generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_DIMENSION), generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_DIMENSION));
  m = &dfdx_mat.matrix;

  /* j is related to the loop of coordinates of vectors
     i is related to the loop of partial differentials */
  for (j = 0; j < num_circulations; j++) {
    int index_x_crd, index_y_crd;
    index_x_crd = 2 * j;
    index_y_crd = index_x_crd + 1;
    for (i = 0; i < num_circulations; i++) {
      if (i == j) {
        double dxdx = 0.0, dxdy = 0.0, dydx = 0.0, dydy = 0.0;
        for (k = 0; k < num_circulations; k++) {
          if (j != k) {
            int index_square = point_vortex_data_square_index(num_circulations, j, k);
            int index_cjk = point_vortex_in_plane_cjk_index(num_circulations, j, k);
            double cjk_diff_x, cjk_diff_y, subtract_x, subtract_y, cjk;
            cjk_diff_x = g_array_index(array_cjk_differential, double, index_cjk * 2);
            cjk_diff_y = g_array_index(array_cjk_differential, double, index_cjk * 2 + 1);
            subtract_x = point_vortex_data_subtract_get(subtract, 2 * index_square, j, k);
            subtract_y = point_vortex_data_subtract_get(subtract, 2 * index_square + 1, j, k);
            cjk = g_array_index(array_cjk, double, index_cjk);
            dxdx += cjk_diff_x * subtract_y;
            dxdy += cjk_diff_y * subtract_y + cjk;
            dydx += cjk_diff_x * subtract_x + cjk;
            dydy += cjk_diff_y * subtract_x;
          }
        }
        gsl_matrix_set(m, index_x_crd, 2 * i, -M_1_2PI * dxdx);
        gsl_matrix_set(m, index_x_crd, 2 * i + 1, -M_1_2PI * dxdy);
        gsl_matrix_set(m, index_y_crd, 2 * i, M_1_2PI * dydx);
        gsl_matrix_set(m, index_y_crd, 2 * i + 1, M_1_2PI * dydy);
      } else {
        int index_square = point_vortex_data_square_index(num_circulations, j, i);
        int index_cjk = point_vortex_in_plane_cjk_index(num_circulations, j, i);
        double cji_diff_x, cji_diff_y, subtract_x, subtract_y, cji;
        cji_diff_x = g_array_index(array_cjk_differential, double, index_cjk * 2);
        cji_diff_y = g_array_index(array_cjk_differential, double, index_cjk * 2 + 1);
        subtract_x = point_vortex_data_subtract_get(subtract, 2 * index_square, j, i);
        subtract_y = point_vortex_data_subtract_get(subtract, 2 * index_square + 1, j, i);
        cji = g_array_index(array_cjk, double, point_vortex_in_plane_cjk_index(num_circulations, j, i));
        gsl_matrix_set(m, index_x_crd, 2 * i, M_1_2PI * cji_diff_x * subtract_y);
        gsl_matrix_set(m, index_x_crd, 2 * i + 1, M_1_2PI * (cji_diff_y * subtract_y + cji));
        gsl_matrix_set(m, index_y_crd, 2 * i, -M_1_2PI * (cji_diff_x * subtract_x + cji));
        gsl_matrix_set(m, index_y_crd, 2 * i + 1, -M_1_2PI * cji_diff_y * subtract_x);
      }
    }
  }

  g_array_free(subtract, TRUE);
  g_array_free(square, TRUE);
  g_array_free(array_cjk, TRUE);
  g_array_free(array_cjk_differential, TRUE);
  return GSL_SUCCESS;
}

static int point_vortex_in_plane_jacobian_time_reversing (double *dfdx, const double x[], GenericParameter *params)
{
  int n, i;
  point_vortex_in_plane_jacobian(dfdx, x, params);
  n = generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) * generic_parameter_int(params, POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS) * 4;
  for (i = 0; i < n; i++) {
    dfdx[i] = -dfdx[i];
  }
  return GSL_SUCCESS;
}

static void point_vortex_in_plane_system_evolution_error_set (DynamicalSystem *dynamical_system, double error, double initial_step_size)
{
  SystemODESolver *ode_solver;
  ode_solver = ((SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, 0))->solver;
  system_ode_solver_set_driver(ode_solver, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk4, initial_step_size, error, 0.0);
}

static DynamicalSystem *point_vortex_in_plane_system_alloc (PointVortexInPlaneSystemOptionData *system_option_data)
{
  SystemODE *system_ode;
  SystemODESolver *solver;
  GenericParameter *parameters;
  int array_int[POINT_VORTEX_IN_PLANE_N_INT_PRMS];
  int (* time_derivative) (double *dydt, const double *y, DataContainer *params);
  int (* jacobian) (double *dfdy, const double *y, DataContainer *params);
  array_int[POINT_VORTEX_IN_PLANE_PRM_DIMENSION] = system_option_data->number_circulations * 2;
  array_int[POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS] = system_option_data->number_circulations;
  parameters = generic_parameter_alloc();
  generic_parameter_set_double(parameters, system_option_data->number_circulations, system_option_data->circulations, NULL);
  generic_parameter_set_int(parameters, POINT_VORTEX_IN_PLANE_N_INT_PRMS, array_int, NULL);
  if (system_option_data->time_reversing) {
    time_derivative = point_vortex_in_plane_function_time_reversing;
    jacobian = point_vortex_in_plane_jacobian_time_reversing;
  } else {
    time_derivative = point_vortex_in_plane_function;
    jacobian = point_vortex_in_plane_jacobian;
  }
  system_ode = system_ode_alloc(array_int[POINT_VORTEX_IN_PLANE_PRM_DIMENSION], time_derivative, jacobian, NULL, parameters, NULL, NULL);
  solver = system_ode_solver_alloc(system_ode);
  return dynamical_system_ode_alloc(solver, TRUE, TRUE);
}

static DynamicalSystem *point_vortex_in_plane_dynamical_system_create (gpointer system_option_data)
{
  DynamicalSystem *dynamical_system;
  PointVortexInPlaneSystemOptionData *option_data;
  option_data = (PointVortexInPlaneSystemOptionData *) system_option_data;
  dynamical_system = point_vortex_in_plane_system_alloc(option_data);
  point_vortex_in_plane_system_evolution_error_set(dynamical_system, option_data->error_evolution, option_data->initial_step_size);
  if (option_data->use_numerical_differential) {
    dynamical_system_ode_system_ode(dynamical_system)->jacobian_of_time_derivative = NULL;
  }
  return dynamical_system;
}

static gpointer point_vortex_in_plane_option_data_alloc (gpointer cmdinit_optional_args)
{
  PointVortexInPlaneSystemOptionData *option_data;
  option_data = (PointVortexInPlaneSystemOptionData *) g_malloc(sizeof(PointVortexInPlaneSystemOptionData));
  option_data->string_circurations = NULL;
  option_data->circulations = NULL;
  option_data->error_evolution = POINT_VORTEX_IN_PLANE_ERROR_EVOLUTION;
  option_data->initial_step_size = POINT_VORTEX_IN_PLANE_INITIAL_STEP_SIZE;
  option_data->use_numerical_differential = FALSE;
  option_data->time_reversing = FALSE;
  return option_data;
}

static void point_vortex_in_plane_option_data_process (gpointer system_option_data)
{
  PointVortexInPlaneSystemOptionData *option_data = (PointVortexInPlaneSystemOptionData *) system_option_data;
  if (option_data->string_circurations) {
    option_data->circulations = mathfluid_utils_split_convert_string_numbers(&option_data->number_circulations, option_data->string_circurations);
  }
}

static void point_vortex_in_plane_option_data_free (gpointer system_option_data)
{
  PointVortexInPlaneSystemOptionData *option_data;
  option_data = (PointVortexInPlaneSystemOptionData *) system_option_data;
  if (option_data->string_circurations) {
    g_free(option_data->string_circurations);
  }
  if (option_data->circulations) {
    g_free(option_data->circulations);
  }
  g_free(option_data);
}

static void point_vortex_in_plane_command_line_arguments_define (CmdOpts *cmd_opts, gpointer system_option_data)
{
  PointVortexInPlaneSystemOptionData *option_data;
  option_data = (PointVortexInPlaneSystemOptionData *) system_option_data;
  cmd_opts_system_option_alloc_add(cmd_opts, "circulation", 0, 0, G_OPTION_ARG_STRING, &option_data->string_circurations, "Comma separated circulations", "NUMS");
  cmd_opts_system_option_alloc_add(cmd_opts, "error-evolution", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_evolution, "Error of evolution", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "initial-step-size", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->initial_step_size, "Initial step size of evolution", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "use-numerical-differential", 0, 0, G_OPTION_ARG_NONE, &option_data->use_numerical_differential, "Use numerical differentials of Jacobian", NULL);
  cmd_opts_system_option_alloc_add(cmd_opts, "time-reversing", 0, 0, G_OPTION_ARG_NONE, &option_data->time_reversing, "Reversing time evolution", NULL);
}

static void point_vortex_in_plane_interface_header_json_save (JsonBuilder *builder, gpointer system_option_data)
{
  int i;
  PointVortexInPlaneSystemOptionData *option_data;
  option_data = (PointVortexInPlaneSystemOptionData *) system_option_data;
  json_builder_set_member_name(builder, "name");
  json_builder_add_string_value(builder, "Point vortex");
  json_builder_set_member_name(builder, "circulation");
  json_builder_begin_array(builder);
  for (i = 0; i < option_data->number_circulations; i++) {
    json_builder_add_double_value(builder, option_data->circulations[i]);
  }
  json_builder_end_array(builder);
  json_builder_set_member_name(builder, "error_evolution");
  json_builder_add_double_value(builder, option_data->error_evolution);
  json_builder_set_member_name(builder, "initial_step_size");
  json_builder_add_double_value(builder, option_data->initial_step_size);
}

void point_vortex_in_plane_interface_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = point_vortex_in_plane_dynamical_system_create;
  iface->option_data_alloc = point_vortex_in_plane_option_data_alloc;
  iface->option_data_process = point_vortex_in_plane_option_data_process;
  iface->option_data_free = point_vortex_in_plane_option_data_free;
  iface->command_line_arguments_define = point_vortex_in_plane_command_line_arguments_define;
  iface->header_json_save = point_vortex_in_plane_interface_header_json_save;
}
