#include "miscellaneous_system_interface.h"

static SystemTableElement system_table[] = {
  { "gray-scott",
    { gray_scott_calc_orbit_new,
      gray_scott_search_equilibrium_new,
      gray_scott_search_periodic_orbit_new,
      gray_scott_lyapunov_analysis_new,
      gray_scott_rayleigh_ritz_new,
      gray_scott_parameter_continuation_new
    }
  },
  { "example01",
    { example01_calc_orbit_new,
      example01_search_equilibrium_new,
      NULL,
      NULL,
      NULL,
      NULL
    }
  },
  { "point-vortex",
    { point_vortex_in_plane_calc_orbit_new,
      point_vortex_in_plane_search_equilibrium_new,
      point_vortex_in_plane_search_periodic_orbit_new,
      point_vortex_in_plane_lyapunov_analysis_new,
      point_vortex_in_plane_rayleigh_ritz_new,
      NULL
    }
  },
  { NULL }
};

void miscellaneous_system_init (char *program_path)
{
  system_table_init(program_path, system_table);
}
