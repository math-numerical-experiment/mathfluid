#include "gray_scott_interface.h"

#define GRAY_SCOTT_ERROR_EVOLUTION 1e-8
#define GRAY_SCOTT_INITIAL_STEP_SIZE 1e-10

typedef struct {
  double *f;
  const double *x;
  GenericParameter *params;
} GrayScottThreadArgs;

static char *gray_scott_param_names[] = { "Du", "Dv", "F", "k", "L", NULL };

static inline double gray_scott_u_differential (double u_prev, double u_cur, double u_next, double v_cur, double *prms, double Du_over_dx2, double uvv)
{
  return Du_over_dx2 * (u_next - 2.0 * u_cur + u_prev) - uvv + prms[GRAY_SCOTT_PRM_F] * (1.0 - u_cur);
}

static inline double gray_scott_v_differential (double v_prev, double v_cur, double v_next, double u_cur, double *prms, double Dv_over_dx2, double uvv)
{
  return Dv_over_dx2 * (v_next - 2.0 * v_cur + v_prev) + uvv - (prms[GRAY_SCOTT_PRM_F] + prms[GRAY_SCOTT_PRM_K]) * v_cur;
}

static void gray_scott_function_each_thread (int task_number, int total_task_number, gpointer ptr_thread_args)
{
  int i, num;
  double *prms, *f, Du_over_dx2, Dv_over_dx2, uvv;;
  const double *x;
  GrayScottThreadArgs *thread_args;
  thread_args = (GrayScottThreadArgs *) ptr_thread_args;
  num = generic_parameter_int(thread_args->params, GRAY_SCOTT_PRM_DIVISION);
  prms = generic_parameter_array_double(thread_args->params);
  Du_over_dx2 = prms[GRAY_SCOTT_PRM_DU] / prms[GRAY_SCOTT_PRM_DX2];
  Dv_over_dx2 = prms[GRAY_SCOTT_PRM_DV] / prms[GRAY_SCOTT_PRM_DX2];
  f = thread_args->f;
  x = thread_args->x;
  /* f[0...num], x[0...num]: u */
  /* f[num...(2 * num)], x[num...num]: v */

  if (task_number == 0) {
    if (generic_parameter_int(thread_args->params, GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE) == GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION) {
      uvv = x[0] * x[num] * x[num];
      f[0] = gray_scott_u_differential(x[num - 1], x[0], x[1], x[num], prms, Du_over_dx2, uvv);
      f[num] = gray_scott_v_differential(x[2 * num - 1], x[num], x[num + 1], x[0], prms, Dv_over_dx2, uvv);
      uvv = x[num - 1] * x[2 * num - 1] * x[2 * num - 1];
      f[num - 1] = gray_scott_u_differential(x[num - 2], x[num - 1], x[0], x[2 * num - 1], prms, Du_over_dx2, uvv);
      f[2 * num - 1] = gray_scott_v_differential(x[2 * num - 2], x[2 * num - 1], x[num], x[num - 1], prms, Dv_over_dx2, uvv);
    } else if (generic_parameter_int(thread_args->params, GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE) == GRAY_SCOTT_HOMOGENEOUS_NEUMANN_BOUNDARY_CONDITION) {
      uvv = x[0] * x[num] * x[num];
      f[0] = Du_over_dx2 * (x[1] - x[0]) - uvv + prms[GRAY_SCOTT_PRM_F] * (1.0 - x[0]);
      f[num] = Dv_over_dx2 * (x[num + 1] - x[num]) + uvv - (prms[GRAY_SCOTT_PRM_F] + prms[GRAY_SCOTT_PRM_K]) * x[num];
      uvv = x[num - 1] * x[2 * num - 1] * x[2 * num - 1];
      f[num - 1] = Du_over_dx2 * (x[num - 2] - x[num - 1]) - uvv + prms[GRAY_SCOTT_PRM_F] * (1.0 - x[num - 1]);
      f[2 * num - 1] = Dv_over_dx2 * (x[2 * num - 2] - x[2 * num - 1]) + uvv - (prms[GRAY_SCOTT_PRM_F] + prms[GRAY_SCOTT_PRM_K]) * x[2 * num - 1];
    } else {
      fprintf(stderr, "Invalid boundary condition\n");
      abort();
    }
    i = total_task_number;
  } else {
    i = task_number;
  }
  for (; i < num - 1; i += total_task_number) {
    uvv = x[i] * x[num + i] * x[num + i];
    f[i] = gray_scott_u_differential(x[i - 1], x[i], x[i + 1], x[num + i], prms, Du_over_dx2, uvv);
    f[num + i] = gray_scott_v_differential(x[num + i - 1], x[num + i], x[num + i + 1], x[i], prms, Dv_over_dx2, uvv);
  }
}

static int gray_scott_function_normal (double f[], const double x[], GenericParameter *params)
{
  int i, num;
  double *prms, Du_over_dx2, Dv_over_dx2, uvv;
  num = generic_parameter_int(params, GRAY_SCOTT_PRM_DIVISION);
  prms = generic_parameter_array_double(params);
  Du_over_dx2 = prms[GRAY_SCOTT_PRM_DU] / prms[GRAY_SCOTT_PRM_DX2];
  Dv_over_dx2 = prms[GRAY_SCOTT_PRM_DV] / prms[GRAY_SCOTT_PRM_DX2];
  /* f[0...num], x[0...num]: u */
  /* f[num...(2 * num)], x[num...num]: v */

  for (i = 1; i < num - 1; i++) {
    uvv = x[i] * x[num + i] * x[num + i];
    f[i] = gray_scott_u_differential(x[i - 1], x[i], x[i + 1], x[num + i], prms, Du_over_dx2, uvv);
    f[num + i] = gray_scott_v_differential(x[num + i - 1], x[num + i], x[num + i + 1], x[i], prms, Dv_over_dx2, uvv);
  }

  if (generic_parameter_int(params, GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE) == GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION) {
    uvv = x[0] * x[num] * x[num];
    f[0] = gray_scott_u_differential(x[num - 1], x[0], x[1], x[num], prms, Du_over_dx2, uvv);
    f[num] = gray_scott_v_differential(x[2 * num - 1], x[num], x[num + 1], x[0], prms, Dv_over_dx2, uvv);
    uvv = x[num - 1] * x[2 * num - 1] * x[2 * num - 1];
    f[num - 1] = gray_scott_u_differential(x[num - 2], x[num - 1], x[0], x[2 * num - 1], prms, Du_over_dx2, uvv);
    f[2 * num - 1] = gray_scott_v_differential(x[2 * num - 2], x[2 * num - 1], x[num], x[num - 1], prms, Dv_over_dx2, uvv);
  } else if (generic_parameter_int(params, GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE) == GRAY_SCOTT_HOMOGENEOUS_NEUMANN_BOUNDARY_CONDITION) {
    uvv = x[0] * x[num] * x[num];
    f[0] = Du_over_dx2 * (x[1] - x[0]) - uvv + prms[GRAY_SCOTT_PRM_F] * (1.0 - x[0]);
    f[num] = Dv_over_dx2 * (x[num + 1] - x[num]) + uvv - (prms[GRAY_SCOTT_PRM_F] + prms[GRAY_SCOTT_PRM_K]) * x[num];
    uvv = x[num - 1] * x[2 * num - 1] * x[2 * num - 1];
    f[num - 1] = Du_over_dx2 * (x[num - 2] - x[num - 1]) - uvv + prms[GRAY_SCOTT_PRM_F] * (1.0 - x[num - 1]);
    f[2 * num - 1] = Dv_over_dx2 * (x[2 * num - 2] - x[2 * num - 1]) + uvv - (prms[GRAY_SCOTT_PRM_F] + prms[GRAY_SCOTT_PRM_K]) * x[2 * num - 1];
  } else {
    fprintf(stderr, "Invalid boundary condition\n");
    abort();
  }
  return GSL_SUCCESS;
}

static int gray_scott_function (double f[], const double x[], GenericParameter *params)
{
  if (generic_parameter_int(params, GRAY_SCOTT_PRM_THREAD_EVOLUTION)) {
    MathFluidThreadPool *thread_pool;
    thread_pool = mathfluid_thread_pool_get();
    if (thread_pool) {
      GrayScottThreadArgs thread_args[1] = { { f, x, params } };
      GArray *signal_id;
      signal_id = mathfluid_thread_pool_push_tasks(thread_pool->max_thread_number, gray_scott_function_each_thread, thread_args);
      mathfluid_thread_pool_wait_tasks(signal_id);
      g_array_free(signal_id, TRUE);
      return GSL_SUCCESS;
    }
  }
  return gray_scott_function_normal(f, x, params);
}

static void gray_scott_system_evolution_error_set (DynamicalSystem *dynamical_system, double error, double initial_step_size)
{
  SystemODESolver *ode_solver;
  ode_solver = ((SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, 0))->solver;
  system_ode_solver_set_driver(ode_solver, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk4, initial_step_size, error, 0.0);
}

static DynamicalSystem *gray_scott_system_alloc (GrayScottSystemOptionData *system_params)
{
  SystemODE *system_ode;
  SystemODESolver *solver;
  GenericParameter *parameters;
  int dim, array_int[GRAY_SCOTT_N_INT_PRMS];
  dim = system_params->division * 2;
  array_int[GRAY_SCOTT_PRM_DIVISION] = system_params->division;
  array_int[GRAY_SCOTT_PRM_THREAD_EVOLUTION] = (int) system_params->thread_evolution;
  array_int[GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE] = system_params->boundary_condition_type;
  parameters = generic_parameter_alloc();
  generic_parameter_set_double(parameters, GRAY_SCOTT_N_DOUBLE_PRMS, system_params->params, gray_scott_param_names);
  generic_parameter_set_int(parameters, GRAY_SCOTT_N_INT_PRMS, array_int, NULL);
  system_ode = system_ode_alloc(dim, gray_scott_function, NULL, NULL, parameters, generic_parameter_set, generic_parameter_get);
  solver = system_ode_solver_alloc(system_ode);
  return dynamical_system_ode_alloc(solver, TRUE, TRUE);
}

static DynamicalSystem *gray_scott_dynamical_system_create (gpointer ptr_option_data)
{
  DynamicalSystem *dynamical_system;
  GrayScottSystemOptionData *option_data;
  option_data = (GrayScottSystemOptionData *) ptr_option_data;
  option_data->params[GRAY_SCOTT_PRM_DX] = option_data->params[GRAY_SCOTT_PRM_L] / ((double) option_data->division - 1.0);
  option_data->params[GRAY_SCOTT_PRM_DX2] = option_data->params[GRAY_SCOTT_PRM_DX] * option_data->params[GRAY_SCOTT_PRM_DX];
  if (option_data->periodic_boundary_condition) {
    option_data->boundary_condition_type = GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION;
  } else if (option_data->homogeneous_neumann_condition) {
    option_data->boundary_condition_type = GRAY_SCOTT_HOMOGENEOUS_NEUMANN_BOUNDARY_CONDITION;
  }
  dynamical_system = gray_scott_system_alloc(option_data);
  gray_scott_system_evolution_error_set(dynamical_system, option_data->error_evolution, option_data->initial_step_size);
  return dynamical_system;
}

static gpointer gray_scott_option_data_alloc ()
{
  GrayScottSystemOptionData *option_data;
  option_data = (GrayScottSystemOptionData *) g_malloc(sizeof(GrayScottSystemOptionData));
  option_data->division = 200;
  option_data->boundary_condition_type = GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION;
  option_data->params[GRAY_SCOTT_PRM_DU] = 2.0e-5;
  option_data->params[GRAY_SCOTT_PRM_DV] = 1.0e-5;
  option_data->params[GRAY_SCOTT_PRM_F] = 0.04;
  option_data->params[GRAY_SCOTT_PRM_K] = 0.06075;
  option_data->params[GRAY_SCOTT_PRM_L] = 1.6;
  option_data->error_evolution = GRAY_SCOTT_ERROR_EVOLUTION;
  option_data->initial_step_size = GRAY_SCOTT_INITIAL_STEP_SIZE;
  option_data->periodic_boundary_condition = FALSE;
  option_data->homogeneous_neumann_condition = FALSE;
  option_data->thread_evolution = FALSE;
  return option_data;
}

static void gray_scott_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_option_data)
{
  GrayScottSystemOptionData *option_data;
  option_data = (GrayScottSystemOptionData *) ptr_option_data;
  cmd_opts_system_option_alloc_add(cmd_opts, "Du", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[GRAY_SCOTT_PRM_DU], "Parameter Du", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "Dv", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[GRAY_SCOTT_PRM_DV], "Parameter Dv", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "F", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[GRAY_SCOTT_PRM_F], "Parameter F", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "k", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[GRAY_SCOTT_PRM_K], "Parameter k", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "L", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[GRAY_SCOTT_PRM_L], "Parameter L", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "divide", 0, 0, G_OPTION_ARG_INT, &option_data->division, "Division number of U and V", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "error-evolution", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_evolution, "Error of evolution", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "initial-step-size", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->initial_step_size, "Initial step size of evolution", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "periodic-boundary", 0, 0, G_OPTION_ARG_NONE, &option_data->periodic_boundary_condition, "Impose periodic boundary condition", NULL);
  cmd_opts_system_option_alloc_add(cmd_opts, "homogeneous-neumann-boundary", 0, 0, G_OPTION_ARG_NONE, &option_data->homogeneous_neumann_condition, "Impose homogeneous Neumann boundary condition", NULL);
  cmd_opts_system_option_alloc_add(cmd_opts, "thread-evolution", 0, 0, G_OPTION_ARG_NONE, &option_data->thread_evolution, "Use thread to evolve", NULL);
}

static void gray_scott_interface_header_json_save (JsonBuilder *builder, gpointer ptr_option_data)
{
  int i;
  GrayScottSystemOptionData *option_data;
  option_data = (GrayScottSystemOptionData *) ptr_option_data;
  json_builder_set_member_name(builder, "name");
  json_builder_add_string_value(builder, "Gray-Scott");
  json_builder_set_member_name(builder, "division");
  json_builder_add_int_value(builder, option_data->division);
  json_builder_set_member_name(builder, "params");
  json_builder_begin_array(builder);
  for (i = 0; i < GRAY_SCOTT_N_DOUBLE_PRMS; i++) {
    json_builder_add_double_value(builder, option_data->params[i]);
  }
  json_builder_end_array(builder);
  json_builder_set_member_name(builder, "error_evolution");
  json_builder_add_double_value(builder, option_data->error_evolution);
  json_builder_set_member_name(builder, "initial_step_size");
  json_builder_add_double_value(builder, option_data->initial_step_size);
  json_builder_set_member_name(builder, "boundary_condition");
  if (option_data->boundary_condition_type == GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION) {
    json_builder_add_string_value(builder, "periodic_boundary_condition");
  } else if (option_data->boundary_condition_type == GRAY_SCOTT_HOMOGENEOUS_NEUMANN_BOUNDARY_CONDITION) {
    json_builder_add_string_value(builder, "homogeneous_Neumann_boundary_condition");
  } else {
    json_builder_add_string_value(builder, "undefined");
  }
}

void gray_scott_interface_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = gray_scott_dynamical_system_create;
  iface->option_data_alloc = gray_scott_option_data_alloc;
  iface->option_data_free = g_free;
  iface->command_line_arguments_define = gray_scott_command_line_arguments_define;
  iface->header_json_save = gray_scott_interface_header_json_save;
}

void gray_scott_initial_value_set (double *point, int division)
{
  int i;
  /* TODO: Change initial value by an option */
  for (i = 0; i < division; i++) {
    point[i] = 1.0;
    if (fabs(i - division / 2.0) < 5.0) {
      point[division + i] = 3.0;
    } else {
      point[division + i] = 0.0;
    }
  }
}
