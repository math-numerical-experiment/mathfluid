#include "miscellaneous_system_interface.h"

typedef struct _GrayScottLyapunovAnalysisClass GrayScottLyapunovAnalysisClass;
typedef struct _GrayScottLyapunovAnalysis GrayScottLyapunovAnalysis;

struct _GrayScottLyapunovAnalysisClass {
  CmdLyapunovAnalysisClass parent;
};

struct _GrayScottLyapunovAnalysis {
  CmdLyapunovAnalysis parent;
};

#define TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS (gray_scott_lyapunov_analysis_get_type ())
#define GRAY_SCOTT_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS, GrayScottLyapunovAnalysis))
#define GRAY_SCOTT_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS, GrayScottLyapunovAnalysisClass))
#define IS_GRAY_SCOTT_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS))
#define IS_GRAY_SCOTT_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS))
#define GRAY_SCOTT_LYAPUNOV_ANALYSIS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS, GrayScottLyapunovAnalysisClass))

GType gray_scott_lyapunov_analysis_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottLyapunovAnalysis, gray_scott_lyapunov_analysis, TYPE_CMD_LYAPUNOV_ANALYSIS,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_lyapunov_analysis_init (GrayScottLyapunovAnalysis *self)
{
}

static void gray_scott_lyapunov_analysis_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  option_data->time_step = 0.01;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_VERBOSE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_LOAD_ARGUMENTS] = TRUE;
  COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_THREAD] = TRUE;
}

static void gray_scott_lyapunov_analysis_process_data (gpointer ptr_self)
{
  CmdLyapunovAnalysisOptionData *app_option_data;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(gray_scott_lyapunov_analysis_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_lyapunov_analysis_set_dimension(ptr_self, system_option_data->division * 2);
    gray_scott_initial_value_set(app_option_data->initial_point, system_option_data->division);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
}

static void gray_scott_lyapunov_analysis_class_init (GrayScottLyapunovAnalysisClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = gray_scott_lyapunov_analysis_set_default_value;
  command_class->process_data = gray_scott_lyapunov_analysis_process_data;
}

gpointer gray_scott_lyapunov_analysis_new (void)
{
  GrayScottLyapunovAnalysis *gray_scott_lyapunov_analysis;
  gray_scott_lyapunov_analysis = command_new(TYPE_GRAY_SCOTT_LYAPUNOV_ANALYSIS, NULL, NULL);
  command_short_description_set(gray_scott_lyapunov_analysis, "-- Lyapunov analysis of Gray-Scott model");
  command_summary_set(gray_scott_lyapunov_analysis, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_lyapunov_analysis;
}
