#ifndef _EXAMPLE01_H_
#define _EXAMPLE01_H_

#include "../command_common/command_common.h"

#define EXAMPLE01_SYSTEM_DESCRIPTION "  System is given by the following:\n\
    u' = u + u_{xx}"

enum {
  EXAMPLE01_PRM_L,
  EXAMPLE01_PRM_DX,
  EXAMPLE01_PRM_DX2,
  EXAMPLE01_N_DOUBLE_PRMS
};

enum {
  EXAMPLE01_PRM_DIVISION,
  EXAMPLE01_N_INT_PRMS
};

typedef struct {
  int division;
  double params[EXAMPLE01_N_DOUBLE_PRMS];
  double error_evolution;
  double initial_step_size;
} Example01SystemOptionData;

typedef struct {
  int division;
  double parameters[EXAMPLE01_N_DOUBLE_PRMS];
} Example01SystemODEParams;

void example01_interface_init (SystemDefineInterface *iface);
void example01_initial_value_set (double *point, int division);

#endif /* _EXAMPLE01_H_ */
