#include "example01_interface.h"

#define EXAMPLE01_ERROR_EVOLUTION 1e-8
#define EXAMPLE01_INITIAL_STEP_SIZE 1e-10

static char *example01_param_names[] = { NULL };

static int example01_function (double f[], const double x[], GenericParameter *params)
{
  int i;
  for (i = 1; i < generic_parameter_int(params, EXAMPLE01_PRM_DIVISION) - 1; i++) {
    f[i] = x[i] + (x[i - 1] + x[i + 1] - 2.0 * x[i]) / generic_parameter_double(params, EXAMPLE01_PRM_DX2);
  }
  f[0] = x[0] + (x[generic_parameter_int(params, EXAMPLE01_PRM_DIVISION) - 2] + x[1] - 2.0 * x[0]) / generic_parameter_double(params, EXAMPLE01_PRM_DX2);
  f[generic_parameter_int(params, EXAMPLE01_PRM_DIVISION) - 1] = f[0];
  return GSL_SUCCESS;
}

static void example01_system_evolution_error_set (DynamicalSystem *dynamical_system, double error, double initial_step_size)
{
  SystemODESolver *ode_solver;
  ode_solver = ((SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, 0))->solver;
  system_ode_solver_set_driver(ode_solver, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk4, initial_step_size, error, 0.0);
}

static DynamicalSystem *example01_system_alloc (Example01SystemOptionData *system_params)
{
  SystemODE *system_ode;
  SystemODESolver *solver;
  GenericParameter *parameters;
  int array_int[EXAMPLE01_N_INT_PRMS];
  array_int[EXAMPLE01_PRM_DIVISION] = system_params->division;
  parameters = generic_parameter_alloc();
  generic_parameter_set_double(parameters, EXAMPLE01_N_DOUBLE_PRMS, system_params->params, example01_param_names);
  generic_parameter_set_int(parameters, EXAMPLE01_N_INT_PRMS, array_int, NULL);
  system_ode = system_ode_alloc(array_int[EXAMPLE01_PRM_DIVISION], example01_function, NULL, NULL, parameters, generic_parameter_set, generic_parameter_get);
  solver = system_ode_solver_alloc(system_ode);
  return dynamical_system_ode_alloc(solver, TRUE, TRUE);
}

static DynamicalSystem *example01_dynamical_system_create (gpointer ptr_option_data)
{
  DynamicalSystem *dynamical_system;
  Example01SystemOptionData *option_data;
  option_data = (Example01SystemOptionData *) ptr_option_data;
  option_data->params[EXAMPLE01_PRM_DX] = option_data->params[EXAMPLE01_PRM_L] / ((double) option_data->division - 1.0);
  option_data->params[EXAMPLE01_PRM_DX2] = option_data->params[EXAMPLE01_PRM_DX] * option_data->params[EXAMPLE01_PRM_DX];
  dynamical_system = example01_system_alloc(option_data);
  example01_system_evolution_error_set(dynamical_system, option_data->error_evolution, option_data->initial_step_size);
  return dynamical_system;
}

static gpointer example01_option_data_alloc ()
{
  Example01SystemOptionData *option_data;
  option_data = (Example01SystemOptionData *) g_malloc(sizeof(Example01SystemOptionData));
  option_data->params[EXAMPLE01_PRM_L] = 2.0 * M_PI;
  option_data->division = 100;
  option_data->error_evolution = EXAMPLE01_ERROR_EVOLUTION;
  option_data->initial_step_size = EXAMPLE01_INITIAL_STEP_SIZE;
  return option_data;
}

static void example01_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_option_data)
{
  Example01SystemOptionData *option_data;
  option_data = (Example01SystemOptionData *) ptr_option_data;
  cmd_opts_system_option_alloc_add(cmd_opts, "L", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->params[EXAMPLE01_PRM_L], "Parameter L", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "divide", 0, 0, G_OPTION_ARG_INT, &option_data->division, "Division number of U and V", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "error-evolution", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->error_evolution, "Error of evolution", "NUM");
  cmd_opts_system_option_alloc_add(cmd_opts, "initial-step-size", 0, 0, G_OPTION_ARG_DOUBLE, &option_data->initial_step_size, "Initial step size of evolution", "NUM");
}

static void example01_interface_header_json_save (JsonBuilder *builder, gpointer ptr_option_data)
{
  int i;
  Example01SystemOptionData *option_data;
  option_data = (Example01SystemOptionData *) ptr_option_data;
  json_builder_set_member_name(builder, "name");
  json_builder_add_string_value(builder, "example01");
  json_builder_set_member_name(builder, "division");
  json_builder_add_int_value(builder, option_data->division);
  json_builder_set_member_name(builder, "params");
  json_builder_begin_array(builder);
  for (i = 0; i < EXAMPLE01_N_DOUBLE_PRMS; i++) {
    json_builder_add_double_value(builder, option_data->params[i]);
  }
  json_builder_end_array(builder);
  json_builder_set_member_name(builder, "error_evolution");
  json_builder_add_double_value(builder, option_data->error_evolution);
  json_builder_set_member_name(builder, "initial_step_size");
  json_builder_add_double_value(builder, option_data->initial_step_size);
}

void example01_interface_init (SystemDefineInterface *iface)
{
  iface->dynamical_system_create = example01_dynamical_system_create;
  iface->option_data_alloc = example01_option_data_alloc;
  iface->option_data_free = g_free;
  iface->command_line_arguments_define = example01_command_line_arguments_define;
  iface->header_json_save = example01_interface_header_json_save;
}

void example01_initial_value_set (double *point, int division)
{
  int i;
  /* TODO: Change initial value by an option */
  for (i = 0; i < division; i++) {
    if (fabs(i - division / 2.0) < 5.0) {
      point[i] = 3.0;
    } else {
      point[i] = 0.0;
    }
  }
}
