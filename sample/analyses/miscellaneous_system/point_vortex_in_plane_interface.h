#ifndef _POINT_VORTEX_IN_PLANE_INTERFACE_H_
#define _POINT_VORTEX_IN_PLANE_INTERFACE_H_

#include "../command_common/command_common.h"

#define POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION "  System is given by the following:\n\
    dz_j / dt = 1 / (2 \\pi i) \\sum \\Gamma_k / (z_j - z_k)"

enum {
  POINT_VORTEX_IN_PLANE_PRM_DIMENSION,
  POINT_VORTEX_IN_PLANE_PRM_NUMBER_CIRCULATIONS,
  POINT_VORTEX_IN_PLANE_N_INT_PRMS
};

typedef struct {
  char* string_circurations;
  int number_circulations;
  double *circulations;
  double error_evolution;
  double initial_step_size;
  gboolean use_numerical_differential;
  gboolean time_reversing;
} PointVortexInPlaneSystemOptionData;

void point_vortex_in_plane_interface_init (SystemDefineInterface *iface);

#endif /* _POINT_VORTEX_IN_PLANE_INTERFACE_H_ */
