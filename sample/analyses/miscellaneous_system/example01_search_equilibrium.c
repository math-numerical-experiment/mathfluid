#include "miscellaneous_system_interface.h"

typedef struct _Example01SearchEquilibriumClass Example01SearchEquilibriumClass;
typedef struct _Example01SearchEquilibrium Example01SearchEquilibrium;

struct _Example01SearchEquilibriumClass {
  CmdSearchEquilibriumClass parent;
};

struct _Example01SearchEquilibrium {
  CmdSearchEquilibrium parent;
};

#define TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM (example01_search_equilibrium_get_type ())
#define EXAMPLE01_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM, Example01SearchEquilibrium))
#define EXAMPLE01_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM, Example01SearchEquilibriumClass))
#define IS_EXAMPLE01_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM))
#define IS_EXAMPLE01_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM))
#define EXAMPLE01_SEARCH_EQUILIBRIUM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM, Example01SearchEquilibriumClass))

GType example01_search_equilibrium_get_type ();

G_DEFINE_TYPE_WITH_CODE(Example01SearchEquilibrium, example01_search_equilibrium, TYPE_CMD_SEARCH_EQUILIBRIUM,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, example01_interface_init))

static void example01_search_equilibrium_init (Example01SearchEquilibrium *self)
{
}

static void example01_search_equilibrium_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_VERBOSE] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void example01_search_process_data (gpointer ptr_self)
{
  CmdSearchEquilibriumOptionData *app_option_data;
  Example01SystemOptionData *system_option_data;
  app_option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(example01_search_equilibrium_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_search_equilibrium_set_dimension(ptr_self, system_option_data->division);
  } else {
    system_option_data->division = app_option_data->dimension;
  }
}

static void example01_search_equilibrium_class_init (Example01SearchEquilibriumClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = example01_search_equilibrium_set_default_value;
  command_class->process_data = example01_search_process_data;
}

gpointer example01_search_equilibrium_new (void)
{
  Example01SearchEquilibrium *example01_search_equilibrium;
  example01_search_equilibrium = command_new(TYPE_EXAMPLE01_SEARCH_EQUILIBRIUM, NULL, NULL);
  command_short_description_set(example01_search_equilibrium, "-- search for equilibria");
  command_summary_set(example01_search_equilibrium, EXAMPLE01_SYSTEM_DESCRIPTION);
  return example01_search_equilibrium;
}
