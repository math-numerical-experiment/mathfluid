#ifndef _SYSTEM_TABLE_H_
#define _SYSTEM_TABLE_H_

#include "../command_common/command_common.h"

enum {
  SYSTEM_OBJECT_CREATE_CALCULATE_ORBIT,
  SYSTEM_OBJECT_CREATE_SEARCH_EQUILIBRIUM,
  SYSTEM_OBJECT_CREATE_SEARCH_PERIODIC_POINT,
  SYSTEM_OBJECT_CREATE_LYAPUNOV_ANALYSIS,
  SYSTEM_OBJECT_CREATE_RAYLEIGH_RITZ,
  SYSTEM_OBJECT_CREATE_PARAMETER_CONTINUATION,
  SYSTEM_OBJECT_CREATE_N
};

typedef struct {
  const char *name;
  gpointer (*object_create[SYSTEM_OBJECT_CREATE_N]) ();
} SystemTableElement;

void system_table_init (char *program_path, SystemTableElement *table);

#endif /* _SYSTEM_TABLE_H_ */
