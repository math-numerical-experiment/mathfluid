#include "miscellaneous_system_interface.h"

typedef struct _PointVortexInPlaneSearchEquilibriumClass PointVortexInPlaneSearchEquilibriumClass;
typedef struct _PointVortexInPlaneSearchEquilibrium PointVortexInPlaneSearchEquilibrium;

struct _PointVortexInPlaneSearchEquilibriumClass {
  CmdSearchEquilibriumClass parent;
};

struct _PointVortexInPlaneSearchEquilibrium {
  CmdSearchEquilibrium parent;
};

#define TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM (point_vortex_in_plane_search_equilibrium_get_type ())
#define POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM, PointVortexInPlaneSearchEquilibrium))
#define POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM, PointVortexInPlaneSearchEquilibriumClass))
#define IS_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM))
#define IS_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM))
#define POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM, PointVortexInPlaneSearchEquilibriumClass))

GType point_vortex_in_plane_search_equilibrium_get_type ();

G_DEFINE_TYPE_WITH_CODE(PointVortexInPlaneSearchEquilibrium, point_vortex_in_plane_search_equilibrium, TYPE_CMD_SEARCH_EQUILIBRIUM,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, point_vortex_in_plane_interface_init))

static void point_vortex_in_plane_search_equilibrium_init (PointVortexInPlaneSearchEquilibrium *self)
{
}

static void point_vortex_in_plane_search_equilibrium_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_VERBOSE] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void point_vortex_in_plane_search_process_data (gpointer ptr_self)
{
  CmdSearchEquilibriumOptionData *app_option_data;
  PointVortexInPlaneSystemOptionData *system_option_data;
  app_option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(point_vortex_in_plane_search_equilibrium_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension == 0 || (app_option_data->dimension != system_option_data->number_circulations * 2)) {
    fprintf(stderr, "Initial points or circulations are not valid\n");
    abort();
  }
}

static void point_vortex_in_plane_search_equilibrium_class_init (PointVortexInPlaneSearchEquilibriumClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = point_vortex_in_plane_search_equilibrium_set_default_value;
  command_class->process_data = point_vortex_in_plane_search_process_data;
}

gpointer point_vortex_in_plane_search_equilibrium_new (void)
{
  PointVortexInPlaneSearchEquilibrium *point_vortex_in_plane_search_equilibrium;
  point_vortex_in_plane_search_equilibrium = command_new(TYPE_POINT_VORTEX_IN_PLANE_SEARCH_EQUILIBRIUM, NULL, NULL);
  command_short_description_set(point_vortex_in_plane_search_equilibrium, "-- search for equilibria of point vortex model");
  command_summary_set(point_vortex_in_plane_search_equilibrium, POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION);
  return point_vortex_in_plane_search_equilibrium;
}
