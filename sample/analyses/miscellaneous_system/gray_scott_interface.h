#ifndef _GRAY_SCOTT_H_
#define _GRAY_SCOTT_H_

#include "../command_common/command_common.h"

#define GRAY_SCOTT_SYSTEM_DESCRIPTION "  System is given by the following:\n\
    u_t = Du \\Delta u - u v^2 + F(1 - u)\n\
    v_t = Dv \\Delta v + u v^2 - (F + k)v"

enum {
  GRAY_SCOTT_PRM_DU,
  GRAY_SCOTT_PRM_DV,
  GRAY_SCOTT_PRM_F,
  GRAY_SCOTT_PRM_K,
  GRAY_SCOTT_PRM_L,
  GRAY_SCOTT_PRM_DX,
  GRAY_SCOTT_PRM_DX2,
  GRAY_SCOTT_N_DOUBLE_PRMS
};

enum {
  GRAY_SCOTT_PRM_BOUNDARY_CONDITION_TYPE,
  GRAY_SCOTT_PRM_DIVISION,
  GRAY_SCOTT_PRM_THREAD_EVOLUTION,
  GRAY_SCOTT_N_INT_PRMS
};

enum {
  GRAY_SCOTT_PERIODIC_BOUNDARY_CONDITION,
  GRAY_SCOTT_HOMOGENEOUS_NEUMANN_BOUNDARY_CONDITION
};

typedef struct {
  int division;
  int boundary_condition_type;
  double params[GRAY_SCOTT_N_DOUBLE_PRMS];
  double error_evolution;
  double initial_step_size;
  gboolean periodic_boundary_condition;
  gboolean homogeneous_neumann_condition;
  gboolean thread_evolution;
} GrayScottSystemOptionData;

void gray_scott_interface_init (SystemDefineInterface *iface);
void gray_scott_initial_value_set (double *point, int division);

#endif /* _GRAY_SCOTT_H_ */
