#include "miscellaneous_system_interface.h"

typedef struct _Example01CalcOrbitClass Example01CalcOrbitClass;
typedef struct _Example01CalcOrbit Example01CalcOrbit;

struct _Example01CalcOrbitClass {
  CmdCalcOrbitClass parent;
};

struct _Example01CalcOrbit {
  CmdCalcOrbit parent;

  int output_point_number;
};

#define TYPE_EXAMPLE01_CALC_ORBIT (example01_calc_orbit_get_type ())
#define EXAMPLE01_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_EXAMPLE01_CALC_ORBIT, Example01CalcOrbit))
#define EXAMPLE01_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_EXAMPLE01_CALC_ORBIT, Example01CalcOrbitClass))
#define IS_EXAMPLE01_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_EXAMPLE01_CALC_ORBIT))
#define IS_EXAMPLE01_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_EXAMPLE01_CALC_ORBIT))
#define EXAMPLE01_CALC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_EXAMPLE01_CALC_ORBIT, Example01CalcOrbitClass))

GType example01_calc_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(Example01CalcOrbit, example01_calc_orbit, TYPE_CMD_CALC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, example01_interface_init))

static void example01_calc_orbit_init (Example01CalcOrbit *self)
{
  self->output_point_number = -1;
}

static void example01_calc_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  option_data->time_max = 100.0;
  option_data->time_step = 0.01;
  option_data->active_options[CALC_ORBIT_OPTION_VECTOR_FIELD] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_MAX] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void example01_calc_orbit_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  Example01CalcOrbit *self;
  self = EXAMPLE01_CALC_ORBIT(ptr_self);
  cmd_opts_application_option_alloc_add(cmd_opts, "number-output-point", 0, 0, G_OPTION_ARG_INT, &self->output_point_number, "Number of output of points", "NUM");
  COMMAND_CLASS(example01_calc_orbit_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void example01_calc_orbit_process_data (gpointer ptr_self)
{
  CmdCalcOrbitOptionData *app_option_data;
  Example01SystemOptionData *system_option_data;
  /* Example01CalcOrbit *self_example01; */
  /* self_example01 = EXAMPLE01_CALC_ORBIT(ptr_self); */
  app_option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(example01_calc_orbit_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_calc_orbit_set_dimension(ptr_self, system_option_data->division);
    example01_initial_value_set(app_option_data->initial_point, system_option_data->division);
  } else {
    system_option_data->division = app_option_data->dimension;
  }
}

static void example01_calc_orbit_class_init (Example01CalcOrbitClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->command_line_arguments_define = example01_calc_orbit_command_line_arguments_define;
  command_class->set_default_value = example01_calc_orbit_set_default_value;
  command_class->process_data = example01_calc_orbit_process_data;
}

gpointer example01_calc_orbit_new ()
{
  Example01CalcOrbit *example01_calc_orbit;
  example01_calc_orbit = command_new(TYPE_EXAMPLE01_CALC_ORBIT, NULL, NULL);
  command_short_description_set(example01_calc_orbit, "-- calculate orbit of example01");
  command_summary_set(example01_calc_orbit, EXAMPLE01_SYSTEM_DESCRIPTION);
  return example01_calc_orbit;
}
