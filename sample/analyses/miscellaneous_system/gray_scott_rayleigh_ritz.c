#include "miscellaneous_system_interface.h"

typedef struct _GrayScottRayleighRitzClass GrayScottRayleighRitzClass;
typedef struct _GrayScottRayleighRitz GrayScottRayleighRitz;

struct _GrayScottRayleighRitzClass {
  CmdRayleighRitzClass parent;
};

struct _GrayScottRayleighRitz {
  CmdRayleighRitz parent;
};

#define TYPE_GRAY_SCOTT_RAYLEIGH_RITZ (gray_scott_rayleigh_ritz_get_type ())
#define GRAY_SCOTT_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_RAYLEIGH_RITZ, GrayScottRayleighRitz))
#define GRAY_SCOTT_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_RAYLEIGH_RITZ, GrayScottRayleighRitzClass))
#define IS_GRAY_SCOTT_RAYLEIGH_RITZ(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_RAYLEIGH_RITZ))
#define IS_GRAY_SCOTT_RAYLEIGH_RITZ_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_RAYLEIGH_RITZ))
#define GRAY_SCOTT_RAYLEIGH_RITZ_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_RAYLEIGH_RITZ, GrayScottRayleighRitzClass))

GType gray_scott_rayleigh_ritz_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottRayleighRitz, gray_scott_rayleigh_ritz, TYPE_CMD_RAYLEIGH_RITZ,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_rayleigh_ritz_init (GrayScottRayleighRitz *self)
{
}

static void gray_scott_rayleigh_ritz_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdRayleighRitzOptionData *option_data;
  option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_VERBOSE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_DIMENSION_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SIMULTANEOUS_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INVERSE_ITERATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_ARNOLDI_METHOD] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_BACKWARD_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_COVARIANT_LYAPUNOV] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_INITIAL_INVARIANT_SPACE] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SORT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[RAYLEIGH_RITZ_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void gray_scott_rayleigh_ritz_process_data (gpointer ptr_self)
{
  CmdRayleighRitzOptionData *app_option_data;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_RAYLEIGH_RITZ(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(gray_scott_rayleigh_ritz_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_rayleigh_ritz_set_dimension(ptr_self, system_option_data->division * 2);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
}

static void gray_scott_rayleigh_ritz_class_init (GrayScottRayleighRitzClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = gray_scott_rayleigh_ritz_set_default_value;
  command_class->process_data = gray_scott_rayleigh_ritz_process_data;
}

gpointer gray_scott_rayleigh_ritz_new (void)
{
  GrayScottRayleighRitz *gray_scott_rayleigh_ritz;
  gray_scott_rayleigh_ritz = command_new(TYPE_GRAY_SCOTT_RAYLEIGH_RITZ, NULL, NULL);
  command_short_description_set(gray_scott_rayleigh_ritz, "-- Rayleigh-Ritz method of Gray-Scott model");
  command_summary_set(gray_scott_rayleigh_ritz, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_rayleigh_ritz;
}
