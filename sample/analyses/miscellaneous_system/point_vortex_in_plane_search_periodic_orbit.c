#include "miscellaneous_system_interface.h"

typedef struct _PointVortexInPlaneSearchPeriodicOrbitClass PointVortexInPlaneSearchPeriodicOrbitClass;
typedef struct _PointVortexInPlaneSearchPeriodicOrbit PointVortexInPlaneSearchPeriodicOrbit;

struct _PointVortexInPlaneSearchPeriodicOrbitClass {
  CmdSearchPeriodicOrbitClass parent;
};

struct _PointVortexInPlaneSearchPeriodicOrbit {
  CmdSearchPeriodicOrbit parent;
};

#define TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT (point_vortex_in_plane_search_periodic_orbit_get_type ())
#define POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT, PointVortexInPlaneSearchPeriodicOrbit))
#define POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT, PointVortexInPlaneSearchPeriodicOrbitClass))
#define IS_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT))
#define IS_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT))
#define POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT, PointVortexInPlaneSearchPeriodicOrbitClass))

GType point_vortex_in_plane_search_periodic_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(PointVortexInPlaneSearchPeriodicOrbit, point_vortex_in_plane_search_periodic_orbit, TYPE_CMD_SEARCH_PERIODIC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, point_vortex_in_plane_interface_init))

static void point_vortex_in_plane_search_periodic_orbit_init (PointVortexInPlaneSearchPeriodicOrbit *self)
{
}

static void point_vortex_in_plane_search_periodic_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchPeriodicOrbitOptionData *option_data;
  option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_VERBOSE] = TRUE;
  /* option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_REMOVE_DUPLICATION] = TRUE; */
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_SORT_PERIODIC_ORBITS] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NEWTON_ERROR_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PRACTICAL_ERROR_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_INITIAL_PERIOD] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_PERIOD_RANGE] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_PERIODIC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void point_vortex_in_plane_search_process_data (gpointer ptr_self)
{
  CmdSearchPeriodicOrbitOptionData *app_option_data;
  PointVortexInPlaneSystemOptionData *system_option_data;
  app_option_data = CMD_SEARCH_PERIODIC_ORBIT(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(point_vortex_in_plane_search_periodic_orbit_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension == 0 || (app_option_data->dimension != system_option_data->number_circulations * 2)) {
    fprintf(stderr, "Initial points or circulations are not valid\n");
    abort();
  }
}

static void point_vortex_in_plane_search_periodic_orbit_class_init (PointVortexInPlaneSearchPeriodicOrbitClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->process_data = point_vortex_in_plane_search_process_data;
  command_class->set_default_value = point_vortex_in_plane_search_periodic_orbit_set_default_value;
}

gpointer point_vortex_in_plane_search_periodic_orbit_new (void)
{
  PointVortexInPlaneSearchPeriodicOrbit *point_vortex_in_plane_search_periodic_orbit;
  point_vortex_in_plane_search_periodic_orbit = command_new(TYPE_POINT_VORTEX_IN_PLANE_SEARCH_PERIODIC_ORBIT, NULL, NULL);
  command_short_description_set(point_vortex_in_plane_search_periodic_orbit, "-- search for periodic orbits of point vortex model");
  command_summary_set(point_vortex_in_plane_search_periodic_orbit, POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION);
  return point_vortex_in_plane_search_periodic_orbit;
}
