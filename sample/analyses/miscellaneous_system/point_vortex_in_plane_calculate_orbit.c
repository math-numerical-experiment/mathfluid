#include "miscellaneous_system_interface.h"

typedef struct _PointVortexInPlaneCalcOrbitClass PointVortexInPlaneCalcOrbitClass;
typedef struct _PointVortexInPlaneCalcOrbit PointVortexInPlaneCalcOrbit;

struct _PointVortexInPlaneCalcOrbitClass {
  CmdCalcOrbitClass parent;
};

struct _PointVortexInPlaneCalcOrbit {
  CmdCalcOrbit parent;
};

#define TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT (point_vortex_in_plane_calc_orbit_get_type ())
#define POINT_VORTEX_IN_PLANE_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT, PointVortexInPlaneCalcOrbit))
#define POINT_VORTEX_IN_PLANE_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT, PointVortexInPlaneCalcOrbitClass))
#define IS_POINT_VORTEX_IN_PLANE_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT))
#define IS_POINT_VORTEX_IN_PLANE_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT))
#define POINT_VORTEX_IN_PLANE_CALC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT, PointVortexInPlaneCalcOrbitClass))

GType point_vortex_in_plane_calc_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(PointVortexInPlaneCalcOrbit, point_vortex_in_plane_calc_orbit, TYPE_CMD_CALC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, point_vortex_in_plane_interface_init))

static void point_vortex_in_plane_calc_orbit_init (PointVortexInPlaneCalcOrbit *self)
{
}

static void point_vortex_in_plane_calc_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  option_data->time_max = 100.0;
  option_data->time_step = 0.01;
  option_data->active_options[CALC_ORBIT_OPTION_VECTOR_FIELD] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_MAX] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void point_vortex_in_plane_calc_orbit_process_data (gpointer ptr_self)
{
  CmdCalcOrbitOptionData *app_option_data;
  PointVortexInPlaneSystemOptionData *system_option_data;
  /* PointVortexInPlaneCalcOrbit *self_point_vortex_in_plane; */
  /* self_point_vortex_in_plane = POINT_VORTEX_IN_PLANE_CALC_ORBIT(ptr_self); */
  app_option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(point_vortex_in_plane_calc_orbit_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension == 0 || (app_option_data->dimension != system_option_data->number_circulations * 2)) {
    fprintf(stderr, "Initial points or circulations are not valid\n");
    abort();
  }
}

static void point_vortex_in_plane_calc_orbit_class_init (PointVortexInPlaneCalcOrbitClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = point_vortex_in_plane_calc_orbit_set_default_value;
  command_class->process_data = point_vortex_in_plane_calc_orbit_process_data;
}

gpointer point_vortex_in_plane_calc_orbit_new (void)
{
  PointVortexInPlaneCalcOrbit *point_vortex_in_plane_calc_orbit;
  point_vortex_in_plane_calc_orbit = command_new(TYPE_POINT_VORTEX_IN_PLANE_CALC_ORBIT, NULL, NULL);
  command_short_description_set(point_vortex_in_plane_calc_orbit, "-- calculate orbit of point vortex model");
  command_summary_set(point_vortex_in_plane_calc_orbit, POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION);
  return point_vortex_in_plane_calc_orbit;
}
