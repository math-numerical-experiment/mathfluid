#include "miscellaneous_system_interface.h"

typedef struct _GrayScottSearchEquilibriumClass GrayScottSearchEquilibriumClass;
typedef struct _GrayScottSearchEquilibrium GrayScottSearchEquilibrium;

struct _GrayScottSearchEquilibriumClass {
  CmdSearchEquilibriumClass parent;
};

struct _GrayScottSearchEquilibrium {
  CmdSearchEquilibrium parent;
};

#define TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM (gray_scott_search_equilibrium_get_type ())
#define GRAY_SCOTT_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM, GrayScottSearchEquilibrium))
#define GRAY_SCOTT_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM, GrayScottSearchEquilibriumClass))
#define IS_GRAY_SCOTT_SEARCH_EQUILIBRIUM(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM))
#define IS_GRAY_SCOTT_SEARCH_EQUILIBRIUM_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM))
#define GRAY_SCOTT_SEARCH_EQUILIBRIUM_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM, GrayScottSearchEquilibriumClass))

GType gray_scott_search_equilibrium_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottSearchEquilibrium, gray_scott_search_equilibrium, TYPE_CMD_SEARCH_EQUILIBRIUM,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_search_equilibrium_init (GrayScottSearchEquilibrium *self)
{
}

static void gray_scott_search_equilibrium_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdSearchEquilibriumOptionData *option_data;
  option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_VERBOSE] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_REMOVE_DUPLICATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SORT_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_ERROR_EQUILIBRIA] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_MAX_ITERATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_DAMPING_PARAMETER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_TIME_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOG_LEVEL] = TRUE;
  option_data->active_options[SEARCH_EQUILIBRIUM_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void gray_scott_search_process_data (gpointer ptr_self)
{
  CmdSearchEquilibriumOptionData *app_option_data;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_SEARCH_EQUILIBRIUM(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(gray_scott_search_equilibrium_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_search_equilibrium_set_dimension(ptr_self, system_option_data->division * 2);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
}

static void gray_scott_search_equilibrium_class_init (GrayScottSearchEquilibriumClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = gray_scott_search_equilibrium_set_default_value;
  command_class->process_data = gray_scott_search_process_data;
}

gpointer gray_scott_search_equilibrium_new (void)
{
  GrayScottSearchEquilibrium *gray_scott_search_equilibrium;
  gray_scott_search_equilibrium = command_new(TYPE_GRAY_SCOTT_SEARCH_EQUILIBRIUM, NULL, NULL);
  command_short_description_set(gray_scott_search_equilibrium, "-- search for equilibria of Gray-Scott model");
  command_summary_set(gray_scott_search_equilibrium, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_search_equilibrium;
}
