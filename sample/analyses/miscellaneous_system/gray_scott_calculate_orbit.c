#include "miscellaneous_system_interface.h"

typedef enum {
  GRAY_SCOTT_OUTPUT_U,
  GRAY_SCOTT_OUTPUT_V,
  GRAY_SCOTT_OUTPUT_BOTH
} OutputFunctionType;

typedef enum {
  OUTPUT_FORMAT_NORMAL,
  OUTPUT_FORMAT_SPLOT
} OutputFormat;

typedef struct _GrayScottCalcOrbitClass GrayScottCalcOrbitClass;
typedef struct _GrayScottCalcOrbit GrayScottCalcOrbit;

struct _GrayScottCalcOrbitClass {
  CmdCalcOrbitClass parent;
};

struct _GrayScottCalcOrbit {
  CmdCalcOrbit parent;

  int output_point_number;
  char *string_output_type;
  char *string_output_format;
  OutputFunctionType output_function_type;
  OutputFormat output_format;
};

#define TYPE_GRAY_SCOTT_CALC_ORBIT (gray_scott_calc_orbit_get_type ())
#define GRAY_SCOTT_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_GRAY_SCOTT_CALC_ORBIT, GrayScottCalcOrbit))
#define GRAY_SCOTT_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_GRAY_SCOTT_CALC_ORBIT, GrayScottCalcOrbitClass))
#define IS_GRAY_SCOTT_CALC_ORBIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_GRAY_SCOTT_CALC_ORBIT))
#define IS_GRAY_SCOTT_CALC_ORBIT_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_GRAY_SCOTT_CALC_ORBIT))
#define GRAY_SCOTT_CALC_ORBIT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_GRAY_SCOTT_CALC_ORBIT, GrayScottCalcOrbitClass))

GType gray_scott_calc_orbit_get_type ();

G_DEFINE_TYPE_WITH_CODE(GrayScottCalcOrbit, gray_scott_calc_orbit, TYPE_CMD_CALC_ORBIT,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, gray_scott_interface_init))

static void gray_scott_calc_orbit_finalize (GObject *gobject)
{
  GrayScottCalcOrbit *self;
  self = GRAY_SCOTT_CALC_ORBIT(gobject);
  if (self->string_output_type) {
    g_free(self->string_output_type);
  }
  if (self->string_output_format) {
    g_free(self->string_output_format);
  }
  G_OBJECT_CLASS(gray_scott_calc_orbit_parent_class)->finalize(gobject);
}

static void gray_scott_calc_orbit_init (GrayScottCalcOrbit *self)
{
  self->output_point_number = -1;
  self->string_output_type = NULL;
  self->string_output_format = NULL;
  self->output_function_type = GRAY_SCOTT_OUTPUT_BOTH;
}

static void gray_scott_calc_orbit_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdCalcOrbitOptionData *option_data;
  option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  option_data->time_max = 100.0;
  option_data->time_step = 0.01;
  option_data->active_options[CALC_ORBIT_OPTION_VECTOR_FIELD] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_PERIODIC_ORBIT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_MAX] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_OUTPUT_TIME_INTERVAL] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[CALC_ORBIT_OPTION_LOAD_ARGUMENTS] = TRUE;
}

static void gray_scott_calc_orbit_command_line_arguments_define (CmdOpts *cmd_opts, gpointer ptr_self)
{
  GrayScottCalcOrbit *self;
  self = GRAY_SCOTT_CALC_ORBIT(ptr_self);
  cmd_opts_application_option_alloc_add(cmd_opts, "output-type", 0, 0, G_OPTION_ARG_STRING, &self->string_output_type, "Set type of output function: v, u or both", "TYPE");
  cmd_opts_application_option_alloc_add(cmd_opts, "output-format", 0, 0, G_OPTION_ARG_STRING, &self->string_output_format, "Set format of output data: normal or splot", "FORMAT");
  cmd_opts_application_option_alloc_add(cmd_opts, "output-point-number", 0, 0, G_OPTION_ARG_INT, &self->output_point_number, "Number of output of points", "NUM");
  COMMAND_CLASS(gray_scott_calc_orbit_parent_class)->command_line_arguments_define(cmd_opts, ptr_self);
}

static void gray_scott_calc_orbit_process_data (gpointer ptr_self)
{
  CmdCalcOrbitOptionData *app_option_data;
  GrayScottCalcOrbit *self_gray_scott;
  GrayScottSystemOptionData *system_option_data;
  app_option_data = CMD_CALC_ORBIT(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  self_gray_scott = GRAY_SCOTT_CALC_ORBIT(ptr_self);
  COMMAND_CLASS(gray_scott_calc_orbit_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension <= 0) {
    cmd_calc_orbit_set_dimension(ptr_self, system_option_data->division * 2);
    gray_scott_initial_value_set(app_option_data->initial_point, system_option_data->division);
  } else {
    if (app_option_data->dimension % 2 == 0) {
      system_option_data->division = app_option_data->dimension / 2;
    } else {
      fprintf(stderr, "Invalid dimension of initial point: dim=%d\n", app_option_data->dimension);
      abort();
    }
  }
  if (self_gray_scott->string_output_type) {
    switch (self_gray_scott->string_output_type[0]) {
    case 'b':
      self_gray_scott->output_function_type = GRAY_SCOTT_OUTPUT_BOTH;
      break;
    case 'u':
      self_gray_scott->output_function_type = GRAY_SCOTT_OUTPUT_U;
      break;
    case 'v':
      self_gray_scott->output_function_type = GRAY_SCOTT_OUTPUT_V;
      break;
    default:
      fprintf(stderr, "Invalid string to specify output function\n");
      exit(1);
    }
  }
  if (self_gray_scott->string_output_format) {
    if (self_gray_scott->string_output_format[0] == 's') {
      self_gray_scott->output_format = OUTPUT_FORMAT_SPLOT;
    }
  }
}

static void print_one_point (GrayScottCalcOrbit *self, double time, double x, double val)
{
  const char *number_format, *number_splitter;
  number_format = command_base_number_format(self);
  number_splitter = command_base_number_splitter(self);
  printf(number_format, time);
  printf("%s", number_splitter);
  printf(number_format, x);
  printf("%s", number_splitter);
  printf(number_format, val);
  printf("\n");
}

static void gray_scott_calc_orbit_print_orbit (gpointer ptr_self, SystemState *state, double *vector_field)
{
  GrayScottCalcOrbit *self;
  GrayScottSystemOptionData *system_option_data;
  int max_point_num, index_start, i;
  double output_interval, output_last, x;
  /* CmdCalcOrbitOptionData *app_option_data; */
  /* app_option_data = CMD_CALC_ORBIT(ptr_self)->option_data; */
  self = GRAY_SCOTT_CALC_ORBIT(ptr_self);
  system_option_data = command_system_option_data(ptr_self);

  if (self->output_function_type == GRAY_SCOTT_OUTPUT_BOTH) {
    max_point_num = system_option_data->division * 2;
    index_start = 0;
  } else {
    max_point_num = system_option_data->division;
    if (self->output_function_type == GRAY_SCOTT_OUTPUT_V) {
      index_start = system_option_data->division;
    } else {
      index_start = 0;
    }
  }
  output_interval = (double) max_point_num / (double) self->output_point_number;
  output_last = output_interval;
  x = 0.0;

  if (self->output_format == OUTPUT_FORMAT_SPLOT) {
    for (i = 0; i < max_point_num - 1; i++) {
      if (output_last >= output_interval) {
        print_one_point(self, state->time, x, state->coordinate[index_start + i]);
        output_last = (output_interval <= 0 ? 0.0 : (output_last - output_interval));
      }
      output_last += 1.0;
      x += system_option_data->params[GRAY_SCOTT_PRM_DX];
    }
    print_one_point(self, state->time, x, state->coordinate[index_start + max_point_num - 1]);
    printf("\n");
  } else {
    const char *number_format, *number_splitter;
    number_format = command_base_number_format(self);
    number_splitter = command_base_number_splitter(self);
    printf(number_format, state->time);
    for (i = 0; i < max_point_num - 1; i++) {
      if (output_last >= output_interval) {
        printf("%s", number_splitter);
        printf(number_format, state->coordinate[index_start + i]);
        output_last = (output_interval <= 0 ? 0.0 : (output_last - output_interval));
      }
      output_last += 1.0;
      x += system_option_data->params[GRAY_SCOTT_PRM_DX];
    }
    printf("%s", number_splitter);
    printf(number_format, state->coordinate[index_start + max_point_num - 1]);
    printf("\n");
  }
}

static void gray_scott_calc_orbit_class_init (GrayScottCalcOrbitClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  CommandClass *command_class = COMMAND_CLASS(klass);
  CmdCalcOrbitClass *cmd_calc_orbit_class = CMD_CALC_ORBIT_CLASS(klass);
  gobject_class->finalize = gray_scott_calc_orbit_finalize;

  command_class->command_line_arguments_define = gray_scott_calc_orbit_command_line_arguments_define;
  command_class->set_default_value = gray_scott_calc_orbit_set_default_value;
  command_class->process_data = gray_scott_calc_orbit_process_data;

  cmd_calc_orbit_class->print_orbit = gray_scott_calc_orbit_print_orbit;
}

gpointer gray_scott_calc_orbit_new (void)
{
  GrayScottCalcOrbit *gray_scott_calc_orbit;
  gray_scott_calc_orbit = command_new(TYPE_GRAY_SCOTT_CALC_ORBIT, NULL, NULL);
  command_short_description_set(gray_scott_calc_orbit, "-- calculate orbit of Gray-Scott model");
  command_summary_set(gray_scott_calc_orbit, GRAY_SCOTT_SYSTEM_DESCRIPTION);
  return gray_scott_calc_orbit;
}
