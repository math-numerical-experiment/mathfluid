#include "miscellaneous_system_interface.h"

typedef struct _PointVortexInPlaneLyapunovAnalysisClass PointVortexInPlaneLyapunovAnalysisClass;
typedef struct _PointVortexInPlaneLyapunovAnalysis PointVortexInPlaneLyapunovAnalysis;

struct _PointVortexInPlaneLyapunovAnalysisClass {
  CmdLyapunovAnalysisClass parent;
};

struct _PointVortexInPlaneLyapunovAnalysis {
  CmdLyapunovAnalysis parent;
};

#define TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS (point_vortex_in_plane_lyapunov_analysis_get_type ())
#define POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS, PointVortexInPlaneLyapunovAnalysis))
#define POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_CAST ((cls), TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS, PointVortexInPlaneLyapunovAnalysisClass))
#define IS_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS))
#define IS_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS_CLASS(cls) (G_TYPE_CHECK_CLASS_TYPE ((cls), TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS))
#define POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS, PointVortexInPlaneLyapunovAnalysisClass))

GType point_vortex_in_plane_lyapunov_analysis_get_type ();

G_DEFINE_TYPE_WITH_CODE(PointVortexInPlaneLyapunovAnalysis, point_vortex_in_plane_lyapunov_analysis, TYPE_CMD_LYAPUNOV_ANALYSIS,
                        G_IMPLEMENT_INTERFACE(TYPE_SYSTEM_DEFINE, point_vortex_in_plane_interface_init))

static void point_vortex_in_plane_lyapunov_analysis_init (PointVortexInPlaneLyapunovAnalysis *self)
{
}

static void point_vortex_in_plane_lyapunov_analysis_set_default_value (gpointer ptr_self, gpointer optional_args)
{
  CmdLyapunovAnalysisOptionData *option_data;
  option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  option_data->time_step = 0.01;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_VERBOSE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_INITIAL_POINT_WITH_DIM] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_SPACE_DERIVATIVE_PERTURBATION] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_FINITE_TIME_COVARIANT_EXPONENT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_BACKWARD_VECTOR] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_COVARIANT_VECTOR_TEST] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_TIME_STEP] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_USE_DATABASE] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_FORMAT] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_NUMBER_SPLITTER] = TRUE;
  option_data->active_options[LYAPUNOV_ANALYSIS_OPTION_LOAD_ARGUMENTS] = TRUE;
  COMMAND_BASE(ptr_self)->option_data->active_options[COMMAND_BASE_OPTION_THREAD] = TRUE;
}

static void point_vortex_in_plane_lyapunov_analysis_process_data (gpointer ptr_self)
{
  CmdLyapunovAnalysisOptionData *app_option_data;
  PointVortexInPlaneSystemOptionData *system_option_data;
  app_option_data = CMD_LYAPUNOV_ANALYSIS(ptr_self)->option_data;
  system_option_data = command_system_option_data(ptr_self);
  COMMAND_CLASS(point_vortex_in_plane_lyapunov_analysis_parent_class)->process_data(ptr_self);
  if (app_option_data->dimension == 0 || (app_option_data->dimension != system_option_data->number_circulations * 2)) {
    fprintf(stderr, "Initial points or circulations are not valid\n");
    abort();
  }
}

static void point_vortex_in_plane_lyapunov_analysis_class_init (PointVortexInPlaneLyapunovAnalysisClass *klass)
{
  CommandClass *command_class = COMMAND_CLASS(klass);
  command_class->set_default_value = point_vortex_in_plane_lyapunov_analysis_set_default_value;
  command_class->process_data = point_vortex_in_plane_lyapunov_analysis_process_data;
}

gpointer point_vortex_in_plane_lyapunov_analysis_new (void)
{
  PointVortexInPlaneLyapunovAnalysis *point_vortex_in_plane_lyapunov_analysis;
  point_vortex_in_plane_lyapunov_analysis = command_new(TYPE_POINT_VORTEX_IN_PLANE_LYAPUNOV_ANALYSIS, NULL, NULL);
  command_short_description_set(point_vortex_in_plane_lyapunov_analysis, "-- Lyapunov analysis of point vortex model");
  command_summary_set(point_vortex_in_plane_lyapunov_analysis, POINT_VORTEX_IN_PLANE_SYSTEM_DESCRIPTION);
  return point_vortex_in_plane_lyapunov_analysis;
}
