#ifndef _MISCELLANEOUS_SYSTEM_INTERFACE_H_
#define _MISCELLANEOUS_SYSTEM_INTERFACE_H_

#include "../command_common/command_common.h"
#include "system_table.h"
#include "gray_scott_interface.h"
#include "example01_interface.h"
#include "point_vortex_in_plane_interface.h"

gpointer gray_scott_calc_orbit_new (void);
gpointer gray_scott_search_equilibrium_new (void);
gpointer gray_scott_search_periodic_orbit_new (void);
gpointer gray_scott_parameter_continuation_new (void);
gpointer gray_scott_lyapunov_analysis_new (void);
gpointer gray_scott_rayleigh_ritz_new (void);

gpointer example01_calc_orbit_new (void);
gpointer example01_search_equilibrium_new (void);

gpointer point_vortex_in_plane_calc_orbit_new (void);
gpointer point_vortex_in_plane_search_equilibrium_new (void);
gpointer point_vortex_in_plane_search_periodic_orbit_new (void);
gpointer point_vortex_in_plane_lyapunov_analysis_new (void);
gpointer point_vortex_in_plane_rayleigh_ritz_new (void);

#endif /* _MISCELLANEOUS_SYSTEM_INTERFACE_H_ */
