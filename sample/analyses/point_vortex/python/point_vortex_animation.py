# -*- coding: utf-8 -*-
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("path_input")
parser.add_argument("path_output", nargs = "?")
parser.add_argument("--xrange")
parser.add_argument("--yrange")
parser.add_argument("--data-time-interval")
parser.add_argument("--animation-interval")
parser.add_argument("--show-point-index", action="store_true")
args = parser.parse_args()

path_input = args.path_input
path_output = args.path_output
data = np.loadtxt(path_input)

graph_xrange = [-10, 10]
graph_yrange = [-10, 10]
data_time_interval = 0.5
animation_interval = 0.1
text_point_ary = None

def parse_string_graph_range(str):
    graph_range = [float(n) for n in str.split(",")]
    if len(graph_range) != 2:
        sys.exit("Range of graph is invalid")
    return graph_range

if args.xrange:
    graph_xrange = parse_string_graph_range(args.xrange)
if args.yrange:
    graph_yrange = parse_string_graph_range(args.yrange)
if args.animation_interval:
    animation_interval = float(args.animation_interval)
if args.data_time_interval:
    data_time_interval = float(args.data_time_interval)

def generate_data():
    time_last = None
    for i, ary in enumerate(data):
        data_time = ary[0]
        if (time_last is None) or (data_time - time_last >= data_time_interval):
            data_ary_x = []
            data_ary_y = []
            for index in range(len(ary)):
                if index % 2 == 1:
                    data_ary_x.append(ary[index])
                    data_ary_y.append(ary[index + 1])
            time_last = data_time
            yield(data_time, data_ary_x, data_ary_y)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xlim(*graph_xrange)
ax.set_ylim(*graph_yrange)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_aspect('equal')
ax.grid()
time_text = ax.text(0.03, 0.95, '', transform=ax.transAxes)

if args.show_point_index:
    text_point_ary = []
    for i in range(0, int((len(data[0]) - 1) / 2)):
        text_point_ary.append(ax.text(0, 0, "%d" % (i + 1), ha="right", va="bottom"))

point, = ax.plot([], [], 'bo', ms=4)

xx, yy = [], []
def func(data_ary):
    time_text.set_text("time: %.2f" % data_ary[0])
    point.set_data(data_ary[1], data_ary[2])
    if not(text_point_ary is None):
        for i, text_point in enumerate(text_point_ary):
            text_point.set_position((data_ary[1][i], data_ary[2][i]))

ani = animation.FuncAnimation(fig, func, generate_data, blit=False, interval=animation_interval * 1000, repeat=False)

if path_output:
    ani.save(path_output)
else:
    plt.show()
