/*
   Examples of usages of RayleighRitz
 */

#include "lorenz.h"

int main (int argc, char *argv[]) {
  DynamicalSystem * ds;
  DataContainer *params;
  gsl_vector *params_vector;
  MatrixVectorProduct *matrix_vector_product;
  ArnoldiMethod *arnoldi_method;
  RayleighRitz *rayleigh_ritz;
  double equilibrium[LORENZ_SYSTEM_DIM] = { 8.48528137423864, 8.48528137423864, 27.00000000000024 };
  double *hessenberg, *invariant_space;
  int i, row_number, column_number, number_eigenvalues;
  double complex *eigenvalue, *eigenvector;

  /*
    Settings of DynamicalSystem
  */
  params_vector = gsl_vector_alloc(LORENZ_N_PRMS);
  gsl_vector_set(params_vector, LORENZ_PRM_SIGMA, 10.0);
  gsl_vector_set(params_vector, LORENZ_PRM_BETA, 8.0 / 3.0);
  gsl_vector_set(params_vector, LORENZ_PRM_RHO, 28.0);
  params = data_container_alloc(1, params_vector, (void *(*) (void *data)) gsl_vector_duplicate, (void (*) (void *data)) gsl_vector_free);
  ds = dynamical_system_alloc(LORENZ_SYSTEM_DIM, params, lorenz_evolve);

  /*
    Define matrix vector product, which is product of Jacobian at an equilibrium and a vector
  */
  matrix_vector_product = jacobian_of_vector_field_product_alloc(ds, equilibrium);

  /*
    Calculate an invariant space obtained by Arnoldi method. In this example, the invariant space is R^3
  */
  arnoldi_method = arnoldi_method_alloc(LORENZ_SYSTEM_DIM, LORENZ_SYSTEM_DIM, matrix_vector_product_duplicate(matrix_vector_product));
  arnoldi_method_set_randomly(arnoldi_method);
  hessenberg = arnoldi_method_iterate_successively(&row_number, &column_number, arnoldi_method);
  invariant_space = arnoldi_method_basis_matrix(arnoldi_method);

  /*
    Configure Rayleigh-Ritz method, in particular, set an invariant manifold and Hessenberg matrix
    from Arnoldi method
  */
  rayleigh_ritz = rayleigh_ritz_alloc(LORENZ_SYSTEM_DIM, matrix_vector_product_duplicate(matrix_vector_product));
  rayleigh_ritz_set_basis(rayleigh_ritz, column_number, invariant_space);
  rayleigh_ritz_set_matrix(rayleigh_ritz, hessenberg);

  /*
    Calculate eigenvalues and eigenvectors by Rayleigh-Ritz method
  */
  number_eigenvalues = rayleigh_ritz_eigen(&eigenvalue, &eigenvector, rayleigh_ritz);

  /*
    Print result
  */
  printf("[Equilibrium]\n");
  mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, equilibrium, "%.14f", "\t");
  printf("\n[Eigenvalues]\n");
  mathfluid_utils_array_of_complex_double_printf(number_eigenvalues, eigenvalue, "%.14f", "\t");
  printf("\n[Eigenvectors]\n");
  for (i = 0; i < number_eigenvalues; i++) {
    mathfluid_utils_array_of_complex_double_printf(LORENZ_SYSTEM_DIM, eigenvector + i * LORENZ_SYSTEM_DIM, "%.14f", "\t");
    printf("\n");
  }

  arnoldi_method_free(arnoldi_method);
  g_free(hessenberg);
  g_free(invariant_space);
  matrix_vector_product_free(matrix_vector_product);
  rayleigh_ritz_free(rayleigh_ritz);
  dynamical_system_free(ds);
  return 0;
}
