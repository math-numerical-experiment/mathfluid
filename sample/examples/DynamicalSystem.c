/*
  Examples of usages of DynamicalSystem
*/

#include "lorenz.h"

#define DERIVATIVE_PERTURBATION_LARGE 0.02
#define DERIVATIVE_PERTURBATION_SMALL 0.0001
#define JACOBIAN_FREE_TIME_PERTURBATION 0.001
#define JACOBIAN_FREE_SPACE_PERTURBATION 0.001
#define PARAMETER_DERIVATIVE_TIME 0.01
#define PARAMETER_DERIVATIVE_PERTURBATION 0.01

int main (int argc, char *argv[]) {
  DynamicalSystem * ds;
  DataContainer *params;
  gsl_vector *params_vector;

  /*
    Prepare gsl_vector that includes three parameters
  */
  params_vector = gsl_vector_alloc(LORENZ_N_PRMS);
  gsl_vector_set(params_vector, LORENZ_PRM_SIGMA, 10.0);
  gsl_vector_set(params_vector, LORENZ_PRM_BETA, 8.0 / 3.0);
  gsl_vector_set(params_vector, LORENZ_PRM_RHO, 28.0);

  /*
    The structure of parameters stored in the structure DynamicalSystem is DataContainer.
    DataContainer has two functions to duplicate and free as its members
    in addition to pointers of arbitrary data structures.
  */
  params = data_container_alloc(1, params_vector, (void *(*) (void *data)) gsl_vector_duplicate, (void (*) (void *data)) gsl_vector_free);

  /*
    Allocate DynamicialSystem, which contains dimension, parameters, and a time evolution function.
  */
  ds = dynamical_system_alloc(LORENZ_SYSTEM_DIM, params, lorenz_evolve);

  /*
    An example of time evolution
  */
  {
    double t = 0.0, pt[LORENZ_SYSTEM_DIM] = { 0.0, 10.0, 10.0 };
    SystemState *state;
    state = system_state_alloc2(LORENZ_SYSTEM_DIM, 0.0, pt);
    printf("**** Time Evolution ****\n");
    while (system_state_get_time(state) < 1.0) {
      t += 0.1;
      dynamical_system_evolve(state, ds, t);
      printf("%.10f\t", system_state_get_time(state));
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, system_state_get_coordinate(state), "%.10f", "\t");
      printf("\n");
    }
  }

  /*
    Examples of calculations of derivatives
  */
  {
    double v[LORENZ_SYSTEM_DIM], pt[LORENZ_SYSTEM_DIM] = { 3.0, -5.0, 8.0 };
    printf("\n**** Derivatives ****\n");
    printf("numerical derivative with large perturbation\n");
    if (dynamical_system_numerical_time_derivative(v, ds, DERIVATIVE_PERTURBATION_LARGE, pt)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, v, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calulate\n");
    }
    printf("numerical derivative with small perturbation\n");
    if (dynamical_system_numerical_time_derivative(v, ds, DERIVATIVE_PERTURBATION_SMALL, pt)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, v, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calulate\n");
    }
    /*
      If the function of the derivative is defined to DynamicalSystem
      by the function "dynamical_system_set_time_derivative"
      then we can also use the function "dynamical_system_time_derivative".
    */
    dynamical_system_set_time_derivative(ds, lorenz_time_derivative);
    printf("Exact derivative\n");
    if (dynamical_system_time_derivative(v, ds, pt)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, v, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calulate\n");
    }
  }

  /*
    Examples of calculations of Jacobian-vector products
  */
  {
    /*
      There are four functions to calculate Jacobian-vector products (refer to documents for details).
      - dynamical_system_product_jacobian_of_time_derivative
      - dynamical_system_suitable_matrix_free_product_jacobian_of_time_derivative
      - dynamical_system_matrix_free_product_jacobian_of_time_derivative_with_exact_time_derivative
      - dynamical_system_matrix_free_product_jacobian_of_time_derivative_without_exact_time_derivative
     */
    double product[LORENZ_SYSTEM_DIM], v[LORENZ_SYSTEM_DIM] = { 0.2, 0.7, -0.3 }, pt[LORENZ_SYSTEM_DIM] = { 3.0, -5.0, 8.0 };
    printf("\n**** Jacobian-vector products ****\n");
    printf("Jacobian-free product\n");
    if (dynamical_system_suitable_matrix_free_product_jacobian_of_time_derivative(product, ds, JACOBIAN_FREE_TIME_PERTURBATION, JACOBIAN_FREE_SPACE_PERTURBATION, pt, v)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, product, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calculate");
    }

    printf("exact product of Jacobian and vector\n");
    dynamical_system_set_jacobian_of_time_derivative(ds, lorenz_jacobian);
    if (dynamical_system_product_jacobian_of_time_derivative(product, ds, pt, v)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, product, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calculate");
    }
  }

  /*
    Examples of calculations of parameter derivatives
   */
  {
    /*
      There are four functions to calculate parameter derivative (refer to documents for details).
      - dynamical_system_parameter_derivative_of_time_derivative
      - dynamical_system_suitable_parameter_derivative_of_time_derivative
      - dynamical_system_parameter_derivative_of_time_derivative_with_exact_time_derivative
      - dynamical_system_parameter_derivative_of_time_derivative_without_exact_time_derivative
    */
    double derivative[LORENZ_SYSTEM_DIM], pt[LORENZ_SYSTEM_DIM] = { -1.3, 3.5, 2.0 };

    /*
      Set function to access parameters by strings to DynamicalSystem
     */
    dynamical_system_set_parameter_accessor(ds, lorenz_parameter_set, lorenz_parameter_get);

    printf("\n**** Parameter derivative ****\n");
    printf("numerical parameter derivative\n");
    if (dynamical_system_suitable_parameter_derivative_of_time_derivative(derivative, ds, PARAMETER_DERIVATIVE_TIME, PARAMETER_DERIVATIVE_PERTURBATION, "rho", pt)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, derivative, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calculate");
    }

    dynamical_system_set_parameter_derivative_of_time_derivative(ds, lorenz_parameter_derivative);
    printf("exact parameter derivative\n");
    if (dynamical_system_parameter_derivative_of_time_derivative(derivative, ds, "rho", pt)) {
      mathfluid_utils_array_of_double_printf(LORENZ_SYSTEM_DIM, derivative, "%.10f", "\t");
      printf("\n");
    } else {
      printf("Fail to calculate");
    }
  }

  dynamical_system_free(ds);
  return 0;
}
