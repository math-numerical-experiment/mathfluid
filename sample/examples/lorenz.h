#ifndef _LORENZ_H_
#define _LORENZ_H_

#include <stdio.h>
#include "mathfluid.h"

#define LORENZ_SYSTEM_DIM 3
#define LORENZ_EULER_TIME_STEP 0.01

enum {
  LORENZ_PRM_SIGMA,
  LORENZ_PRM_BETA,
  LORENZ_PRM_RHO,
  LORENZ_N_PRMS
};

void lorenz_func (double *f, const double *prms, const double* x);
gboolean lorenz_time_derivative (double *vec, DataContainer *data_container, const double *x);
gboolean lorenz_jacobian (double *m, DataContainer *data_container, const double *x);
gboolean lorenz_evolve (SystemState *state, DataContainer *data_container, const double t1);
void lorenz_parameter_set (DataContainer *data_container, const char *key, GValue *val);
void lorenz_parameter_get (GValue *val, DataContainer *data_container, const char *key);
gboolean lorenz_parameter_derivative (double *vec, DataContainer *data_container, const char *key, const double *x);

#endif /* _LORENZ_H_ */
