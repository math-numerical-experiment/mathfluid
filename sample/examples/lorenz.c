#include "lorenz.h"

void lorenz_func (double *f, const double *prms, const double* x)
{
  f[0] = prms[LORENZ_PRM_SIGMA] * (x[1] - x[0]);
  f[1] = x[0] * (prms[LORENZ_PRM_RHO] - x[2]) - x[1];
  f[2] = x[0] * x[1] - prms[LORENZ_PRM_BETA] * x[2];
}

gboolean lorenz_time_derivative (double *vec, DataContainer *data_container, const double *x)
{
  double *prms;
  prms = gsl_vector_ptr((gsl_vector *) data_container_data_ptr(data_container, 0), 0);
  lorenz_func(vec, prms, x);
  return TRUE;
}

gboolean lorenz_jacobian (double *m, DataContainer *data_container, const double *x)
{
  double *prms;
  gsl_matrix_view mat = gsl_matrix_view_array (m, LORENZ_SYSTEM_DIM, LORENZ_SYSTEM_DIM);
  prms = gsl_vector_ptr((gsl_vector *) data_container_data_ptr(data_container, 0), 0);

  gsl_matrix_set(&mat.matrix, 0, 0, -prms[LORENZ_PRM_SIGMA]);
  gsl_matrix_set(&mat.matrix, 0, 1, prms[LORENZ_PRM_SIGMA]);
  gsl_matrix_set(&mat.matrix, 0, 2, 0.0);
  gsl_matrix_set(&mat.matrix, 1, 0, prms[LORENZ_PRM_RHO] - x[2]);
  gsl_matrix_set(&mat.matrix, 1, 1, -1.0);
  gsl_matrix_set(&mat.matrix, 1, 2, -x[0]);
  gsl_matrix_set(&mat.matrix, 2, 0, x[1]);
  gsl_matrix_set(&mat.matrix, 2, 1, x[0]);
  gsl_matrix_set(&mat.matrix, 2, 2, -prms[LORENZ_PRM_BETA]);

  return TRUE;
}

/*
  Function to update both time and coordinate of state by Euler method.
  We can use SystemODESolver instead of coding time evolution of ODE.
*/
gboolean lorenz_evolve (SystemState *state, DataContainer *data_container, const double t1)
{
  double *prms, t, t_last, f[LORENZ_SYSTEM_DIM], dt, *coordinate;
  int i;
  dt = LORENZ_EULER_TIME_STEP;
  /*
    Take out parameters from DataContainer with type casting.
  */
  prms = gsl_vector_ptr((gsl_vector *) data_container_data_ptr(data_container, 0), 0);
  coordinate = system_state_get_coordinate(state);
  t = system_state_get_time(state);
  while (t < t1) {
    t_last = t;
    t += LORENZ_EULER_TIME_STEP;
    lorenz_func(f, prms, coordinate);
    if (t > t1) {
      dt = t1 - t_last;
      t = t1;
    }
    for (i = 0; i < system_state_get_dimension(state); i++) {
      coordinate[i] += f[i] * dt;
    }
  }
  system_state_set_time(state, t1);
  return TRUE;
}

/*
   Function to set a parameter from GValue to DataContainer.
   Note that we use GValue structure to deal with both int and double types,
   which is a variable container that consists of a type identifier and a specific value of that type.
   Please see documents of GLib for the usage of GValue.
 */
void lorenz_parameter_set (DataContainer *data_container, const char *key, GValue *val)
{
  double val_double, *prms;
  prms = gsl_vector_ptr((gsl_vector *) data_container_data_ptr(data_container, 0), 0);
  if (G_VALUE_HOLDS_DOUBLE(val)) {
    val_double = g_value_get_double(val);
  } else {
    fprintf(stderr, "Invalid value of parameter\n");
    abort();
  }
  if (strcmp(key, "sigma") == 0) {
    prms[LORENZ_PRM_SIGMA] = val_double;
  } else if (strcmp(key, "beta") == 0) {
    prms[LORENZ_PRM_BETA] = val_double;
  } else if (strcmp(key, "rho") == 0) {
    prms[LORENZ_PRM_RHO] = val_double;
  } else {
    fprintf(stderr, "Can not set the parameter %s\n", key);
    abort();
  }
}

/*
   Function to get a parameter from DataContainer to GValue
 */
void lorenz_parameter_get (GValue *val, DataContainer *data_container, const char *key)
{
  double val_double, *prms;
  prms = gsl_vector_ptr((gsl_vector *) data_container_data_ptr(data_container, 0), 0);
  if (G_IS_VALUE(val)) {
    g_value_unset(val);
  }
  g_value_init(val, G_TYPE_DOUBLE);
  if (strcmp(key, "sigma") == 0) {
    val_double = prms[LORENZ_PRM_SIGMA];
  } else if (strcmp(key, "beta") == 0) {
    val_double = prms[LORENZ_PRM_BETA];
  } else if (strcmp(key, "rho") == 0) {
    val_double = prms[LORENZ_PRM_RHO];
  } else {
    fprintf(stderr, "Can not set the parameter %s\n", key);
    abort();
  }
  g_value_set_double(val, val_double);
}

gboolean lorenz_parameter_derivative (double *vec, DataContainer *data_container, const char *key, const double *x)
{
  if (strcmp(key, "sigma") == 0) {
    vec[0] = x[1] - x[0];
    vec[1] = 0.0;
    vec[2] = 0.0;
  } else if (strcmp(key, "beta") == 0) {
    vec[0] = 0.0;
    vec[1] = 0.0;
    vec[2] = x[2];
  } else if (strcmp(key, "rho") == 0) {
    vec[0] = 0.0;
    vec[1] = x[0];
    vec[2] = 0.0;
  } else {
    return FALSE;
  }
  return TRUE;
}
