#!/usr/bin/env ruby

# 
# Generate "config-files.m4" and "subdirs.am" by probing Makefile.am
# from files and directories written in "autoprobe_list.txt"
# 
# Refer to https://lists.gnu.org/archive/html/automake/2013-01/msg00030.html
# 

def sort_path(ary)
  ary.sort do |path1, path2|
    cmp = (path2.scan("/").size <=> path1.scan("/").size)
    if cmp == 0
      cmp = (path1 <=> path2)
    end
    cmp
  end
end

makefile_am_files = ["Makefile.am"]
subdirs = []
File.read("autoprobe_list.txt").each_line do |line|
  path = line.strip.sub(/\/$/, "")
  makefile_am_append = nil
  if File.directory?(path)
    makefile_am_append = Dir.glob(File.join(path, "**/Makefile.am"))
    subdirs_append = makefile_am_append.map { |path| File.dirname(path) }
    subdirs_append = sort_path(subdirs_append)
    subdirs.concat(subdirs_append)
  elsif /Makefile\.am$/ =~ path and File.exist?(path)
    subdirs << File.dirname(path)
    makefile_am_append = Dir.glob(File.join(File.dirname(path), "**/Makefile.am"))
  end
  if makefile_am_append
    makefile_am_append = sort_path(makefile_am_append)
    makefile_am_files.concat(makefile_am_append)
  end
end

open("config-files.m4", "w") do |f|
  makefile_am_files.each do |path|
    f.puts "AC_CONFIG_FILES([#{path.sub(/\.am$/, '')}])"
  end
end

open("subdirs.am", "w") do |f|
  f.puts "SUBDIRS = #{subdirs.join(' ')}"
end
