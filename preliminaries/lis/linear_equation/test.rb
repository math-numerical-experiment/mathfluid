# Calculate x defined by mat * x = vec
def get_result_by_maxima(mat, vec)
  mat_str = mat.map { |ary| "[" + ary.map { |n| n.to_f.to_s }.join(", ") + "]" }.join(", ")
  vec_str = vec.map { |n| n.to_f.to_s }.join(', ')
  IO.popen("maxima --very-quiet", "r+") do |io|
    io.puts "A : matrix(#{mat_str}) $"
    io.puts "b : [#{vec_str}] $"
    io.puts "x : A^^-1 . b;"
    io.puts "quit();"
    str = ""
    while l = io.gets
      if /error/ =~ l
        str = nil
        break
      end
      str << l.gsub(/[\[\] ]+/, "")
    end
    if str
      str.split.map(&:to_f)
    else
      nil
    end
  end
end


def get_result_by_linear_equation(mat, vec)
  result = []
  dim = mat.size
  IO.popen("./linear_equation --lis '-i gmres'", "r+") do |io|
    dim.times do |row|
      str = mat[row].map { |n| n.to_f.to_s }.join(" ")
      str << " " << vec[row].to_f.to_s
      io.puts str
    end
    while l = io.gets
      result << l.to_f
    end
  end
  result
end

def test_result(mat, vec)
  if result_maxima = get_result_by_maxima(mat, vec)
    result_linear_equation = get_result_by_linear_equation(mat, vec)
    err_max = 0
    result_maxima.each_with_index do |val, i|
      err = (val - result_linear_equation[i]).abs
      if err_max < err
        err_max = err
      end
    end
    if err_max > 1e-5
      p mat
      p vec
      raise "Too large error"
    end
  end
end

[{ :matrix => [[1.2, 1], [-3.0, -1.1]], :vector => [-0.3, 1.5] },
 { :matrix => [[8.9, -5.22, 3.8, 1.2, 1], [2.9, 3.7, -8.2, -3.0, -1.1],
               [-2.2, -2.72, 3.12, 6.7, 8.9], [4.1, 7.7, 8.1, 0, 0],
               [0, 2, 0, 3, 0]],
   :vector => [8, 6, -0.3, 9, 1.5] }
].each do |data|
  test_result(data[:matrix], data[:vector])
end
puts "All tests are passed!"
