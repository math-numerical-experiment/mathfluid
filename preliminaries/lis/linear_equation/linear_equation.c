#include <stdio.h>
#include <stdlib.h>
#include <glib-2.0/glib.h>
#include <lis.h>

#define split_line_by_white(str) g_regex_split_simple("[ \t\r\n\f]+", str, G_REGEX_OPTIMIZE, 0);

static void initialize (int *argc_ptr, char ***argv_ptr)
{
  gchar *lis_options = NULL, *lis_options_src;
  GOptionEntry entries[] = {
    { "lis", 0, 0, G_OPTION_ARG_STRING, &lis_options, "Options of Lis", "STR" },
    { NULL }
  };
  gchar **lis_argv;
  int lis_argc;

  GError *error = NULL;
  GOptionContext *context;

  context = g_option_context_new ("- solve a linear equation");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, argc_ptr, argv_ptr, &error)) {
    g_print ("option parsing failed: %s\n", error->message);
    exit (1);
  }

  lis_options_src = g_strconcat(argv_ptr[0][0], " ", lis_options, NULL);
  lis_options_src = g_strchomp(lis_options_src);
  lis_argv = split_line_by_white(lis_options_src);
  lis_argc = 1;
  while (lis_argv[lis_argc]) {
    lis_argc += 1;
  }

  lis_initialize(&lis_argc, &lis_argv);

  g_free(lis_options);
  g_free(lis_options_src);
  g_strfreev(lis_argv);
}

static void finalize ()
{
  lis_finalize();
}

int main (int argc, char **argv)
{
  LIS_INT i, dim, row, col;
  LIS_VECTOR b, x;
  LIS_MATRIX A;
  LIS_SCALAR val;
  LIS_SOLVER solver;
  GIOChannel *io;
  GString *buf;

  initialize(&argc, &argv);

  lis_vector_create(0, &b);
  lis_vector_create(0, &x);
  lis_matrix_create(0, &A);

  dim = 0;
  row = 0;
  io = g_io_channel_unix_new(0); /* File descriptor 0 is stdin */
  buf = g_string_sized_new(64);
  while (g_io_channel_read_line_string(io, buf, NULL, NULL) == G_IO_STATUS_NORMAL) {
    /* buf saves a row of the augmented matrix */
    gchar **nums;
    nums = split_line_by_white(g_strchomp(buf->str));
    if (dim == 0) {
      while (nums[dim + 1]) {
        dim += 1;
      }
      lis_vector_set_size(b, 0, dim);
      lis_vector_set_size(x, 0, dim);
      lis_matrix_set_size(A, 0, dim);
    }
    for (col = 0; col < dim; col++) {
      lis_matrix_set_value(LIS_INS_VALUE, row, col, mathfluid_utils_string_to_double(nums[col]), A);
    }
    lis_vector_set_value(LIS_INS_VALUE, row, mathfluid_utils_string_to_double(nums[dim]), b);
    g_strfreev(nums);
    row += 1;
    if (row == dim) {
      break;
    }
  }
  g_string_free(buf, TRUE);

  lis_matrix_set_type(A, LIS_MATRIX_CSR);
  lis_matrix_assemble(A);

  lis_solver_create(&solver);
  lis_solver_set_optionC(solver);
  lis_solve(A, b, x, solver);

  for (i = 0; i < dim; i++) {
    lis_vector_get_value(x, i, &val);
    printf("%.14lf\n", val);
  }

  lis_vector_destroy(b);
  lis_vector_destroy(x);
  lis_matrix_destroy(A);

  finalize();
  return 0;
}
