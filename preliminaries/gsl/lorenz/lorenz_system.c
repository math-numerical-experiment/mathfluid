#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

#define LORENZ_SYSTEM_DIM 3

#define INITIAL_STEP_SIZE 1e-6
#define EPS_ABSOLUTE 1e-6
#define EPS_RELATIVE 0.0

/*
  x' = \sigma (y - x)
  y' = x (\rho - z) - y
  z' = xy - \beta z
  params = { \sigma, \beta, \rho }
*/
static int lorenz_function (double t, const double x[], double f[], void *params)
{
  double *prms;
  prms = (double *) params;

  f[0] = prms[0] * (x[1] - x[0]);
  f[1] = x[0] * (prms[2] - x[2]) - x[1];
  f[2] = x[0] * x[1] - prms[1] * x[2];

  return GSL_SUCCESS;
}

/*
  Jacobian matrix
  -\sigma     \sigma    0
  \rho - z    -1        x
  y           x         -\beta
  params = { \sigma, \beta, \rho }
*/
static int lorenz_jacobian (double t, const double x[], double *dfdy, double dfdt[], void *params)
{
  double *prms;
  prms = (double *) params;

  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, LORENZ_SYSTEM_DIM, LORENZ_SYSTEM_DIM);
  gsl_matrix *m = &dfdy_mat.matrix; 
  gsl_matrix_set(m, 0, 0, -prms[0]);
  gsl_matrix_set(m, 0, 1, prms[0]);
  gsl_matrix_set(m, 0, 2, 0.0);
  gsl_matrix_set(m, 1, 0, prms[2] - x[2]);
  gsl_matrix_set(m, 1, 1, -1.0);
  gsl_matrix_set(m, 1, 2, -x[0]);
  gsl_matrix_set(m, 2, 0, x[1]);
  gsl_matrix_set(m, 2, 1, x[0]);
  gsl_matrix_set(m, 2, 2, -prms[1]);

  dfdt[0] = 0.0;
  dfdt[1] = 0.0;
  dfdt[2] = 0.0;

  return GSL_SUCCESS;
}

int main (int argc, char **argv)
{
  double params[LORENZ_SYSTEM_DIM] = { 10.0, 8.0 / 3.0, 28.0 };
  gsl_odeiv2_system odeiv_system = { lorenz_function, lorenz_jacobian, LORENZ_SYSTEM_DIM, params };
  gsl_odeiv2_driver *odeiv_driver = gsl_odeiv2_driver_alloc_y_new(&odeiv_system, gsl_odeiv2_step_rk8pd, INITIAL_STEP_SIZE, EPS_ABSOLUTE, EPS_RELATIVE);
  double t = 0.0, t1 = 100.0;
  double y[LORENZ_SYSTEM_DIM] = { 10.0, 0.0, 0.0 };
  int i;

  for (i = 1; i <= 10000; i++) {
    double ti = i * t1 / 10000.0;
    int status = gsl_odeiv2_driver_apply(odeiv_driver, &t, ti, y);

    if (status != GSL_SUCCESS) {
      printf("error, return value=%d\n", status);
      break;
    }

    printf("%.12lf %.12lf %.12lf %.12lf\n", t, y[0], y[1], y[2]);
  }

  gsl_odeiv2_driver_free(odeiv_driver);
  return 0;
}
