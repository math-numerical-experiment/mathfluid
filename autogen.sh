#!/bin/sh

run()
{
    $@
    if test $? -ne 0; then
        echo "Failed $@"
        exit 1
    fi
}

if [ `which glibtoolize` ]; then
    LIBTOOLIZE=glibtoolize
else
    LIBTOOLIZE=libtoolize
fi

run aclocal ${ACLOCAL_ARGS}
run $LIBTOOLIZE --copy --force
run autoheader
run automake --add-missing --foreign --copy
run autoconf
