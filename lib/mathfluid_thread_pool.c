/**
 * @file mathfluid_thread_pool.c
 * @brief Functions to use thread pool
 */

#include "header.h"

/**
 * \addtogroup MathFluidThreadPool
 * @{
 */

static MathFluidThreadPool *mathfluid_thread_pool = NULL;

MathFluidThreadPool *mathfluid_thread_pool_get ()
{
  return mathfluid_thread_pool;
}

static void mathfluid_thread_pool_exec (gpointer ptr_signal_id, gpointer thread_signal_ptr)
{
  GPtrArray *thread_signal;
  MathFluidThreadSignal *signal;
  thread_signal = (GPtrArray *) thread_signal_ptr;
  signal = (MathFluidThreadSignal *) g_ptr_array_index(thread_signal, *((int *) ptr_signal_id));
  signal->argument->function(signal->argument->task_number, signal->argument->total_task_number, signal->argument->data);
  g_mutex_lock(&signal->mutex);
  signal->finished = TRUE;
  g_cond_signal(&signal->cond);
  g_mutex_unlock(&signal->mutex);
}

static void mathfluid_thread_signal_free (gpointer ptr_signal)
{
  MathFluidThreadSignal *signal;
  signal = (MathFluidThreadSignal *) ptr_signal;
  g_cond_clear(&signal->cond);
  g_mutex_clear(&signal->mutex);
  g_free(signal);
}

static MathFluidThreadSignal *mathfluid_thread_pool_signal_get (int *signal_id)
{
  MathFluidThreadPool *thread_pool;
  MathFluidThreadSignal *signal;
  thread_pool = mathfluid_thread_pool_get();
  if (thread_pool->unused_signal_id->len > 0) {
    int index;
    index = thread_pool->unused_signal_id->len - 1;
    *signal_id = g_array_index(thread_pool->unused_signal_id, int, index);
    thread_pool->unused_signal_id = g_array_remove_index(thread_pool->unused_signal_id, index);
    signal = g_ptr_array_index(thread_pool->signal_array, *signal_id);
  } else {
    signal = (MathFluidThreadSignal *) g_malloc(sizeof(MathFluidThreadSignal));
    g_ptr_array_add(thread_pool->signal_array, signal);
    g_cond_init(&signal->cond);
    g_mutex_init(&signal->mutex);
    *signal_id = thread_pool->signal_array->len - 1;
  }
  return signal;
}

void mathfluid_thread_pool_initialize (int max_thread_number)
{
  if (mathfluid_thread_pool) {
    fprintf(stderr, "Thread pool has been already initialized.\n");
    abort();
  }
  mathfluid_thread_pool = (MathFluidThreadPool *) g_malloc(sizeof(MathFluidThreadPool));
  mathfluid_thread_pool->max_thread_number = max_thread_number;
  mathfluid_thread_pool->signal_array = g_ptr_array_new_with_free_func(mathfluid_thread_signal_free);
  mathfluid_thread_pool->unused_signal_id = g_array_new(FALSE, FALSE, sizeof(int));
  mathfluid_thread_pool->pool = g_thread_pool_new(mathfluid_thread_pool_exec, mathfluid_thread_pool->signal_array, mathfluid_thread_pool->max_thread_number, FALSE, NULL);
}

void mathfluid_thread_pool_finalize ()
{
  if (mathfluid_thread_pool) {
    g_ptr_array_free(mathfluid_thread_pool->signal_array, TRUE);
    g_array_unref(mathfluid_thread_pool->unused_signal_id);
    g_thread_pool_free(mathfluid_thread_pool->pool, TRUE, TRUE);
    g_free(mathfluid_thread_pool);
    mathfluid_thread_pool = NULL;
  }
}

GArray *mathfluid_thread_pool_push_tasks (int total_task_number, void (*function) (int task_number, int total_task_number, gpointer data), gpointer data)
{
  MathFluidThreadPool *thread_pool;
  MathFluidThreadSignal *signal;
  GArray *signal_id;
  int i, *ptr_id;
  thread_pool = mathfluid_thread_pool_get();
  signal_id = g_array_sized_new(FALSE, FALSE, sizeof(int), total_task_number);
  signal_id->len = total_task_number;
  for (i = 0; i < total_task_number; i++) {
    ptr_id = &g_array_index(signal_id, int, i);
    signal = mathfluid_thread_pool_signal_get(ptr_id);
    signal->finished = FALSE;
    signal->argument->task_number = i;
    signal->argument->total_task_number = total_task_number;
    signal->argument->data = data;
    signal->argument->function = function;
    fflush(stdout);
    g_thread_pool_push(thread_pool->pool, (gpointer) ptr_id, NULL);
  }
  return signal_id;
}

void mathfluid_thread_pool_wait_tasks (GArray *signal_id)
{
  int i, task_id;
  MathFluidThreadPool *thread_pool;
  MathFluidThreadSignal *signal;
  thread_pool = mathfluid_thread_pool_get();
  for (i = 0; i < signal_id->len; i++) {
    task_id = g_array_index(signal_id, int, i);
    signal = g_ptr_array_index(thread_pool->signal_array, task_id);
    g_mutex_lock(&signal->mutex);
    while (!signal->finished) {
      g_cond_wait(&signal->cond, &signal->mutex);
    }
    g_mutex_unlock(&signal->mutex);
    thread_pool->unused_signal_id = g_array_append_val(thread_pool->unused_signal_id, task_id);
  }
}

int mathfluid_thread_pool_max_thread_number (void)
{
  if (mathfluid_thread_pool) {
    return mathfluid_thread_pool->max_thread_number;
  } else {
    return 0;
  }
}

MathFluidThreadVariable *mathfluid_thread_variable_alloc (void *(*variable_alloc) (void *data, int index), void (*variable_free) (void *variable), void *data)
{
  MathFluidThreadVariable *thread_variable;
  thread_variable = (MathFluidThreadVariable *) g_malloc(sizeof(MathFluidThreadVariable));
  thread_variable->array = g_ptr_array_new_with_free_func(variable_free);
  thread_variable->variable_alloc = variable_alloc;
  thread_variable->data = data;
  g_rw_lock_init(thread_variable->lock);
  return thread_variable;
}

void mathfluid_thread_variable_free (MathFluidThreadVariable *thread_variable)
{
  g_ptr_array_free(thread_variable->array, TRUE);
  g_rw_lock_clear(thread_variable->lock);
  g_free(thread_variable);
}

void mathfluid_thread_variable_clear (MathFluidThreadVariable *thread_variable)
{
  if (thread_variable->array->len > 0) {
    g_ptr_array_remove_range(thread_variable->array, 0, thread_variable->array->len);
  }
}

void *mathfluid_thread_variable_get (MathFluidThreadVariable *thread_variable, int index)
{
  if (index >= thread_variable->array->len) {
    g_rw_lock_writer_lock(thread_variable->lock);
    if (index >= thread_variable->array->len) {
      void *data;
      int i;
      for (i = thread_variable->array->len; i <= index; i++) {
        data = thread_variable->variable_alloc(thread_variable->data, index);
        g_ptr_array_add(thread_variable->array, data);
      }
    }
    g_rw_lock_writer_unlock(thread_variable->lock);
  }
  return g_ptr_array_index(thread_variable->array, index);
}

/** @} */  /* End of MathFluidThreadPool */
