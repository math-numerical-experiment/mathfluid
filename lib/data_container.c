/**
 * @file data_container.c
 * @brief Functions of data containers
 */

#include "header.h"

/**
 * \addtogroup DataContainer
 * @{
 */

static void data_container_add_va_list (DataContainer *data_container, int n, va_list args)
{
  int i;
  for (i = 0; i < n * 3; i++) {
    g_ptr_array_add(((GPtrArray *) data_container), va_arg(args, void *));
  }
}

/**
 * void *data, void *(* data_duplicate) (void *data), void (*data_free) (void *data)
 * Allocate DataContainer which is mainly used to store parameters of a dynamical system.
 * @param[in]    n    Number of data
 * Data is a triple of (void *data), (void *(* data_duplicate) (void *data)), and (void (*data_free) (void *data)).
 * We call this function like
 * d = data_container_alloc(1, data, data_duplicate, data_free);
 * - data              A pointer of data
 * - data_duplicate    A pointer of a function to allocate and copy "data"
 * - data_free         A pointer of a function to free "data"
 */
DataContainer *data_container_alloc (int n, ...)
{
  va_list args;
  DataContainer *data_container;
  data_container = g_ptr_array_sized_new(n * 3);
  va_start(args, n);
  data_container_add_va_list(data_container, n, args);
  va_end(args);
  return data_container;
}

/**
 * Free DataContainer by calling DataContainer::data_free
 * @param[in]    data_container    A pointer of DataContainer
 */
void data_container_free (DataContainer *data_container)
{
  int i;
  void *data;
  void (* data_free) (void *data);
  if (((GPtrArray *) data_container)->len > 0) {
    for (i = 0; i < ((GPtrArray *) data_container)->len; i += 3) {
      data = g_ptr_array_index(((GPtrArray *) data_container), i);
      data_free = (void (*) (void *data)) g_ptr_array_index(((GPtrArray *) data_container), i + 2);
      if (data && data_free) {
        data_free(data);
      }
    }
  }
  g_ptr_array_free(((GPtrArray *) data_container), TRUE);
}

/**
 * Duplicate DataContainer by calling DataContainer::data_duplicate
 * @param[in]    data_container    A pointer of DataContainer
 */
DataContainer *data_container_duplicate (DataContainer *data_container)
{
  int i;
  void *data;
  void *(* data_duplicate) (void *data);
  void *data_new;
  DataContainer *data_container_new;
  data_container_new = data_container_alloc(0);
  if (((GPtrArray *) data_container)->len > 0) {
    for (i = 0; i < ((GPtrArray *) data_container)->len; i += 3) {
      data = g_ptr_array_index(((GPtrArray *) data_container), i);
      data_duplicate = (void *(*) (void *data)) g_ptr_array_index(((GPtrArray *) data_container), i + 1);
      if (data) {
        if (!data_duplicate) {
          fprintf(stderr, "Can not duplicate data\n");
          abort();
        }
        data_new = data_duplicate(data);
        data_container_add(data_container_new, 1, data_new, data_duplicate, g_ptr_array_index(((GPtrArray *) data_container), i + 2));
      }
    }
  }
  return data_container_new;
}

void data_container_add (DataContainer *data_container, int n, ...)
{
  va_list args;
  va_start(args, n);
  data_container_add_va_list(data_container, n, args);
  va_end(args);
}

/**
 * Return just pointer with no allocation. This function is used to copy pointer with no allocation
 * for data_container_duplicate.
 */
void *data_container_utils_return_pointer (void *pointer)
{
  return pointer;
}

double *data_container_utils_double_duplicate (double *val)
{
  double *v;
  v = (double *) g_malloc(sizeof(double));
  *v = *val;
  return v;
}

int *data_container_utils_int_duplicate (int *val)
{
  int *v;
  v = (int *) g_malloc(sizeof(int));
  *v = *val;
  return v;
}

/** @} */  /* End of DataContainer */
