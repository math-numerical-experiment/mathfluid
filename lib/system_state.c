/**
 * @file system_state.c
 * @brief Functions of system state
 */

#include "header.h"

/**
 * \addtogroup SystemState
 * @{
 */

/**
 * @param[in]    dimension    Dimension of phase space
 * @return    A pointer of allocated SystemState
 */
SystemState *system_state_alloc (uint dimension)
{
  SystemState *state;
  state = (SystemState *) g_malloc(sizeof(SystemState));
  state->dimension = dimension;
  state->coordinate = (double *) g_malloc(sizeof(double) * state->dimension);
  system_state_set_zero(state);
  return state;
}

/**
 * @param[in]    dimension    Dimension of phase space
 * @param[in]    time         Initial time
 * @param[in]    coordinate   Initial coordinate
 * @return    A pointer of allocated SystemState whose time and coordinate are set
 */
SystemState *system_state_alloc2 (uint dimension, double time, const double *coordinate)
{
  SystemState* state;
  state = system_state_alloc(dimension);
  system_state_set_time(state, time);
  system_state_set_coordinate(state, coordinate);
  return state;
}

/**
 * @param[in]    state    Source of SystemState
 * @return    A pointer of allocated SystemState whose properties are same as the argument
 */
SystemState *system_state_alloc_copy (SystemState *state)
{
  SystemState *state2;
  state2 = system_state_alloc(state->dimension);
  system_state_copy(state2, state);
  return state2;
}

/**
 * @param[in]    state    A pointer of SystemState to be freed
 */
void system_state_free (SystemState *state)
{
  if (state->coordinate) {
    g_free(state->coordinate);
  }
  g_free(state);
}

/**
 * Copy both src->coordinate and src->time to those of dest.
 */
void system_state_copy (SystemState *dest, SystemState *src)
{
  if (dest->dimension != src->dimension) {
    fprintf(stderr, "Can not copy SystemState; dimensions does not coincide");
    abort();
  }
  memcpy(dest->coordinate, src->coordinate, sizeof(double) * src->dimension);
  dest->time = src->time;
}

/**
 * Copy vec to state->coordinate
 * @param[out]    state    A pointer of destination
 * @param[in]     vec      A pointer of array of source whose length must be equal to state->dimension
 */
void system_state_set_coordinate (SystemState *state, const double *vec)
{
  memcpy(state->coordinate, vec, sizeof(double) * state->dimension);
}

void system_state_set_time (SystemState *state, double time)
{
  state->time = time;
}

/**
 * Set zero to both time and all elements of coordinate
 */
void system_state_set_zero (SystemState *state)
{
  int i;
  state->time = 0.0;
  for (i = 0; i < state->dimension; i++) {
    state->coordinate[i] = 0.0;
  }
}

/**
 * Copy coordinate to array of double
 * @param[out]   coordinate    A pointer of array whose size must be the dimension
 * @param[in]    state         Source of coordinate
 */
void system_state_copy_coordinate (double *coordinate, SystemState *state)
{
  memcpy(coordinate, state->coordinate, sizeof(double) * state->dimension);
}

gsl_vector_view system_state_coordinate_vector_view (SystemState *state)
{
  return gsl_vector_view_array(state->coordinate, state->dimension);
}

void system_state_fprintf (FILE *out, SystemState *state)
{
  int i;
  fprintf(out, "%.14lf", state->time);
  for (i = 0; i < state->dimension; i++) {
    fprintf(out, " %.14lf", state->coordinate[i]);
  }
  fprintf(out, "\n");
}

/** @} */  /* End of SystemState */
