/**
 * @file generic_parameter.c
 * @brief Functions of generic parameter set
 */

#include "header.h"

/**
 * \addtogroup GenericParameter
 * @{
 */

/**
 * Allocate GenericParameter
 */
GenericParameter *generic_parameter_alloc ()
{
  return (GenericParameter *) data_container_alloc(GENERIC_PARAMETER_N, g_array_new(FALSE, FALSE, sizeof(double)), g_array_duplicate, g_array_free_completely, g_array_new(FALSE, FALSE, sizeof(int)), g_array_duplicate, g_array_free_completely, data_container_alloc(0), data_container_duplicate, data_container_free, data_container_alloc(0), data_container_duplicate, data_container_free, NULL, data_container_duplicate, data_container_free);
}

static void generic_parameter_set_values_with_keys (GArray *array_nums, DataContainer *array_keys, int dim, gchar *nums, char **keys)
{
  int i;
  g_array_set_size(array_nums, 0);
  if (nums && dim > 0) {
    g_array_insert_vals(array_nums, 0, nums, dim);
  }
  if (keys) {
    for (i = 0; i < dim; i++) {
      if (keys[i]) {
        data_container_add(array_keys, 1,  g_strdup(keys[i]), strdup, g_free);
      } else {
        break;
      }
    }
  }
}

/**
 * @param[in]    params    A pointer of GenericParameter
 * @param[in]    dim    Dimension of parameters
 * @param[in]    nums   An arrays of double of "dim" numbers whose copy is stored
 * @param[in]    keys   A pointer of an array of keys that terminates NULL
 */
void generic_parameter_set_double (GenericParameter *params, int dim, double *nums, char **keys)
{
  generic_parameter_set_values_with_keys((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_DOUBLE), (DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_DOUBLE), dim, (gchar *) nums, keys);
}

/**
 * @param[in]    params    A pointer of GenericParameter
 * @param[in]    dim    Dimension of parameters
 * @param[in]    nums   An arrays of int of "dim" numbers whose copy is stored
 * @param[in]    keys   A pointer of an array of keys that terminates NULL
 */
void generic_parameter_set_int (GenericParameter *params, int dim, int *nums, char **keys)
{
  generic_parameter_set_values_with_keys((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_INT), (DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_INT), dim, (gchar *) nums, keys);
}

/**
 * @param[in]    params    A pointer of GenericParameter
 * @param[in]    data      An optional data
 */
void generic_parameter_data_set (GenericParameter *params, DataContainer *data)
{
  data_container_data_ptr(params, GENERIC_PARAMETER_DATA) = data;
}

/**
 * Allocate new GenericParameter and copy members to it
 * @param[in]    params    Source of new GenericParameter
 */
GenericParameter *generic_parameter_duplicate (GenericParameter *params)
{
  return data_container_duplicate(params);
}

/**
 * Free GenericParameter
 * @param[in]    params    A pointer of GenericParameter
 */
void generic_parameter_free (GenericParameter *params)
{
  data_container_free(params);
}

static int generic_parameter_find_key (DataContainer *array_keys, const char *key)
{
  int i;
  char *k;
  for (i = 0; i < data_container_size(array_keys); i++) {
    k = data_container_data_ptr(array_keys, i);
    if (k && (g_strcmp0(k, key) == 0)) {
      return i;
    }
  }
  return -1;
}

/**
 * Set a parameter of GenericParameter
 * @param[in]    params    A pointer of GenericParameter
 * @param[in]    key       A key of a parameter
 * @param[in]    kal       A value of the parameter specified by "key"
 */
void generic_parameter_set (GenericParameter *params, const char *key, GValue *val)
{
  int index;
  index = generic_parameter_find_key((DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_DOUBLE), key);
  if (index >= 0) {
    if (G_VALUE_HOLDS_DOUBLE(val)) {
      g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_DOUBLE), double, index) = g_value_get_double(val);
    } else {
      fprintf(stderr, "Parameter '%s' must be double\n", key);
      abort();
    }
    return;
  }
  index = generic_parameter_find_key((DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_INT), key);
  if (index >= 0) {
    if (G_VALUE_HOLDS_INT(val)) {
      g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_INT), int, index) = g_value_get_int(val);
    } else {
      fprintf(stderr, "Parameter '%s' must be int\n", key);
      abort();
    }
    return;
  }
  fprintf(stderr, "Can not set the parameter %s\n", key);
  abort();
}

/**
 * Get a parameter of GenericParameter
 * @param[out]   val       A pointer of GValue to store output
 * @param[in]    params    A pointer of GenericParameter
 * @param[in]    key       A key of a parameter
 */
void generic_parameter_get (GValue *val, GenericParameter *params, const char *key)
{
  if (G_IS_VALUE(val)) {
    g_value_unset(val);
  }
  int index;
  index = generic_parameter_find_key((DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_DOUBLE), key);
  if (index >= 0) {
    g_value_init(val, G_TYPE_DOUBLE);
    g_value_set_double(val, g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_DOUBLE), double, index));
    return;
  }
  index = generic_parameter_find_key((DataContainer *) data_container_data_ptr(params, GENERIC_PARAMETER_KEY_INT), key);
  if (index >= 0) {
    g_value_init(val, G_TYPE_INT);
    g_value_set_int(val, g_array_index((GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_INT), int, index));
    return;
  }
  fprintf(stderr, "Can not get the parameter %s\n", key);
  abort();
}

int *generic_parameter_array_int (GenericParameter *params)
{
  GArray *array;
  array = (GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_INT);
  if (array->len == 0) {
    return NULL;
  } else {
    return ((int *) array->data);
  }
}

double *generic_parameter_array_double (GenericParameter *params)
{
  GArray *array;
  array = (GArray *) data_container_data_ptr(params, GENERIC_PARAMETER_ARRAY_DOUBLE);
  if (array->len == 0) {
    return NULL;
  } else {
    return ((double *) array->data);
  }
}

/** @} */  /* End of GenericParameter */
