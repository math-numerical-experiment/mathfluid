/**
 * @file leveldb_stack_cache.c
 * @brief Stack cache implemented by LevelDB
 */

#include "header.h"

#define LEVEL_DB_DUMP_DATA_NUMBER_KEY -10

/**
 * \addtogroup LevelDBStackCache
 * @{
 */

#define LEVEL_DB_STACK_CACHE_DEFAULT_CACHE_MAX_NUMBER 100

static void leveldb_stack_cache_rw_func (gpointer data, gpointer user_data);

typedef struct {
  LevelDBStackCacheThreadType type;
  gpointer argument;
} LevelDBStackCacheThreadArgument;

LevelDBStackCache *leveldb_stack_cache_alloc (char *(*data_dump) (size_t *size, gpointer data), gpointer (*data_load) (char *dump, size_t size), void (*data_free) (gpointer data))
{
  LevelDBStackCache *stack_cache;
  stack_cache = g_malloc(sizeof(LevelDBStackCache));
  stack_cache->stack = g_ptr_array_new();
  stack_cache->db_path = NULL;
  stack_cache->db = NULL;
  stack_cache->db_options = NULL;
  stack_cache->db_roptions = NULL;
  stack_cache->db_woptions = NULL;
  stack_cache->db_current_number = -1;
  stack_cache->data_dump = data_dump;
  stack_cache->data_load = data_load;
  stack_cache->data_free = data_free;
  stack_cache->db_rw_thread_pool = NULL;
  stack_cache->db_cache_max_number = LEVEL_DB_STACK_CACHE_DEFAULT_CACHE_MAX_NUMBER;
  return stack_cache;
}

static void leveldb_stack_cache_send_signal_finish (LevelDBStackCache *stack_cache)
{
  g_mutex_lock(stack_cache->db_mutex);
  stack_cache->db_thread_working = LEVELDB_STACK_CACHE_THREAD_WAITING;
  g_cond_signal(stack_cache->db_cond);
  g_mutex_unlock(stack_cache->db_mutex);
}

static LevelDBStackCacheThreadArgument *leveldb_stack_cache_thread_argument_alloc (LevelDBStackCacheThreadType type, gpointer argument)
{
  LevelDBStackCacheThreadArgument *arg;
  arg = (LevelDBStackCacheThreadArgument *) g_malloc(sizeof(LevelDBStackCacheThreadArgument));
  arg->type = type;
  arg->argument = argument;
  return arg;
}
static void leveldb_stack_cache_put_with_abort_of_error (LevelDBStackCache *stack_cache, char *key, size_t key_size, char *data, size_t data_size)
{
  char *err = NULL;
  leveldb_put(stack_cache->db, stack_cache->db_woptions, key, key_size, data, data_size, &err);
  if (err) {
    fprintf(stderr, "Writing to database fails\n");
    abort();
  }
  leveldb_free(err);
}

static char *leveldb_stack_cache_get_with_abort_of_error (size_t *len, LevelDBStackCache *stack_cache, char *key, size_t key_size)
{
  char *err = NULL, *dump_data;
  dump_data = leveldb_get(stack_cache->db, stack_cache->db_roptions, key, key_size, len, &err);
  if (err) {
    fprintf(stderr, "Reading database fails\n");
    abort();
  }
  leveldb_free(err);
  return dump_data;
}

static int leveldb_stack_cache_dump_data_number (LevelDBStackCache *stack_cache)
{
  int key, num;
  size_t len;
  char *val;
  key = LEVEL_DB_DUMP_DATA_NUMBER_KEY;
  val = leveldb_stack_cache_get_with_abort_of_error(&len, stack_cache, (char *) &key, sizeof(int));
  if (val) {
    num = *(int *) val;
    leveldb_free(val);
  } else {
    num = -1;
  }
  return num;
}

void leveldb_stack_cache_set_db (LevelDBStackCache *stack_cache, const char *path)
{
  char *err = NULL;
  if (stack_cache->db_path) {
    fprintf(stderr, "Path of database has been already set\n");
    abort();
  }
  stack_cache->db_path = g_strdup(path);
  stack_cache->db_options = leveldb_options_create();
  leveldb_options_set_create_if_missing(stack_cache->db_options, 1);
  stack_cache->db = leveldb_open(stack_cache->db_options, stack_cache->db_path, &err);
  if (err) {
    fprintf(stderr, "Open of database fails\n");
    abort();
  }
  leveldb_free(err);
  stack_cache->db_roptions = leveldb_readoptions_create();
  stack_cache->db_woptions = leveldb_writeoptions_create();
  stack_cache->db_thread_working = LEVELDB_STACK_CACHE_THREAD_WAITING;
  g_mutex_init(stack_cache->db_mutex);
  g_cond_init(stack_cache->db_cond);
  stack_cache->db_rw_thread_pool = g_thread_pool_new(leveldb_stack_cache_rw_func, stack_cache, 1, TRUE, NULL);
  stack_cache->db_current_number = leveldb_stack_cache_dump_data_number(stack_cache);
}

void leveldb_stack_cache_free (LevelDBStackCache *stack_cache, gboolean destroy)
{
  if (stack_cache->stack) {
    if (stack_cache->stack->len > 0) {
      fprintf(stderr, "Stack is not empty, but LevelDBStackCache is freed\n");
    }
    g_ptr_array_free(stack_cache->stack, TRUE);
  }
  if (stack_cache->db) {
    leveldb_close(stack_cache->db);
    if (destroy) {
      char *err = NULL;
      leveldb_destroy_db(stack_cache->db_options, stack_cache->db_path, &err);
      if (err) {
        fprintf(stderr, "Destruction of database fails\n");
        abort();
      }
      leveldb_free(err);
    } else {
      if (stack_cache->stack->len > 0 && stack_cache->db_current_number != leveldb_stack_cache_dump_data_number(stack_cache)) {
        fprintf(stderr, "Unsynchronized data with database exists\n");
      }
    }
  }
  if (stack_cache->db_options) {
    leveldb_options_destroy(stack_cache->db_options);
  }
  if (stack_cache->db_roptions) {
    leveldb_readoptions_destroy(stack_cache->db_roptions);
  }
  if (stack_cache->db_woptions) {
    leveldb_writeoptions_destroy(stack_cache->db_woptions);
  }
  if (stack_cache->db_path) {
    g_free(stack_cache->db_path);
  }
  if (stack_cache->db_rw_thread_pool) {
    g_thread_pool_free(stack_cache->db_rw_thread_pool, TRUE, TRUE);
    g_mutex_clear(stack_cache->db_mutex);
    g_cond_clear(stack_cache->db_cond);
  }
  g_free(stack_cache);
}

static void leveldb_stack_cache_thread_argument_free (LevelDBStackCacheThreadArgument *arg)
{
  g_free(arg);
}

static void leveldb_stack_cache_db_push_byte_array (LevelDBStackCache *stack_cache, GByteArray *byte_array)
{
  stack_cache->db_current_number += 1;
  leveldb_stack_cache_put_with_abort_of_error(stack_cache, (char *) &stack_cache->db_current_number, sizeof(uint), (char *) byte_array->data, byte_array->len);
}

static void leveldb_stack_cache_db_push (LevelDBStackCache *stack_cache, GPtrArray *data_push)
{
  int i;
  gpointer data;
  char *dump_data;
  size_t dump_size;
  GByteArray *dump_data_all;
  dump_data_all = g_byte_array_new();
  g_byte_array_append(dump_data_all, (guint8 *) &data_push->len, sizeof(guint));
  for (i = 0; i < data_push->len; i++) {
    data = g_ptr_array_index(data_push, i);
    dump_data = stack_cache->data_dump(&dump_size, data);
    g_byte_array_append(dump_data_all, (guint8 *) &dump_size, sizeof(size_t));
    g_byte_array_append(dump_data_all, (guint8 *) dump_data, dump_size);
    g_free(dump_data);
    stack_cache->data_free(data);
    g_ptr_array_index(data_push, i) = NULL;
  }
  leveldb_stack_cache_db_push_byte_array(stack_cache, dump_data_all);
  g_byte_array_free(dump_data_all, TRUE);
  g_ptr_array_free(data_push, TRUE);
}

static char *leveldb_stack_cache_db_pop_dump_data (size_t *len, LevelDBStackCache *stack_cache)
{
  char *dump_data;
  if (stack_cache->db_current_number < 0) {
    dump_data = NULL;
  } else {
    dump_data = leveldb_stack_cache_get_with_abort_of_error(len, stack_cache, (char *) &stack_cache->db_current_number, sizeof(uint));
    stack_cache->db_current_number -= 1;
  }
  return dump_data;
}

static void leveldb_stack_cache_db_pop (LevelDBStackCache *stack_cache)
{
  char *dump_data;
  size_t len_dump_data;
  if (stack_cache->stack->len > 0) {
    fprintf(stderr, "Stack is not empty\n");
    return;
  }
  dump_data = leveldb_stack_cache_db_pop_dump_data(&len_dump_data, stack_cache);
  if (dump_data) {
    gpointer data;
    int i, read_num;
    char *ptr;
    size_t len;
    ptr = dump_data;
    read_num = *(guint *) ptr;
    ptr += sizeof(guint);
    for (i = 0; i < read_num; i++) {
      len = *(size_t *) ptr;
      ptr += sizeof(size_t);
      data = stack_cache->data_load(ptr, len);
      ptr += len;
      g_ptr_array_add(stack_cache->stack, data);
    }
    g_free(dump_data);
  }
}

/**
 * Note that only one thread can read and write database.
 */
static void leveldb_stack_cache_rw_func (gpointer thread_data, gpointer thread_user_data)
{
  LevelDBStackCache *stack_cache;
  LevelDBStackCacheThreadArgument *arg;
  stack_cache = (LevelDBStackCache *) thread_user_data;
  arg = (LevelDBStackCacheThreadArgument *) thread_data;
  if (arg->type == LEVELDB_STACK_CACHE_THREAD_READING) {
    leveldb_stack_cache_db_pop(stack_cache);
  } else if (arg->type == LEVELDB_STACK_CACHE_THREAD_WRITING) {
    leveldb_stack_cache_db_push(stack_cache, (GPtrArray *) arg->argument);
  }
  leveldb_stack_cache_thread_argument_free(arg);
  leveldb_stack_cache_send_signal_finish(stack_cache);
}

static void leveldb_stack_cache_wait_thread_finish (LevelDBStackCache *stack_cache)
{
  g_mutex_lock(stack_cache->db_mutex);
  while (stack_cache->db_thread_working != LEVELDB_STACK_CACHE_THREAD_WAITING) {
    g_cond_wait(stack_cache->db_cond, stack_cache->db_mutex);
  }
  g_mutex_unlock(stack_cache->db_mutex);
}

/**
 * Note that this function does not check existence of stack_cache->db
 */
static void leveldb_stack_cache_push_task_of_flush (LevelDBStackCache *stack_cache)
{
  GPtrArray *data_push;
  LevelDBStackCacheThreadArgument *arg;
  leveldb_stack_cache_wait_thread_finish(stack_cache);
  data_push = stack_cache->stack;
  stack_cache->stack = g_ptr_array_new();
  arg = leveldb_stack_cache_thread_argument_alloc(LEVELDB_STACK_CACHE_THREAD_WRITING, data_push);
  stack_cache->db_thread_working = LEVELDB_STACK_CACHE_THREAD_WRITING;
  g_thread_pool_push(stack_cache->db_rw_thread_pool, arg, NULL);
}

/**
 * Flush all data to database file
 */
void leveldb_stack_cache_flush (LevelDBStackCache *stack_cache)
{
  int key;
  if (!stack_cache->db) {
    fprintf(stderr, "Can not flush to file because database is not set\n");
    abort();
  }
  leveldb_stack_cache_wait_thread_finish(stack_cache);
  if (stack_cache->stack->len > 0) {
    leveldb_stack_cache_push_task_of_flush(stack_cache);
    leveldb_stack_cache_wait_thread_finish(stack_cache);
  }
  key = LEVEL_DB_DUMP_DATA_NUMBER_KEY;
  leveldb_stack_cache_put_with_abort_of_error(stack_cache, (char *) &key, sizeof(int), (char *) &stack_cache->db_current_number, sizeof(int));
}

void leveldb_stack_cache_push (LevelDBStackCache *stack_cache, gpointer data)
{
  if (stack_cache->db && stack_cache->db_thread_working == LEVELDB_STACK_CACHE_THREAD_READING) {
    leveldb_stack_cache_wait_thread_finish(stack_cache);
  }
  g_ptr_array_add(stack_cache->stack, data);
  if (stack_cache->db && stack_cache->stack->len >= stack_cache->db_cache_max_number) {
    leveldb_stack_cache_push_task_of_flush(stack_cache);
  }
}

gpointer leveldb_stack_cache_pop (LevelDBStackCache *stack_cache)
{
  gpointer data;
  if (stack_cache->db) {
    if (stack_cache->db_thread_working == LEVELDB_STACK_CACHE_THREAD_READING ||
        (stack_cache->db_thread_working == LEVELDB_STACK_CACHE_THREAD_WRITING && stack_cache->stack->len == 0)) {
      leveldb_stack_cache_wait_thread_finish(stack_cache);
    }
  }
  if (stack_cache->stack->len > 0) {
    data = g_ptr_array_remove_index(stack_cache->stack, stack_cache->stack->len - 1);
  } else {
    data = NULL;
  }
  if (stack_cache->db && stack_cache->stack->len == 0) {
    leveldb_stack_cache_wait_thread_finish(stack_cache);
    if (stack_cache->db_current_number >= 0) {
      LevelDBStackCacheThreadArgument *arg;
      arg = leveldb_stack_cache_thread_argument_alloc(LEVELDB_STACK_CACHE_THREAD_READING, NULL);
      stack_cache->db_thread_working = LEVELDB_STACK_CACHE_THREAD_READING;
      g_thread_pool_push(stack_cache->db_rw_thread_pool, arg, NULL);
      if (!data) {
        return leveldb_stack_cache_pop(stack_cache);
      }
    }
  }
  return data;
}

/** @} */  /* End of LevelDBStackCache */
