/**
 * @file rayleigh_ritz.c
 * @brief Functions of Rayleigh-Ritz method
 */

#include "header.h"

typedef struct {
  double abs;
  int index;
} AbsIndex;

/**
 * \addtogroup RayleighRitz
 * @{
 */

/**
 * Allocate settings of Rayleigh-Ritz method.
 * @param[in]    dimension    Dimension of original matrix
 * @param[in]    product      Definition of matrix vector product, whose memory is also freed when RayleighRitz structure is freed by rayleigh_ritz_free
 */
RayleighRitz *rayleigh_ritz_alloc (uint dimension, MatrixVectorProduct *product)
{
  RayleighRitz *rayleigh_ritz;
  rayleigh_ritz = (RayleighRitz *) g_malloc(sizeof(RayleighRitz));
  rayleigh_ritz->dimension = dimension;
  rayleigh_ritz->product = product;
  rayleigh_ritz->basis_number = 0;
  rayleigh_ritz->basis = NULL;
  rayleigh_ritz->matrix = NULL;
  return rayleigh_ritz;
}

/**
 * Free allocated memory of RayleighRitz
 * @param[in]    rayleigh_ritz    Settings of Raylegh-Ritz method
 */
void rayleigh_ritz_free (RayleighRitz *rayleigh_ritz)
{
  if (rayleigh_ritz->product) {
    matrix_vector_product_free(rayleigh_ritz->product);
  }
  if (rayleigh_ritz->basis) {
    g_free(rayleigh_ritz->basis);
  }
  if (rayleigh_ritz->matrix) {
    gsl_matrix_free(rayleigh_ritz->matrix);
  }
  g_free(rayleigh_ritz);
}

static double *rayleigh_ritz_basis_alloc_copy (uint dimension, uint basis_number, double *basis)
{
  double *allocated;
  size_t size;
  size = sizeof(double) * dimension * basis_number;
  allocated = (double *) g_malloc(size);
  memcpy(allocated, basis, size);
  return allocated;
}

/**
 * Set basis of an invariant space which is calculated by a suitable method.
 * For example, we can use ArnoldiMethod or SimultaneousIteration
 * in order to obtain an approximation of an invariant space.
 * @param[in]    rayleigh_ritz    Settings of Raylegh-Ritz method
 * @param[in]    basis_number Number of basis
 * @param[in]    basis        A pointer of the matrix in row-major order whose columns are basis vectors and whose size is (dimension * basis_number)
 */
void rayleigh_ritz_set_basis (RayleighRitz *rayleigh_ritz, uint basis_number, double *basis)
{
  if (basis_number <= 0) {
    fprintf(stderr, "Number of basis must be positive: %d\n", basis_number);
    abort();
  }
  if (rayleigh_ritz->basis) {
    g_free(rayleigh_ritz->basis);
  }
  rayleigh_ritz->basis_number = basis_number;
  rayleigh_ritz->basis = rayleigh_ritz_basis_alloc_copy(rayleigh_ritz->dimension, basis_number, basis);
}

/**
 * Set a definition of matrix-vector product
 * @param[in]    rayleigh_ritz    Settings of Raylegh-Ritz method
 * @param[in]    product          Definition of matrix vector product that is freed by rayleigh_ritz_free
 */
void rayleigh_ritz_set_matrix_vector_product (RayleighRitz *rayleigh_ritz, MatrixVectorProduct *product)
{
  if (rayleigh_ritz->product) {
    matrix_vector_product_free(rayleigh_ritz->product);
  }
  rayleigh_ritz->product = product;
}

/**
 * Set a square matrix in Rayleigh-Ritz method if the matrix is calculated by a way specific to the basis.
 * Note that this function is called after setting a basis of an invariant space.
 * @param[in]    rayleigh_ritz    Settings of Raylegh-Ritz method
 * @param[in]    matrix       A pointer of the square matrix in row-major-order, for which the eigenvalues and eigenvectors are actually calculated. The dimension of the square matrix equals number of bases.
 */
void rayleigh_ritz_set_matrix (RayleighRitz *rayleigh_ritz, const double *matrix)
{
  if (matrix && rayleigh_ritz->basis_number == 0) {
    fprintf(stderr, "Basis of invariant space must be set before setting matrix\n");
    abort();
  }
  if (matrix) {
    gsl_matrix_const_view m = gsl_matrix_const_view_array(matrix, rayleigh_ritz->basis_number, rayleigh_ritz->basis_number);
    if (!rayleigh_ritz->matrix) {
      rayleigh_ritz->matrix = gsl_matrix_alloc(rayleigh_ritz->basis_number, rayleigh_ritz->basis_number);
    }
    gsl_matrix_memcpy(rayleigh_ritz->matrix, &m.matrix);
  } else {
    if (rayleigh_ritz->matrix) {
      gsl_matrix_free(rayleigh_ritz->matrix);
      rayleigh_ritz->matrix = NULL;
    }
  }
}

static gsl_matrix *rayleigh_ritz_calculate_square_matrix (RayleighRitz *rayleigh_ritz)
{
  gsl_matrix *square;
  gsl_matrix_view basis_matrix;
  gsl_vector *v, *product_vector;
  gsl_vector_view col;
  int i;
  if (!rayleigh_ritz->product) {
    fprintf(stderr, "Can not calculate a square matrix in Rayleigh-Ritz method\nbecause the matrix-vector product is not defined.\n");
    abort();
  }
  square = gsl_matrix_alloc(rayleigh_ritz->basis_number, rayleigh_ritz->basis_number);
  basis_matrix = gsl_matrix_view_array(rayleigh_ritz->basis, rayleigh_ritz->dimension, rayleigh_ritz->basis_number);
  v = gsl_vector_alloc(rayleigh_ritz->dimension);
  product_vector = gsl_vector_alloc(rayleigh_ritz->dimension);
  for (i = 0; i < rayleigh_ritz->basis_number; i++) {
    gsl_matrix_get_col(v, &basis_matrix.matrix, i);
    matrix_vector_product_calculate(product_vector->data, rayleigh_ritz->product, v->data);
    col = gsl_matrix_column(square, i);
    gsl_blas_dgemv(CblasTrans, 1.0, &basis_matrix.matrix, product_vector, 0.0, &col.vector);
  }
  gsl_vector_free(v);
  gsl_vector_free(product_vector);
  return square;
}

static void rayleigh_ritz_normalize_double_complex_vector (int dim, double complex *vec)
{
  double norm;
  int i;
  norm = 0.0;
  for (i = 0; i < dim; i++) {
    norm += (creal(vec[i]) * creal(vec[i]) + cimag(vec[i]) * cimag(vec[i]));
  }
  norm = sqrt(norm);
  for (i = 0; i < dim; i++) {
    vec[i] = vec[i] / norm;
  }
}

static void rayleigh_ritz_convert_eigenvectors (double complex **eigenvector, RayleighRitz *rayleigh_ritz, double complex *eigenvector_small_matrix)
{
  int i, j, k;
  double complex val;
  for (i = 0; i < rayleigh_ritz->basis_number; i++) {
    for (j = 0; j < rayleigh_ritz->dimension; j++) {
      val = 0.0;
      for (k = 0; k < rayleigh_ritz->basis_number; k++) {
        val += (rayleigh_ritz->basis[j * rayleigh_ritz->basis_number + k] * eigenvector_small_matrix[rayleigh_ritz->basis_number * i + k]);
      }
      (*eigenvector)[i * rayleigh_ritz->dimension + j] = val;
    }
  }
  for (i = 0; i < rayleigh_ritz->basis_number; i++) {
    rayleigh_ritz_normalize_double_complex_vector(rayleigh_ritz->dimension, *eigenvector + i * rayleigh_ritz->dimension);
  }
}

/**
 * Allocate memory of eigenvalue and eigenvector and calculate values of the eigenvalue and the eigenector.
 * If both rayleigh_ritz->product and rayleigh_ritz->matrix are NULL then an error is raised
 * @param[out]    eigenvalue       A pointer to store eigenvalues which is allocate for (rayleigh_ritz->basis_number) complex numbers
 * @param[out]    eigenvector      A pointer of an array storing eigenvectors in order. The dimension of the array is (rayleigh_ritz->dimension * rayleigh_ritz->basis_number)
 * @param[in]     rayleigh_ritz    Settings of Raylegh-Ritz method
 * @return    Number of eigenvalues and eigenvectors. Return 0 if all eigenvalues can not be found.
 */
int rayleigh_ritz_eigen (double complex **eigenvalue, double complex **eigenvector, RayleighRitz *rayleigh_ritz)
{
  double complex *eigenvector_small_matrix;
  int number_of_eigenvectors;
  if (!rayleigh_ritz->product && !rayleigh_ritz->matrix) {
    fprintf(stderr, "Can not obtain a square matrix in Rayleigh-Ritz method\n");
    abort();
  }
  if (!rayleigh_ritz->matrix) {
    rayleigh_ritz->matrix = rayleigh_ritz_calculate_square_matrix(rayleigh_ritz);
  }
  number_of_eigenvectors = gsl_wrap_eigen(eigenvalue, &eigenvector_small_matrix, rayleigh_ritz->matrix);
  if (number_of_eigenvectors > 0) {
    *eigenvector = (double complex *) g_malloc(sizeof(double complex) * rayleigh_ritz->basis_number * rayleigh_ritz->dimension);
    rayleigh_ritz_convert_eigenvectors(eigenvector, rayleigh_ritz, eigenvector_small_matrix);
    g_free(eigenvector_small_matrix);
    return rayleigh_ritz->basis_number;
  } else {
    return 0;
  }
}

/**
 * Utility function to convert eigenvalues and eigenvectors which are calculated
 * by inverse iteration method with shift number.
 */
void rayleigh_ritz_convert_inverse_eigenvalues (double complex *eigenvalue, int num, double number_shift)
{
  int i;
  for (i = 0; i < num; i++) {
    eigenvalue[i] = 1.0 / eigenvalue[i] + number_shift;
  }
}

static gint abs_index_compare (gconstpointer a, gconstpointer b)
{
  if (((AbsIndex *) a)->abs < ((AbsIndex *) b)->abs) {
    return -1;
  } else if (((AbsIndex *) a)->abs > ((AbsIndex *) b)->abs) {
    return 1;
  } else {
    return 0;
  }
}

void rayleigh_ritz_eigen_sort_by_absolute_value (double complex *eigenvalue, double complex *eigenvector, int num, int dim)
{
  GArray *array;
  double complex *eigenvalue_tmp, *eigenvector_tmp;
  int i, vector_size, index;
  array = g_array_sized_new(FALSE, FALSE, sizeof(AbsIndex), num);
  g_array_set_size(array, num);
  for (i = 0; i < num; i++) {
    g_array_index(array, AbsIndex, i).index = i;
    g_array_index(array, AbsIndex, i).abs = cabs(eigenvalue[i]);
  }
  g_array_sort(array, abs_index_compare);

  vector_size = sizeof(double complex) * dim;
  eigenvalue_tmp = (double complex *) g_malloc(sizeof(double complex) * num);
  eigenvector_tmp = (double complex *) g_malloc(vector_size * num);
  memcpy(eigenvalue_tmp, eigenvalue, sizeof(double complex) * num);
  memcpy(eigenvector_tmp, eigenvector, vector_size * num);
  for (i = 0; i < num; i++) {
    index = g_array_index(array, AbsIndex, num - 1 - i).index;
    eigenvalue[i] = eigenvalue_tmp[index];
    memcpy(eigenvector + (i * dim), eigenvector_tmp + (index * dim), vector_size);
  }
  g_free(eigenvalue_tmp);
  g_free(eigenvector_tmp);
  g_array_free(array, TRUE);
}

/** @} */  /* End of RayleighRitz */
