/**
 * @file parameter_continuation_gmres.c
 * @brief GMRES to calculate tangent vector for parameter continuation
 */

#include "header.h"

/**
 * \addtogroup ParameterContinuationGMRES
 * @{
 */

DataContainer *parameter_continuation_gmres_setting_alloc (const char *parameter_key, int extended_dim)
{
  DataContainer *setting;
  char *key;
  gsl_vector *tangent_vector, *pt;
  key = g_strdup(parameter_key);
  tangent_vector = gsl_vector_alloc(extended_dim);
  pt = gsl_vector_alloc(extended_dim);
  setting = data_container_alloc(PARAMETER_CONTINUATION_GMRES_N, key, g_strdup, g_free, tangent_vector, gsl_vector_duplicate, gsl_vector_free, pt, gsl_vector_duplicate, gsl_vector_free, NULL, NULL, NULL);
  return setting;
}

DataContainer *parameter_continuation_newton_gmres_setting_alloc (const char *parameter_key, int extended_dim, gsl_vector *pt_with_parameter)
{
  DataContainer *setting;
  char *key;
  gsl_vector *tangent_vector, *pt_last;
  key = g_strdup(parameter_key);
  tangent_vector = gsl_vector_alloc(extended_dim);
  pt_last = gsl_vector_alloc(extended_dim);
  setting = data_container_alloc(PARAMETER_CONTINUATION_GMRES_N, key, g_strdup, g_free, tangent_vector, gsl_vector_duplicate, gsl_vector_free, pt_with_parameter, NULL, NULL, pt_last, gsl_vector_duplicate, gsl_vector_free);
  return setting;
}

void parameter_continuation_gmres_setting_set_tangent_vector (DataContainer *setting, gsl_vector *tangent_vector)
{
  gsl_vector *v;
  v = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_TANGENT_VECTOR);
  if (!v) {
    fprintf(stderr, "Can not set tangent vector\n");
    abort();
  }
  gsl_vector_memcpy(v, tangent_vector);
}

void parameter_continuation_gmres_setting_set_pt_last (DataContainer *setting, gsl_vector *pt_last)
{
  gsl_vector *v;
  v = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_LAST);
  if (!v) {
    fprintf(stderr, "Can not set value of last point\n");
    abort();
  }
  gsl_vector_memcpy(v, pt_last);
}

void parameter_continuation_gmres_setting_set_pt_with_parameter (DataContainer *setting, gsl_vector *pt_with_parameter)
{
  gsl_vector *pt;
  pt = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER);
  gsl_vector_memcpy(pt, pt_with_parameter);
}

static void parameter_continuation_gmres_product_jacobian_with_parameter_derivative (double *av, JacobianFreeGMRES *gmres, DataContainer *setting, const double *v)
{
  int i, d;
  char *parameter_key;
  gsl_vector *pt_with_parameter, *parameter_derivative;
  parameter_key = (char *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PARAMETER_KEY);
  pt_with_parameter = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER);
  d = pt_with_parameter->size - 1;
  dynamical_system_parameter_set_double(jacobian_free_gmres_dynamical_system(gmres), parameter_key, gsl_vector_get(pt_with_parameter, d));
  parameter_derivative = gsl_vector_alloc(d);
  jacobian_free_gmres_product_jacobian_vector(av, gmres, pt_with_parameter->data, v);
  jacobian_free_gmres_parameter_derivative(parameter_derivative->data, gmres, parameter_key, pt_with_parameter->data);
  for (i = 0; i < d; i++) {
    av[i] += gsl_vector_get(parameter_derivative, i) * v[d];
  }
  gsl_vector_free(parameter_derivative);
}

double parameter_continuation_gmres_inner_product (gsl_vector *vec, const double *v)
{
  int i;
  double val;
  val = 0.0;
  for (i = 0; i < vec->size; i++) {
    val += gsl_vector_get(vec, i) * v[i];
  }
  return val;
}

static void parameter_continuation_gmres_product_of_matrix_vector (double *av, JacobianFreeGMRES *gmres, const double *v)
{
  DataContainer *setting;
  setting = (DataContainer *) gmres->data;
  parameter_continuation_gmres_product_jacobian_with_parameter_derivative(av, gmres, setting, v);
  av[parameter_continuation_gmres_setting_dimension(setting) - 1] = parameter_continuation_gmres_inner_product((gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_TANGENT_VECTOR), v);
}

JacobianFreeGMRES *parameter_continuation_gmres_alloc (DynamicalSystem *dynamical_system, DataContainer *parameter_continuation_gmres_setting)
{
  JacobianFreeGMRES *gmres;
  gmres = jacobian_free_gmres_alloc(dynamical_system, dynamical_system_dimension(dynamical_system) + 1, parameter_continuation_gmres_setting, parameter_continuation_gmres_product_of_matrix_vector);
  return gmres;
}

/** @} */  /* End of group */
