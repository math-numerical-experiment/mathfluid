#ifndef _PARAMETER_CONTINUATION_GMRES_H_
#define _PARAMETER_CONTINUATION_GMRES_H_

/**
 * \addtogroup ParameterContinuationGMRES
 * @{
 */

enum {
  PARAMETER_CONTINUATION_GMRES_PARAMETER_KEY,
  PARAMETER_CONTINUATION_GMRES_TANGENT_VECTOR,
  PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER,
  PARAMETER_CONTINUATION_GMRES_PT_LAST,
  PARAMETER_CONTINUATION_GMRES_N
};

double parameter_continuation_gmres_inner_product (gsl_vector *vec, const double *v);
DataContainer *parameter_continuation_gmres_setting_alloc (const char *parameter_key, int extended_dim);
DataContainer *parameter_continuation_newton_gmres_setting_alloc (const char *parameter_key, int extended_dim, gsl_vector *pt_with_parameter);
void parameter_continuation_gmres_setting_set_tangent_vector (DataContainer *setting, gsl_vector *t);
void parameter_continuation_gmres_setting_set_pt_with_parameter (DataContainer *setting, gsl_vector *pt_with_parameter);
JacobianFreeGMRES *parameter_continuation_gmres_alloc (DynamicalSystem *dynamical_system, DataContainer *parameter_continuation_gmres_setting);

#define parameter_continuation_gmres_setting_extended_dimension(setting) ((gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER))->size

/** @} */  /* End of ParameterContinuationGMRES */

#endif /* _PARAMETER_CONTINUATION_GMRES_H_ */
