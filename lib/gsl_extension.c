/**
 * @file gsl_extension.c
 * @brief Extensions of GSL
 */

#include "header.h"

/**
 * \addtogroup GSL
 * @{
 */

#define RANDOM_MATRIX_GENERATION_MAX_ABS 500

/**
 * Set components of a vector from an array of double
 * @param[in]    v      A vector
 * @param[in]    src    Source array of double
 */
void gsl_vector_set_components (gsl_vector *v, const double *src)
{
  if (!src) {
    fprintf(stderr, "An array of double must not be NULL\n");
    abort();
  }
  if (v->stride == 1) {
    memcpy(v->data, src, sizeof(double) * v->size);
  } else {
    int i;
    for (i = 0; i < v->size; i++) {
      gsl_vector_set(v, i, src[i]);
    }
  }
}

/**
 * Allocate a vector and set its components.
 * @param[in]    n      Dimension of the vector
 * @param[in]    src    A pointer of an array of vector components
 * @return    A pointer of the allocated vector
 */
gsl_vector *gsl_vector_alloc_set_components (const size_t n, const double *src)
{
  gsl_vector *v;
  v = gsl_vector_alloc(n);
  gsl_vector_set_components(v, src);
  return v;
}

/**
 * Copy the components of a vector to an array of double.
 * @param[out]   components   A pointer of output array
 * @param[in]    v          Source vector
 */
void gsl_vector_copy_components (double *components, gsl_vector *v)
{
  if (v->stride == 1) {
    memcpy(components, v->data, sizeof(double) * v->size);
  } else {
    int i;
    for (i = 0; i < v->size; i++) {
      components[i] = gsl_vector_get(v, i);
    }
  }
}

/**
 * Allocate an array of double and copy the components of a vector to the array.
 * @param[in]    v          Source vector
 * @return    A pointer of the allocated array of double
 */
double *gsl_vector_alloc_copy_components (gsl_vector *v)
{
  double *components;
  components = g_malloc(sizeof(double) * v->size);
  gsl_vector_copy_components(components, v);
  return components;
}

/**
 * Allocate a duplicated vector.
 * @param[in]    vector    Source vector
 * @return    A pointer of the allocated vector whose components are same as those of the argument "vector"
 */
gsl_vector *gsl_vector_duplicate (gsl_vector *vector)
{
  gsl_vector *vector_new;
  vector_new = gsl_vector_alloc(vector->size);
  gsl_vector_memcpy(vector_new, vector);
  return vector_new;
}

void gsl_vector_set_random_normalized_components (gsl_vector *vector)
{
  int i;
  double norm;
  for (i = 0; i < vector->size; i++) {
    gsl_vector_set(vector, i,(double) mathfluid_utils_random_integer(RANDOM_MATRIX_GENERATION_MAX_ABS));
  }
  norm = gsl_blas_dnrm2(vector);
  gsl_vector_scale(vector, 1.0 / norm);
}

/**
 * Set elements of a matrix from an array of double
 * @param[in]    m      A matrix
 * @param[in]    src    Source array of double in row-major order
 */
void gsl_matrix_set_elements (gsl_matrix *m, const double *src)
{
  if (!src) {
    fprintf(stderr, "An array of double must not be NULL\n");
    abort();
  }
  if (m->tda == m->size2) {
    memcpy(m->data, src, sizeof(double) * m->size1 * m->size2);
  } else {
    int i, j;
    for (i = 0; i < m->size1; i++) {
      for (j = 0; j < m->size2; j++) {
        gsl_matrix_set(m, i, j, src[i * m->size1 + j]);
      }
    }
  }
}

/**
 * Allocate a matrix and set its elements.
 * @param[in]    n1     Size of row of the matrix
 * @param[in]    n2     Size of column of the matrix
 * @param[in]    src    A pointer of an array of matrix elements
 * @return    A pointer of the allocated matrix
 */
gsl_matrix *gsl_matrix_alloc_set_elements (const size_t n1, const size_t n2, const double *src)
{
  gsl_matrix *m;
  m = gsl_matrix_alloc(n1, n2);
  gsl_matrix_set_elements(m, src);
  return m;
}

/**
 * Copy the elements of a matrix to an array of double.
 * @param[out]   elements   A pointer of output array
 * @param[in]    m          Source matrix
 */
void gsl_matrix_copy_elements (double *elements, gsl_matrix *m)
{
  if (m->tda == m->size2) {
    memcpy(elements, m->data, sizeof(double) * m->size1 * m->size2);
  } else {
    int i, j;
    for (i = 0; i < m->size1; i++) {
      for (j = 0; j < m->size2; j++) {
        elements[i * m->size1 + j] = gsl_matrix_get(m, i, j);
      }
    }
  }
}

/**
 * Allocate an array of double and copy the elements of a matrix to the array.
 * @param[in]    m          Source matrix
 * @return    A pointer of the allocated array of double
 */
double *gsl_matrix_alloc_copy_elements (gsl_matrix *m)
{
  double *elements;
  elements = g_malloc(sizeof(double) * m->size1 * m->size2);
  gsl_matrix_copy_elements(elements, m);
  return elements;
}

/**
 * Allocate a duplicated matrix.
 * @param[in]    matrix    Source matrix
 * @return    A pointer of the allocated matrix whose elements are same as those of the argument "matrix"
 */
gsl_matrix *gsl_matrix_duplicate (gsl_matrix *matrix)
{
  gsl_matrix *matrix_new;
  matrix_new = gsl_matrix_alloc(matrix->size1, matrix->size2);
  gsl_matrix_memcpy(matrix_new, matrix);
  return matrix_new;
}

void gsl_matrix_orthonormalize_columns (gsl_matrix *q) {
  int i, j;
  double inner_product;
  gsl_vector_view v;
  gsl_vector *w;
  w = gsl_vector_alloc(q->size1);
  for (i = 0; i < q->size2; i++) {
    v = gsl_matrix_column(q, i);
    for (j = 0; j < i; j++) {
      gsl_matrix_get_col(w, q, j);
      gsl_blas_ddot(w, &v.vector, &inner_product);
      gsl_vector_scale(w, -inner_product);
      gsl_vector_sub(&v.vector, w);
    }
    gsl_vector_scale(&v.vector, 1.0 / gsl_blas_dnrm2(&v.vector));
  }
  gsl_vector_free(w);
}

/**
 * Set randomly generated normalized orthogonal vectors to columns of matrix.
 * Note that we suppose that columns generated randomly are linearly independent.
 */
void gsl_matrix_set_random_normalized_orthogonal_columns (gsl_matrix *q)
{
  int i, j;
  for (i = 0; i < q->size1; i++) {
    for (j = 0; j < q->size2; j++) {
      gsl_matrix_set(q, i, j, (double) mathfluid_utils_random_integer(RANDOM_MATRIX_GENERATION_MAX_ABS));
    }
  }
  gsl_matrix_orthonormalize_columns(q);
}

void gsl_matrix_normalize_all_columns (gsl_matrix *a)
{
  gsl_vector_view view_col;
  int i;
  for (i = 0; i < a->size2; i++) {
    view_col = gsl_matrix_column(a, i);
    gsl_vector_scale(&view_col.vector, 1.0 / gsl_blas_dnrm2(&view_col.vector));
  }
}

/**
 * @param[in]    dim    Dimension of square matrix
 * @return    Square upper triangular matrix with random elements
 */
gsl_matrix *gsl_matrix_alloc_random_upper_triangular_matrix (int dim)
{
  int i, j;
  gsl_matrix *m;
  m = gsl_matrix_alloc(dim, dim);
  for (i = 0; i < dim; i++) {
    for (j = 0; j < i; j++) {
      gsl_matrix_set(m, i, j, 0.0);
    }
    for (; j < dim; j++) {
      gsl_matrix_set(m, i, j, mathfluid_utils_random_integer(RANDOM_MATRIX_GENERATION_MAX_ABS));
    }
  }
  gsl_matrix_normalize_all_columns(m);
  return m;
}

/** @} */  /* End of GSL */
