/**
 * @file search_periodic_orbit.c
 * @brief Functions of search for periodic orbits of ODE
 */

#include "header.h"

/**
 * \addtogroup SearchPeriodicOrbit
 * @{
 */

static void search_periodic_orbit_update_pt_after_evolution_of_period (SearchPeriodicOrbit *search)
{
  if (!search->pt_after_evolution_of_period_updated) {
    search->pt_after_evolution_of_period->time = 0.0;
    memcpy(search->pt_after_evolution_of_period->coordinate, search_periodic_orbit_current_initial_point_ptr(search), sizeof(double) * search_periodic_orbit_system_dimension(search));
    if (!dynamical_system_evolve(search->pt_after_evolution_of_period, search_periodic_orbit_dynamical_system(search), search_periodic_orbit_current_period(search))) {
      fprintf(stderr, "Can not evolve dynamical system\n");
      abort();
    }
    search->pt_after_evolution_of_period_updated = TRUE;
  }
}

/**
 * This function defines F for equation F(x) = 0
 */
static void search_periodic_orbit_calculate_newton_b (double *value, JacobianFreeGMRES *gmres, const double *pt)
{
  /*
   * To cut computation, we ignore pt and use search_periodic_orbit_current_initial_point_ptr(search).
   * So, when calling this function, pt must be equal to search_periodic_orbit_current_initial_point_ptr(search).
   */
  SearchPeriodicOrbit *search;
  int i;
  search = (SearchPeriodicOrbit *) gmres->data;
  search_periodic_orbit_update_pt_after_evolution_of_period(search);
  /* for (i = 0; i < search_periodic_orbit_system_dimension(search); i++) { */
  /*   if (search_periodic_orbit_current_initial_point_ptr(search)[i] != pt[i]) { */
  /*     printf("Not coincide: %.14lf %.14lf\n", search_periodic_orbit_current_initial_point_ptr(search)[i], pt[i]); */
  /*     abort(); */
  /*   } */
  /* } */
  for (i = 0; i < search_periodic_orbit_system_dimension(search); i++) {
    value[i] = search->pt_after_evolution_of_period->coordinate[i] - search_periodic_orbit_current_initial_point_ptr(search)[i];
  }
  value[search_periodic_orbit_system_dimension(search)] = 0.0;
}

/**
 * This function defines product of dF/dx and v for equation F(x) = 0
 */
static void search_periodic_orbit_product_of_matrix_vector (double *av, JacobianFreeGMRES *gmres, const double *v)
{
  SearchPeriodicOrbit *search;
  DerivativeOfSystem *derivative_of_system;
  SystemState *state;
  int i;
  double *f1, *f2;
  search = (SearchPeriodicOrbit *) gmres->data;
  search_periodic_orbit_update_pt_after_evolution_of_period(search);
  derivative_of_system = gmres->derivative;

  /* state->coordinate : phi_T(X + dx) */
  state = system_state_alloc(search_periodic_orbit_system_dimension(search));
  state->time = 0.0;
  for (i = 0; i < search_periodic_orbit_system_dimension(search); i++) {
    state->coordinate[i] = search_periodic_orbit_current_initial_point_ptr(search)[i] + v[i];
  }
  if (!dynamical_system_evolve(state, search_periodic_orbit_dynamical_system(search), search_periodic_orbit_current_period(search))) {
    fprintf(stderr, "Can not evolve\n");
    abort();
  }

  /* f1 : F(phi_T(X)) */
  /* f2 : F(X) */
  f1 = (double *) g_malloc(sizeof(double) * search_periodic_orbit_system_dimension(search));
  f2 = (double *) g_malloc(sizeof(double) * search_periodic_orbit_system_dimension(search));
  derivative_of_system_time_derivative(f1, derivative_of_system, search->pt_after_evolution_of_period->coordinate);
  derivative_of_system_time_derivative(f2, derivative_of_system, search_periodic_orbit_current_initial_point_ptr(search));
  for (i = 0; i < search_periodic_orbit_system_dimension(search); i++) {
    av[i] = state->coordinate[i] - search->pt_after_evolution_of_period->coordinate[i] - v[i] + f1[i] * v[search_periodic_orbit_system_dimension(search)];
  }
  av[search_periodic_orbit_system_dimension(search)] = cblas_ddot(search_periodic_orbit_system_dimension(search), f2, 1, v, 1);

  system_state_free(state);
  g_free(f1);
  g_free(f2);
}

static void search_periodic_orbit_function_after_newton_gmres_iteration (JacobianFreeNewtonGMRES *newton_gmres)
{
  SearchPeriodicOrbit *search;
  search = (SearchPeriodicOrbit *) newton_gmres->gmres->data;
  search->pt_after_evolution_of_period_updated = FALSE;
}

/**
 * Allocate SearchPeriodicOrbit that must be freed by search_periodic_orbit_free later.
 * @param[in]    dynamical_system    Dynamical system to be dealt with
 * @param[in]    damping_parameter   Damp modified vectors with scale 2^{-damping_parameter}. Nonpositive value is ignored.
 * @param[in]    max_error           Maximum error of Newton method
 * @param[in]    practical_error     Practical error of Newton method
 */
SearchPeriodicOrbit *search_periodic_orbit_alloc (DynamicalSystem *dynamical_system, int damping_parameter, double max_error, double practical_error)
{
  SearchPeriodicOrbit *search;
  search = g_malloc(sizeof(SearchPeriodicOrbit));
  search->pt_after_evolution_of_period_updated = FALSE;
  search->pt_after_evolution_of_period = system_state_alloc(dynamical_system_dimension(dynamical_system));
  search->gmres = jacobian_free_gmres_alloc(dynamical_system, dynamical_system_dimension(dynamical_system) + 1, search, search_periodic_orbit_product_of_matrix_vector);
  search->newton_gmres = jacobian_free_newton_gmres_alloc(search->gmres, search_periodic_orbit_calculate_newton_b, damping_parameter, max_error);
  jacobian_free_newton_gmres_set_function_after_iteration(search->newton_gmres, search_periodic_orbit_function_after_newton_gmres_iteration);
  jacobian_free_newton_gmres_set_practical_error(search->newton_gmres, practical_error);
  return search;
}

/**
 * Free SearchPeriodicOrbit
 * @param[in]    search    Settings of search periodic orbits
 */
void search_periodic_orbit_free (SearchPeriodicOrbit *search)
{
  if (search->pt_after_evolution_of_period) {
    system_state_free(search->pt_after_evolution_of_period);
  }
  if (search->gmres) {
    jacobian_free_gmres_free(search->gmres);
  }
  if (search->newton_gmres) {
    jacobian_free_newton_gmres_free(search->newton_gmres);
  }
  g_free(search);
}

/**
 * Set log levels of SearchPeriodicOrbit
 * @param[in]    search    Settings of search periodic orbits
 * @param[in]    newton_level    Log level of Newton method
 * @param[in]    gmres_level     Log level of GMRES solver
 */
void search_periodic_orbit_set_log_level (SearchPeriodicOrbit *search, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level)
{
  jacobian_free_newton_gmres_set_log_level(search->newton_gmres, newton_level);
  jacobian_free_gmres_set_log_level(search->gmres, gmres_level);
}

/**
 * Set the coordinate of an initial point and initial period
 * @param[in]    search    Settings of search periodic orbits
 * @param[in]    point     An array of the coordinate of an initial point. If we do not want to change the value then the value is NULL.
 * @param[in]    period    Initial period. If we do not want to change the value then the value is nonpositive.
 */
void search_periodic_orbit_set2 (SearchPeriodicOrbit *search, const double *point, double period)
{
  double *vector;
  vector = (double *) g_malloc(sizeof(double) * search_periodic_orbit_total_dimension(search));
  if (point) {
    memcpy(vector, point, sizeof(double) * search_periodic_orbit_system_dimension(search));
  } else {
    memcpy(vector, search_periodic_orbit_solution_ptr(search), sizeof(double) * search_periodic_orbit_system_dimension(search));
  }
  if (period > 0.0) {
    vector[search_periodic_orbit_system_dimension(search)] = period;
  } else {
    vector[search_periodic_orbit_system_dimension(search)] = search_periodic_orbit_current_period(search);
  }
  search_periodic_orbit_set(search, vector);
  g_free(vector);
}

/** @} */  /* End of SearchPeriodicOrbit */
