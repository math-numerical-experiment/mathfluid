/**
 * @file mathfluid_argv.c
 * @brief Functions to deal with argv
 */

#include "header.h"

/**
 * \addtogroup MathFluidARGV
 * @{
 */

MathFluidARGV *mathfluid_argv_alloc ()
{
  MathFluidARGV *mathfluid_argv;
  mathfluid_argv = (MathFluidARGV *) g_malloc(sizeof(MathFluidARGV));
  mathfluid_argv->array = g_ptr_array_new();
  mathfluid_argv->allocated = g_ptr_array_new_with_free_func(g_free);
  return mathfluid_argv;
}

static GPtrArray *mathfluid_argv_read_arguments_from_io (FILE *io)
{
  int i, n;
  size_t allocated;
  char *buf;
  GPtrArray *arguments;
  arguments = g_ptr_array_new_with_free_func(g_free);
  buf = NULL;
  allocated = 0;
  while (TRUE) {
    /* I do not know whether getline function supports all linefeed codes \n, \r, and \r\n. */
    n = getline(&buf, &allocated, io);
    if (n <= 0) {
      break;
    }
    if (buf[0] != '#') {
      for (i = 0; i < n; i++) {
        if (buf[i] == '\r' || buf[i] == '\n') {
          buf[i] = '\0';
          n = i + 1;
          break;
        }
      }
      if (n > 0) {
        g_ptr_array_add(arguments, g_strdup(buf));
      }
    }
  }
  if (buf) {
    g_free(buf);
  }
  return arguments;
}

static GPtrArray *mathfluid_argv_read_arguments_from_file (char *filename)
{
  if (g_file_test(filename, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)) {
    FILE *io;
    GPtrArray *arguments;
    io = fopen(filename, "r");
    if (io == NULL) {
      fprintf(stderr, "Can not open file %s\n", filename);
      abort();
    }
    arguments = mathfluid_argv_read_arguments_from_io(io);
    fclose(io);
    return arguments;
  } else {
    return NULL;
  }
}

static void mathfluid_argv_take_arguments_in (MathFluidARGV *mathfluid_argv, GPtrArray *arguments, int index)
{
  int i;
  for (i = 0; i < arguments->len; i++) {
    g_ptr_array_insert(mathfluid_argv->allocated, mathfluid_argv->allocated->len, g_ptr_array_index(arguments, i));
    g_ptr_array_insert(mathfluid_argv->array, index, g_ptr_array_index(arguments, i));
    g_ptr_array_index(arguments, i) = NULL;
    if (index >= 0) {
      index += 1;
    }
  }
}

static void mathfluid_argv_read_io (MathFluidARGV *mathfluid_argv, FILE *io)
{
  GPtrArray *arguments;
  arguments = mathfluid_argv_read_arguments_from_io(io);
  mathfluid_argv_take_arguments_in(mathfluid_argv, arguments, -1);
  g_ptr_array_free(arguments, TRUE);
}

static void mathfluid_argv_append_with_reading_file (MathFluidARGV *mathfluid_argv, int argc, char **argv, int index_read_file)
{
  int i;
  for (i = 0; i < argc; i++) {
    if (i == index_read_file) {
      if (!mathfluid_argv_read_file(mathfluid_argv, argv[i])) {
        g_ptr_array_add(mathfluid_argv->array, argv[i]);
      }
    } else {
      g_ptr_array_add(mathfluid_argv->array, argv[i]);
    }
  }
}

/**
 * Allocate MathFluidARGV and set arguments from argc and argv.
 * Each line of input data of stdin and a file corresponds to a command line argument.
 * A line whose first character is "#" is a comment line and is ignored.
 * @param[in]     argc        Number of command line arguments
 * @param[in]     argv        String array of command line arguments
 * @param[in]     read_stdin  Read arguments from piped stdin
 * @param[in]     index_read_file   Read arguments from a file if the specified argument is file path. Set -1 if there is no file to read.
 * @return    Allocated MathFluidARGV that is freed by mathfluid_argv_free
 */
MathFluidARGV *mathfluid_argv_alloc_parse (int argc, char **argv, gboolean read_stdin, int index_read_file)
{
  MathFluidARGV *mathfluid_argv;
  mathfluid_argv = mathfluid_argv_alloc();
  if (read_stdin) {
    if (!isatty(fileno(stdin))) {
      mathfluid_argv_read_io(mathfluid_argv, stdin);
    }
  }
  if (argc > 1) {
    mathfluid_argv_append_with_reading_file(mathfluid_argv, argc, argv, index_read_file);
  }
  return mathfluid_argv;
}

void mathfluid_argv_free (MathFluidARGV *mathfluid_argv)
{
  g_ptr_array_free(mathfluid_argv->allocated, TRUE);
  g_ptr_array_free(mathfluid_argv->array, TRUE);
  g_free(mathfluid_argv);
}

void mathfluid_argv_reset (MathFluidARGV *mathfluid_argv)
{
  mathfluid_argv->array = g_ptr_array_remove_range(mathfluid_argv->array, 0, mathfluid_argv->array->len);
}

void mathfluid_argv_append (MathFluidARGV *mathfluid_argv, int argc, char **argv)
{
  int i;
  for (i = 0; i < argc; i++) {
    g_ptr_array_add(mathfluid_argv->array, argv[i]);
  }
}

char *mathfluid_argv_take_out (MathFluidARGV *mathfluid_argv, int index)
{
  if (mathfluid_argv->array->len > index) {
    return (char *) g_ptr_array_remove_index(mathfluid_argv->array, index);
  } else {
    return NULL;
  }
}

gboolean mathfluid_argv_read_file (MathFluidARGV *mathfluid_argv, char *filename)
{
  GPtrArray *arguments;
  arguments = mathfluid_argv_read_arguments_from_file(filename);
  if (arguments) {
    mathfluid_argv_take_arguments_in(mathfluid_argv, arguments, -1);
    g_ptr_array_free(arguments, TRUE);
    return TRUE;
  } else {
    return FALSE;
  }
}

void mathfluid_argv_parse_file_option (MathFluidARGV *mathfluid_argv, char *file_option)
{
  int i;
  char *option;
  for (i = 0; i < mathfluid_argv->array->len; i++) {
    option = (char *) g_ptr_array_index(mathfluid_argv->array, i);
    if (g_strcmp0(option, file_option) == 0) {
      g_ptr_array_remove_index(mathfluid_argv->array, i);
      option = (char *) g_ptr_array_index(mathfluid_argv->array, i);
      if (option) {
        GPtrArray *arguments;
        arguments = mathfluid_argv_read_arguments_from_file(option);
        if (arguments) {
          if (arguments->len == 0) {
            fprintf(stderr, "No arguments can be loaded from %s\n", option);
            abort();
          }
          g_ptr_array_remove_index(mathfluid_argv->array, i);
          mathfluid_argv_take_arguments_in(mathfluid_argv, arguments, i);
        } else {
          fprintf(stderr, "File '%s' does not exist\n", option);
          abort();
        }
      } else {
        fprintf(stderr, "Specify a file that store arguments of command line options\n");
        abort();
      }
    }
  }
}

/** @} */  /* End of MathFluidARGV */
