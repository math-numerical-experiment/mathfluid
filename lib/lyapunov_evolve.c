/**
 * @file lyapunov_evolve.c
 * @brief Time evolution of Lyapunov analysis
 */

#include "header.h"

enum {
  LYAPUNOV_EVOLVE_THREAD_DATA_DERIVATIVE,
  LYAPUNOV_EVOLVE_THREAD_DATA_DYNAMICAL_SYSTEM,
  LYAPUNOV_EVOLVE_THREAD_DATA_N
};

typedef struct {
  gsl_matrix *qr;
  LyapunovEvolve *evolve;
  double time_step;
  GPtrArray *orbit;
  gsl_matrix *q;
} LyapunovVectorsEvolveThreadArgs;

/**
 * \addtogroup LyapunovEvolve
 * @{
 */

static void *lyapunov_evolve_alloc_derivative_of_system_for_thread (void *data, int index)
{
  DerivativeOfSystem *derivative_src, *derivative_dup;
  DataContainer *container;
  DynamicalSystem *dynamical_system_dup;
  derivative_src = (DerivativeOfSystem *) data;
  derivative_dup = derivative_of_system_duplicate(derivative_src);
  dynamical_system_dup = dynamical_system_duplicate(derivative_src->dynamical_system);
  derivative_src->dynamical_system = dynamical_system_dup;
  container = data_container_alloc(LYAPUNOV_EVOLVE_THREAD_DATA_N, derivative_dup, derivative_of_system_duplicate, derivative_of_system_free, dynamical_system_dup, dynamical_system_duplicate, dynamical_system_free);
  return (void *) container;
}

LyapunovEvolve *lyapunov_evolve_alloc (DynamicalSystem *dynamical_system, gboolean use_thread, LyapunovEvolveAlgorithm algorithm, double time_derivative_perturbation)
{
  LyapunovEvolve *evolve;
  evolve = (LyapunovEvolve *) g_malloc(sizeof(LyapunovEvolve));
  evolve->dynamical_system = dynamical_system;
  evolve->use_thread = use_thread;
  evolve->algorithm = algorithm;
  evolve->derivative = derivative_of_system_alloc(evolve->dynamical_system);
  derivative_of_system_set_time_derivative_perturbation(evolve->derivative, time_derivative_perturbation);
  evolve->thread_variable = mathfluid_thread_variable_alloc(lyapunov_evolve_alloc_derivative_of_system_for_thread, (void (*) (void *data)) dynamical_system_free, evolve->derivative);
  return evolve;
}

void lyapunov_evolve_free (LyapunovEvolve *evolve)
{
  if (evolve->thread_variable) {
    mathfluid_thread_variable_free(evolve->thread_variable);
  }
  if (evolve->derivative) {
    derivative_of_system_free(evolve->derivative);
  }
  g_free(evolve);
}

/**
 * Evolve the column_number-th column of q at x for time_step by Jacobian of time derivative of orbit.
 * @param[out]   qr                  Evolved matrix
 * @param[in]    evolve              Settings of evolution
 * @param[in]    derivative          Derivative of settings for current thread
 * @param[in]    time_step           Time to evolve
 * @param[in]    orbit               Orbit in phase space
 * @param[in]    q                   Matrix to be evolved
 * @param[in]    column_number       Index of target column
 */
static void lyapunov_evolve_euler (gsl_matrix *qr, LyapunovEvolve *evolve, DerivativeOfSystem *derivative, double time_step, GPtrArray *orbit, gsl_matrix *q, int column_number)
{
  gsl_vector *col, *col_new;
  SystemState *state;
  col = gsl_vector_alloc(lyapunov_evolve_system_dimension(evolve));
  col_new = gsl_vector_alloc(lyapunov_evolve_system_dimension(evolve));
  state = (SystemState *) g_ptr_array_index(orbit, 0);
  gsl_matrix_get_col(col, q, column_number);
  derivative_of_system_jacobian_vector_product(col_new->data, derivative, state->coordinate, col->data);
  gsl_vector_scale(col_new, time_step);
  gsl_vector_add(col_new, col);
  gsl_matrix_set_col(qr, column_number, col_new);
  gsl_vector_free(col);
  gsl_vector_free(col_new);
}

static void lyapunov_evolve_rk4 (gsl_matrix *qr, LyapunovEvolve *evolve, DerivativeOfSystem *derivative, double time_step, GPtrArray *orbit, gsl_matrix *q, int column_number)
{
  gsl_vector *col, *col_new, *k[4];
  int i;
  col = gsl_vector_alloc(lyapunov_evolve_system_dimension(evolve));
  col_new = gsl_vector_alloc(lyapunov_evolve_system_dimension(evolve));
  for (i = 0; i < 4; i++) {
    k[i] = gsl_vector_alloc(lyapunov_evolve_system_dimension(evolve));
  }
  gsl_matrix_get_col(col, q, column_number);

  derivative_of_system_jacobian_vector_product(k[0]->data, derivative, ((SystemState *) g_ptr_array_index(orbit, 0))->coordinate, col->data);
  gsl_vector_memcpy(col_new, k[0]);
  gsl_vector_scale(col_new, time_step / 2.0);
  gsl_vector_add(col_new, col);
  derivative_of_system_jacobian_vector_product(k[1]->data, derivative, ((SystemState *) g_ptr_array_index(orbit, 1))->coordinate, col_new->data);
  gsl_vector_memcpy(col_new, k[1]);
  gsl_vector_scale(col_new, time_step / 2.0);
  gsl_vector_add(col_new, col);
  derivative_of_system_jacobian_vector_product(k[2]->data, derivative, ((SystemState *) g_ptr_array_index(orbit, 1))->coordinate, col_new->data);
  gsl_vector_memcpy(col_new, k[2]);
  gsl_vector_scale(col_new, time_step);
  gsl_vector_add(col_new, col);
  derivative_of_system_jacobian_vector_product(k[3]->data, derivative, ((SystemState *) g_ptr_array_index(orbit, 2))->coordinate, col_new->data);

  gsl_vector_memcpy(col_new, k[1]);
  gsl_vector_add(col_new, k[2]);
  gsl_vector_scale(col_new, 2.0);
  gsl_vector_add(col_new, k[0]);
  gsl_vector_add(col_new, k[3]);
  gsl_vector_scale(col_new, time_step / 6.0);
  gsl_vector_add(col_new, col);

  gsl_matrix_set_col(qr, column_number, col_new);
  gsl_vector_free(col);
  gsl_vector_free(col_new);
  for (i = 0; i < 4; i++) {
    gsl_vector_free(k[i]);
  }
}

/**
 * Note that the argument dynamical_system is evolve->dynamical_system or a copy of evolve->dynamical_system.
 * If we call lyapunov_evolve_forward_every_column_step in some threads simultaneously,
 * we should use a copy of evolve->dynamical_system.
 */
static void lyapunov_evolve_forward_every_column_step (gsl_matrix *qr, LyapunovEvolve *evolve, DerivativeOfSystem *derivative, double time_step, GPtrArray *orbit, gsl_matrix *q, int column_start, int column_step)
{
  int i;
  void (*func_evolve) (gsl_matrix *qr, LyapunovEvolve *evolve, DerivativeOfSystem *derivative, double time_step, GPtrArray *orbit, gsl_matrix *q, int column_number);
  if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_EULER) {
    func_evolve = lyapunov_evolve_euler;
  } else if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_RK4) {
    func_evolve = lyapunov_evolve_rk4;
  } else {
    fprintf(stderr, "Invalid algorithm\n");
    exit(1);
  }
  for (i = column_start; i < qr->size2; i += column_step) {
    func_evolve(qr, evolve, derivative, time_step, orbit, q, i);
  }
}

static gsl_matrix *lyapunov_evolve_forward_without_threads (LyapunovEvolve *evolve, double time_step, GPtrArray *orbit, gsl_matrix *q)
{
  gsl_matrix *qr;
  qr = gsl_matrix_alloc(q->size1, q->size2);
  lyapunov_evolve_forward_every_column_step(qr, evolve, evolve->derivative, time_step, orbit, q, 0, 1);
  return qr;
}

static DerivativeOfSystem *lyapunov_evolve_get_derivative_for_current_thread (LyapunovEvolve *evolve, int task_number)
{
  DataContainer *container;
  container = (DataContainer *) mathfluid_thread_variable_get(evolve->thread_variable, task_number);
  return (DerivativeOfSystem *) data_container_data_ptr(container, LYAPUNOV_EVOLVE_THREAD_DATA_DERIVATIVE);
}

static void lyapunov_evolve_forward_lyapunov_vectors_each_thread (int task_number, int total_task_number, gpointer ptr_thread_args)
{
  LyapunovVectorsEvolveThreadArgs *thread_args;
  DerivativeOfSystem *derivative;
  thread_args = (LyapunovVectorsEvolveThreadArgs *) ptr_thread_args;
  derivative = lyapunov_evolve_get_derivative_for_current_thread(thread_args->evolve, task_number);
  lyapunov_evolve_forward_every_column_step(thread_args->qr, thread_args->evolve, derivative, thread_args->time_step, thread_args->orbit, thread_args->q, task_number, total_task_number);
}

static gsl_matrix *lyapunov_analysis_evolve_matrix_lyapunov_vectors (LyapunovEvolve *evolve, double time_step, GPtrArray *orbit, gsl_matrix *q)
{
  gsl_matrix *qr;
  MathFluidThreadPool *thread_pool;
  thread_pool = mathfluid_thread_pool_get();
  if (evolve->use_thread && thread_pool) {
    LyapunovVectorsEvolveThreadArgs thread_args[1];
    GArray *signal_id;
    qr = gsl_matrix_alloc(q->size1, q->size2);
    thread_args->qr = qr;
    thread_args->evolve = evolve;
    thread_args->time_step = time_step;
    thread_args->orbit = orbit;
    thread_args->q = q;
    signal_id = mathfluid_thread_pool_push_tasks(thread_pool->max_thread_number, lyapunov_evolve_forward_lyapunov_vectors_each_thread, thread_args);
    mathfluid_thread_pool_wait_tasks(signal_id);
    g_array_free(signal_id, TRUE);
  } else {
    qr = lyapunov_evolve_forward_without_threads(evolve, time_step, orbit, q);
  }
  return qr;
}

/**
 * Return points on orbit which are needed to evolve tangent vectors
 */
static GPtrArray *lyapunov_analysis_orbit_points (LyapunovEvolve *evolve, SystemState *state, double time_step)
{
  GPtrArray *orbit;
  SystemState *state_tmp;
  orbit = g_ptr_array_new_with_free_func((void (*)(gpointer)) system_state_free);
  state_tmp = system_state_alloc_copy(state);
  if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_EULER) {
    g_ptr_array_add(orbit, system_state_alloc_copy(state_tmp));
    dynamical_system_evolve(state_tmp, evolve->dynamical_system, state_tmp->time + time_step);
    g_ptr_array_add(orbit, state_tmp);
  } else if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_RK4) {
    g_ptr_array_add(orbit, system_state_alloc_copy(state_tmp));
    dynamical_system_evolve(state_tmp, evolve->dynamical_system, state_tmp->time + time_step / 2.0);
    g_ptr_array_add(orbit, system_state_alloc_copy(state_tmp));
    dynamical_system_evolve(state_tmp, evolve->dynamical_system, state_tmp->time + time_step / 2.0);
    g_ptr_array_add(orbit, state_tmp);
  } else {
    fprintf(stderr, "Invalid step type of tangent space\n");
    abort();
  }
  return orbit;
}

gsl_matrix *lyapunov_evolve_forward (SystemState *state_evolved, LyapunovEvolve *evolve, SystemState *state, double time_step, gsl_matrix *q)
{
  gsl_matrix *qr;
  GPtrArray *orbit;
  orbit = lyapunov_analysis_orbit_points(evolve, state, time_step);
  qr = lyapunov_analysis_evolve_matrix_lyapunov_vectors(evolve, time_step, orbit, q);
  if (state_evolved) {
    system_state_copy(state_evolved, (SystemState *) g_ptr_array_index(orbit, orbit->len - 1));
  }
  g_ptr_array_free(orbit, TRUE);
  return qr;
}

gsl_matrix *lyapunov_evolve_forward_at_equilibrium (LyapunovEvolve *evolve, SystemState *equilibrium, double time_step, gsl_matrix *q)
{
  gsl_matrix *qr;
  GPtrArray *orbit;
  orbit = g_ptr_array_new();
  if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_EULER) {
    g_ptr_array_add(orbit, equilibrium);
  } else if (evolve->algorithm == LYAPUNOV_EVOLVE_ALGORITHM_RK4) {
    int i;
    for (i = 0; i < 3; i++) {
      g_ptr_array_add(orbit, equilibrium);
    }
  }
  qr = lyapunov_analysis_evolve_matrix_lyapunov_vectors(evolve, time_step, orbit, q);
  g_ptr_array_free(orbit, TRUE);
  return qr;
}

/** @} */  /* End of LyapunovEvolve */
