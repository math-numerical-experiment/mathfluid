/**
 * @file parameter_continuation_corrector.c
 * @brief Corrector of parameter continuation
 */

#include "header.h"

/**
 * \addtogroup ParameterContinuationCorrector
 * @{
 */

static void parameter_continuation_corrector_keller_function (double *value, JacobianFreeGMRES *gmres_keller, const double *pt)
{
  gsl_vector *v, *tangent_vector, *pt_last;
  DataContainer *setting = (DataContainer *) gmres_keller->data;
  int extended_dim = parameter_continuation_gmres_setting_extended_dimension(setting);
  gsl_vector_const_view view_pt = gsl_vector_const_view_array(pt, extended_dim);
  tangent_vector = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_TANGENT_VECTOR);
  pt_last = (gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_LAST);
  dynamical_system_parameter_set_double(jacobian_free_gmres_dynamical_system(gmres_keller), (char *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PARAMETER_KEY), pt[extended_dim - 1]);
  jacobian_free_gmres_time_derivative(value, gmres_keller, pt);
  v = gsl_vector_duplicate(pt_last);
  gsl_vector_sub(v, &view_pt.vector);
  value[extended_dim - 1] = parameter_continuation_gmres_inner_product(tangent_vector, v->data);
  gsl_vector_free(v);
}

ParameterContinuationCorrector *parameter_continuation_corrector_alloc (ParameterContinuationCorrection correction_type, DynamicalSystem *dynamical_system, const char *parameter_key, int newton_damping_parameter, double newton_error)
{
  int extended_dim;
  ParameterContinuationCorrector *corrector;
  corrector = (ParameterContinuationCorrector *) g_malloc(sizeof(ParameterContinuationCorrector));
  extended_dim = dynamical_system_dimension(dynamical_system) + 1;
  if (correction_type == PARAMETER_CONTINUATION_CORRECTION_KELLER) {
    corrector->setting = parameter_continuation_newton_gmres_setting_alloc(parameter_key, extended_dim, NULL);
    corrector->gmres_keller = parameter_continuation_gmres_alloc(dynamical_system, corrector->setting);
    corrector->newton_keller = jacobian_free_newton_gmres_alloc(corrector->gmres_keller, parameter_continuation_corrector_keller_function, newton_damping_parameter, newton_error);
    data_container_data_ptr(corrector->setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER) = corrector->newton_keller->vector;
    corrector->gauss_newton = NULL;
  } else if (correction_type == PARAMETER_CONTINUATION_CORRECTION_GAUSS_NEWTON) {
    corrector->gmres_keller = NULL;
    corrector->newton_keller = NULL;
    corrector->setting = parameter_continuation_gmres_setting_alloc(parameter_key, extended_dim);
    corrector->gauss_newton = parameter_continuation_corrector_gauss_newton_alloc(dynamical_system, corrector->setting, newton_error);
  } else {
    fprintf(stdout, "Method of this correction is not implemented\n");
    exit(0);
  }
  return corrector;
}

void parameter_continuation_corrector_free (ParameterContinuationCorrector *corrector)
{
  if (corrector->setting) {
    data_container_free(corrector->setting);
  }
  if (corrector->gmres_keller) {
    jacobian_free_gmres_free(corrector->gmres_keller);
  }
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_free(corrector->newton_keller);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_free(corrector->gauss_newton);
  }
  g_free(corrector);
}

void parameter_continuation_corrector_set_tangent_vector_and_point (ParameterContinuationCorrector *corrector, gsl_vector *tangent_vector, gsl_vector *pt_last)
{
  parameter_continuation_gmres_setting_set_tangent_vector(corrector->setting, tangent_vector);
  if (corrector->newton_keller) {
    parameter_continuation_gmres_setting_set_pt_last(corrector->setting, pt_last);
  }
}

int parameter_continuation_corrector_search_equilibrium (ParameterContinuationCorrector *corrector, const double *equilibrium_with_parameter_initial, int newton_max_iteration)
{
  int iter;
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_initial_solution(corrector->newton_keller, equilibrium_with_parameter_initial);
    iter = jacobian_free_newton_gmres_iterate_successively(corrector->newton_keller, newton_max_iteration);
  } else if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_initial_solution(corrector->gauss_newton, equilibrium_with_parameter_initial);
    iter = parameter_continuation_corrector_gauss_newton_iterate_successively(corrector->gauss_newton, newton_max_iteration);
  } else {
    fprintf(stdout, "No allocation of Newton method\n");
    exit(0);
  }
  return iter;
}

gboolean parameter_continuation_corrector_solution_copy (double *vector, ParameterContinuationCorrector *corrector)
{
  if (corrector->newton_keller) {
    return jacobian_free_newton_gmres_solution_copy(vector, corrector->newton_keller);
  } else if (corrector->gauss_newton) {
    return parameter_continuation_corrector_gauss_newton_solution_copy(vector, corrector->gauss_newton);
  }
  fprintf(stdout, "No allocation of Newton method\n");
  exit(0);
}

double parameter_continuation_corrector_next_step_length (ParameterContinuationCorrector *corrector, int iter, double step_length)
{
  int iter_estimate, exponent;
  double ratio;
  iter_estimate = (iter <= 3 ? 3 : iter - 1);
  if (corrector->newton_keller) {
    ratio = (corrector->newton_keller->max_error * 0.1) / corrector->newton_keller->error_cache;
  } else if (corrector->gauss_newton) {
    ratio = (corrector->gauss_newton->max_error * 0.1) / corrector->gauss_newton->error_cache;
  } else {
    fprintf(stdout, "Not allocation of Newton method\n");
    exit(0);
  }
  exponent = pow(2.0, (double) (-iter_estimate + 1));
  return step_length * pow(ratio, exponent);
}

void parameter_continuation_corrector_set_log_level (ParameterContinuationCorrector *corrector, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_log_level(corrector->gmres_keller, gmres_level);
  }
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_log_level(corrector->newton_keller, newton_level);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_log_level(corrector->gauss_newton, newton_level, gmres_level);
  }
}

void parameter_continuation_corrector_set_ignore_exact_jacobian (ParameterContinuationCorrector *corrector, gboolean ignore)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_ignore_exact_jacobian(corrector->gmres_keller, ignore);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_ignore_exact_jacobian(corrector->gauss_newton->gmres, ignore);
  }
}

void parameter_continuation_corrector_set_space_derivative_perturbation (ParameterContinuationCorrector *corrector, double space_derivative_perturbation)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_space_derivative_perturbation(corrector->gmres_keller, space_derivative_perturbation);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_space_derivative_perturbation(corrector->gauss_newton->gmres, space_derivative_perturbation);
  }
}

void parameter_continuation_corrector_set_time_derivative_perturbation (ParameterContinuationCorrector *corrector, double time_derivative_perturbation)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_time_derivative_perturbation(corrector->gmres_keller, time_derivative_perturbation);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_time_derivative_perturbation(corrector->gauss_newton->gmres, time_derivative_perturbation);
  }
}

void parameter_continuation_corrector_set_parameter_derivative_perturbation (ParameterContinuationCorrector *corrector, double parameter_perturbation)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_parameter_derivative_perturbation(corrector->gmres_keller, parameter_perturbation);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_parameter_derivative_perturbation(corrector->gauss_newton->gmres, parameter_perturbation);
  }
}

void parameter_continuation_corrector_gmres_set_max_iteration (ParameterContinuationCorrector *corrector, int max_iteration)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_max_iteration(corrector->gmres_keller, max_iteration);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_max_iteration(corrector->gauss_newton->gmres, max_iteration);
  }
}

void parameter_continuation_corrector_gmres_set_error_residual_ratio (ParameterContinuationCorrector *corrector, double ratio)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_error_residual_ratio(corrector->gmres_keller, ratio);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_error_residual_ratio(corrector->gauss_newton->gmres, ratio);
  }
}

void parameter_continuation_corrector_gmres_set_min_error (ParameterContinuationCorrector *corrector, double min_error)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_min_error(corrector->gmres_keller, min_error);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_min_error(corrector->gauss_newton->gmres, min_error);
  }
}

void parameter_continuation_corrector_gmres_set_restart_parameter (ParameterContinuationCorrector *corrector, GMRESRestartType restart_type, ...)
{
  va_list args;
  va_start(args, restart_type);
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_restart_parameter(corrector->gmres_keller, restart_type, args);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_restart_parameter(corrector->gauss_newton->gmres, restart_type, args);
  }
}

void parameter_continuation_corrector_gmres_set_restart_parameter_va_list (ParameterContinuationCorrector *corrector, GMRESRestartType restart_type, va_list args)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_restart_parameter_va_list(corrector->gmres_keller, restart_type, args);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_restart_parameter_va_list(corrector->gauss_newton->gmres, restart_type, args);
  }
}

void parameter_continuation_corrector_gmres_set_ignore_gmres_convergence (ParameterContinuationCorrector *corrector, gboolean ignore)
{
  if (corrector->gmres_keller) {
    jacobian_free_gmres_set_ignore_gmres_convergence(corrector->gmres_keller, ignore);
  }
  if (corrector->gauss_newton) {
    jacobian_free_gmres_set_ignore_gmres_convergence(corrector->gauss_newton->gmres, ignore);
  }
}

double parameter_continuation_corrector_newton_gmres_max_error (ParameterContinuationCorrector *corrector)
{
  if (corrector->newton_keller) {
    return jacobian_free_newton_gmres_max_error(corrector->newton_keller);
  } else if (corrector->gauss_newton) {
    return parameter_continuation_corrector_gauss_newton_max_error(corrector->gauss_newton);
  }
  fprintf(stdout, "No allocation of Newton method\n");
  exit(0);
}

double parameter_continuation_corrector_newton_gmres_practical_error (ParameterContinuationCorrector *corrector)
{
  if (corrector->newton_keller) {
    return jacobian_free_newton_gmres_practical_error(corrector->newton_keller);
  } else if (corrector->gauss_newton) {
    return parameter_continuation_corrector_gauss_newton_practical_error(corrector->gauss_newton);
  }
  fprintf(stdout, "No allocation of Newton method\n");
  exit(0);
}

double parameter_continuation_corrector_newton_gmres_max_uniform_norm (ParameterContinuationCorrector *corrector)
{
  if (corrector->newton_keller) {
    return jacobian_free_newton_gmres_max_uniform_norm(corrector->newton_keller);
  } else if (corrector->gauss_newton) {
    return parameter_continuation_corrector_gauss_newton_max_uniform_norm(corrector->gauss_newton);
  }
  fprintf(stdout, "No allocation of Newton method\n");
  exit(0);
}

int parameter_continuation_corrector_newton_gmres_damping_parameter (ParameterContinuationCorrector *corrector)
{
  if (corrector->newton_keller) {
    return jacobian_free_newton_gmres_damping_parameter(corrector->newton_keller);
  } else if (corrector->gauss_newton) {
    return 0;
  }
  fprintf(stdout, "No allocation of Newton method\n");
  exit(0);
}

void parameter_continuation_corrector_newton_gmres_set_max_error (ParameterContinuationCorrector *corrector, double max_error)
{
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_max_error(corrector->newton_keller, max_error);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_max_error(corrector->gauss_newton, max_error);
  }
}

void parameter_continuation_corrector_newton_gmres_set_practical_error (ParameterContinuationCorrector *corrector, double practical_error)
{
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_practical_error(corrector->newton_keller, practical_error);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_practical_error(corrector->gauss_newton, practical_error);
  }
}

void parameter_continuation_corrector_newton_gmres_set_max_uniform_norm (ParameterContinuationCorrector *corrector, double max_uniform_norm)
{
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_max_uniform_norm(corrector->newton_keller, max_uniform_norm);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_max_uniform_norm(corrector->gauss_newton, max_uniform_norm);
  }
}

void parameter_continuation_corrector_newton_gmres_set_damping_parameter (ParameterContinuationCorrector *corrector, int damping_parameter)
{
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_damping_parameter(corrector->newton_keller, damping_parameter);
  }
}

void parameter_continuation_corrector_newton_gmres_set_norm_function (ParameterContinuationCorrector *corrector, MathFluidNormFunc norm_function, void *norm_args)
{
  if (corrector->newton_keller) {
    jacobian_free_newton_gmres_set_norm_function(corrector->newton_keller, norm_function, norm_args);
  }
  if (corrector->gauss_newton) {
    parameter_continuation_corrector_gauss_newton_set_norm_function(corrector->gauss_newton, norm_function, norm_args);
  }
}

/** @} */  /* End of group */
