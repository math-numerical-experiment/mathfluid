/**
 * @file derivative_of_system.c
 * @brief Functions of derivatives of dynamical systems
 */

#include "header.h"

#define derivative_of_system_dynamical_system(derivative_of_system) (derivative_of_system->dynamical_system)

/**
 * \addtogroup DerivativeOfSystem
 * @{
 */

/**
 * @param[in]    dynamical_system    Configured dynamical system
 * @note The argument 'dynamical_system' is not freed together with DerivativeOfSystem by derivative_of_system_free
 */
DerivativeOfSystem *derivative_of_system_alloc (DynamicalSystem *dynamical_system)
{
  DerivativeOfSystem *derivative_of_system;
  derivative_of_system = (DerivativeOfSystem *) g_malloc(sizeof(DerivativeOfSystem));
  derivative_of_system->dynamical_system = dynamical_system;
  derivative_of_system->ignore_exact_jacobian = FALSE;
  derivative_of_system->ignore_exact_time_derivative = FALSE;
  derivative_of_system->ignore_exact_parameter_derivative = FALSE;
  derivative_of_system->time_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_TIME_DERIVATIVE_PERTURBATION;
  derivative_of_system->parameter_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_PARAMETER_DERIVATIVE_PERTURBATION;
  derivative_of_system->space_derivative_perturbation = DERIVATIVE_OF_SYSTEM_DEFAULT_SPACE_DERIVATIVE_PERTURBATION;
  return derivative_of_system;
}

void derivative_of_system_free (DerivativeOfSystem *derivative_of_system)
{
  g_free(derivative_of_system);
}

/**
 * Duplicate DerivativeOfSystem, that is, allocate memory and copy member variables to the allocated object.
 * @note Duplicated system has common pointer of DynamicalSystem. So, if we change member variable somewhere,
 * all of duplicated DerivativeOfSystem are influenced.
 */
DerivativeOfSystem *derivative_of_system_duplicate (DerivativeOfSystem *derivative_of_system)
{
  DerivativeOfSystem *derivative_dup;
  /* Note that derivative_dup and derivative_of_system have
     common pointer as the member dynamical_system */
  derivative_dup = derivative_of_system_alloc(derivative_of_system_dynamical_system(derivative_of_system));
  derivative_of_system_set_time_derivative_perturbation(derivative_dup, derivative_of_system->time_derivative_perturbation);
  derivative_of_system_set_space_derivative_perturbation(derivative_dup, derivative_of_system->space_derivative_perturbation);
 derivative_of_system_set_parameter_derivative_perturbation(derivative_dup, derivative_of_system->parameter_derivative_perturbation);
 derivative_of_system_set_ignore_exact_jacobian(derivative_dup, derivative_of_system->ignore_exact_jacobian);
 derivative_of_system_set_ignore_exact_time_derivative(derivative_dup, derivative_of_system->ignore_exact_time_derivative);
 derivative_of_system_set_ignore_exact_parameter_derivative(derivative_dup, derivative_of_system->ignore_exact_parameter_derivative);
  return derivative_dup;
}

/**
 * Set time step size of numerical differential of vector field, which is used when the differential can not be calculated directly.
 */
void derivative_of_system_set_time_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double time_evolution)
{
  derivative_of_system->time_derivative_perturbation = time_evolution;
}

/**
 * Set perturbation scale of Jacobian free product
 */
void derivative_of_system_set_space_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double perturbation)
{
  derivative_of_system->space_derivative_perturbation = perturbation;
}

/**
 * Set perturbation scale of Jacobian free product
 */
void derivative_of_system_set_parameter_derivative_perturbation (DerivativeOfSystem *derivative_of_system, double perturbation)
{
  derivative_of_system->parameter_derivative_perturbation = perturbation;
}

void derivative_of_system_set_ignore_exact_jacobian (DerivativeOfSystem *derivative_of_system, gboolean ignore)
{
  derivative_of_system->ignore_exact_jacobian = ignore;
}

void derivative_of_system_set_ignore_exact_time_derivative (DerivativeOfSystem *derivative_of_system, gboolean ignore)
{
  derivative_of_system->ignore_exact_time_derivative = ignore;
}

void derivative_of_system_set_ignore_exact_parameter_derivative (DerivativeOfSystem *derivative_of_system, gboolean ignore)
{
  derivative_of_system->ignore_exact_parameter_derivative = ignore;
}

/**
 * Calculate product of Jacobian and a vector at a specified point.
 * @note If the derivative can not be calculated by any algorithm, this function causes abort.
 */
void derivative_of_system_jacobian_vector_product (double *av, DerivativeOfSystem *derivative_of_system, const double *pt, const double *v)
{
  if (!derivative_of_system->ignore_exact_jacobian) {
    if (dynamical_system_product_jacobian_of_time_derivative(av, derivative_of_system_dynamical_system(derivative_of_system), pt, v)) {
      return;
    }
  }
  if (!derivative_of_system->ignore_exact_time_derivative) {
    if (dynamical_system_matrix_free_product_jacobian_of_time_derivative_with_exact_time_derivative(av, derivative_of_system_dynamical_system(derivative_of_system), derivative_of_system->space_derivative_perturbation, pt, v)) {
      return;
    }
  }
  if (dynamical_system_matrix_free_product_jacobian_of_time_derivative_without_exact_time_derivative(av, derivative_of_system_dynamical_system(derivative_of_system), derivative_of_system->time_derivative_perturbation, derivative_of_system->space_derivative_perturbation, pt, v)) {
    return;
  }
  fprintf(stderr, "DerivativeOfSystem can not calculate a product of Jacobian of the vector field and a vector\n");
  abort();
}

/**
 * Calculate time derivative at a specified point.
 * @note If the derivative can not be calculated by any algorithm, this function causes abort.
 */
void derivative_of_system_time_derivative (double *time_derivative, DerivativeOfSystem *derivative_of_system, const double *x)
{
  if (!derivative_of_system->ignore_exact_time_derivative) {
    if (dynamical_system_time_derivative(time_derivative, derivative_of_system_dynamical_system(derivative_of_system), x)) {
      return;
    }
  }
  if (dynamical_system_numerical_time_derivative(time_derivative, derivative_of_system_dynamical_system(derivative_of_system), derivative_of_system->time_derivative_perturbation, x)) {
    return;
  }
  fprintf(stderr, "DerivativeOfSystem can not calculate a time derivative of an orbit\n");
  abort();
}

/**
 * Calculate parameter derivative at a specified point.
 * @note If the derivative can not be calculated by any algorithm, this function causes abort.
 */
void derivative_of_system_parameter_derivative (double *parameter_derivative, DerivativeOfSystem *derivative_of_system, const char* parameter_key, const double *x)
{
  if (!derivative_of_system->ignore_exact_parameter_derivative) {
    if (dynamical_system_parameter_derivative_of_time_derivative(parameter_derivative, derivative_of_system_dynamical_system(derivative_of_system), parameter_key, x)) {
      return;
    }
  }
  if (!derivative_of_system->ignore_exact_time_derivative) {
    if (dynamical_system_parameter_derivative_of_time_derivative_with_exact_time_derivative(parameter_derivative, derivative_of_system_dynamical_system(derivative_of_system), derivative_of_system->parameter_derivative_perturbation, parameter_key, x)) {
      return;
    }
  }
  if (dynamical_system_parameter_derivative_of_time_derivative_without_exact_time_derivative(parameter_derivative, derivative_of_system_dynamical_system(derivative_of_system), derivative_of_system->time_derivative_perturbation, derivative_of_system->parameter_derivative_perturbation, parameter_key, x)) {
    return;
  }
  fprintf(stderr, "DerivativeOfSystem can not calculate a parameter derivative of time derivative\n");
  abort();
}

/** @} */  /* End of DerivativeOfSystem */
