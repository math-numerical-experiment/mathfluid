/**
 * @file glib_extension.c
 * @brief Extensions of GLIB
 */

#include "header.h"

/**
 * \addtogroup GLIB
 * @{
 */

void g_array_free_completely (GArray *array)
{
  g_array_free(array, TRUE);
}

GArray *g_array_duplicate (GArray *array)
{
  GArray *array_copy;
  array_copy = g_array_new(FALSE, FALSE, g_array_get_element_size(array));
  array_copy = g_array_insert_vals(array_copy, 0, array->data, array->len);
  return array_copy;
}

/** @} */  /* End of GLIB */
