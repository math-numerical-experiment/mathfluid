/**
 * @file gsl_wrap.c
 * @brief Functions to wrap some GSL's functions
 */

#include "header.h"

/**
 * \addtogroup GSL
 * @{
 */

/**
 * @param[out]    eigenvalue     A pointer of array of eigenvalues.
 *                               Enough memory is allocated to *eigenvalue
 * @param[out]    eigenvector    A pointer of array of eigenvectors.
 *                               Enough memory is allocated to *eigenvector
 *                               n-th eigenvector is stored from (*eigenvector)[(n-1) * dimension]
 *                               to (*eigenvector)[n * dimension - 1].
 * @param[in,out]    m           An input matrix whose elements are changed by this function
 *                               in the calculation process
 * @return    Number of eigenvectors. Return 0 if all eigenvalues can not be found.
 */
int gsl_wrap_eigen_with_overwrite (double complex **eigenvalue, double complex **eigenvector, gsl_matrix *m)
{
  gsl_eigen_nonsymmv_workspace *ws;
  gsl_vector_complex *eigenvalues_by_gsl;
  gsl_matrix_complex *eigenvectors_by_gsl;
  int success, number_eigenvectors;
  ws = gsl_eigen_nonsymmv_alloc(m->size1);
  eigenvalues_by_gsl = gsl_vector_complex_alloc(m->size1);
  eigenvectors_by_gsl = gsl_matrix_complex_alloc(m->size1, m->size1);
  success = gsl_eigen_nonsymmv(m, eigenvalues_by_gsl, eigenvectors_by_gsl, ws);
  if (success == GSL_SUCCESS) {
    int i, j;
    gsl_complex val;
    gsl_vector_complex_view evec;
    gsl_eigen_nonsymmv_sort(eigenvalues_by_gsl, eigenvectors_by_gsl, GSL_EIGEN_SORT_ABS_DESC);
    *eigenvalue = (double complex *) g_malloc(sizeof(double complex) * m->size1);
    *eigenvector = (double complex *) g_malloc(sizeof(double complex) * m->size1 * m->size1);
    for (i = 0; i < m->size1; i++) {
      val = gsl_vector_complex_get(eigenvalues_by_gsl, i);
      (*eigenvalue)[i] = GSL_REAL(val) + _Complex_I * GSL_IMAG(val);
      evec = gsl_matrix_complex_column(eigenvectors_by_gsl, i);
      for (j = 0; j < m->size1; j++) {
        val = gsl_vector_complex_get(&evec.vector, j);
        (*eigenvector)[i * m->size1 + j] = GSL_REAL(val) + _Complex_I * GSL_IMAG(val);
      }
    }
    number_eigenvectors = m->size1;
  } else {
    *eigenvalue = NULL;
    *eigenvector = NULL;
    number_eigenvectors = 0;
  }
  gsl_eigen_nonsymmv_free(ws);
  gsl_vector_complex_free(eigenvalues_by_gsl);
  gsl_matrix_complex_free(eigenvectors_by_gsl);
  return number_eigenvectors;
}

int gsl_wrap_eigen (double complex **eigenvalue, double complex **eigenvector, gsl_matrix *m)
{
  int number_eigenvectors;
  gsl_matrix *matrix;
  matrix = gsl_matrix_duplicate(m);
  number_eigenvectors = gsl_wrap_eigen_with_overwrite(eigenvalue, eigenvector, matrix);
  gsl_matrix_free(matrix);
  return number_eigenvectors;
}

/**
 * QR decomposition of a matrix qr is stored to a matrix q and a matrix r,
 * where Householder transformation is used.
 * The decomposition is not strictly QR decomposition because the matrix q is not an orthogonal matrix.
 * Note that a matrix qr is changed in process of QR decomposition.
 * @param[out]        q     An n1*n2 matrix
 * @param[out]        r     An n2*n2 matrix
 * @param[in,out]     qr    An n1*n2 matrix
 */
void gsl_wrap_qr_decomp_with_overwrite (gsl_matrix *q, gsl_matrix *r, gsl_matrix *qr)
{
  int i, j;
  gsl_vector_view col_view;
  gsl_matrix *q_full, *r_full;
  gsl_vector *tau;
  if (qr->size1 < qr->size2) {
    fprintf(stderr, "QR decomposition of matrices whose column number is larger than row number is not supported. Size is %d*%d\n", (int) qr->size1, (int) qr->size2);
    abort();
  }
  tau = gsl_vector_alloc(qr->size2);
  q_full = gsl_matrix_alloc(qr->size1, qr->size1);
  r_full = gsl_matrix_alloc(qr->size1, qr->size2);
  /* gsl_linalg_QR_decomp generate QR decomposition by Householder transformation */
  /* QR decomposition by Householder transformation needs square matrix to generate matrix Q */
  gsl_linalg_QR_decomp(qr, tau);
  gsl_linalg_QR_unpack(qr, tau, q_full, r_full);
  for (i = 0; i < qr->size2; i++) {
    col_view = gsl_matrix_column(q, i);
    gsl_matrix_get_col(&col_view.vector, q_full, i);
    for (j = 0; j < qr->size2; j++) {
      gsl_matrix_set(r, i, j, gsl_matrix_get(r_full, i, j));
    }
  }
  gsl_matrix_free(q_full);
  gsl_matrix_free(r_full);
  gsl_vector_free(tau);
}

void gsl_wrap_qr_decomp (gsl_matrix *q, gsl_matrix *r, gsl_matrix *qr)
{
  gsl_matrix *qr_tmp;
  qr_tmp = gsl_matrix_duplicate(qr);
  gsl_wrap_qr_decomp_with_overwrite(q, r, qr_tmp);
  gsl_matrix_free(qr_tmp);
}

/** @} */  /* End of GSL */
