/**
 * @file lyapunov_matrix_store.c
 * @brief Store of matrices for Ginelli's method
 */

#include "header.h"

/**
 * \addtogroup LyapunovMatrixStore
 * @{
 */

static gsl_matrix *lyapunov_matrix_data_copy_gsl_matrix (gsl_matrix *m)
{
  gsl_matrix *m2;
  m2 = gsl_matrix_alloc(m->size1, m->size2);
  gsl_matrix_memcpy(m2, m);
  return m2;
}

LyapunovMatrixData *lyapunov_matrix_data_alloc (SystemState *state, gsl_matrix *q, gsl_matrix *r)
{
  LyapunovMatrixData *data;
  data = (LyapunovMatrixData *) g_malloc(sizeof(LyapunovMatrixData));
  if (state) {
    data->state = system_state_alloc_copy(state);
  } else {
    data->state = NULL;
  }
  if (q) {
    data->q = lyapunov_matrix_data_copy_gsl_matrix(q);
  } else {
    data->q = NULL;
  }
  if (r) {
    data->r = lyapunov_matrix_data_copy_gsl_matrix(r);
  } else {
    data->r = NULL;
  }
  return data;
}

void lyapunov_matrix_data_free (LyapunovMatrixData *data)
{
  if (data->state) {
    system_state_free(data->state);
  }
  if (data->q) {
    gsl_matrix_free(data->q);
  }
  if (data->r) {
    gsl_matrix_free(data->r);
  }
  g_free(data);
}

static char *lyapunov_matrix_data_gsl_matrix_element_dump (char *dump, gsl_matrix *matrix)
{
  char *dump_current;
  dump_current = dump;
  if (matrix->tda == matrix->size2) {
    int size;
    size = matrix->size1 * matrix->size2 * sizeof(double);
    memcpy(dump, (char *) matrix->data, size);
    dump_current += size;
  } else {
    int i, size_row;
    size_row = matrix->size2 * sizeof(double);
    for (i = 0; i < matrix->size1; i++) {
      memcpy(dump_current, ((char *) matrix->data) + i * matrix->tda, size_row);
      dump_current += size_row;
    }
  }
  return dump_current;
}

static gsl_matrix *lyapunov_matrix_data_gsl_matrix_element_load (char *dump, int row_dim, int column_dim)
{
  gsl_matrix *m;
  m = gsl_matrix_alloc(row_dim, column_dim);
  if (m->tda == m->size2) {
    memcpy(m->data, dump, m->size1 * m->size2 * sizeof(double));
  } else {
    fprintf(stderr, "Not yet implemented\n");
    abort();
  }
  return m;
}

static char *lyapunov_matrix_data_dump (size_t *dump_size, gpointer data_ptr)
{
  LyapunovMatrixData *data;
  uint32_t dim, r_dim, state_size, q_size, r_size, size_total;
  char *dump, *dump_current;
  data = (LyapunovMatrixData *) data_ptr;
  if (data->state) {
    dim = data->state->dimension;
  } else if (data->q) {
    dim = data->q->size1;
  } else {
    dim = 0;
  }
  if (data->r) {
    r_dim = data->r->size1;
  } else {
    r_dim = 0;
  }
  state_size = (data->state ? sizeof(double) * (data->state->dimension + 1) : 0);
  q_size = (data->q ? sizeof(double) * data->q->size1 * data->q->size2 : 0);
  r_size = (data->r ? sizeof(double) * data->r->size1 * data->r->size2 : 0);
  size_total = sizeof(uint32_t) * 5 + state_size + q_size + r_size;

  dump = (char *) g_malloc(size_total);
  dump_current = dump;
  memcpy(dump_current, &dim, sizeof(uint32_t));
  dump_current += sizeof(uint32_t);
  memcpy(dump_current, &r_dim, sizeof(uint32_t));
  dump_current += sizeof(uint32_t);
  memcpy(dump_current, &state_size, sizeof(uint32_t));
  dump_current += sizeof(uint32_t);
  memcpy(dump_current, &q_size, sizeof(uint32_t));
  dump_current += sizeof(uint32_t);
  memcpy(dump_current, &r_size, sizeof(uint32_t));
  dump_current += sizeof(uint32_t);

  if (state_size > 0) {
    memcpy(dump_current, &data->state->time, sizeof(double));
    memcpy(dump_current + sizeof(double), data->state->coordinate, sizeof(double) * data->state->dimension);
    dump_current += state_size;
  }
  if (q_size > 0) {
    dump_current = lyapunov_matrix_data_gsl_matrix_element_dump(dump_current, data->q);
  }
  if (r_size > 0) {
    dump_current = lyapunov_matrix_data_gsl_matrix_element_dump(dump_current, data->r);
  }

  *dump_size = size_total;
  return dump;
}

static gpointer lyapunov_matrix_data_load (char *dump, size_t size)
{
  LyapunovMatrixData *data;
  uint32_t dim, r_dim, state_size, q_size, r_size;
  char *dump_current;
  data = (LyapunovMatrixData *) g_malloc(sizeof(LyapunovMatrixData));
  dump_current = dump;
  dim = *((uint32_t *) dump_current);
  dump_current += sizeof(uint32_t);
  r_dim = *((uint32_t *) dump_current);
  dump_current += sizeof(uint32_t);
  state_size = *((uint32_t *) dump_current);
  dump_current += sizeof(uint32_t);
  q_size = *((uint32_t *) dump_current);
  dump_current += sizeof(uint32_t);
  r_size = *((uint32_t *) dump_current);
  dump_current += sizeof(uint32_t);

  if (state_size > 0) {
    data->state = system_state_alloc(dim);
    system_state_set_time(data->state, *((double *) dump_current));
    dump_current += sizeof(double);
    system_state_set_coordinate(data->state, (double *) dump_current);
    dump_current += sizeof(double) * dim;
  } else {
    data->state = NULL;
  }
  if (q_size > 0) {
    data->q = lyapunov_matrix_data_gsl_matrix_element_load(dump_current, dim, r_dim);
    dump_current += q_size;
  } else {
    data->q = NULL;
  }
  if (r_size > 0) {
    data->r = lyapunov_matrix_data_gsl_matrix_element_load(dump_current, r_dim, r_dim);
    dump_current += r_size;
  } else {
    data->r = NULL;
  }
  return (gpointer) data;
}

LyapunovMatrixStore *lyapunov_matrix_store_alloc ()
{
  LyapunovMatrixStore *store;
  store = g_malloc(sizeof(LyapunovMatrixStore));
  store->stack = leveldb_stack_cache_alloc(lyapunov_matrix_data_dump, lyapunov_matrix_data_load, (void (*) (gpointer)) lyapunov_matrix_data_free);
  return store;
}

void lyapunov_matrix_store_set_db (LyapunovMatrixStore *store, const char *path)
{
  leveldb_stack_cache_set_db(store->stack, path);
}

void lyapunov_matrix_store_free (LyapunovMatrixStore *store)
{
  if (store->stack) {
    leveldb_stack_cache_free(store->stack, TRUE);
  }
  g_free(store);
}

void lyapunov_matrix_store_push (LyapunovMatrixStore *store, SystemState *state, gsl_matrix *q, gsl_matrix *r)
{
  LyapunovMatrixData *data;
  data = lyapunov_matrix_data_alloc(state, q, r);
  leveldb_stack_cache_push(store->stack, data);
}

void lyapunov_matrix_store_pop (LyapunovMatrixStore *store, SystemState **state, gsl_matrix **q, gsl_matrix **r)
{
  LyapunovMatrixData *data;
  data = (LyapunovMatrixData *) leveldb_stack_cache_pop(store->stack);
  if (state) {
    *state = data->state;
  }
  if (q) {
    *q = data->q;
  }
  if (r) {
    *r = data->r;
  }
  data->state = NULL;
  data->q = NULL;
  data->r = NULL;
  lyapunov_matrix_data_free(data);
}

/** @} */  /* End of LyapunovMatrixStore */
