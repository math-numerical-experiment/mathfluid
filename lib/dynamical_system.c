/**
 * @file dynamical_system.c
 * @brief Functions of general dynamical system
 */

#include "header.h"

/**
 * \addtogroup DynamicalSystem
 * @{
 */

/**
 * Allocate data structure of dynamical system that is autonomous.
 * @param[in]    dimension        Dimension of system
 * @param[in]    data_container   Data of parameters. If parameters are not needed then this argument is NULL. It is freed when dynamical_system_free is called
 * @param[in]    evolve           A pointer of function to evolve state of system
 * @return    An allocated pointer of DynamicalSystem
 */
DynamicalSystem *dynamical_system_alloc (uint dimension, DataContainer *data_container,
                                         gboolean (* evolve) (SystemState *state, DataContainer *data_container, const double t1))
{
  DynamicalSystem *dynamical_system;
  dynamical_system = (DynamicalSystem *) g_malloc(sizeof(DynamicalSystem));
  dynamical_system->dimension = dimension;
  dynamical_system->data_container = data_container;
  dynamical_system->evolve = evolve;
  dynamical_system->time_derivative = NULL;
  dynamical_system->jacobian_of_time_derivative = NULL;
  dynamical_system->parameter_derivative_of_time_derivative = NULL;
  dynamical_system->parameter_set = NULL;
  dynamical_system->parameter_get = NULL;
  return dynamical_system;
}

/**
 * Set function to calculate time derivative, that is, vector field.
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    time_derivative  A pointer of function to calculate value of vector field.
 *                                If the system does not give the vector field explicitly then this argument is NULL.
 */
void dynamical_system_set_time_derivative (DynamicalSystem *dynamical_system, gboolean (* time_derivative) (double *vec, DataContainer *data, const double *x))
{
  dynamical_system->time_derivative = time_derivative;
}

/**
 * Set function to calculate of Jacobian matrix of time derivative (that is, vector field).
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    jacobian         A pointer of function to calculate Jacobian matrix of time derivative.
 *                                If the system does not give the Jacobian matrix explicitly then this argument is NULL.
 */
void dynamical_system_set_jacobian_of_time_derivative (DynamicalSystem *dynamical_system, gboolean (* jacobian) (double *dfdy, DataContainer *data, const double *x))
{
  dynamical_system->jacobian_of_time_derivative = jacobian;
}

/**
 * Set function to calculate of parameter derivative of time derivative (that is, vector field).
 * @param[in]    dynamical_system                           Settings of dynamical system
 * @param[in]    parameter_derivative_of_time_derivative    A pointer of function to calculate parameter derivative of time derivative. If the system does not give explicitly then this argument is NULL.
 */
void dynamical_system_set_parameter_derivative_of_time_derivative (DynamicalSystem *dynamical_system, gboolean (*parameter_derivative_of_time_derivative) (double *vec, DataContainer *data_container, const char *parameter_key, const double *x))
{
  dynamical_system->parameter_derivative_of_time_derivative = parameter_derivative_of_time_derivative;
}

/**
 * Set setter function and getter function of parameters.
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    parameter_set    A pointer of function to set a value of parameter. If there is no parameter then this argument is NULL.
 * @param[in]    parameter_get    A pointer of function to get a value of parameter. If there is no parameter then this argument is NULL.
 */
void dynamical_system_set_parameter_accessor (DynamicalSystem *dynamical_system, void (* parameter_set) (DataContainer *data_container, const char *key, GValue *val), void (* parameter_get) (GValue *val, DataContainer *data_container, const char *key))
{
  dynamical_system->parameter_set = parameter_set;
  dynamical_system->parameter_get = parameter_get;
}

/**
 * Free memory of DynamicalSystem.
 * @param[in]    dynamical_system Settings of dynamical system
 */
void dynamical_system_free (DynamicalSystem *dynamical_system)
{
  if (dynamical_system->data_container) {
    data_container_free(dynamical_system->data_container);
  }
  g_free(dynamical_system);
}

/**
 * Duplicate dynamical system.
 * @note    Because we use this function in order to calculate in multiple threads,
 *          all functions for related DynamicalSystem must be thread-safe.
 * @param[in]    dynamical_system Settings of dynamical system
 */
DynamicalSystem *dynamical_system_duplicate (DynamicalSystem *dynamical_system)
{
  DynamicalSystem *dynamical_system_new;
  DataContainer *data;
  if (dynamical_system->data_container) {
    data = data_container_duplicate(dynamical_system->data_container);
  } else {
    data = NULL;
  }
  dynamical_system_new = dynamical_system_alloc(dynamical_system->dimension, data, dynamical_system->evolve);
  dynamical_system_set_time_derivative(dynamical_system_new, dynamical_system->time_derivative);
  dynamical_system_set_jacobian_of_time_derivative(dynamical_system_new, dynamical_system->jacobian_of_time_derivative);
  dynamical_system_set_parameter_accessor(dynamical_system_new, dynamical_system->parameter_set, dynamical_system->parameter_get);
  return dynamical_system_new;
}

/**
 * Set value of parameter specified by key.
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 * @param[in]    val    New value of parameter
 */
void dynamical_system_parameter_set (DynamicalSystem *dynamical_system, const char *key, GValue *val)
{
  if (dynamical_system->data_container && dynamical_system->parameter_set) {
    dynamical_system->parameter_set(dynamical_system->data_container, key, val);
  } else {
    fprintf(stderr, "Can not set parameter %s\n", key);
    abort();
  }
}

/**
 * Get value of parameter specified by key.
 * @param[out]   val    Container of the parameter value
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 */
void dynamical_system_parameter_get (GValue *val, DynamicalSystem *dynamical_system, const char *key)
{
  if (dynamical_system->data_container && dynamical_system->parameter_get) {
    return dynamical_system->parameter_get(val, dynamical_system->data_container, key);
  } else {
    fprintf(stderr, "Can not get parameter %s\n", key);
    abort();
  }
}

/**
 * Set parameter with double type
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 * @param[in]    val    New value of the parameter
 */
void dynamical_system_parameter_set_double (DynamicalSystem *dynamical_system, const char *key, double val)
{
  GValue gval = G_VALUE_INIT;
  g_value_init(&gval, G_TYPE_DOUBLE);
  g_value_set_double(&gval, val);
  dynamical_system_parameter_set(dynamical_system, key, &gval);
}

/**
 * Get parameter with double type
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 * @return    Value of the parameter
 */
double dynamical_system_parameter_get_double (DynamicalSystem *dynamical_system, const char *key)
{
  GValue gval = G_VALUE_INIT;
  dynamical_system_parameter_get(&gval, dynamical_system, key);
  if (!G_VALUE_HOLDS_DOUBLE(&gval)) {
    fprintf(stderr, "Parameter '%s' is not double\n", key);
    abort();
  }
  return g_value_get_double(&gval);
}

/**
 * Set parameter with int type
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 * @param[in]    val    New value of the parameter
 */
void dynamical_system_parameter_set_int (DynamicalSystem *dynamical_system, const char *key, int val)
{
  GValue gval = G_VALUE_INIT;
  g_value_init(&gval, G_TYPE_INT);
  g_value_set_int(&gval, val);
  dynamical_system_parameter_set(dynamical_system, key, &gval);
}

/**
 * Get parameter with int type
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    key    Key of the parameter
 * @return    Value of the parameter
 */
int dynamical_system_parameter_get_int (DynamicalSystem *dynamical_system, const char *key)
{
  GValue gval = G_VALUE_INIT;
  dynamical_system_parameter_get(&gval, dynamical_system, key);
  if (!G_VALUE_HOLDS_INT(&gval)) {
    fprintf(stderr, "Parameter '%s' is not int\n", key);
    abort();
  }
  return g_value_get_int(&gval);
}

/**
 * Update state by evolution up to specified time.
 * @param[in,out]    state    Input and output of state
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    t1    This function evolves the system up to time t1.
 * @return    TRUE if the evolution succeeds. Otherwise, FALSE.
 */
gboolean dynamical_system_evolve (SystemState *state, DynamicalSystem *dynamical_system, const double t1)
{
  return dynamical_system->evolve(state, dynamical_system->data_container, t1);
}

/**
 * Calculate time derivative, that is, the value of the vector field at the point x.
 * @param[out]   vec    Container of value of time derivative
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    x      A point of the space
 * @return    TRUE if the time derivative is defined and the calculation succeeds. Otherwise, FALSE.
 */
gboolean dynamical_system_time_derivative (double *vec, DynamicalSystem *dynamical_system, const double *x)
{
  if (dynamical_system_time_derivative_defined_p(dynamical_system)) {
    return dynamical_system->time_derivative(vec, dynamical_system->data_container, x);
  } else {
    return FALSE;
  }
}

/**
 * Calculate approximation of time derivative of an orbit by forward difference;
 * (x(t0 + t) - x(t0)) / t
 * for dx/dt = f(x).
 * When the dynamical system does not have the function to calculate time derivative,
 * this function is used alternatively.
 * @param[out]    vec    A pointer of array to store the value of time derivative
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    t    Small time to calculate forward difference
 * @param[in]    x    A point
 * @return    TRUE if the calculation succeeds. FALSE if the calculation fails, that is, time evolution fails.
 */
gboolean dynamical_system_numerical_time_derivative (double *vec, DynamicalSystem *dynamical_system, double t, const double *x)
{
  SystemState *state;
  gsl_vector_view state_view;
  gsl_vector_const_view x_view = gsl_vector_const_view_array(x, dynamical_system->dimension);
  state = system_state_alloc2(dynamical_system->dimension, 0.0, x);
  state_view = system_state_coordinate_vector_view(state);

  if (!dynamical_system_evolve(state, dynamical_system, t)) {
    return FALSE;
  }
  gsl_vector_sub(&state_view.vector, &x_view.vector);
  gsl_vector_scale(&state_view.vector, 1.0 / t);
  system_state_copy_coordinate(vec, state);

  system_state_free(state);
  return TRUE;
}

/**
 * Calculate Jacobian of time derivative if if the function to calculate Jacobian is set.
 * Note that even if dynamical_system_jacobian_of_time_derivative_defined_p returns TRUE,
 * dynamical_system_jacobian_of_time_derivative may return FALSE.
 * @param[out]    jac    A pointer of array whose dimension is square of system dimension.
 * @param[in]     dynamical_system    Dynamical system
 * @param[in]     x      A point
 * @return    TRUE if the calculation succeeds. Otherwise, FALSE.
 */
gboolean dynamical_system_jacobian_of_time_derivative (double *jac, DynamicalSystem *dynamical_system, const double *x)
{
  if (dynamical_system_jacobian_of_time_derivative_defined_p(dynamical_system)) {
    return dynamical_system->jacobian_of_time_derivative(jac, dynamical_system->data_container, x);
  } else {
    return FALSE;
  }
}

/**
 * Calculate the product of Jacobian of time derivative of orbit and a vector vec
 * if the function to calculate the Jacobian is set.
 * @note memory of full size square matrix is needed.
 * @param[out]    product    A pointer of array that stores the product of Jacobian and the input vector. The size of the array is dynamical_system->dimension
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    x    A coordinate that is the argument of Jacobian
 * @param[in]    vec  Calculate Jacobian * vec
 * @return TRUE if the Jacobian can be obtained by dynamical_system->jacobian_of_time_derivative. Otherwise, FALSE
 */
gboolean dynamical_system_product_jacobian_of_time_derivative (double *product, DynamicalSystem *dynamical_system, const double *x, const double *vec)
{
  if (dynamical_system_jacobian_of_time_derivative_defined_p(dynamical_system)) {
    gboolean success;
    double *jac;
    jac = (double *) g_malloc(sizeof(double) * dynamical_system->dimension * dynamical_system->dimension);
    success = dynamical_system_jacobian_of_time_derivative(jac, dynamical_system, x);
    if (success) {
      gsl_matrix_view view_jac;
      gsl_vector_view view_product;
      gsl_vector_const_view view_v = gsl_vector_const_view_array(vec, dynamical_system->dimension);
      view_jac = gsl_matrix_view_array(jac, dynamical_system->dimension, dynamical_system->dimension);
      view_product = gsl_vector_view_array(product, dynamical_system->dimension);
      gsl_vector_set_zero(&view_product.vector);
      gsl_blas_dgemv(CblasNoTrans, 1.0, &view_jac.matrix, &view_v.vector, 0.0, &view_product.vector);
    }
    g_free(jac);
    return success;
  } else {
    return FALSE;
  }
}

static gsl_vector *dynamical_system_matrix_free_product_perturbation_vector (DynamicalSystem *dynamical_system, const double *vec, double perturbation_scale)
{
  gsl_vector *vec_perturbation;
  vec_perturbation = gsl_vector_alloc_set_components(dynamical_system->dimension, vec);
  gsl_vector_scale(vec_perturbation, perturbation_scale);
  return vec_perturbation;
}

/**
 * Calculate matrix free product of Jacobian of time derivative of orbit and a vector vec.
 * If time derivative of orbit is defined then we utilize it.
 * Otherwise, we calculate time derivative of the orbit by numerical differentiation.
 * In contrast to dynamical_system_product_jacobian_of_time_derivative,
 * this function is inaccurate, but requires only memory usage of vector.
 * @param[out]    product    A pointer of array that stores the product of Jacobian and the input vector. The size of the array is dynamical_system->dimension
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    t    Small time for evolving solutions. If the Jacobian of time derivative is defined, the argument is ignored.
 * @param[in]    perturbation    Scale of perturbation
 * @param[in]    x    A coordinate that is the argument of Jacobian
 * @param[in]    vec  Calculate Jacobian * vec, which should be a small vector
 * @return TRUE if the product can be calculated. Otherwise, FALSE
 */
gboolean dynamical_system_suitable_matrix_free_product_jacobian_of_time_derivative (double *product, DynamicalSystem *dynamical_system, double t, double perturbation, const double *x, const double *vec)
{
  if (!dynamical_system_matrix_free_product_jacobian_of_time_derivative_with_exact_time_derivative(product, dynamical_system, perturbation, x, vec)) {
    if (!dynamical_system_matrix_free_product_jacobian_of_time_derivative_without_exact_time_derivative(product, dynamical_system, t, perturbation, x, vec)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Calculate matrix free product of Jacobian of time derivative of orbit and a vector vec
 * by central difference formula;
 * J(x) v approximated by (f(x + hv) - f(x - hv)) / (2h)
 * where J is Jacobian and h is scale of perturbation.
 * @param[out]    product    A pointer of array that stores the product of Jacobian and the input vector. The size of the array is dynamical_system->dimension
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    perturbation    Scale of perturbation
 * @param[in]    x    A coordinate that is the argument of Jacobian
 * @param[in]    vec  Calculate Jacobian * vec
 * @return TRUE if the product can be calculated. Otherwise, FALSE
 */
gboolean dynamical_system_matrix_free_product_jacobian_of_time_derivative_with_exact_time_derivative (double *product, DynamicalSystem *dynamical_system, double perturbation, const double *x, const double *vec)
{
  if (dynamical_system_time_derivative_defined_p(dynamical_system)) {
    gsl_vector *x1, *x2, *d1, *d2, *vec_perturbation;
    if (perturbation <= 0) {
      fprintf(stderr, "Perturbation must be positive\n");
      abort();
    }
    vec_perturbation = dynamical_system_matrix_free_product_perturbation_vector(dynamical_system, vec, perturbation);
    x1 = gsl_vector_alloc_set_components(dynamical_system->dimension, x);
    x2 = gsl_vector_alloc_set_components(dynamical_system->dimension, x);
    gsl_vector_sub(x1, vec_perturbation);
    gsl_vector_add(x2, vec_perturbation);
    d1 = gsl_vector_alloc(dynamical_system->dimension);
    d2 = gsl_vector_alloc(dynamical_system->dimension);

    dynamical_system_time_derivative(d1->data, dynamical_system, x1->data);
    dynamical_system_time_derivative(d2->data, dynamical_system, x2->data);

    gsl_vector_sub(d2, d1);
    gsl_vector_scale(d2, 1.0 / (2.0 * perturbation));
    gsl_vector_copy_components(product, d2);

    gsl_vector_free(x1);
    gsl_vector_free(x2);
    gsl_vector_free(d1);
    gsl_vector_free(d2);
    gsl_vector_free(vec_perturbation);
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * Calculate matrix free product of Jacobian of time derivative of orbit and a vector vec by central difference formula.
 * Time derivatives of orbit are also calculated by numerical differentiation.
 * @param[out]    product    A pointer of array that stores the product of Jacobian and the input vector. The size of the array is dynamical_system->dimension
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    t    Small time for evolving solutions
 * @param[in]    perturbation    Scale of perturbation
 * @param[in]    x    A coordinate that is the argument of Jacobian
 * @param[in]    vec  Calculate Jacobian * vec
 * @return TRUE if the product can be calculated. Otherwise, FALSE
 */
gboolean dynamical_system_matrix_free_product_jacobian_of_time_derivative_without_exact_time_derivative (double *product, DynamicalSystem *dynamical_system, double t, double perturbation, const double *x, const double *vec)
{
  gsl_vector *x1, *x2, *d1, *d2, *vec_perturbation;
  if (perturbation <= 0) {
    fprintf(stderr, "Perturbation must be positive\n");
    abort();
  }
  vec_perturbation = dynamical_system_matrix_free_product_perturbation_vector(dynamical_system, vec, perturbation);
  x1 = gsl_vector_alloc_set_components(dynamical_system->dimension, x);
  x2 = gsl_vector_alloc_set_components(dynamical_system->dimension, x);
  gsl_vector_sub(x1, vec_perturbation);
  gsl_vector_add(x2, vec_perturbation);
  d1 = gsl_vector_alloc(dynamical_system->dimension);
  d2 = gsl_vector_alloc(dynamical_system->dimension);

  dynamical_system_numerical_time_derivative(d1->data, dynamical_system, t, x1->data);
  dynamical_system_numerical_time_derivative(d2->data, dynamical_system, t, x2->data);

  gsl_vector_sub(d2, d1);
  gsl_vector_scale(d2, 1.0 / (2.0 * perturbation));
  gsl_vector_copy_components(product, d2);

  gsl_vector_free(x1);
  gsl_vector_free(x2);
  gsl_vector_free(d1);
  gsl_vector_free(d2);
  gsl_vector_free(vec_perturbation);
  return TRUE;
}

/**
 * Calculate matrix free product of Jacobian of time evolution on an orbit and a vector vec by central difference formula.
 * @param[out]    product    A pointer of array that stores the product of Jacobian and the input vector. The size of the array is dynamical_system->dimension
 * @param[in]    dynamical_system    Dynamical system
 * @param[in]    t    Time of evolution
 * @param[in]    perturbation    Scale of perturbation
 * @param[in]    x    A coordinate of initial point
 * @param[in]    vec  Calculate Jacobian * vec
 * @return TRUE if the product can be calculated. Otherwise, FALSE
 */
gboolean dynamical_system_matrix_free_product_jacobian_of_orbit (double *product, DynamicalSystem *dynamical_system, double t, double perturbation, const double *x, const double *vec)
{
  gsl_vector *vec_perturbation;
  gsl_vector_view v1, v2;
  SystemState *state1, *state2;
  vec_perturbation = dynamical_system_matrix_free_product_perturbation_vector(dynamical_system, vec, perturbation);
  state1 = system_state_alloc2(dynamical_system->dimension, 0.0, x);
  state2 = system_state_alloc2(dynamical_system->dimension, 0.0, x);
  v1 = system_state_coordinate_vector_view(state1);
  v2 = system_state_coordinate_vector_view(state2);
  gsl_vector_sub(&v1.vector, vec_perturbation);
  gsl_vector_add(&v2.vector, vec_perturbation);

  dynamical_system_evolve(state1, dynamical_system, t);
  dynamical_system_evolve(state2, dynamical_system, t);

  gsl_vector_sub(&v2.vector, &v1.vector);
  gsl_vector_scale(&v2.vector, 1.0 / (2.0 * perturbation));
  system_state_copy_coordinate(product, state2);

  gsl_vector_free(vec_perturbation);
  system_state_free(state1);
  system_state_free(state2);
  return TRUE;
}

/**
 * Calculate parameter derivative of time derivative if it is defined.
 * @param[out]   vec    Container of value of parameter derivative
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    parameter_key    Key of parameter derivative
 * @param[in]    x    A point of space
 */
gboolean dynamical_system_parameter_derivative_of_time_derivative (double *vec, DynamicalSystem *dynamical_system, const char* parameter_key, const double *x)
{
  if (dynamical_system_parameter_derivative_of_time_derivative_defined_p(dynamical_system)) {
    return dynamical_system->parameter_derivative_of_time_derivative(vec, dynamical_system->data_container, parameter_key, x);
  } else {
    return FALSE;
  }
}

/**
 * Calculate parameter derivative of time derivative.
 * If the parameter derivative is defined then we utilize it.
 * Otherwise, we calculate parameter derivative by numerical differentiation.
 * If exact time derivative is defined then it is utilized and
 * if not then time derivative is also calculated by numerical differentiation.
 * @param[out]   vec    Container of value of parameter derivative
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    time    Size of time perturbation to calculate time derivative by numerical differentiation
 * @param[in]    parameter_perturbation    Size of parameter perturbation to calculate parameter derivative by numerical differentiation
 * @param[in]    parameter_key    Key of parameter derivative
 * @param[in]    x    A point of space
 */
gboolean dynamical_system_suitable_parameter_derivative_of_time_derivative (double *vec, DynamicalSystem *dynamical_system, double time, double parameter_perturbation, const char *parameter_key, const double *x)
{
  if (!dynamical_system_parameter_derivative_of_time_derivative(vec, dynamical_system, parameter_key, x)) {
    if (!dynamical_system_parameter_derivative_of_time_derivative_with_exact_time_derivative(vec, dynamical_system, parameter_perturbation, parameter_key, x)) {
      if (!dynamical_system_parameter_derivative_of_time_derivative_without_exact_time_derivative(vec, dynamical_system, time, parameter_perturbation, parameter_key, x)) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

/**
 * @param[out]   vec    Container of value of parameter derivative
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    parameter_perturbation    Size of parameter perturbation to calculate parameter derivative by numerical differentiation
 * @param[in]    parameter_key    Key of parameter derivative
 * @param[in]    x    A point of space
 * @note The parameter is temporarily changed in this function.
 *       When we call this function in multiple threads, we need to pay attention to the chanegs of the parameter.
 */
gboolean dynamical_system_parameter_derivative_of_time_derivative_with_exact_time_derivative (double *vec, DynamicalSystem *dynamical_system, double parameter_perturbation, const char *parameter_key, const double *x)
{
  if (dynamical_system_time_derivative_defined_p(dynamical_system) && dynamical_system->parameter_get && dynamical_system->parameter_set) {
    double parameter_old;
    gsl_vector *vec2;
    gsl_vector_view vec_view;
    vec_view = gsl_vector_view_array(vec, dynamical_system->dimension);
    vec2 = gsl_vector_alloc(dynamical_system->dimension);
    parameter_old = dynamical_system_parameter_get_double(dynamical_system, parameter_key);

    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old + parameter_perturbation);
    dynamical_system_time_derivative((&vec_view.vector)->data, dynamical_system, x);
    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old - parameter_perturbation);
    dynamical_system_time_derivative(vec2->data, dynamical_system, x);
    gsl_vector_sub(&vec_view.vector, vec2);
    gsl_vector_scale(&vec_view.vector, 1.0 / (2.0 * parameter_perturbation));

    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old);
    gsl_vector_free(vec2);
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * @param[out]   vec    Container of value of parameter derivative
 * @param[in]    dynamical_system Settings of dynamical system
 * @param[in]    time    Size of time perturbation to calculate time derivative by numerical differentiation
 * @param[in]    parameter_perturbation    Size of parameter perturbation to calculate parameter derivative by numerical differentiation
 * @param[in]    parameter_key    Key of parameter derivative
 * @param[in]    x    A point of space
 * @note The parameter is temporarily changed in this function.
 *       When we call this function in multiple threads, we need to pay attention to the chanegs of the parameter.
 */
gboolean dynamical_system_parameter_derivative_of_time_derivative_without_exact_time_derivative (double *vec, DynamicalSystem *dynamical_system, double time, double parameter_perturbation, const char *parameter_key, const double *x)
{
  if (dynamical_system->parameter_get && dynamical_system->parameter_set) {
    double parameter_old;
    gsl_vector *vec2;
    gsl_vector_view vec_view;
    vec_view = gsl_vector_view_array(vec, dynamical_system->dimension);
    vec2 = gsl_vector_alloc(dynamical_system->dimension);
    parameter_old = dynamical_system_parameter_get_double(dynamical_system, parameter_key);

    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old + parameter_perturbation);
    dynamical_system_numerical_time_derivative((&vec_view.vector)->data, dynamical_system, time, x);
    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old - parameter_perturbation);
    dynamical_system_numerical_time_derivative(vec2->data, dynamical_system, time, x);
    gsl_vector_sub(&vec_view.vector, vec2);
    gsl_vector_scale(&vec_view.vector, 1.0 / (2.0 * parameter_perturbation));

    dynamical_system_parameter_set_double(dynamical_system, parameter_key, parameter_old);
    gsl_vector_free(vec2);
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * Calculate eigenvalues and eigenvectors of Jacobian of time derivative
 * @param[out]    eigenvalue       A pointer to store eigenvalues which is allocate for (dynamical_system->dimension) complex numbers
 * @param[out]    eigenvector      A pointer of the matrix in row-major order whose columns are eigenvectors and dimension is (dynamical_system->dimension * dynamical_system->dimension)
 * @param[in]    dynamical_system    Dynamical system
 * @return    Number of eigenvalues and eigenvectors. Return -1 if the function to calculate Jacobian
 *            of time derivative and return 0 if all eigenvalues can not be found.
 */
int dynamical_system_eigen_of_linearized_system (double complex **eigenvalue, double complex **eigenvector, DynamicalSystem *dynamical_system, const double *x)
{
  int number_of_eigenvectors = -1;
  if (dynamical_system_jacobian_of_time_derivative_defined_p(dynamical_system)) {
    gsl_matrix *jac;
    jac = gsl_matrix_alloc(dynamical_system_dimension(dynamical_system), dynamical_system_dimension(dynamical_system));
    if (dynamical_system_jacobian_of_time_derivative(jac->data, dynamical_system, x)) {
      number_of_eigenvectors = gsl_wrap_eigen_with_overwrite(eigenvalue, eigenvector, jac);
    }
    gsl_matrix_free(jac);
  }
  return number_of_eigenvectors;
}

/** @} */  /* End of DynamicalSystem */
