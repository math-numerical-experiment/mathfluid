/**
 * @file parameter_continuation_corrector_gauss_newton.c
 * @brief Gauss-Newton method for parameter continuation by using GMRES method
 */

#include "header.h"

/**
 * \addtogroup ParameterContinuationCorrectorGaussNewton
 * @{
 * Initialization of ParameterContinuationCorrectorGaussNewton is the following.
 * @code
 * gauss_newton = parameter_continuation_corrector_gauss_newton_alloc(...);
 * parameter_continuation_corrector_gauss_newton_set_initial_solution(gauss_newton, ...);
 * ...
 * parameter_continuation_corrector_gauss_newton_free(gauss_newton);
 * @Endcode
 */

enum {
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_INITIAL_VALUE,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE_NORM,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR_NORM,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR1,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR2,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_RESULT,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE,
  PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_N_LOGS,
};

static void parameter_continuation_corrector_gauss_newton_log_initial_value (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative_norm (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector_norm (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector1 (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector2 (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_result (FILE *out, va_list args);
static void parameter_continuation_corrector_gauss_newton_log_iterate_successively_convergence (FILE *out, va_list args);

static MathFluidLogFunc parameter_continuation_corrector_gauss_newton_log_function[PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_N_LOGS] = {
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_INITIAL_VALUE, MATH_FLUID_LOG_DEBUG, parameter_continuation_corrector_gauss_newton_log_initial_value },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE_NORM, MATH_FLUID_LOG_INFO, parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative_norm },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE, MATH_FLUID_LOG_DEBUG, parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR_NORM, MATH_FLUID_LOG_INFO, parameter_continuation_corrector_gauss_newton_log_iterate_error_vector_norm },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR1, MATH_FLUID_LOG_DEBUG, parameter_continuation_corrector_gauss_newton_log_iterate_error_vector1 },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR2, MATH_FLUID_LOG_DEBUG, parameter_continuation_corrector_gauss_newton_log_iterate_error_vector2 },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_RESULT, MATH_FLUID_LOG_DEBUG, parameter_continuation_corrector_gauss_newton_log_iterate_result },
  { PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, MATH_FLUID_LOG_INFO, parameter_continuation_corrector_gauss_newton_log_iterate_successively_convergence }
};

static void parameter_continuation_corrector_gauss_newton_log_print_one_vector_at_iteration (FILE *out, va_list args, const char *vector_name)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  double *x;
  gauss_newton = va_arg(args, ParameterContinuationCorrectorGaussNewton *);
  x = va_arg(args, double *);
  if (x) {
    fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate %d/%s: ", gauss_newton->iterate, vector_name);
    mathfluid_utils_array_of_double_fprintf(out, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton), x, "%.14lf", " ");
    fprintf(out, "\n");
  } else {
    fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate %d/%s: NULL\n", gauss_newton->iterate, vector_name);
  }
}

static double parameter_continuation_corrector_gauss_newton_calculate_norm (ParameterContinuationCorrectorGaussNewton *gauss_newton, double *coordinate)
{
  if (gauss_newton->norm_function == NULL) {
    fprintf(stderr, "Function to calculate norm is NULL\n");
    abort();
  }
  return gauss_newton->norm_function(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton), coordinate, gauss_newton->norm_args);
}

static void parameter_continuation_corrector_gauss_newton_log_print_one_vector_norm_at_iteration (FILE *out, va_list args, const char *vector_name)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  double *x, norm;
  gauss_newton = va_arg(args, ParameterContinuationCorrectorGaussNewton *);
  x = va_arg(args, double *);
  if (x) {
    norm = parameter_continuation_corrector_gauss_newton_calculate_norm(gauss_newton, x);
    fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate %d/norm of %s: %.14le\n", gauss_newton->iterate, vector_name, norm);
  } else {
    fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate %d/norm of %s: Can not calculate\n", gauss_newton->iterate, vector_name);
  }
}

static void parameter_continuation_corrector_gauss_newton_log_initial_value (FILE *out, va_list args)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  gauss_newton = va_arg(args, ParameterContinuationCorrectorGaussNewton *);
  fprintf(out, "ParameterContinuationCorrectorGaussNewton/initial value: ");
  mathfluid_utils_array_of_double_fprintf(out, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton), gauss_newton->vector->data, "%.14lf", " ");
  fprintf(out, "\n");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative_norm (FILE *out, va_list args)
{
  parameter_continuation_corrector_gauss_newton_log_print_one_vector_norm_at_iteration(out, args, "tangent vector");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_time_derivative (FILE *out, va_list args)
{
  parameter_continuation_corrector_gauss_newton_log_print_one_vector_at_iteration(out, args, "tangent vector");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector_norm (FILE *out, va_list args)
{
  parameter_continuation_corrector_gauss_newton_log_print_one_vector_norm_at_iteration(out, args, "modified vector norm");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector1 (FILE *out, va_list args)
{
  parameter_continuation_corrector_gauss_newton_log_print_one_vector_at_iteration(out, args, "modified vector1");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_error_vector2 (FILE *out, va_list args)
{
  parameter_continuation_corrector_gauss_newton_log_print_one_vector_at_iteration(out, args, "modified vector2");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_result (FILE *out, va_list args)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  gauss_newton = va_arg(args, ParameterContinuationCorrectorGaussNewton *);
  fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate %d/result: ", gauss_newton->iterate);
  mathfluid_utils_array_of_double_fprintf(out, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton), gauss_newton->vector->data, "%.14lf", " ");
  fprintf(out, "\n");
}

static void parameter_continuation_corrector_gauss_newton_log_iterate_successively_convergence (FILE *out, va_list args)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  gauss_newton = va_arg(args, ParameterContinuationCorrectorGaussNewton *);
  fprintf(out, "ParameterContinuationCorrectorGaussNewton/iterate successively/convergent: %.14le\n", gauss_newton->error_cache);
}

static void parameter_continuation_corrector_gauss_newton_function (double *value, JacobianFreeGMRES *gmres, const double *pt)
{
  DataContainer *setting = (DataContainer *) gmres->data;
  int extended_dim = parameter_continuation_gmres_setting_extended_dimension(setting);
  dynamical_system_parameter_set_double(jacobian_free_gmres_dynamical_system(gmres), (char *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PARAMETER_KEY), pt[extended_dim - 1]);
  jacobian_free_gmres_time_derivative(value, gmres, pt);
  value[extended_dim - 1] = 0.0;
}

/**
 * Allocate memory of ParameterContinuationCorrectorGaussNewton
 * @param[in]    gmres      Settings of GMRES
 * @param[in]    function   Function whose roots are searched for
 * @param[in]    damping_parameter    Damping parameter of Newton method
 * @param[in]    max_error  Maximum error of Newton method. If Euclid norm of a modified vector is less than the value then Newton method stops.
 */
/* ParameterContinuationCorrectorGaussNewton *parameter_continuation_corrector_gauss_newton_alloc (JacobianFreeGMRES *gmres, void (* function) (double *value, JacobianFreeGMRES *gmres, const double *pt), int damping_parameter, double max_error) */
ParameterContinuationCorrectorGaussNewton *parameter_continuation_corrector_gauss_newton_alloc (DynamicalSystem *dynamical_system, DataContainer *continuation_setting, double max_error)
{
  ParameterContinuationCorrectorGaussNewton *gauss_newton;
  gauss_newton = (ParameterContinuationCorrectorGaussNewton *) g_malloc(sizeof(ParameterContinuationCorrectorGaussNewton));
  gauss_newton->setting = continuation_setting;
  gauss_newton->gmres = parameter_continuation_gmres_alloc(dynamical_system, gauss_newton->setting);
  gauss_newton->function = parameter_continuation_corrector_gauss_newton_function;
  gauss_newton->norm_function = mathfluid_norm_euclid;
  gauss_newton->norm_args = NULL;
  gauss_newton->vector = (gsl_vector *) data_container_data_ptr(gauss_newton->setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER);
  gauss_newton->vector_last = gsl_vector_alloc(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gauss_newton->vector_diff = gsl_vector_alloc(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gauss_newton->vector_zero = gsl_vector_alloc(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gauss_newton->vector_b0 = gsl_vector_alloc(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gauss_newton->vector_b = gsl_vector_alloc(parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gauss_newton->vector_b_updated = FALSE;
  gsl_vector_set_zero(gauss_newton->vector_b0);
  gsl_vector_set(gauss_newton->vector_b0, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton) - 1, -1.0);
  /* gauss_newton->vector_zero is used as an initial value of GMRES method */
  gsl_vector_set_zero(gauss_newton->vector_zero);
  /* -1 means that the initial gauss_newton->vector is not set yet. */
  gauss_newton->iterate = -1;
  gauss_newton->max_error = max_error;
  gauss_newton->practical_error = 0.0;
  gauss_newton->max_uniform_norm = 0.0;
  gauss_newton->logger = mathfluid_logger_alloc(stdout, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_N_LOGS, parameter_continuation_corrector_gauss_newton_log_function);
  return gauss_newton;
}

/**
 * Free memory of ParameterContinuationCorrectorGaussNewton.
 * Note that you should free manually gauss_newton->gmres because gauss_newton->gmres is not freed.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 */
void parameter_continuation_corrector_gauss_newton_free (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (gauss_newton->vector_last) {
    gsl_vector_free(gauss_newton->vector_last);
  }
  if (gauss_newton->vector_diff) {
    gsl_vector_free(gauss_newton->vector_diff);
  }
  if (gauss_newton->vector_zero) {
    gsl_vector_free(gauss_newton->vector_zero);
  }
  if (gauss_newton->vector_b0) {
    gsl_vector_free(gauss_newton->vector_b0);
  }
  if (gauss_newton->vector_b) {
    gsl_vector_free(gauss_newton->vector_b);
  }
  if (gauss_newton->logger) {
    mathfluid_logger_free(gauss_newton->logger);
  }
  g_free(gauss_newton);
}

/**
 * Set log levels of ParameterContinuationCorrectorGaussNewton
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    level    Log level of Gauss-Newton method
 */
void parameter_continuation_corrector_gauss_newton_set_log_level (ParameterContinuationCorrectorGaussNewton *gauss_newton, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level)
{
  jacobian_free_gmres_set_log_level(gauss_newton->gmres, gmres_level);
  mathfluid_logger_set_log_level(gauss_newton->logger, newton_level);
}

/**
 * Set function to calculate norm
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    func    Function to calculate norm
 * @param[in]    norm_args    Additional arguments of the function func
 */
void parameter_continuation_corrector_gauss_newton_set_norm_function (ParameterContinuationCorrectorGaussNewton *gauss_newton, MathFluidNormFunc *func, void *norm_args)
{
  gauss_newton->norm_function = func;
  gauss_newton->norm_args = norm_args;
}

/**
 * Set maximum error of Newton method.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    max_error       Maximum error to test convergence of Newton method
 */
void parameter_continuation_corrector_gauss_newton_set_max_error (ParameterContinuationCorrectorGaussNewton *gauss_newton, double max_error)
{
  gauss_newton->max_error = max_error;
}

/**
 * Set practical error. For meanings of practical error,
 * see documents of parameter_continuation_corrector_gauss_newton_converged_p.
 * If we do not use practical error, set 0.0. The default value is 0.0.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    practical_error    Maximum norm of value of function at current solution
 */
void parameter_continuation_corrector_gauss_newton_set_practical_error (ParameterContinuationCorrectorGaussNewton *gauss_newton, double practical_error)
{
  gauss_newton->practical_error = practical_error;
}

/**
 * Set maximum uniform norm of solution.
 * If we do not test maximum uniform norm, then we set 0.0. The default value is 0.0.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    practical_error    Maximum uniform norm of solution
 */
void parameter_continuation_corrector_gauss_newton_set_max_uniform_norm (ParameterContinuationCorrectorGaussNewton *gauss_newton, double max_uniform_norm)
{
  gauss_newton->max_uniform_norm = max_uniform_norm;
}

/**
 * Copy current solution to an array of double.
 * @param[out]   vector    A point of an array of double whose dimension must be same as the dimension of gauss_newton
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @return FALSE if the gauss_newton->vector is not set yet. Otherwise, copy of solution succeeds and TRUE is returned.
 */
gboolean parameter_continuation_corrector_gauss_newton_solution_copy (double *vector, ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (gauss_newton->iterate < 0) {
    return FALSE;
  }
  gsl_vector_copy_components(vector, gauss_newton->vector);
  return TRUE;
}

/**
 * Allocate memory of a vector and copy current solution to the memroy space.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @return       A pointer of an allocated array of double whose size is the dimension of gauss_newton.
 */
double *parameter_continuation_corrector_gauss_newton_solution (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (gauss_newton->iterate < 0) {
    return NULL;
  }
  return gsl_vector_alloc_copy_components(gauss_newton->vector);
}

/**
 * Set initial vector of Newton method.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @param[in]    vector    A point of an array of double whose dimension must be same as the dimension of gauss_newton
 */
void parameter_continuation_corrector_gauss_newton_set_initial_solution (ParameterContinuationCorrectorGaussNewton *gauss_newton, const double *vector)
{
  gauss_newton->iterate = 0;
  memcpy(gauss_newton->vector->data, vector, sizeof(double) * parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
  gsl_vector_set_zero(gauss_newton->vector_last);
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_INITIAL_VALUE, gauss_newton);
  gauss_newton->vector_b_updated = FALSE;
}

/**
 * Return current error. Note that the value is stored to gauss_newton->error_cache.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @return    Current error that is norm of difference of current solution and last solution.
 */
double parameter_continuation_corrector_gauss_newton_current_error (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (gauss_newton->iterate < 0) {
    fprintf(stderr, "Initial value is not set yet\n");
    abort();
  }
  gsl_vector_memcpy(gauss_newton->vector_diff, gauss_newton->vector_last);
  gsl_vector_sub(gauss_newton->vector_diff, gauss_newton->vector);
  gauss_newton->error_cache = parameter_continuation_corrector_gauss_newton_calculate_norm(gauss_newton, gauss_newton->vector_diff->data);
  return gauss_newton->error_cache;
}

static void jacobian_free_gmres_update_vector_b (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (!gauss_newton->vector_b_updated) {
    gauss_newton->function(gauss_newton->vector_b->data, gauss_newton->gmres, gauss_newton->vector->data);
    gauss_newton->vector_b_updated = TRUE;
  }
}

/**
 * Test convergence of Newton method. By default, this function test whether Euclid norm of the modified vector
 * is less than gauss_newton->max_error.
 * We can change the function to calculate norms by parameter_continuation_corrector_gauss_newton_set_norm_function.
 * If gauss_newton->practical_error is a positive number then
 * this function also test whether the absolute value of the target function
 * is less than gauss_newton->practical_error.
 * Note that the value is stored to gauss_newton->error_cache.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @return    TRUE if the differences of current equilibrium and last one is
 *            less than ParameterContinuationCorrectorGaussNewton::error. Otherwise, FALSE.
 *            If the norm of gauss_newton->vector_b is larger than
 *            gauss_newton->practical_error or no iteration is executed
 *            then we also obtain FALSE.
 */
gboolean parameter_continuation_corrector_gauss_newton_converged_p (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  if (gauss_newton->iterate <= 0) {
    return FALSE;
  }
  if (gauss_newton->practical_error > 0.0) {
    jacobian_free_gmres_update_vector_b(gauss_newton);
    if (parameter_continuation_corrector_gauss_newton_calculate_norm(gauss_newton, gauss_newton->vector_b->data) >= gauss_newton->practical_error) {
      return FALSE;
    }
  }
  /*
    - It might be better to check gauss_newton->gmres_solver->error.
    - It may be better to test ratio of norms of resultant vector and modified vector.
  */
  return (parameter_continuation_corrector_gauss_newton_current_error(gauss_newton) < gauss_newton->max_error ? TRUE : FALSE);
}

/**
 * Run a step of Newton method.
 * @param[in]    gauss_newton    Settings of Gauss-Newton method
 * @return    TRUE if the iteration is executed properly. FALSE if the iteration does not finish completely.
 */
gboolean parameter_continuation_corrector_gauss_newton_iterate (ParameterContinuationCorrectorGaussNewton *gauss_newton)
{
  /*
    To solve f(x) = 0,
    we determine dx from A dx = f(x) and we set x_new = x - D dx,
    where D is a damping coefficient.
    gauss_newton->vector_b is f(x) and dx is obtained by GMRES method for A dx = f(x).
  */
  gboolean success;
  double *x1, *x2;
  if (gauss_newton->iterate < 0) {
    fprintf(stderr, "Initial value is not set yet\n");
    abort();
  }
  jacobian_free_gmres_update_vector_b(gauss_newton);
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE_NORM, gauss_newton, gauss_newton->vector_b->data);
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_TIME_DERIVATIVE, gauss_newton, gauss_newton->vector_b->data);
  x1 = jacobian_free_gmres_solve(gauss_newton->gmres, gauss_newton->vector_zero->data, gauss_newton->vector_b0->data);
  x2 = jacobian_free_gmres_solve(gauss_newton->gmres, gauss_newton->vector_zero->data, gauss_newton->vector_b->data);
  /* mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR_NORM, gauss_newton, x); */
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR1, gauss_newton, x1);
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_ERROR_VECTOR2, gauss_newton, x2);
  if (x1 && x2) {
    double eta;
    gsl_vector_view view_x1, view_x2;
    success = TRUE;
    gsl_vector_memcpy(gauss_newton->vector_last, gauss_newton->vector);
    view_x1 = gsl_vector_view_array(x1, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
    view_x2 = gsl_vector_view_array(x2, parameter_continuation_corrector_gauss_newton_dimension(gauss_newton));
    gsl_blas_ddot(&view_x1.vector, &view_x2.vector, &eta);
    eta = eta / gsl_blas_dnrm2(&view_x1.vector);
    gsl_blas_daxpy(eta, &view_x1.vector, &view_x2.vector);
    gsl_vector_sub(gauss_newton->vector, &view_x2.vector);
    g_free(x1);
    g_free(x2);
    if (gauss_newton->max_uniform_norm > 0.0) {
      int i;
      for (i = 0; i < gauss_newton->vector->size; i++) {
        if (gsl_vector_get(gauss_newton->vector, i) > gauss_newton->max_uniform_norm) {
          success = FALSE;
          break;
        }
      }
    }
  } else {
    success = FALSE;
    if (x1) {
      g_free(x1);
    }
    if (x2) {
      g_free(x2);
    }
  }
  mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_RESULT, gauss_newton);
  gauss_newton->iterate += 1;
  gauss_newton->vector_b_updated = FALSE;
  return success;
}

/**
 * Search a root of the function by running Newton method successively.
 * @param[in]    gauss_newton     Settings of Gauss-Newton method
 * @param[in]    max_iteration    Maximum number of iteration of Newton method
 * @return     A number of iterations until the solution converges.
 *             If the error of Newton method becomes less than the maximum error
 *             then the function returns a positive integer.
 *             Otherwise, return a negative integer. Zero is not returned for any case.
 */
int parameter_continuation_corrector_gauss_newton_iterate_successively (ParameterContinuationCorrectorGaussNewton *gauss_newton, uint max_iteration)
{
  int iter;
  for (iter = 1; iter <= max_iteration; iter++) {
    if (!parameter_continuation_corrector_gauss_newton_iterate(gauss_newton)) {
      fprintf(stderr, "Can not continue Gauss-Newton method\n");
      return -iter;             /* Iteration fails */
    }
    if (parameter_continuation_corrector_gauss_newton_converged_p(gauss_newton)) {
      mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, gauss_newton);
      return iter;
    }
    if (!isfinite(gauss_newton->error_cache)) {
      fprintf(stderr, "Iteration of ParameterContinuationCorrectorGaussNewton can not be continued: error = %le\n", gauss_newton->error_cache);
      return -iter;
    }
  }
  fprintf(stderr, "Iteration of ParameterContinuationCorrectorGaussNewton has not been converged: error = %.14le\n", gauss_newton->error_cache);
  return -iter;
}

/**
 * Search a root of the function by running Newton method successively with monotonic convergence.
 * @param[in]    gauss_newton     Settings of Gauss-Newton method
 * @param[in]    max_iteration    Maximum number of iteration of Newton method
 * @return     A number of iterations until the solution converges.
 *             If the error of Newton method becomes less than the maximum error
 *             then the function returns a positive integer.
 *             Otherwise, return a negative integer. Zero is not returned for any case.
 */
int parameter_continuation_corrector_gauss_newton_iterate_successively_monotonic_convergence (ParameterContinuationCorrectorGaussNewton *gauss_newton, uint max_iteration)
{
  int iter;
  double error_last;
  error_last = parameter_continuation_corrector_gauss_newton_current_error(gauss_newton);
  for (iter = 1; iter <= max_iteration; iter++) {
    if (!parameter_continuation_corrector_gauss_newton_iterate(gauss_newton)) {
      fprintf(stderr, "Can not continue Gauss-Newton method\n");
      return -iter;             /* Iteration fails */
    }
    if (parameter_continuation_corrector_gauss_newton_converged_p(gauss_newton)) {
      mathfluid_logger_log(gauss_newton->logger, PARAMETER_CONTINUATION_CORRECTOR_GAUSS_NEWTON_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, gauss_newton);
      return iter;
    }
    if (!isfinite(gauss_newton->error_cache)) {
      fprintf(stderr, "Iteration of ParameterContinuationCorrectorGaussNewton can not be continued: error = %le\n", gauss_newton->error_cache);
      return -iter;
    }
    if (gauss_newton->error_cache > error_last) {
      fprintf(stderr, "Error of ParameterContinuationCorrectorGaussNewton increases: error = %le\n", gauss_newton->error_cache);
      return -iter;
    }
    error_last = gauss_newton->error_cache;
  }
  fprintf(stderr, "Iteration of ParameterContinuationCorrectorGaussNewton has not been converged: error = %.14le\n", gauss_newton->error_cache);
  return -iter;
}

/** @} */  /* End of ParameterContinuationCorrectorGaussNewton */
