/**
 * @file parameter_continuation.c
 * @brief Numerical continuation of a parameter
 */

#include "header.h"

/**
 * \addtogroup ParameterContinuation
 * @{
 */

enum {
  PARAMETER_CONTINUATION_LOG_PREDICTOR_VECTOR,
  PARAMETER_CONTINUATION_LOG_PARAMETER,
  PARAMETER_CONTINUATION_LOG_STEP_LENGTH,
  PARAMETER_CONTINUATION_LOG_INITIAL_VECTOR,
  PARAMETER_CONTINUATION_N_LOGS,
};

static void parameter_continuation_log_predictor_vector (FILE *out, va_list args);
static void parameter_continuation_log_parameter (FILE *out, va_list args);
static void parameter_continuation_log_step_length (FILE *out, va_list args);
static void parameter_continuation_log_initial_vector (FILE *out, va_list args);

static MathFluidLogFunc parameter_continuation_log_functions[PARAMETER_CONTINUATION_N_LOGS] = {
  { PARAMETER_CONTINUATION_LOG_PREDICTOR_VECTOR, MATH_FLUID_LOG_DEBUG, parameter_continuation_log_predictor_vector },
  { PARAMETER_CONTINUATION_LOG_PARAMETER, MATH_FLUID_LOG_INFO, parameter_continuation_log_parameter },
  { PARAMETER_CONTINUATION_LOG_STEP_LENGTH, MATH_FLUID_LOG_INFO, parameter_continuation_log_step_length },
  { PARAMETER_CONTINUATION_LOG_INITIAL_VECTOR, MATH_FLUID_LOG_DEBUG, parameter_continuation_log_initial_vector }
};

static double parameter_continuation_get_current_parameter (ParameterContinuation *continuation)
{
  return dynamical_system_parameter_get_double(parameter_continuation_dynamical_system(continuation), continuation->parameter_key);
}

/**
 * Set key of parameter and set the initial prediction vector from the direction
 * @param[in]    continuation    Settings of continuation
 * @param[in]    parameter_key   Key of parameter that is continued
 * @param[in]    initial_direction    Direction of parameter continuation
 */
static void parameter_continuation_set_parameter_settings (ParameterContinuation *continuation, const char *parameter_key, ParameterContinuationDirection initial_direction)
{
  continuation->current_arc_length = 0.0;
  if (continuation->parameter_key) {
    g_free(continuation->parameter_key);
    continuation->parameter_key = NULL;
  }
  if (parameter_key) {
    continuation->parameter_key = g_strdup(parameter_key);
    /* The parameter value is set to the last element of continuation->equilibrium_with_parameter */
    gsl_vector_set(continuation->equilibrium_with_parameter, parameter_continuation_extended_dimension(continuation) - 1, parameter_continuation_get_current_parameter(continuation));
  }
  gsl_vector_set_zero(continuation->tangent_vector_last);
  if (initial_direction == PARAMETER_CONTINUATION_DIRECTION_POSITIVE) {
    gsl_vector_set(continuation->tangent_vector_last, parameter_continuation_extended_dimension(continuation) - 1, 1.0);
  } else {
    gsl_vector_set(continuation->tangent_vector_last, parameter_continuation_extended_dimension(continuation) - 1, -1.0);
  }
}

/**
 * Allocate ParameterContinuation
 * @param[in]    correction_method           Type of Newton method for correction
 * @param[in]    dynamical_system            Settings of dynamical system
 * @param[in]    newton_damping_parameter    Damping parameter of Newton method. For Gauss-Newton method ignores this argument.
 * @param[in]    newton_error                Maximum error of Newton method
 */
ParameterContinuation *parameter_continuation_alloc (ParameterContinuationCorrection correction_method, DynamicalSystem *dynamical_system, const char *parameter_key, ParameterContinuationDirection direction, int newton_damping_parameter, double newton_error)
{
  int extended_dim;
  ParameterContinuation *continuation;
  extended_dim = dynamical_system_dimension(dynamical_system) + 1;
  continuation = (ParameterContinuation *) g_malloc(sizeof(ParameterContinuation));
  continuation->logger = mathfluid_logger_alloc(stdout, PARAMETER_CONTINUATION_N_LOGS, parameter_continuation_log_functions);
  continuation->parameter_key = NULL;

  continuation->step_length = PARAMETER_CONTINUATION_DEFAULT_STEP_LENGTH;
  continuation->min_step_length = -1.0;
  continuation->current_arc_length = 0.0;
  continuation->control = PARAMETER_CONTINUATION_STEP_LENGTH_FIXED;

  continuation->predictor_setting = parameter_continuation_gmres_setting_alloc(parameter_key, extended_dim);
  continuation->predictor_gmres = parameter_continuation_gmres_alloc(dynamical_system, continuation->predictor_setting);
  continuation->corrector = parameter_continuation_corrector_alloc(correction_method, dynamical_system, parameter_key, newton_damping_parameter, newton_error);

  continuation->newton_max_iteration = PARAMETER_CONTINUATION_DEFAULT_NEWTON_MAX_ITERATION;
  continuation->equilibrium_with_parameter = gsl_vector_alloc(extended_dim);
  continuation->tangent_vector_last = gsl_vector_alloc(extended_dim);
  continuation->predictor_b = gsl_vector_alloc(extended_dim);
  gsl_vector_set_zero(continuation->predictor_b);
  gsl_vector_set(continuation->predictor_b, extended_dim - 1, 1.0);
  continuation->tangent_vector_current = gsl_vector_alloc(extended_dim);

  parameter_continuation_set_parameter_settings(continuation, parameter_key, direction);
  return continuation;
}

/**
 * Free allocated memory of ParameterContinuation.
 * Note that because cantinuation->search_equilibrium is not freed, we should free it manually.
 * @param[in]    continuation    Settings of continuation
 */
void parameter_continuation_free (ParameterContinuation *continuation)
{
  if (continuation->logger) {
    mathfluid_logger_free(continuation->logger);
  }
  if (continuation->parameter_key) {
    g_free(continuation->parameter_key);
  }
  if (continuation->predictor_setting) {
    data_container_free(continuation->predictor_setting);
  }
  if (continuation->predictor_gmres) {
    jacobian_free_gmres_free(continuation->predictor_gmres);
  }
  if (continuation->corrector) {
    parameter_continuation_corrector_free(continuation->corrector);
  }
  if (continuation->tangent_vector_last) {
    gsl_vector_free(continuation->tangent_vector_last);
  }
  if (continuation->predictor_b) {
    gsl_vector_free(continuation->predictor_b);
  }
  if (continuation->tangent_vector_current) {
    gsl_vector_free(continuation->tangent_vector_current);
  }
  if (continuation->equilibrium_with_parameter) {
    gsl_vector_free(continuation->equilibrium_with_parameter);
  }
  g_free(continuation);
}

/**
 * Set log level
 * @param[in]    continuation    Settings of continuation
 * @param[in]    continuation_level    Log level of continuation
 * @param[in]    newton_level          Log level of Newton method
 * @param[in]    gmres_level           Log level of GMRES solver
 */
void parameter_continuation_set_log_level (ParameterContinuation *continuation, MathFluidLogLevel continuation_level, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level)
{
  mathfluid_logger_set_log_level(continuation->logger, continuation_level);
  jacobian_free_gmres_set_log_level(continuation->predictor_gmres, gmres_level);
  parameter_continuation_corrector_set_log_level(continuation->corrector, newton_level, gmres_level);
}

void parameter_continuation_reset_arc_length (ParameterContinuation *continuation)
{
  continuation->current_arc_length = 0.0;
}

/**
 * @code
 * parameter_continuation_set_step_length_control(continuation, PARAMETER_CONTINUATION_STEP_LENGTH_FIXED, step_length);
 * parameter_continuation_set_step_length_control(continuation, PARAMETER_CONTINUATION_STEP_LENGTH_ADJUSTED, step_length, min_step_length)
 * @endcode
 */
void parameter_continuation_set_step_length_control (ParameterContinuation *continuation, ParameterContinuationStepLengthControl control, ...)
{
  double step_length;
  va_list args;
  va_start(args, control);
  step_length = va_arg(args, double);
  if (step_length <= 0.0) {
    fprintf(stderr, "Step size of arc length of ParameterContinuation must be positive: %.14le\n", step_length);
    abort();
  }
  continuation->step_length = step_length;
  if (control == PARAMETER_CONTINUATION_STEP_LENGTH_FIXED) {
    continuation->min_step_length = -1.0;
  } else if (control == PARAMETER_CONTINUATION_STEP_LENGTH_ADJUSTED) {
    continuation->min_step_length = va_arg(args, double);
    if (continuation->min_step_length <= 0) {
      fprintf(stderr, "Minimum step size of arc length of ParameterContinuation must be positive: %.14le\n", continuation->min_step_length);
      abort();
    }
  } else {
    fprintf(stderr, "Invalid control of ParameterContinuation\n");
    abort();
  }
  va_end(args);
}

/**
 * Set maximum number of iterations of Newton steps to search an equilibria.
 * @param[in]    continuation    Settings of continuation
 * @param[in]    newton_max_iteration    Maximum number of iterations of Newton steps
 */
void parameter_continuation_set_newton_max_iteration (ParameterContinuation *continuation, int newton_max_iteration)
{
  if (newton_max_iteration <= 0) {
    fprintf(stderr, "Maximum number of iterations of Newton method for ParameterContinuation must be positive: %d\n", newton_max_iteration);
    abort();
  }
  continuation->newton_max_iteration = newton_max_iteration;
}

void parameter_continuation_set_ignore_exact_jacobian (ParameterContinuation *continuation, gboolean ignore)
{
  jacobian_free_gmres_set_ignore_exact_jacobian(continuation->predictor_gmres, ignore);
  parameter_continuation_corrector_set_ignore_exact_jacobian(continuation->corrector, ignore);
}

/**
 * Set perturbation of Jacobian with respect to space free product.
 * @param[in]    continuation    Settings of continuation
 */
void parameter_continuation_set_space_derivative_perturbation (ParameterContinuation *continuation, double space_derivative_perturbation)
{
  jacobian_free_gmres_set_space_derivative_perturbation(continuation->predictor_gmres, space_derivative_perturbation);
  parameter_continuation_corrector_set_space_derivative_perturbation(continuation->corrector, space_derivative_perturbation);
}

/**
 * Set time evolution of numerical derivative.
 * If the derivative of orbit is explicitly given then the value is not used.
 * @param[in]    continuation    Settings of continuation
 */
void parameter_continuation_set_time_derivative_perturbation (ParameterContinuation *continuation, double time_derivative_perturbation)
{
  jacobian_free_gmres_set_time_derivative_perturbation(continuation->predictor_gmres, time_derivative_perturbation);
  parameter_continuation_corrector_set_time_derivative_perturbation(continuation->corrector, time_derivative_perturbation);
}

/**
 * Set perturbation size of parameter to calculate parameter derivative.
 * If the parameter derivative is explicitly given then the value is not used.
 * @param[in]    continuation    Settings of continuation
 * @param[in]    parameter_perturbation    Perturbation size of parameter
 */
void parameter_continuation_set_parameter_derivative_perturbation (ParameterContinuation *continuation, double parameter_perturbation)
{
  jacobian_free_gmres_set_parameter_derivative_perturbation(continuation->predictor_gmres, parameter_perturbation);
  parameter_continuation_corrector_set_parameter_derivative_perturbation(continuation->corrector, parameter_perturbation);
}

/**
 * Set the coordinate of initial equilibrium.
 * @param[in]    continuation    Settings of continuation
 * @param[in]    equilibrium     An array of double that is the coordinate of equilibrium
 */
void parameter_continuation_set_equilibrium (ParameterContinuation *continuation, const double *equilibrium)
{
  memcpy(continuation->equilibrium_with_parameter->data, equilibrium, parameter_continuation_system_dimension(continuation) * sizeof(double));
}

/**
 * Copy current equilibrium and parameter to pointers.
 * @param[out]    equilibrium    A pointer of array to store the coordinate of equilibrium
 * @param[out]    parameter      A pointer to store a parameter of the equilibrium
 * @param[in]     continuation   Settings of continuation
 */
void parameter_continuation_get_equilibrium (double *equilibrium, double *parameter, ParameterContinuation *continuation)
{
  memcpy(equilibrium, continuation->equilibrium_with_parameter->data, sizeof(double) * parameter_continuation_system_dimension(continuation));
  memcpy(parameter, ((double *) continuation->equilibrium_with_parameter->data) + parameter_continuation_system_dimension(continuation), sizeof(double));
}

static void parameter_continuation_log_predictor_vector (FILE *out, va_list args)
{
  ParameterContinuation *continuation;
  double *x;
  continuation = va_arg(args, ParameterContinuation *);
  x = va_arg(args, double *);
  if (x) {
    fprintf(out, "ParameterContinuation/predict %.14lf/parameter %.14lf/predictor vector: ", continuation->current_arc_length, x[parameter_continuation_extended_dimension(continuation) - 1]);
    mathfluid_utils_array_of_double_fprintf(out, parameter_continuation_system_dimension(continuation), x, "%.14lf", " ");
    fprintf(out, "\n");
  } else {
    fprintf(out, "ParameterContinuation/predict %.14lf/predictor vector: NULL\n", continuation->current_arc_length);
  }
}

/**
 * Solve the linear equation:
 * G(pt_with_parameter)_x x_x + G(pt_with_parameter)_a x_a = 0
 * t_x X_x + t_a X_a = 1
 * @return    Normalized vector of predictor
 */
static double *parameter_continuation_calc_tangent_vector_of_predictor (ParameterContinuation *continuation)
{
  double *vec;
  parameter_continuation_gmres_setting_set_tangent_vector(continuation->predictor_setting, continuation->tangent_vector_last);
  parameter_continuation_gmres_setting_set_pt_with_parameter(continuation->predictor_setting, continuation->equilibrium_with_parameter);
  vec = jacobian_free_gmres_solve(continuation->predictor_gmres, continuation->tangent_vector_last->data, continuation->predictor_b->data);
  if (vec) {
    double norm;
    gsl_vector_view vec_view;
    vec_view = gsl_vector_view_array(vec, parameter_continuation_extended_dimension(continuation));
    norm = gsl_blas_dnrm2(&vec_view.vector);
    gsl_vector_scale(&vec_view.vector, 1.0 / norm);
  }
  mathfluid_logger_log(continuation->logger, PARAMETER_CONTINUATION_LOG_PREDICTOR_VECTOR, continuation, vec);
  return vec;
}

static void parameter_continuation_log_parameter (FILE *out, va_list args)
{
  ParameterContinuation *continuation;
  double *x;
  continuation = va_arg(args, ParameterContinuation *);
  x = va_arg(args, double *);
  fprintf(out, "ParameterContinuation/initial parameter: %.14lf\n", x[parameter_continuation_extended_dimension(continuation) - 1]);
}

static void parameter_continuation_log_step_length (FILE *out, va_list args)
{
  ParameterContinuation *continuation;
  continuation = va_arg(args, ParameterContinuation *);
  fprintf(out, "ParameterContinuation/parameter %.14lf/step length: %.14lf\n", gsl_vector_get(continuation->equilibrium_with_parameter, parameter_continuation_extended_dimension(continuation) - 1), continuation->step_length);
}

static void parameter_continuation_log_initial_vector (FILE *out, va_list args)
{
  ParameterContinuation *continuation;
  double *x;
  continuation = va_arg(args, ParameterContinuation *);
  x = va_arg(args, double *);
  fprintf(out, "ParameterContinuation/initial parameter %.14lf/initial vector: ", x[parameter_continuation_extended_dimension(continuation) - 1]);
  mathfluid_utils_array_of_double_fprintf(out, parameter_continuation_system_dimension(continuation), x, "%.14lf", " ");
  fprintf(out, "\n");
}

/**
 * If an equilibrium is found then continuation->equilibrium_with_parameter is updated.
 * @return    Number of iterations of Newton method.
 *            A positive integer if an equilibrium is found. Otherwise, a negative integer.
 */
static int parameter_continuation_search_equilibrium (ParameterContinuation *continuation, const double *equilibrium_with_parameter_initial)
{
  int iter;
  mathfluid_logger_log(continuation->logger, PARAMETER_CONTINUATION_LOG_PARAMETER, continuation, equilibrium_with_parameter_initial);
  mathfluid_logger_log(continuation->logger, PARAMETER_CONTINUATION_LOG_INITIAL_VECTOR, continuation, equilibrium_with_parameter_initial);
  iter = parameter_continuation_corrector_search_equilibrium(continuation->corrector, equilibrium_with_parameter_initial, continuation->newton_max_iteration);
  if (iter > 0) {
    parameter_continuation_corrector_solution_copy(continuation->equilibrium_with_parameter->data, continuation->corrector);
  }
  return iter;
}

/**
 * Calculate initial point of correction step
 */
static gsl_vector *parameter_continuation_update_corrector_initial_point (ParameterContinuation *continuation)
{
  gsl_vector *corrector_init;
  corrector_init = gsl_vector_duplicate(continuation->tangent_vector_current);
  gsl_vector_scale(corrector_init, continuation->step_length);
  gsl_vector_add(corrector_init, continuation->equilibrium_with_parameter);
  return corrector_init;
}

/**
 * Correction step
 */
static int parameter_continuation_step_forward_iteration_number (ParameterContinuation *continuation)
{
  int iter;
  gsl_vector *corrector_init;
  corrector_init = parameter_continuation_update_corrector_initial_point(continuation);
  parameter_continuation_corrector_set_tangent_vector_and_point(continuation->corrector, continuation->tangent_vector_current, corrector_init);
  iter = parameter_continuation_search_equilibrium(continuation, corrector_init->data);
  gsl_vector_free(corrector_init);
  if (iter > 0) {
    gsl_vector_set_components(continuation->tangent_vector_last, continuation->tangent_vector_current->data);
    continuation->current_arc_length += continuation->step_length;
  }
  return iter;
}

/**
 * Update continuation->tangent_vector_current, which is normalized prediction vector.
 */
static gboolean parameter_continuation_update_tangent_vector_current (ParameterContinuation *continuation)
{
  double *vec_normalized;
  vec_normalized = parameter_continuation_calc_tangent_vector_of_predictor(continuation);
  if (!vec_normalized) {
    return FALSE;
  }
  gsl_vector_set_components(continuation->tangent_vector_current, vec_normalized);
  g_free(vec_normalized);
  return TRUE;
}

/**
 * Step forward continuation; continuation->tangent_vector_last, continuation->equilibrium_with_parameter, and
 * continuation->current_arc_length are updated if the continuation is succeeded.
 * @param[in]    continuation    Settings of parameter continuation
 * @return    TRUE if the next equilibrium is found. Otherwise, FALSE.
 */
gboolean parameter_continuation_step_forward (ParameterContinuation *continuation)
{
  gboolean found;
  if (!parameter_continuation_update_tangent_vector_current(continuation)) {
    return FALSE;
  }
  if (continuation->control == PARAMETER_CONTINUATION_STEP_LENGTH_FIXED) {
    found = (parameter_continuation_step_forward_iteration_number(continuation) > 0);
  } else {
    int iter;
    while (TRUE) {
      iter = parameter_continuation_step_forward_iteration_number(continuation);
      found = (iter > 0);
      if (found) {
        /* Set next step_length */
        continuation->step_length = parameter_continuation_corrector_next_step_length(continuation->corrector, iter, continuation->step_length);
        mathfluid_logger_log(continuation->logger, PARAMETER_CONTINUATION_LOG_STEP_LENGTH, continuation);
        break;
      } else {
        /* Shrink step_length and continue to search */
        double step_length_new;
        step_length_new = continuation->step_length / 2.0;
        if (step_length_new < continuation->min_step_length) {
          step_length_new = continuation->min_step_length;
        }
        if (step_length_new == continuation->step_length) {
          break;
        }
        continuation->step_length = step_length_new;
        mathfluid_logger_log(continuation->logger, PARAMETER_CONTINUATION_LOG_STEP_LENGTH, continuation);
      }
    }
  }
  return found;
}

/**
 * Parameter continuation is carried on until the arc length reaches the argument "max_arc_length".
 * @param[in]    continuation      Settings of parameter continuation
 * @param[in]    max_arc_length    Maximum arc length
 * @param[in]    func              A function that is called at each continuation step
 * @param[in]    data              A pointer of data that is an argument of the argument "func"
 * @return    TRUE if the arc length becomes larger than or equal to the argument "max_arc_length".
 *            Otherwise, FALSE.
 */
gboolean parameter_continuation_step_forward_until (ParameterContinuation *continuation, double max_arc_length, void (* func) (ParameterContinuation *continuation, void *data), void *data)
{
  gboolean found;
  while (continuation->current_arc_length < max_arc_length) {
    found = parameter_continuation_step_forward(continuation);
    if (found) {
      func(continuation, data);
    } else {
      fprintf(stderr, "Can not find equilibrium at parameter %.14lf\n", parameter_continuation_get_current_parameter(continuation));
      return FALSE;
    }
  }
  return TRUE;
}

/** @} */  /* End of ParameterContinuation */
