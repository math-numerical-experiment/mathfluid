/**
 * @file logger.c
 * @brief Functions of logger
 */

#include "header.h"

/**
 * \addtogroup MathFluidLogger
 * @{
 */

/**
 * @param[in]    out    Output stream
 * @param[in]    len    Length of the array log_func_array
 * @param[in]    log_func_array    An array of functions
 * @return    An allocated pointer of MathFluidLogger
 */
MathFluidLogger *mathfluid_logger_alloc (FILE *out, int len, MathFluidLogFunc *log_func_array)
{
  MathFluidLogger *logger;
  logger = (MathFluidLogger *) g_malloc(sizeof(MathFluidLogger));
  logger->level = MATH_FLUID_NO_LOG;
  logger->out = out;
  logger->len = len;
  logger->fflush_after_logging = TRUE;
  logger->time_logging = TRUE;
  logger->time_format = g_strdup(MATHFLUID_LOGGER_DEFAULT_TIME_FORMAT);
  if (logger->len > 0) {
    GArray *ary;
    int i;
    ary = g_array_sized_new(FALSE, FALSE, sizeof(MathFluidLogFunc),logger->len);
    for (i = 0; i < len; i++) {
      if (log_func_array[i].id < 0 || log_func_array[i].id >= len) {
        fprintf(stderr, "Invalid definition of logger\n");
        abort();
      }
      g_array_index(ary, MathFluidLogFunc, log_func_array[i].id) = log_func_array[i];
    }
    logger->log_func = (MathFluidLogFunc *) ary->data;
    g_array_free(ary, FALSE);
  } else {
    logger->log_func = NULL;
  }
  return logger;
}

void mathfluid_logger_free (MathFluidLogger *logger)
{
  if (logger->time_format) {
    g_free(logger->time_format);
  }
  if (logger->log_func) {
    g_free(logger->log_func);
  }
  g_free(logger);
}

void mathfluid_logger_set_log_level (MathFluidLogger *logger, MathFluidLogLevel level)
{
  logger->level = level;
}

void mathfluid_logger_set_fflush_after_logging (MathFluidLogger *logger, gboolean fflush_after_logging)
{
  logger->fflush_after_logging = fflush_after_logging;
}

void mathfluid_logger_set_time_logging (MathFluidLogger *logger, gboolean time_logging)
{
  logger->time_logging = time_logging;
}

void mathfluid_logger_set_time_format (MathFluidLogger *logger, char *time_format)
{
  if (logger->time_format) {
    g_free(logger->time_format);
  }
  logger->time_format = g_strdup(time_format);
}

void mathfluid_logger_log (MathFluidLogger *logger, int id, ...)
{
  if (id < 0 || id >= logger->len) {
    fprintf(stderr, "Invalid ID of log function\n");
    abort();
  } else {
    MathFluidLogFunc *log_func;
    log_func = &logger->log_func[id];
    if (logger->level <= log_func->level && log_func->func) {
      va_list args;
      va_start(args, id);
      if (logger->time_logging) {
        GDateTime *date_time;
        char *time_string;
        date_time = g_date_time_new_now_local();
        time_string = g_date_time_format(date_time, logger->time_format);
        fprintf(logger->out, "%s ", time_string);
        g_free(time_string);
        g_date_time_unref(date_time);
      }
      log_func->func(logger->out, args);
      if (logger->fflush_after_logging) {
        fflush(logger->out);
      }
      va_end(args);
    }
  }
}

void mathfluid_logger_copy_settings (MathFluidLogger *logger, MathFluidLogger *logger_src)
{
  mathfluid_logger_set_log_level(logger, logger_src->level);
  mathfluid_logger_set_fflush_after_logging(logger, logger_src->fflush_after_logging);
  mathfluid_logger_set_time_logging(logger, logger_src->time_logging);
  mathfluid_logger_set_time_format(logger, logger_src->time_format);
}

/** @} */  /* End of MathFluidLogger */
