/**
 * @file lyapunov_matrix_store.h
 * @brief Header of lyapunov_matrix_store.c
 */

#ifndef _LYAPUNOV_MATRIX_STORE_H_
#define _LYAPUNOV_MATRIX_STORE_H_

/**
 * \addtogroup LyapunovMatrixStore
 * @{
 */

typedef struct {
  SystemState *state;
  gsl_matrix *q;
  gsl_matrix *r;
} LyapunovMatrixData;

typedef struct {
  LevelDBStackCache *stack;
} LyapunovMatrixStore;

LyapunovMatrixData *lyapunov_matrix_data_alloc (SystemState *state, gsl_matrix *q, gsl_matrix *r);
void lyapunov_matrix_data_free (LyapunovMatrixData *data);
LyapunovMatrixStore *lyapunov_matrix_store_alloc ();
void lyapunov_matrix_store_set_db (LyapunovMatrixStore *store, const char *path);
void lyapunov_matrix_store_free (LyapunovMatrixStore *store);
void lyapunov_matrix_store_push (LyapunovMatrixStore *store, SystemState *state, gsl_matrix *q, gsl_matrix *r);
void lyapunov_matrix_store_pop (LyapunovMatrixStore *store, SystemState **state, gsl_matrix **q, gsl_matrix **r);

/** @} */  /* End of LyapunovMatrixStore */

#endif /* _LYAPUNOV_MATRIX_STORE_H_ */
