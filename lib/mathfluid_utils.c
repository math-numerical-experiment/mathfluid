/**
 * @file mathfluid_utils.c
 * @brief Some utility functions
 */

#include "header.h"

/**
 * \addtogroup MathFluidUtils
 * @{
 */

#define PATTERN_SPLIT_STRING_NUMBERS "[ \t\r\n\f]*[ \t,][ \t\r\n\f]*"
#define TMPDIR_TEMPLATE "mathfluid.XXXXXX"
static char *mathfluid_utils_tmpdir = NULL;

long mathfluid_utils_string_to_long (const char *str)
{
  char *str_new, *s, *endp;
  long val;
  str_new = g_strdup(str);
  s = g_strstrip(str_new);
  val = strtol(s, &endp, 0);
  if (*endp != '\0') {
    printf("Can not convert to a long number: '%s' '%s'\n", str, endp);
    abort();
  } else if (val == LONG_MAX) {
    printf("Larger than maximum value of long number: %s\n", str);
    abort();
  } else if (val == LONG_MIN) {
    printf("Less than minimum value of long number: %s\n", str);
    abort();
  } else if (val == 0 && errno == ERANGE) {
    printf("Invalid value: %s\n", str);
    abort();
  }
  g_free(str_new);
  return val;
}

double mathfluid_utils_string_to_double (const char *str)
{
  char *str_new, *s, *endp;
  double val;
  str_new = g_strdup(str);
  s = g_strstrip(str_new);
  val = strtod(s, &endp);
  if (*endp != '\0') {
    printf("Can not convert to a double number: '%s' '%s'\n", str, endp);
    abort();
  } else if (fabs(val) == HUGE_VAL) {
    printf("Overflow for a double number: %s\n", str);
    abort();
  } else if (val == 0.0 && errno == ERANGE) {
    printf("Underflow for a double number: %s\n", str);
    abort();
  }
  g_free(str_new);
  return val;
}

/**
 * Split a string that stores numbers separated by commas, white speces and so on.
 * @param[out]    count    Number of separated numbers
 * @param[in]    str    A source string
 * @return    An array of pointers of strings, which should be freed by g_strfreev.
 */
gchar **mathfluid_utils_split_string_numbers (int *count, const char *str)
{
  gchar **coordinate_string_array;
  *count = 0;
  coordinate_string_array = g_regex_split_simple(PATTERN_SPLIT_STRING_NUMBERS, str, G_REGEX_OPTIMIZE, 0);
  while (coordinate_string_array[*count]) {
    *count += 1;
  }
  return coordinate_string_array;
}

/**
 * Split a string that stores numbers separated by commas, white speces and so on and convert them to double.
 * @param[out]    count    Number of separated numbers
 * @param[in]    str    A source string
 * @return    A pointer of an array of double, which should be freed by g_free.
 */
double *mathfluid_utils_split_convert_string_numbers (int *count, const char *str)
{
  double *nums;
  gchar **num_string_array;
  int i;
  num_string_array = mathfluid_utils_split_string_numbers(count, str);
  nums = (double *) g_malloc(sizeof(double) * *count);
  for (i = 0; i < *count; i++) {
    nums[i] = mathfluid_utils_string_to_double(num_string_array[i]);
  }
  g_strfreev(num_string_array);
  return nums;
}

/**
 * Parse a string that stores numbers and save to an array of double.
 * If number of numbers stored in the string does not coincide n, then the current program exits immediately.
 * @param[out]    nums    An array to store numbers
 * @param[in]    n    Number of numbers
 * @param[in]    str    Source string
 * @param[in]    err_message    Error message to be shown when number of numbers does not match n
 */
void mathfluid_utils_string_numbers_to_array_of_double (double *nums, int n, const char *str, const char *err_message)
{
  int count_string_array, i;
  gchar **string_array;
  string_array = mathfluid_utils_split_string_numbers(&count_string_array, str);
  if (count_string_array != n) {
    fprintf(stderr, "%s; %d numbers are required, but %d numbers", err_message, n, count_string_array);
    exit(1);
  }
  for (i = 0; i < count_string_array; i++) {
    nums[i] = mathfluid_utils_string_to_double(string_array[i]);
  }
  g_strfreev(string_array);
}

/**
 * Parse a string that stores numbers and save to an allocated array of double.
 * @param[in]    n    Number of numbers
 * @param[in]    str    Source string
 * @param[in]    err_message    Error message to be shown when number of numbers does not match n
 * @return    A pointer of an allocated array of double, which should be freed by free.
 */
double *mathfluid_utils_string_numbers_to_allocated_array_of_double (int n, const char *str, const char *err_message)
{
  double *nums;
  nums = (double *) g_malloc(sizeof(double) * n);
  mathfluid_utils_string_numbers_to_array_of_double(nums, n, str, err_message);
  return nums;
}

void mathfluid_utils_array_of_double_fprintf (FILE *out, int n, double *ary, const char *format, const char *splitter)
{
  int i;
  if (n <= 0) {
    fprintf(stderr, "Length of array must be positive: n=%d\n", n);
    return;
  }
  fprintf(out, format, ary[0]);
  for (i = 1; i < n; i++) {
    fprintf(out, "%s", splitter);
    fprintf(out, format, ary[i]);
  }
}

void mathfluid_utils_array_of_double_printf (int n, double *ary, const char *format, const char *splitter)
{
  mathfluid_utils_array_of_double_fprintf(stdout, n, ary, format, splitter);
}

void mathfluid_utils_complex_double_fprintf (FILE *out, complex double num, const char *format, const char *splitter)
{
  fprintf(out, format, creal(num));
  fprintf(out, "%s", splitter);
  fprintf(out, format, cimag(num));
}

void mathfluid_utils_complex_double_printf (complex double num, const char *format, const char *splitter)
{
  mathfluid_utils_complex_double_fprintf(stdout, num, format, splitter);
}

void mathfluid_utils_array_of_complex_double_fprintf (FILE *out, int n, complex double *ary, const char *format, const char *splitter)
{
  int i;
  if (n <= 0) {
    fprintf(stderr, "Length of array must be positive: n=%d\n", n);
    return;
  }
  mathfluid_utils_complex_double_fprintf(out, ary[0], format, splitter);
  for (i = 1; i < n; i++) {
    fprintf(out, "%s", splitter);
    mathfluid_utils_complex_double_fprintf(out, ary[i], format, splitter);
  }
}

void mathfluid_utils_array_of_complex_double_printf (int n, complex double *ary, const char *format, const char *splitter)
{
  mathfluid_utils_array_of_complex_double_fprintf(stdout, n, ary, format, splitter);
}

gboolean mathfluid_utils_remove_recursively (const char *path)
{
  gboolean error_occur = FALSE;
  if (g_file_test(path, G_FILE_TEST_EXISTS)) {
    if (g_file_test(path, G_FILE_TEST_IS_DIR)) {
      GDir *dir;
      dir = g_dir_open(path, 0, NULL);
      while (TRUE) {
        const gchar *file_name = g_dir_read_name(dir);
        if (file_name) {
          gchar *file_path;
          file_path = g_strdup_printf("%s/%s", path, file_name);
          if (g_file_test(file_path, G_FILE_TEST_IS_DIR)) {
            if (!mathfluid_utils_remove_recursively(file_path)) {
              error_occur = TRUE;
            }
          } else {
            if (g_remove(file_path) != 0) {
              error_occur = TRUE;
            }
          }
          g_free(file_path);
          if (error_occur) {
            break;
          }
        } else {
          break;
        }
      }
      g_dir_close(dir);
    }
    if (g_remove(path) != 0) {
      error_occur = TRUE;
    }
  }
  return !error_occur;
}

/**
 * @return    A pointer of string that must be freed later
 */
char *mathfluid_utils_tmpdir_root ()
{
  if (mathfluid_utils_tmpdir) {
    return g_strdup(mathfluid_utils_tmpdir);
  } else {
    return NULL;
  }
}

void mathfluid_utils_tmpdir_set_root (const char *root_tmpdir)
{
  if (mathfluid_utils_tmpdir) {
    fprintf(stderr, "Root of tmpdir is already set: %s\n", mathfluid_utils_tmpdir);
    abort();
  }
  if (root_tmpdir) {
    gchar *template;
    template = g_strdup_printf("%s/%s", root_tmpdir, TMPDIR_TEMPLATE);
    mathfluid_utils_tmpdir = g_mkdtemp(template);
  } else {
    mathfluid_utils_tmpdir = g_dir_make_tmp(TMPDIR_TEMPLATE, NULL);
  }
  if (!mathfluid_utils_tmpdir) {
    fprintf(stderr, "Can not create root of temporary directory\n");
    abort();
  }
}

void mathfluid_utils_tmpdir_clear ()
{
  if (mathfluid_utils_tmpdir) {
    mathfluid_utils_remove_recursively(mathfluid_utils_tmpdir);
    g_free(mathfluid_utils_tmpdir);
    mathfluid_utils_tmpdir = NULL;
  }
}

char *mathfluid_utils_tmpdir_get_subdir (const char *prefix)
{
  gchar *template, *subdir;
  if (!mathfluid_utils_tmpdir) {
    mathfluid_utils_tmpdir_set_root(NULL);
  }
  template = g_strdup_printf("%s/%s.XXXXXX", mathfluid_utils_tmpdir, prefix);
  subdir = g_mkdtemp(template);
  return subdir;
}

MathFluidFile *mathfluid_utils_file_open (const char *path)
{
  MathFluidFile *file;
  file = (MathFluidFile *) g_malloc(sizeof(MathFluidFile));
  file->path = g_strdup(path);
  file->buffer = NULL;
  file->buffer_size = 0;
  file->io = fopen(file->path, "r");
  if (!file->io) {
    fprintf(stderr, "Can not open %s\n", file->path);
    abort();
  }
  return file;
}

void mathfluid_utils_file_close (MathFluidFile *file)
{
  if (file->path) {
    g_free(file->path);
  }
  if (file->io) {
    fclose(file->io);
  }
  if (file->buffer) {
    g_free(file->buffer);
  }
  g_free(file);
}

/**
 * @return    String of next line except for comment line
 */
char *mathfluid_utils_file_get_data_line (MathFluidFile *file)
{
  int line_size;
  while (TRUE) {
    line_size = getline(&file->buffer, &file->buffer_size, file->io);
    if (line_size <= 0) {
      return NULL;
    } else if (file->buffer[0] != '#') {
      return g_strdup(file->buffer);
    }
  }
}

/**
 * If the macro "MATHFLUID_UTILS_SRAND_SEED" is defined
 * then the function "srand" is called with the argument "MATHFLUID_UTILS_SRAND_SEED".
 * Otherwise, the argument is an integer of current time.
 */
void mathfluid_utils_srand (void)
{
  static gboolean initialized = FALSE;
  if (!initialized) {
    initialized = TRUE;
#ifdef MATHFLUID_UTILS_SRAND_SEED
    srand(MATHFLUID_UTILS_SRAND_SEED);
#else
    srand((unsigned) time(NULL));
#endif
  }
}

/** @} */  /* End of MathFluidUtils */
