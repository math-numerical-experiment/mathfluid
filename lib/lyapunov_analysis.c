/**
 * @file lyapunov_analysis.c
 * @brief Functions of Lyapunov analysis
 */

#include "header.h"

/**
 * \addtogroup LyapunovAnalysis
 * @{
 */

/**
 * States of forward evolution steps
 */
typedef struct {
  SystemState *state;
  gsl_matrix *q;
  gsl_matrix *q_new;
  gsl_matrix *r_new;
} LyapunovAnalysisState;

static LyapunovAnalysisState *lyapunov_analysis_state_alloc (LyapunovAnalysis *analysis, const double *x)
{
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = (LyapunovAnalysisState *) g_malloc(sizeof(LyapunovAnalysisState));
  lyapunov_analysis_state->state = system_state_alloc2(lyapunov_analysis_system_dimension(analysis), 0.0, x);
  lyapunov_analysis_state->q = gsl_matrix_alloc(lyapunov_analysis_system_dimension(analysis), analysis->number);
  lyapunov_analysis_state->q_new = gsl_matrix_alloc(lyapunov_analysis_system_dimension(analysis), analysis->number);
  lyapunov_analysis_state->r_new = gsl_matrix_alloc(analysis->number, analysis->number);

  mathfluid_utils_srand();    /* Call srand once */
  gsl_matrix_set_random_normalized_orthogonal_columns(lyapunov_analysis_state->q);
  return lyapunov_analysis_state;
}

static void lyapunov_analysis_state_free (LyapunovAnalysisState *lyapunov_analysis_state)
{
  system_state_free(lyapunov_analysis_state->state);
  gsl_matrix_free(lyapunov_analysis_state->q);
  gsl_matrix_free(lyapunov_analysis_state->q_new);
  gsl_matrix_free(lyapunov_analysis_state->r_new);
  g_free(lyapunov_analysis_state);
}

static LyapunovAnalysisState *lyapunov_analysis_state_duplicate (LyapunovAnalysisState *lyapunov_analysis_state)
{
  LyapunovAnalysisState *lyapunov_analysis_state_dup;
  lyapunov_analysis_state_dup = (LyapunovAnalysisState *) g_malloc(sizeof(LyapunovAnalysisState));
  lyapunov_analysis_state_dup->state = system_state_alloc_copy(lyapunov_analysis_state->state);
  lyapunov_analysis_state_dup->q = gsl_matrix_duplicate(lyapunov_analysis_state->q);
  lyapunov_analysis_state_dup->q_new = gsl_matrix_duplicate(lyapunov_analysis_state->q_new);
  lyapunov_analysis_state_dup->r_new = gsl_matrix_duplicate(lyapunov_analysis_state->r_new);
  return lyapunov_analysis_state_dup;
}

LyapunovAnalysis *lyapunov_analysis_alloc (DynamicalSystem *dynamical_system, int num, double time_step)
{
  LyapunovAnalysis *analysis;
  analysis = (LyapunovAnalysis *) g_malloc(sizeof(LyapunovAnalysis));
  analysis->number = num;
  analysis->time_step = time_step;
  analysis->cache_directory = NULL;
  analysis->evolve = lyapunov_evolve_alloc(dynamical_system, FALSE, LYAPUNOV_EVOLVE_ALGORITHM_RK4, time_step);
  return analysis;
}

void lyapunov_analysis_free (LyapunovAnalysis *analysis)
{
  if (analysis->evolve) {
    lyapunov_evolve_free(analysis->evolve);
  }
  if (analysis->cache_directory) {
    g_free(analysis->cache_directory);
  }
  g_free(analysis);
}

void lyapunov_analysis_set_use_thread (LyapunovAnalysis *analysis, gboolean use_thread)
{
  analysis->evolve->use_thread = use_thread;
}

void lyapunov_analysis_set_space_derivative_perturbation (LyapunovAnalysis *analysis, double perturbation)
{
  derivative_of_system_set_space_derivative_perturbation(analysis->evolve->derivative, perturbation);
  mathfluid_thread_variable_clear(analysis->evolve->thread_variable);
}

void lyapunov_analysis_set_ignore_exact_jacobian (LyapunovAnalysis *analysis, gboolean ignore)
{
  derivative_of_system_set_ignore_exact_jacobian(analysis->evolve->derivative, ignore);
  mathfluid_thread_variable_clear(analysis->evolve->thread_variable);
}

void lyapunov_analysis_set_cache_directory_path (LyapunovAnalysis *analysis, const char *cache_directory)
{
  analysis->cache_directory = g_strdup(cache_directory);
}

static void lyapunov_analysis_set_iterated_vector (LyapunovAnalysis *analysis, LyapunovAnalysisState *lyapunov_analysis_state, const double *pt, int iterate)
{
  DynamicalSystem *dynamical_system;
  MatrixVectorProduct *matrix_vector_product;
  DerivativeOfSystem *derivative;
  SimultaneousIteration *simultaneous_iteration;

  dynamical_system = lyapunov_analysis_dynamical_system(analysis);
  matrix_vector_product = jacobian_of_vector_field_product_alloc(dynamical_system, pt);
  derivative = jacobian_of_vector_field_product_derivative_of_system_ptr(matrix_vector_product);
  derivative_of_system_set_time_derivative_perturbation(derivative, analysis->time_step);
  derivative_of_system_set_space_derivative_perturbation(derivative, analysis->evolve->derivative->space_derivative_perturbation);

  simultaneous_iteration = simultaneous_iteration_alloc(dynamical_system_dimension(dynamical_system), analysis->number, matrix_vector_product);
  simultaneous_iteration_set_qr_randomly(simultaneous_iteration);
  simultaneous_iteration_iterate_successively(simultaneous_iteration, iterate);
  gsl_matrix_memcpy(lyapunov_analysis_state->q, simultaneous_iteration->q);

  simultaneous_iteration_free(simultaneous_iteration);
}

static void lyapunov_analysis_init_iterate_time_step (int *iterate, double *time_step, LyapunovAnalysis *analysis, double t)
{
  *iterate = ceil(t / analysis->time_step);
  *time_step = t / (double) *iterate;
}

static void lyapunov_analysis_gsl_matrix_ptr_swap (gsl_matrix **m1, gsl_matrix **m2)
{
  gsl_matrix *p;
  p = *m1;
  *m1 = *m2;
  *m2 = p;
}

static void lyapunov_analysis_iterate_forward_with_evolve (LyapunovAnalysisState *lyapunov_analysis_state, LyapunovAnalysis *analysis, double time_step)
{
  gsl_matrix *qr;
  qr = lyapunov_evolve_forward(lyapunov_analysis_state->state, analysis->evolve, lyapunov_analysis_state->state, time_step, lyapunov_analysis_state->q);
  gsl_wrap_qr_decomp_with_overwrite(lyapunov_analysis_state->q_new, lyapunov_analysis_state->r_new, qr);
  lyapunov_analysis_gsl_matrix_ptr_swap(&(lyapunov_analysis_state->q), &(lyapunov_analysis_state->q_new));
  gsl_matrix_free(qr);
}

static void lyapunov_analysis_iterate_forward_at_equilibrium (LyapunovAnalysisState *lyapunov_analysis_state, LyapunovAnalysis *analysis, double time_step)
{
  gsl_matrix *qr;
  qr = lyapunov_evolve_forward_at_equilibrium(analysis->evolve, lyapunov_analysis_state->state, time_step, lyapunov_analysis_state->q);
  gsl_wrap_qr_decomp_with_overwrite(lyapunov_analysis_state->q_new, lyapunov_analysis_state->r_new, qr);
  lyapunov_analysis_gsl_matrix_ptr_swap(&(lyapunov_analysis_state->q), &(lyapunov_analysis_state->q_new));
  gsl_matrix_free(qr);
}

static double *lyapunov_analysis_lyapunov_exponents_alloc (LyapunovAnalysis *analysis)
{
  double *lyapunov_exponents;
  int i;
  lyapunov_exponents = (double *) g_malloc(sizeof(double) * analysis->number);
  for (i = 0; i < analysis->number; i++) {
    lyapunov_exponents[i] = 0.0;
  }
  return lyapunov_exponents;
}

static void lyapunov_analysis_lyapunov_exponents_add_variation (double *lyapunov_exponents, LyapunovAnalysis *analysis, gsl_matrix *r, double time_total)
{
  int i;
  for (i = 0; i < analysis->number; i++) {
    lyapunov_exponents[i] += log(fabs(gsl_matrix_get(r, i, i))) / time_total;
  }
}

/**
 * Calculate Lyapunov exponents of the orbit of x and backward Lyapunov vectors at t.
 * @param[in]    analysis    Setting of Lyapunov analysis
 * @param[in]    t    Time so that Lyapunov exponents converge.
 * @param[in]    x    The initial point of the orbit.
 */
double *lyapunov_analysis_lyapunov_exponents (LyapunovAnalysis *analysis, double t, const double *x)
{
  int i, iterate;
  double *lyapunov_exponents, time_step;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, x);
  lyapunov_exponents = lyapunov_analysis_lyapunov_exponents_alloc(analysis);
  lyapunov_analysis_init_iterate_time_step(&iterate, &time_step, analysis, t);
  for (i = 0; i < iterate; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step);
    lyapunov_analysis_lyapunov_exponents_add_variation(lyapunov_exponents, analysis, lyapunov_analysis_state->r_new, t);
  }
  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return lyapunov_exponents;
}

static double *lyapunov_analysis_current_finite_time_lyapunov_exponents (LyapunovAnalysisState *lyapunov_analysis_state, LyapunovAnalysis *analysis, double time_current, double time_step)
{
  double *lyapunov_exponents;
  int i;
  /* First element is time and the others are Lyapunov exponents. */
  lyapunov_exponents = (double *) g_malloc(sizeof(double) * (analysis->number + 1));
  lyapunov_exponents[0] = time_current;
  for (i = 0; i < analysis->number; i++) {
    lyapunov_exponents[i + 1] = log(fabs(gsl_matrix_get(lyapunov_analysis_state->r_new, i, i))) / time_step;
  }
  return lyapunov_exponents;
}

/**
 * Calculate finite time Lyapunov exponents from t1 to t2.
 * @param[in]    analysis    Settings to calculate finite time Lyapunov exponents
 * @param[in]    t1    Minimum time value of finite time Lyapunov exponents, which should be large so that the process of forward iteration converges
 * @param[in]    t2    Maximum time value of finite time Lyapunov exponents
 * @param[in]    x    Initial point of the orbit
 * @return  An array of pointers of finite time Lyapunov exponents, which should be freed by g_ptr_array_free.
 *          Elements of a returned pointer array are pointers of an array of double.
 *          g_ptr_array_free(array, TRUE) frees automatically all elements that are pointers of arrays.
 */
GPtrArray *lyapunov_analysis_finite_time_lyapunov_exponents (LyapunovAnalysis *analysis, double t1, double t2, const double *x)
{
  int i, iterate[2];
  double time_step[2], time_current, *lyapunov_exponents;
  GPtrArray *finite_time_lyapunov_exponents;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, x);
  lyapunov_analysis_init_iterate_time_step(&iterate[0], &time_step[0], analysis, t1);
  lyapunov_analysis_init_iterate_time_step(&iterate[1], &time_step[1], analysis, t2 - t1);
  finite_time_lyapunov_exponents = g_ptr_array_new_full(iterate[1] + 1, (void (*) (gpointer)) g_free);
  for (i = 0; i < iterate[0]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[0]);
  }
  for (i = 0; i < iterate[1]; i++) {
    time_current = lyapunov_analysis_state->state->time;
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[1]);
    lyapunov_exponents = lyapunov_analysis_current_finite_time_lyapunov_exponents(lyapunov_analysis_state, analysis, time_current, time_step[1]);
    g_ptr_array_add(finite_time_lyapunov_exponents, lyapunov_exponents);
  }
  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return finite_time_lyapunov_exponents;
}

/**
 * Solve AX = B for an upper triangular matrix A and a matrix B.
 * @param[in]    x    Matrix to store solution
 * @param[in]    a    Square matrix
 * @param[in]    b    A matrix
 * Note that the dimension of the matrix A must equal the number of rows of both the matrices X and B
 * and the matrix X must have the same number of columns as that of the matrix B.
 */
static void lyapunov_analysis_solve_upper_triangular_matrix_equation (gsl_matrix *x, gsl_matrix *a, gsl_matrix *b)
{
  int i;
  gsl_vector *v;
  v = gsl_vector_alloc(a->size1);
  for (i = 0; i < b->size2; i++) {
    gsl_matrix_get_col(v, b, i);
    gsl_blas_dtrsv(CblasUpper, CblasNoTrans, CblasNonUnit, a, v);
    gsl_matrix_set_col(x, i, v);
  }
  gsl_vector_free(v);
}

static LyapunovMatrixStore *lyapunov_analysis_matrix_store_alloc (LyapunovAnalysis *analysis)
{
  LyapunovMatrixStore *matrix_store;
  matrix_store = lyapunov_matrix_store_alloc();
  if (analysis->cache_directory) {
    gchar *template, *dir;
    template = g_strdup_printf("%s/lyapunov_matrix_store.XXXXXX", analysis->cache_directory);
    dir = g_mkdtemp(template);
    lyapunov_matrix_store_set_db(matrix_store, dir);
    g_free(dir);
  }
  return matrix_store;
}

static LyapunovVectorSet *lyapunov_analysis_vector_set_alloc (LyapunovAnalysis *analysis)
{
  LyapunovVectorSet *vector_set;
  vector_set = lyapunov_vector_set_alloc();
  if (analysis->cache_directory) {
    gchar *template, *dir;
    template = g_strdup_printf("%s/lyapunov_vector_set.XXXXXX", analysis->cache_directory);
    dir = g_mkdtemp(template);
    lyapunov_vector_set_set_db(vector_set, dir);
    g_free(dir);
  }
  return vector_set;
}

/**
 * Calculate backward Lyapunov vectors from t1 to t2.
 * @param[in]    analysis    Settings to calculate backward Lyapunov vectors
 * @param[in]    t1    Minimum time value of backward Lyapunov vectors, which should be large so that the process of forward iteration converges
 * @param[in]    t2    Maximum time value of backward Lyapunov vectors
 * @param[in]    x    Initial point of the orbit
 * @return  Set of Lyapunov vectors that stores vectors in order of time
 */
LyapunovVectorSet *lyapunov_analysis_backward_lyapunov_vectors (LyapunovAnalysis *analysis, double t1, double t2, const double *x)
{
  int i, iterate[2];
  double time_step[2];
  LyapunovVectorSet *backward_lyapunov_vectors;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, x);
  lyapunov_analysis_init_iterate_time_step(&iterate[0], &time_step[0], analysis, t1);
  lyapunov_analysis_init_iterate_time_step(&iterate[1], &time_step[1], analysis, t2 - t1);
  backward_lyapunov_vectors = lyapunov_analysis_vector_set_alloc(analysis);

  for (i = 0; i < iterate[0]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[0]);
  }

  lyapunov_vector_set_push(backward_lyapunov_vectors, lyapunov_vector_alloc_set(analysis, lyapunov_analysis_state->state, lyapunov_analysis_state->q));
  for (i = 0; i < iterate[1]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[1]);

    lyapunov_vector_set_push(backward_lyapunov_vectors, lyapunov_vector_alloc_set(analysis, lyapunov_analysis_state->state, lyapunov_analysis_state->q));
  }

  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return backward_lyapunov_vectors;
}

static LyapunovMatrixStore *lyapunov_analysis_covariant_lyapunov_vectors_forward_process (LyapunovAnalysis *analysis, LyapunovAnalysisState *lyapunov_analysis_state, int *iterate, double t1, double t2, double t3, const double *x)
{
  int i;
  double time_step[3];
  LyapunovMatrixStore *matrix_store;
  lyapunov_analysis_init_iterate_time_step(&iterate[0], &time_step[0], analysis, t1);
  lyapunov_analysis_init_iterate_time_step(&iterate[1], &time_step[1], analysis, t2 - t1);
  lyapunov_analysis_init_iterate_time_step(&iterate[2], &time_step[2], analysis, t3 - t2);

  matrix_store = lyapunov_analysis_matrix_store_alloc(analysis);

  for (i = 0; i < iterate[0]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[0]);
  }

  lyapunov_matrix_store_push(matrix_store, lyapunov_analysis_state->state, lyapunov_analysis_state->q, lyapunov_analysis_state->r_new);
  for (i = 0; i < iterate[1]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[1]);
    lyapunov_matrix_store_push(matrix_store, lyapunov_analysis_state->state, lyapunov_analysis_state->q, lyapunov_analysis_state->r_new);
  }

  for (i = 0; i < iterate[2]; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, time_step[2]);
    lyapunov_matrix_store_push(matrix_store, NULL, NULL, lyapunov_analysis_state->r_new);
  }

  return matrix_store;
}

static LyapunovVectorSet *lyapunov_analysis_covariant_lyapunov_vectors_backward_process (LyapunovAnalysis *analysis, LyapunovMatrixStore *matrix_store, int *iterate, gsl_matrix *initial_matrix)
{
  int i;
  gsl_matrix *a, *a_new, *clv, *r, *q;
  SystemState *state;
  LyapunovVector *vector_ptr;
  LyapunovVectorSet *covariant_lyapunov_vectors;

  a = gsl_matrix_duplicate(initial_matrix);
  a_new = gsl_matrix_alloc(a->size1, a->size2);
  for (i = 0; i < iterate[2]; i++) {
    lyapunov_matrix_store_pop(matrix_store, NULL, NULL, &r);
    lyapunov_analysis_solve_upper_triangular_matrix_equation(a_new, r, a);
    gsl_matrix_free(r);
    lyapunov_analysis_gsl_matrix_ptr_swap(&a, &a_new);
    gsl_matrix_normalize_all_columns(a);
  }

  covariant_lyapunov_vectors = lyapunov_analysis_vector_set_alloc(analysis);
  clv = gsl_matrix_alloc(lyapunov_analysis_system_dimension(analysis), analysis->number);
  lyapunov_matrix_store_pop(matrix_store, &state, &q, &r);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, q, a, 0.0, clv);
  vector_ptr = lyapunov_vector_alloc_set(analysis, state, clv);
  lyapunov_vector_set_push(covariant_lyapunov_vectors, vector_ptr);
  for (i = 0; i < iterate[1]; i++) {
    lyapunov_analysis_solve_upper_triangular_matrix_equation(a_new, r, a);
    lyapunov_analysis_gsl_matrix_ptr_swap(&a, &a_new);
    gsl_matrix_normalize_all_columns(a);

    system_state_free(state);
    gsl_matrix_free(q);
    gsl_matrix_free(r);

    lyapunov_matrix_store_pop(matrix_store, &state, &q, &r);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, q, a, 0.0, clv);
    vector_ptr = lyapunov_vector_alloc_set(analysis, state, clv);
    lyapunov_vector_set_push(covariant_lyapunov_vectors, vector_ptr);
  }
  system_state_free(state);
  gsl_matrix_free(q);
  gsl_matrix_free(r);

  gsl_matrix_free(a);
  gsl_matrix_free(a_new);
  gsl_matrix_free(clv);

  return covariant_lyapunov_vectors;
}

/**
 * Calculate covariant Lyapunov vectors from t1 to t2.
 * @param[in]    analysis    Settings to calculate covariant Lyapunov vectors
 * @param[in]    t1    Minimum time value of covariant Lyapunov vectors, which should be large so that the process of forward iteration converges
 * @param[in]    t2    Maximum time value of covariant Lyapunov vectors
 * @param[in]    t3    Maximum time value of iteration process, which should be large so that the process of backward iteration converges
 * @param[in]    x    Initial point of the orbit
 * @return  Set of Lyapunov vectors that stores vectors in order of time
 */
LyapunovVectorSet *lyapunov_analysis_covariant_lyapunov_vectors (LyapunovAnalysis *analysis, double t1, double t2, double t3, const double *x)
{
  int iterate[3];
  gsl_matrix *upper_triangular_matrix;
  LyapunovMatrixStore *matrix_store;
  LyapunovVectorSet *covariant_lyapunov_vectors;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, x);
  matrix_store = lyapunov_analysis_covariant_lyapunov_vectors_forward_process(analysis, lyapunov_analysis_state, iterate, t1, t2, t3, x);
  upper_triangular_matrix = gsl_matrix_alloc_random_upper_triangular_matrix(analysis->number);
  covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors_backward_process(analysis, matrix_store, iterate, upper_triangular_matrix);
  lyapunov_matrix_store_free(matrix_store);
  gsl_matrix_free(upper_triangular_matrix);
  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return covariant_lyapunov_vectors;
}

/**
 * Calculate covariant Lyapunov vectors from t1 to t2 with initial iteration at x.
 * @param[in]    analysis    Settings to calculate covariant Lyapunov vectors
 * @param[in]    t1    Minimum time value of covariant Lyapunov vectors, which should be large so that the process of forward iteration converges
 * @param[in]    t2    Maximum time value of covariant Lyapunov vectors
 * @param[in]    t3    Maximum time value of iteration process, which should be large so that the process of backward iteration converges
 * @param[in]    x    Initial point of the orbit
 * @param[in]    initial_iterate    Iteration at initial point x
 * @param[in]    last_iterate       Iteration at last point
 * @return  Set of Lyapunov vectors that stores vectors in order of time
 */
LyapunovVectorSet *lyapunov_analysis_covariant_lyapunov_vectors_with_iteration (LyapunovAnalysis *analysis, double t1, double t2, double t3, const double *x, int initial_iterate, int last_iterate)
{
  int iterate[3];
  gsl_matrix *upper_triangular_matrix;
  LyapunovMatrixStore *matrix_store;
  LyapunovVectorSet *covariant_lyapunov_vectors;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, x);
  if (initial_iterate > 0) {
    lyapunov_analysis_set_iterated_vector(analysis, lyapunov_analysis_state, x, initial_iterate);
  }
  matrix_store = lyapunov_analysis_covariant_lyapunov_vectors_forward_process(analysis, lyapunov_analysis_state, iterate, t1, t2, t3, x);
  upper_triangular_matrix = gsl_matrix_alloc_random_upper_triangular_matrix(analysis->number);
  if (last_iterate > 0) {
    gsl_matrix *a, *a_new, *r;
    int i;
    for (i = 0; i < last_iterate; i++) {
      LyapunovAnalysisState *lyapunov_analysis_state_dup = lyapunov_analysis_state_duplicate(lyapunov_analysis_state);
      lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state_dup, analysis, analysis->time_step);
      lyapunov_matrix_store_push(matrix_store, NULL, NULL, lyapunov_analysis_state_dup->r_new);
      lyapunov_analysis_state_free(lyapunov_analysis_state_dup);
    }
    a = gsl_matrix_duplicate(upper_triangular_matrix);
    a_new = gsl_matrix_alloc(a->size1, a->size2);
    for (i = 0; i < last_iterate; i++) {
      lyapunov_matrix_store_pop(matrix_store, NULL, NULL, &r);
      lyapunov_analysis_solve_upper_triangular_matrix_equation(a_new, r, a);
      gsl_matrix_free(r);
      lyapunov_analysis_gsl_matrix_ptr_swap(&a, &a_new);
      gsl_matrix_normalize_all_columns(a);
    }
    gsl_matrix_free(a);
    gsl_matrix_free(a_new);
  }
  covariant_lyapunov_vectors = lyapunov_analysis_covariant_lyapunov_vectors_backward_process(analysis, matrix_store, iterate, upper_triangular_matrix);
  lyapunov_matrix_store_free(matrix_store);
  gsl_matrix_free(upper_triangular_matrix);
  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return covariant_lyapunov_vectors;
}

LyapunovVector *lyapunov_analysis_backward_lyapunov_vectors_of_equilibrium (LyapunovAnalysis *analysis, int iterate, const double *equilibrium)
{
  int i;
  LyapunovVector *vector_ptr;
  LyapunovAnalysisState *lyapunov_analysis_state;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, equilibrium);
  for (i = 0; i < iterate; i++) {
    lyapunov_analysis_iterate_forward_with_evolve(lyapunov_analysis_state, analysis, analysis->time_step);
  }
  vector_ptr = lyapunov_vector_alloc_set(analysis, lyapunov_analysis_state->state, lyapunov_analysis_state->q);
  lyapunov_analysis_state_free(lyapunov_analysis_state);
  return vector_ptr;
}

LyapunovVector *lyapunov_analysis_covariant_lyapunov_vectors_of_equilibrium (double **lyapunov_exponents, LyapunovAnalysis *analysis, int iterate1, int iterate2, const double *equilibrium)
{
  int i;
  double time_total;
  gsl_matrix *a, *a_new, *clv, *r, *q;
  SystemState *state;
  LyapunovVector *vector_ptr;
  LyapunovAnalysisState *lyapunov_analysis_state;
  LyapunovMatrixStore *matrix_store;
  lyapunov_analysis_state = lyapunov_analysis_state_alloc(analysis, equilibrium);
  clv = gsl_matrix_alloc(lyapunov_analysis_system_dimension(analysis), analysis->number);
  *lyapunov_exponents = lyapunov_analysis_lyapunov_exponents_alloc(analysis);
  matrix_store = lyapunov_analysis_matrix_store_alloc(analysis);

  for (i = 0; i < iterate1; i++) {
    lyapunov_analysis_iterate_forward_at_equilibrium(lyapunov_analysis_state, analysis, analysis->time_step);
  }

  lyapunov_matrix_store_push(matrix_store, lyapunov_analysis_state->state, lyapunov_analysis_state->q, lyapunov_analysis_state->r_new);
  time_total = analysis->time_step * iterate2;
  for (i = 0; i < iterate2; i++) {
    lyapunov_analysis_iterate_forward_at_equilibrium(lyapunov_analysis_state, analysis, analysis->time_step);
    lyapunov_matrix_store_push(matrix_store, NULL, NULL, lyapunov_analysis_state->r_new);
    lyapunov_analysis_lyapunov_exponents_add_variation(*lyapunov_exponents, analysis, lyapunov_analysis_state->r_new, time_total);
  }

  a = gsl_matrix_alloc_random_upper_triangular_matrix(analysis->number);
  a_new = gsl_matrix_alloc(analysis->number, analysis->number);
  for (i = 0; i < iterate2; i++) {
    lyapunov_matrix_store_pop(matrix_store, NULL, NULL, &r);
    lyapunov_analysis_solve_upper_triangular_matrix_equation(a_new, r, a);
    gsl_matrix_free(r);
    lyapunov_analysis_gsl_matrix_ptr_swap(&a, &a_new);
    gsl_matrix_normalize_all_columns(a);
  }

  lyapunov_matrix_store_pop(matrix_store, &state, &q, &r);
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, q, a, 0.0, clv);
  vector_ptr = lyapunov_vector_alloc_set(analysis, state, clv);
  system_state_free(state);
  gsl_matrix_free(q);
  gsl_matrix_free(r);

  lyapunov_analysis_state_free(lyapunov_analysis_state);
  gsl_matrix_free(a);
  gsl_matrix_free(a_new);
  gsl_matrix_free(clv);
  lyapunov_matrix_store_free(matrix_store);
  return vector_ptr;
}

static gsl_matrix *lyapunov_analysis_set_matrix_of_lyapunov_vectors (LyapunovAnalysis *analysis, LyapunovVector *lyapunov_vector)
{
  gsl_matrix *matrix;
  gsl_vector_view col;
  int i;
  if (analysis->number > lyapunov_vector->number) {
    fprintf(stdout, "Number of vectors does not coincide settings of analysis\n");
    abort();
  }
  matrix = gsl_matrix_alloc(lyapunov_analysis_system_dimension(analysis), analysis->number);
  for (i = 0; i < analysis->number; i++) {
    col = gsl_vector_view_array(lyapunov_vector->vector[i], lyapunov_analysis_system_dimension(analysis));
    gsl_matrix_set_col(matrix, i, &col.vector);
  }
  return matrix;
}

static double *lyapunov_analysis_finite_time_covariant_lyapunov_exponent_of_vector (LyapunovAnalysis *analysis, LyapunovVector *lyapunov_vector)
{
  int i;
  double *lyapunov_exponents;
  gsl_matrix *m1, *m2;
  gsl_vector_view col1, col2;
  m1 = lyapunov_analysis_set_matrix_of_lyapunov_vectors(analysis, lyapunov_vector);
  m2 = lyapunov_evolve_forward(NULL, analysis->evolve, lyapunov_vector->state, analysis->time_step, m1);
  /* First element is time and the others are Lyapunov exponents. */
  lyapunov_exponents = (double *) g_malloc(sizeof(double) * (analysis->number + 1));
  lyapunov_exponents[0] = lyapunov_vector->state->time;
  for (i = 0; i < analysis->number; i++) {
    col1 = gsl_matrix_column(m1, i);
    col2 = gsl_matrix_column(m2, i);
    lyapunov_exponents[i + 1] = log(gsl_blas_dnrm2(&col2.vector) / gsl_blas_dnrm2(&col1.vector)) / analysis->time_step;
  }
  gsl_matrix_free(m2);
  gsl_matrix_free(m1);
  return lyapunov_exponents;
}

/**
 * @param[in]    analysis    Settings of Lyapunov analysis
 * @param[in]    covariant_lyapunov_vectors    Set of Lyapunov vectors
 * @return    A pointer array of arrays of double whose first element is time and others are finite time covariant Lyapunov exponents.
 */
GPtrArray *lyapunov_analysis_finite_time_covariant_lyapunov_exponents (LyapunovAnalysis *analysis, LyapunovVectorSet *covariant_lyapunov_vectors)
{
  double *lyapunov_exponents;
  GPtrArray *finite_time_covariant_lyapunov_exponents;
  LyapunovVector *lyapunov_vector;
  finite_time_covariant_lyapunov_exponents = g_ptr_array_new_with_free_func((void (*) (gpointer)) g_free);
  while (TRUE) {
    lyapunov_vector = lyapunov_vector_set_pop(covariant_lyapunov_vectors);
    if (!lyapunov_vector) {
      break;
    }
    lyapunov_exponents = lyapunov_analysis_finite_time_covariant_lyapunov_exponent_of_vector(analysis, lyapunov_vector);
    g_ptr_array_add(finite_time_covariant_lyapunov_exponents, lyapunov_exponents);
    lyapunov_vector_free(lyapunov_vector);
  }
  return finite_time_covariant_lyapunov_exponents;
}

static LyapunovVector *lyapunov_analysis_lyapunov_vector_from_data_line (LyapunovAnalysis *analysis, const char *buf)
{
  double *nums;
  LyapunovVector *lyapunov_vector;
  int count, num_vectors, ind, i, j;
  nums = mathfluid_utils_split_convert_string_numbers(&count, buf);
  num_vectors = (count - 1) / lyapunov_analysis_system_dimension(analysis) - 1;
  lyapunov_vector = lyapunov_vector_alloc(lyapunov_analysis_system_dimension(analysis), num_vectors);
  ind = 0;
  lyapunov_vector->state->time = nums[ind];
  ind += 1;
  for (i = 0; i < lyapunov_analysis_system_dimension(analysis); i++) {
    lyapunov_vector->state->coordinate[i] = nums[ind];
    ind += 1;
  }
  for (j = 0; j < num_vectors; j++) {
    for (i = 0; i < lyapunov_analysis_system_dimension(analysis); i++) {
      lyapunov_vector->vector[j][i] = nums[ind];
      ind += 1;
    }
  }
  g_free(nums);
  return lyapunov_vector;
}

static double *lyapunov_analysis_finite_time_covariant_lyapunov_exponents_from_data_line (LyapunovAnalysis *analysis, const char *buf)
{
  double *lyapunov_exponents;
  LyapunovVector *covariant_lyapunov_vector;
  covariant_lyapunov_vector = lyapunov_analysis_lyapunov_vector_from_data_line(analysis, buf);
  lyapunov_exponents = lyapunov_analysis_finite_time_covariant_lyapunov_exponent_of_vector(analysis, covariant_lyapunov_vector);
  lyapunov_vector_free(covariant_lyapunov_vector);
  return lyapunov_exponents;
}

GPtrArray *lyapunov_analysis_finite_time_covariant_lyapunov_exponents2 (LyapunovAnalysis *analysis, const char *path_covariant_lyapunov_vectors)
{
  double *lyapunov_exponents;
  GPtrArray *finite_time_covariant_lyapunov_exponents;
  MathFluidFile *input;
  char *buf;
  input = mathfluid_utils_file_open(path_covariant_lyapunov_vectors);
  finite_time_covariant_lyapunov_exponents = g_ptr_array_new_with_free_func((void (*) (gpointer)) g_free);
  while (TRUE) {
    buf = mathfluid_utils_file_get_data_line(input);
    if (buf) {
      lyapunov_exponents = lyapunov_analysis_finite_time_covariant_lyapunov_exponents_from_data_line(analysis, buf);
      g_ptr_array_add(finite_time_covariant_lyapunov_exponents, lyapunov_exponents);
      g_free(buf);
    } else {
      break;
    }
  }
  mathfluid_utils_file_close(input);
  return finite_time_covariant_lyapunov_exponents;
}

static double *lyapunov_analysis_inner_product_abs_after_evolution (LyapunovAnalysis *analysis, LyapunovVector *clv_current, LyapunovVector *clv_last)
{
  double *inner_products;
  int i;
  inner_products = (double *) g_malloc(sizeof(double) * (analysis->number + 1));
  inner_products[0] = clv_last->state->time;
  for (i = 0; i < analysis->number; i++) {
    inner_products[i + 1] = fabs(cblas_ddot(lyapunov_analysis_system_dimension(analysis), clv_last->vector[i], 1, clv_current->vector[i], 1));
  }
  return inner_products;
}

/**
 * @param[in]    analysis    Settings of Lyapunov analysis
 * @param[in]    path_clv    Path of a file that stores covariant Lyapunov vectors
 * @return    An array whose elements store time and absolute values of inner products of covariant Lyapunov vectors
 *            and vectors obtained by evolving covariant Lyapunov vectors
 */
GPtrArray *lyapunov_analysis_covariant_lyapunov_vectors_test (LyapunovAnalysis *analysis, const char *path_clv)
{
  double *inner_products;
  GPtrArray *ary_inner_product;
  MathFluidFile *input;
  char *buf;
  LyapunovVector *clv_current, *clv_last;
  clv_current = NULL;
  clv_last = NULL;
  input = mathfluid_utils_file_open(path_clv);
  ary_inner_product = g_ptr_array_new_with_free_func((void (*) (gpointer)) g_free);
  while (TRUE) {
    buf = mathfluid_utils_file_get_data_line(input);
    if (buf) {
      clv_current = lyapunov_analysis_lyapunov_vector_from_data_line(analysis, buf);
      if (clv_last) {
        inner_products = lyapunov_analysis_inner_product_abs_after_evolution(analysis, clv_current, clv_last);
        g_ptr_array_add(ary_inner_product, inner_products);
        lyapunov_vector_free(clv_last);
      }
      g_free(buf);
      clv_last = clv_current;
      clv_current = NULL;
    } else {
      break;
    }
  }
  if (clv_last) {
    lyapunov_vector_free(clv_last);
  }
  mathfluid_utils_file_close(input);
  return ary_inner_product;
}

/** @} */  /* End of LyapunovAnalysis */
