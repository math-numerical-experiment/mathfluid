/**
 * @file lyapunov_vector.c
 * @brief Store of Lyapunov vectors
 */

#include "header.h"

/**
 * \addtogroup LyapunovVector
 * @{
 */

LyapunovVector *lyapunov_vector_alloc (int dimension, int number)
{
  int i;
  LyapunovVector *lyapunov_vector;
  lyapunov_vector = (LyapunovVector *) g_malloc(sizeof(LyapunovVector));
  lyapunov_vector->state = system_state_alloc(dimension);
  lyapunov_vector->number = number;
  lyapunov_vector->vector = (double **) g_malloc(sizeof(double *) * lyapunov_vector->number);
  for (i = 0; i < lyapunov_vector->number; i++) {
    lyapunov_vector->vector[i] = (double *) g_malloc(sizeof(double) * lyapunov_vector->state->dimension);
  }
  return lyapunov_vector;
}

static void lyapunov_vector_set_gsl_matrix (LyapunovVector *lyapunov_vector, SystemState *state, const gsl_matrix *vectors)
{
  int i;
  gsl_vector_view v1;
  system_state_copy(lyapunov_vector->state, state);
  for (i = 0; i < lyapunov_vector->number; i++) {
    gsl_vector_const_view v2 = gsl_matrix_const_column(vectors, i);
    v1 = gsl_vector_view_array(lyapunov_vector->vector[i], lyapunov_vector->state->dimension);
    gsl_vector_memcpy(&v1.vector, &v2.vector);
  }
}

/**
 * @param[in]    analysis    Settings of Lyapunov analysis
 * @param[in]    state       A state on an orbit
 * @param[in]    vectors     A matrix whose column vectors are vectors on tangent space
 */
LyapunovVector *lyapunov_vector_alloc_set (LyapunovAnalysis *analysis, SystemState *state, gsl_matrix *vectors)
{
  LyapunovVector *lyapunov_vector;
  lyapunov_vector = lyapunov_vector_alloc(lyapunov_analysis_system_dimension(analysis), vectors->size2);
  lyapunov_vector_set_gsl_matrix(lyapunov_vector, state, vectors);
  return lyapunov_vector;
}

void lyapunov_vector_free (LyapunovVector *lyapunov_vector)
{
  int i;
  system_state_free(lyapunov_vector->state);
  for (i = 0; i < lyapunov_vector->number; i++) {
    g_free(lyapunov_vector->vector[i]);
  }
  g_free(lyapunov_vector->vector);
  g_free(lyapunov_vector);
}

/**
 * Return an allocated double array whose columns are Lyapunov vectors
 */
double *lyapunov_vector_double_array_row_major_order (LyapunovVector *lyapunov_vector)
{
  double *array;
  int i, j;
  gsl_matrix_view m;
  array = (double *) g_malloc(sizeof(double) * lyapunov_vector->state->dimension * lyapunov_vector->number);
  m = gsl_matrix_view_array(array, lyapunov_vector->state->dimension, lyapunov_vector->number);
  for (i = 0; i < lyapunov_vector->number; i++) {
    for (j = 0; j < lyapunov_vector->state->dimension; j++) {
      gsl_matrix_set(&m.matrix, j, i, lyapunov_vector->vector[i][j]);
    }
  }
  return array;
}

static char *lyapunov_vector_dump (size_t *size, gpointer data_ptr)
{
  int i;
  char *dump, *dump_cur;
  uint32_t size_total, dim, num, size_vector;
  LyapunovVector *data;
  data = (LyapunovVector *) data_ptr;
  dim = data->state->dimension;
  num = data->number;
  size_vector = sizeof(double) * dim;
  /* dimension, number, time, coordinate, vectors */
  size_total = sizeof(uint32_t) + sizeof(uint32_t) + sizeof(double) + size_vector + size_vector * num;
  dump = (char *) g_malloc(size_total);
  dump_cur = dump;
  memcpy(dump_cur, &dim, sizeof(uint32_t));
  dump_cur += sizeof(uint32_t);
  memcpy(dump_cur, &num, sizeof(uint32_t));
  dump_cur += sizeof(uint32_t);
  memcpy(dump_cur, &data->state->time, sizeof(double));
  dump_cur += sizeof(double);
  memcpy(dump_cur, data->state->coordinate, size_vector);
  dump_cur += size_vector;

  for (i = 0; i < data->number; i++) {
    memcpy(dump_cur, data->vector[i], size_vector);
    dump_cur += size_vector;
  }

  *size = size_total;
  return dump;
}

static gpointer lyapunov_vector_load (char *dump, size_t size)
{
  int i;
  uint32_t dim, num, size_vector;
  LyapunovVector *data;
  memcpy(&dim, dump, sizeof(uint32_t));
  dump += sizeof(uint32_t);
  memcpy(&num, dump, sizeof(uint32_t));
  dump += sizeof(uint32_t);

  size_vector = sizeof(double) * dim;
  data = lyapunov_vector_alloc(dim, num);

  memcpy(&data->state->time, dump, sizeof(double));
  dump += sizeof(double);
  memcpy(data->state->coordinate, dump, size_vector);
  dump += size_vector;

  for (i = 0; i < num; i++) {
    memcpy(data->vector[i], dump, size_vector);
    dump += size_vector;
  }

  return data;
}

LyapunovVectorSet *lyapunov_vector_set_alloc ()
{
  LyapunovVectorSet *vector_set;
  vector_set = g_malloc(sizeof(LyapunovVectorSet));
  vector_set->stack = leveldb_stack_cache_alloc(lyapunov_vector_dump, lyapunov_vector_load, (void (*) (gpointer)) lyapunov_vector_free);
  return vector_set;
}

void lyapunov_vector_set_set_db (LyapunovVectorSet *vector_set, const char *path)
{
  leveldb_stack_cache_set_db(vector_set->stack, path);
}

void lyapunov_vector_set_free (LyapunovVectorSet *vector_set)
{
  if (vector_set->stack) {
    leveldb_stack_cache_free(vector_set->stack, TRUE);
  }
  g_free(vector_set);
}

void lyapunov_vector_set_push (LyapunovVectorSet *vector_set, LyapunovVector *vector)
{
  leveldb_stack_cache_push(vector_set->stack, vector);
}

LyapunovVector *lyapunov_vector_set_pop (LyapunovVectorSet *vector_set)
{
  return (LyapunovVector *) leveldb_stack_cache_pop(vector_set->stack);
}

/**
 * Print data of Lyapunov vectors: time, orbit, vector, ..., vector
 * @param[out]    out    Output IO
 * @param[in]    lyapunov_vectors    Set of Lyapunov vectors. Note that lyapunov_vectors is freed in this function
 * @param[in]    number_format    Format of numbers
 * @param[in]    number_splitter    String of splitter
 */
void lyapunov_vector_set_fprintf (FILE *out, LyapunovVectorSet *lyapunov_vectors, const char *number_format, const char *number_splitter)
{
  LyapunovVector *v;
  int i;
  while (TRUE) {
    v = lyapunov_vector_set_pop(lyapunov_vectors);
    if (!v) {
      break;
    }
    printf(number_format, v->state->time);
    printf("%s", number_splitter);
    mathfluid_utils_array_of_double_printf(v->state->dimension, v->state->coordinate, number_format, number_splitter);
    for (i = 0; i < v->number; i++) {
      printf("%s", number_splitter);
      mathfluid_utils_array_of_double_printf(v->state->dimension, v->vector[i], number_format, number_splitter);
    }
    printf("\n");
    lyapunov_vector_free(v);
  }
  lyapunov_vector_set_free(lyapunov_vectors);
}

/** @} */  /* End of LyapunovVector */
