/**
 * @file arnoldi_method.c
 * @brief Functions of Arnoldi method
 */

#include "header.h"

/**
 * \addtogroup ArnoldiMethod
 * @{
 * Initialization of ArnoldiMethod is the following.
 * @code
 * arnoldi = arnoldi_method_alloc(...);
 * arnoldi_method_set_min_norm(arnoldi, ...); // if necessary
 * arnoldi_method_set(arnoldi, ...);          // or use arnoldi_method_set_randomly
 * ...
 * arnoldi_method_free(arnoldi);
 * @endcode
 */

/**
 * Allocate memory of ArnoldiMethod.
 * @param[in]    dimension    Dimension of vectors
 * @param[in]    max_iteration     Maximum iteration number that is equal to maximum dimension of Krylov subspace
 * @param[in]    matrix_vector_product    Definition of matrix vector product
 */
ArnoldiMethod *arnoldi_method_alloc (uint dimension, uint max_iteration, MatrixVectorProduct *matrix_vector_product)
{
  ArnoldiMethod *arnoldi_method;
  int i;
  arnoldi_method = (ArnoldiMethod *) g_malloc(sizeof(ArnoldiMethod));
  arnoldi_method->dimension = dimension;
  arnoldi_method->max_iteration = max_iteration;
  arnoldi_method->matrix_vector_product = matrix_vector_product;
  /* -1 means that the initial vector is not set yet. */
  arnoldi_method->iterate = -1;
  arnoldi_method->min_norm = GSL_DBL_EPSILON;
  arnoldi_method->w = gsl_vector_alloc(arnoldi_method->dimension);
  arnoldi_method->basis = (gsl_vector **) g_malloc(sizeof(gsl_vector *) * arnoldi_method->max_iteration);
  arnoldi_method->hessenberg_column = (double *) g_malloc(sizeof(double) * (arnoldi_method->max_iteration + 1));
  for (i = 0; i < arnoldi_method->max_iteration; i++) {
    arnoldi_method->basis[i] = NULL;
  }
  return arnoldi_method;
}

/**
 * Free memory of ArnoldiMethod.
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 */
void arnoldi_method_free (ArnoldiMethod *arnoldi_method)
{
  if (arnoldi_method->matrix_vector_product) {
    matrix_vector_product_free(arnoldi_method->matrix_vector_product);
  }
  if (arnoldi_method->w) {
    gsl_vector_free(arnoldi_method->w);
  }
  if (arnoldi_method->basis) {
    int i;
    for (i = 0; i < arnoldi_method->max_iteration; i++) {
      if (arnoldi_method->basis[i]) {
        gsl_vector_free(arnoldi_method->basis[i]);
        arnoldi_method->basis[i] = NULL;
      }
    }
    g_free(arnoldi_method->basis);
  }
  if (arnoldi_method->hessenberg_column) {
    g_free(arnoldi_method->hessenberg_column);
  }
  g_free(arnoldi_method);
}

/**
 * Set minimum norm of a perpendicular line from new vector to Krylov subspace.
 * If the norm of the perpendicular line is less than the minimum norm,
 * iteration of Arnoldi method stops.
 * The default value of the minimum norm is GSL_DBL_EPSILON.
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @param[in]    min_norm          Value of minimum norm
 */
void arnoldi_method_set_min_norm (ArnoldiMethod *arnoldi_method, double min_norm)
{
  arnoldi_method->min_norm = min_norm;
}

static gsl_vector *arnoldi_method_get_allocated_basis (ArnoldiMethod *arnoldi_method, int i)
{
  if (!arnoldi_method->basis[i]) {
    arnoldi_method->basis[i] = gsl_vector_alloc(arnoldi_method->dimension);
  }
  return arnoldi_method->basis[i];
}

/**
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 */
static void arnoldi_method_hessenberg_column_reset (ArnoldiMethod *arnoldi_method)
{
  int i;
  for (i = 0; i <= arnoldi_method->max_iteration; i++) {
    arnoldi_method->hessenberg_column[i] = 0.0;
  }
}

/**
 * Set initial vector and reset.
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @param[in]    v                 An initial vector of Arnoldi method
 */
void arnoldi_method_set (ArnoldiMethod *arnoldi_method, const double *v)
{
  gsl_vector *vec;
  double norm;
  arnoldi_method->iterate = 0;
  vec = arnoldi_method_get_allocated_basis(arnoldi_method, 0);
  gsl_vector_set_components(vec, v);
  norm = gsl_blas_dnrm2(vec);
  gsl_vector_scale(vec, 1.0 / norm);
}

/**
 * Set initial vector randomly and reset.
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 */
void arnoldi_method_set_randomly (ArnoldiMethod *arnoldi_method)
{
  gsl_vector *vec;
  arnoldi_method->iterate = 0;
  vec = arnoldi_method_get_allocated_basis(arnoldi_method, 0);
  gsl_vector_set_random_normalized_components(vec);
}

/**
 * Iterate one step and update arnoldi_method->hessenberg_column that is the column of Hessenberg matrix.
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @return    TRUE if next iteration step exists, that is, the vector from Krylov subspace is sufficiently large. Otherwise, FALSE.
 */
gboolean arnoldi_method_iterate (ArnoldiMethod *arnoldi_method)
{
  gsl_vector *vi_tmp;
  double h;
  int i;
  if (arnoldi_method->iterate < 0) {
    fprintf(stderr, "Initial vector is not set yet\n");
    abort();
  }
  vi_tmp = gsl_vector_alloc(arnoldi_method->dimension);
  arnoldi_method_hessenberg_column_reset(arnoldi_method);
  matrix_vector_product_calculate(arnoldi_method->w->data, arnoldi_method->matrix_vector_product, arnoldi_method_get_allocated_basis(arnoldi_method, arnoldi_method->iterate)->data);
  for (i = 0; i <= arnoldi_method->iterate; i++) {
    gsl_vector_memcpy(vi_tmp, arnoldi_method_get_allocated_basis(arnoldi_method, i));
    gsl_blas_ddot(vi_tmp, arnoldi_method->w, &h);
    arnoldi_method->hessenberg_column[i] = h;
    gsl_vector_scale(vi_tmp, h);
    gsl_vector_sub(arnoldi_method->w, vi_tmp);
  }
  h = gsl_blas_dnrm2(arnoldi_method->w);
  arnoldi_method->hessenberg_column[arnoldi_method->iterate + 1] = h;
  arnoldi_method->iterate += 1;
  gsl_vector_free(vi_tmp);
  if (h < arnoldi_method->min_norm || arnoldi_method->iterate >= arnoldi_method->max_iteration) {
    return FALSE;
  } else {
    gsl_vector_scale(arnoldi_method->w, 1.0 / h);
    vi_tmp = arnoldi_method_get_allocated_basis(arnoldi_method, arnoldi_method->iterate);
    gsl_vector_memcpy(vi_tmp, arnoldi_method->w);
    return TRUE;
  }
}

/**
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @return    A pointer of current column of Hessenberg matrix.
 */
const double *arnoldi_method_hessenberg_column (ArnoldiMethod *arnoldi_method)
{
  return arnoldi_method->hessenberg_column;
}

const double *arnoldi_method_basis_ptr (ArnoldiMethod *arnoldi_method, int ind)
{
  if (ind < 0 || ind > arnoldi_method->iterate || ind > arnoldi_method->max_iteration) {
    return NULL;
  } else {
    return arnoldi_method->basis[ind]->data;
  }
}

/**
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @return    A pointer of the allocated matrix in row-major order whose columns are bases.
 */
double *arnoldi_method_basis_matrix (ArnoldiMethod *arnoldi_method)
{
  double *m;
  gsl_matrix_view view;
  int i;
  if (arnoldi_method->iterate <= 0) {
    fprintf(stderr, "No basis is calculated\n");
    abort();
  }
  m = (double *) g_malloc(sizeof(double) * arnoldi_method->dimension * arnoldi_method->iterate);
  view = gsl_matrix_view_array(m, arnoldi_method->dimension, arnoldi_method->iterate);
  for (i = 0; i < arnoldi_method->iterate; i++) {
    const double *basis = arnoldi_method_basis_ptr(arnoldi_method, i);
    gsl_vector_const_view v = gsl_vector_const_view_array(basis, arnoldi_method->dimension);
    gsl_matrix_set_col(&view.matrix, i, &v.vector);
  }
  return m;
}

/* Calculate all columns of Hessenberg matrix from initial state */
static double **arnoldi_method_all_hessenberg_columns (ArnoldiMethod *arnoldi_method, int *column_number)
{
  gboolean finish;
  double **columns;
  size_t column_data_size;
  int i;
  if (arnoldi_method->iterate != 0) {
    fprintf(stderr, "ArnoldiMethod is not initial state\n");
    abort();
  }
  columns = (double **) g_malloc(sizeof(double *) * arnoldi_method->max_iteration);
  *column_number = arnoldi_method->max_iteration;
  for (i = 0; i < arnoldi_method->max_iteration; i++) {
    finish = !arnoldi_method_iterate(arnoldi_method);
    column_data_size = sizeof(double) * (arnoldi_method->iterate + 1);
    columns[i] = (double *) g_malloc(column_data_size);
    memcpy(columns[i], arnoldi_method_hessenberg_column(arnoldi_method), column_data_size);
    if (finish) {
      *column_number = i + 1;
      break;
    }
  }
  return columns;
}

static double *arnoldi_method_hessenberg_matrix_alloc (ArnoldiMethod *arnoldi_method, double **columns, int column_number)
{
  int i, j;
  double *hessenberg;
  gsl_matrix_view view_hessenberg;
  hessenberg = (double *) g_malloc(sizeof(double) * (column_number + 1) * column_number);
  view_hessenberg = gsl_matrix_view_array(hessenberg, column_number + 1, column_number);
  for (i = 0; i < column_number; i++) {
    for (j = 0; j <= i + 1; j++) {
      gsl_matrix_set(&view_hessenberg.matrix, j, i, columns[i][j]);
    }
    for (; j <= column_number; j++) {
      gsl_matrix_set(&view_hessenberg.matrix, j, i, 0.0);
    }
  }
  return hessenberg;
}

/**
 * @param[out]   row_number        Row number of returned matrix
 * @param[out]   column_number     Column number of returned matrix
 * @param[in]    arnoldi_method    Settings of Arnoldi method
 * @return    A pointer of the allocated matrix in row-major order
 *            that is Hessenberg matrix whose size is ((iterate + 1) * iterate)
 */
double *arnoldi_method_iterate_successively (int *row_number, int *column_number, ArnoldiMethod *arnoldi_method)
{
  double **columns, *hessenberg;
  int i, col;
  columns = arnoldi_method_all_hessenberg_columns(arnoldi_method, &col);
  hessenberg = arnoldi_method_hessenberg_matrix_alloc(arnoldi_method, columns, col);
  *column_number = col;
  *row_number = col + 1;
  for (i = 0; i < col; i++) {
    g_free(columns[i]);
  }
  g_free(columns);
  return hessenberg;
}

/** @} */  /* End of ArnoldiMethod */
