/**
 * @file lyapunov_evolve.h
 * @brief Header of lyapunov_evolve.c
 */

#ifndef _LYAPUNOV_EVOLVE_H_
#define _LYAPUNOV_EVOLVE_H_

LyapunovEvolve *lyapunov_evolve_alloc (DynamicalSystem *dynamical_system, gboolean use_thread, LyapunovEvolveAlgorithm algorithm, double time_derivative_perturbation);
void lyapunov_evolve_free (LyapunovEvolve *evolve);
gsl_matrix *lyapunov_evolve_forward (SystemState *state_evolved, LyapunovEvolve *evolve, SystemState *state, double time_step, gsl_matrix *q);
gsl_matrix *lyapunov_evolve_forward_at_equilibrium (LyapunovEvolve *evolve, SystemState *equilibrium, double time_step, gsl_matrix *q);

/**
 * \addtogroup LyapunovEvolve
 * @{
 */

#define lyapunov_evolve_system_dimension(evolve) (evolve->dynamical_system->dimension)

/** @} */  /* End of LyapunovEvolve */

#endif /* _LYAPUNOV_EVOLVE_H_ */
