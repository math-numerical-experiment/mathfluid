/**
 * @file mathfluid_norm.c
 * @brief Some norm functions
 */

#include "header.h"

/**
 * \addtogroup MathFluidNorm
 * @{
 */

/**
 * Function to calculate Euclid norm
 */
double mathfluid_norm_euclid (int dimension, double *coordinate, void *norm_args)
{
  int i;
  double norm = 0.0;
  for (i = 0; i < dimension; i++) {
    norm += coordinate[i] * coordinate[i];
  }
  norm = sqrt(norm);
  return norm;
}

/**
 * Function to calculate maximum norm
 */
double mathfluid_norm_maximum (int dimension, double *coordinate, void *norm_args)
{
  int i;
  double norm = 0.0;
  double abs;
  for (i = 0; i < dimension; i++) {
    abs = fabs(coordinate[i]);
    if (abs > norm) {
      norm = abs;
    }
  }
  return norm;
}

/** @} */  /* End of MathFluidNorm */
