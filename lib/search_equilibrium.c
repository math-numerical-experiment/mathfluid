/**
 * @file search_equilibrium.c
 * @brief Functions of search for equilibria of ODE
 */

#include "header.h"

/**
 * \addtogroup SearchEquilibrium
 * @{
 */

static void search_equilibrium_product_of_matrix_vector (double *av, JacobianFreeGMRES *gmres, const double *v)
{
  jacobian_free_gmres_product_jacobian_vector(av, gmres, ((SearchEquilibrium *) gmres->data)->newton_gmres->vector->data, v);
}

static void search_equilibrium_time_derivative_function (double *value, JacobianFreeGMRES *gmres, const double *pt)
{
  jacobian_free_gmres_time_derivative(value, gmres, pt);
}

/**
 * Allocate SearchEquilibrium that must be freed by search_equilibrium_free later.
 * To configure other parameters of SearchEquilibrium, we call search_equilibrium_set_gmres_max_iteration and
 * search_equilibrium_set_time_derivative_perturbation.
 * To configure GMRES method used internally, we can also access solver of GMRES method by search_equilibrium_gmres_solver.
 * @param[in]    dynamical_system    Dynamical system to be dealt with
 * @param[in]    damping_parameter   Damp modified vectors with scale 2^{-damping_parameter}. Nonpositive value is ignored.
 * @param[in]    error               Error of equilibrium, that is, if the variation of Newton method is less than this value
 *                                   we regard the iteration process of Newton method to be convergent.
 */
SearchEquilibrium *search_equilibrium_alloc (DynamicalSystem *dynamical_system, int damping_parameter, double error)
{
  SearchEquilibrium *search;
  search = g_malloc(sizeof(SearchEquilibrium));
  search->gmres = jacobian_free_gmres_alloc(dynamical_system, dynamical_system_dimension(dynamical_system), search, search_equilibrium_product_of_matrix_vector);
  search->newton_gmres = jacobian_free_newton_gmres_alloc(search->gmres, search_equilibrium_time_derivative_function, damping_parameter, error);
  return search;
}

/**
 * Free SearchEquilibrium
 * @param[in]    search    Settings of search equilibria
 */
void search_equilibrium_free (SearchEquilibrium *search)
{
  jacobian_free_gmres_free(search->gmres);
  jacobian_free_newton_gmres_free(search->newton_gmres);
  g_free(search);
}

/**
 * Set log levels of SearchEquilibrium
 * @param[in]    search    Settings of search equilibria
 * @param[in]    newton_level    Log level of Newton method
 * @param[in]    gmres_level     Log level of GMRES solver
 */
void search_equilibrium_set_log_level (SearchEquilibrium *search, MathFluidLogLevel newton_level, MathFluidLogLevel gmres_level)
{
  jacobian_free_newton_gmres_set_log_level(search->newton_gmres, newton_level);
  jacobian_free_gmres_set_log_level(search->gmres, gmres_level);
}

/** @} */  /* End of SearchEquilibrium */
