/**
 * @file matrix_vector_product.c
 * @brief Functions to define matrix vector product
 */

#include "header.h"

enum {
  USUAL_MATRIX_VECTOR_PRODUCT_MATRIX,
  USUAL_MATRIX_VECTOR_PRODUCT_N
};

enum {
  JACOBIAN_VECTOR_PRODUCT_DERIVATIVE,
  JACOBIAN_VECTOR_PRODUCT_POINT,
  JACOBIAN_VECTOR_PRODUCT_N
};

/**
 * \addtogroup MatrixVectorProduct
 * @{
 */

/**
 * Allocate MatrixVectorProduct.
 * Note that matrix_data is freed by matrix_vector_product_free.
 * @param[in]    matrix_data    Data to calculate matrix vector product
 * @param[in]    product        Function to calculate matrix vector product
 */
MatrixVectorProduct *matrix_vector_product_alloc (DataContainer *matrix_data, void (* product) (double *product, DataContainer *matrix_data, const double *vector))
{
  return (MatrixVectorProduct *) data_container_alloc(MATRIX_VECTOR_PRODUCT_N, product, data_container_utils_return_pointer, NULL, matrix_data, data_container_duplicate, data_container_free);
}

/**
 * Free memory of MatrixVectorProduct
 * @param[in]    matrix_vector_product    Settings of matrix vector product
 */
void matrix_vector_product_free (MatrixVectorProduct *matrix_vector_product)
{
  data_container_free((DataContainer *) matrix_vector_product);
}

/**
 * Duplicate memory of MatrixVectorProduct if possible
 * @param[in]    matrix_vector_product    Settings of matrix vector product
 */
MatrixVectorProduct *matrix_vector_product_duplicate (MatrixVectorProduct *matrix_vector_product)
{
  return data_container_duplicate((DataContainer *) matrix_vector_product);
}

/**
 * Calculate matrix vector product
 * @param[out]   product                  A pointer of an array to store value of the product
 * @param[in]    matrix_vector_product    Settings of matrix vector product
 * @param[in]    vector                   A pointer of an array of an input vector
 */
void matrix_vector_product_calculate (double *product, MatrixVectorProduct *matrix_vector_product, const double *vector)
{
  matrix_vector_product_function_ptr(matrix_vector_product)(product, matrix_vector_product_matrix_data_ptr(matrix_vector_product), vector);
}

static void usual_matrix_vector_product_calculate (double *product, DataContainer *matrix_data, const double *vector)
{
  gsl_matrix *matrix = (gsl_matrix *) data_container_data_ptr(matrix_data, USUAL_MATRIX_VECTOR_PRODUCT_MATRIX);
  gsl_vector_view view_product;
  gsl_vector_const_view view_vector = gsl_vector_const_view_array(vector, matrix->size2);
  view_product = gsl_vector_view_array(product, matrix->size2);
  gsl_blas_dgemv(CblasNoTrans, 1.0, matrix, &view_vector.vector, 0.0, &view_product.vector);
}

/**
 * Allocate MatrixVectorProduct to calculate usual matrix vector product
 * @param[in]    size_row        Size of matrix rows
 * @param[in]    size_column     Size of matrix columns
 * @param[in]    data            A pointer of an array of matrix elements in row-major order
 */
MatrixVectorProduct *usual_matrix_vector_product_alloc (int size_row, int size_column, const double *data)
{
  MatrixVectorProduct *matrix_vector_product;
  DataContainer *matrix_data;
  gsl_matrix *matrix;
  matrix = gsl_matrix_alloc_set_elements(size_row, size_column, data);
  matrix_data = data_container_alloc(USUAL_MATRIX_VECTOR_PRODUCT_N, matrix, (void *(*) (void *matrix)) gsl_matrix_duplicate, (void (*) (void *matrix)) gsl_matrix_free);
  matrix_vector_product = matrix_vector_product_alloc(matrix_data, usual_matrix_vector_product_calculate);
  return matrix_vector_product;
}

static void jacobian_of_vector_field_product_calculate (double *av, DataContainer *matrix_data, const double *v)
{
  DerivativeOfSystem *derivative;
  double *pt;
  derivative = (DerivativeOfSystem *) data_container_data_ptr(matrix_data, JACOBIAN_VECTOR_PRODUCT_DERIVATIVE);
  pt = (double *) ((GArray *) data_container_data_ptr(matrix_data, JACOBIAN_VECTOR_PRODUCT_POINT))->data;
  derivative_of_system_jacobian_vector_product(av, derivative, pt, v);
}

/**
 * @param[in]    dynamical_system    Configured dynamical system that is freed together with JacobianVectorProduct
 * @param[in]    pt                  Coordinate of a point
 */
JacobianVectorProduct *jacobian_of_vector_field_product_alloc (DynamicalSystem *dynamical_system, const double *pt)
{
  MatrixVectorProduct *matrix_vector_product;
  DataContainer *matrix_data;
  DerivativeOfSystem *derivative;
  GArray *pt_dup;
  pt_dup = g_array_sized_new(FALSE, FALSE, sizeof(double), dynamical_system_dimension(dynamical_system));
  g_array_insert_vals(pt_dup, 0, pt, dynamical_system_dimension(dynamical_system));
  derivative = derivative_of_system_alloc(dynamical_system);
  matrix_data = data_container_alloc(JACOBIAN_VECTOR_PRODUCT_N, derivative, derivative_of_system_duplicate, derivative_of_system_free, pt_dup, g_array_duplicate, g_array_free_completely);
  matrix_vector_product = matrix_vector_product_alloc(matrix_data, jacobian_of_vector_field_product_calculate);
  return matrix_vector_product;
}

DerivativeOfSystem *jacobian_of_vector_field_product_derivative_of_system_ptr (JacobianVectorProduct *matrix_vector_product)
{
  DataContainer *data;
  data = matrix_vector_product_matrix_data_ptr(matrix_vector_product);
  return (DerivativeOfSystem *) data_container_data_ptr(data, JACOBIAN_VECTOR_PRODUCT_DERIVATIVE);
}

/**
 * Calculate (A - number_shift I) v
 */
static void inverse_matrix_vector_product_gmres_product (double *av, DataContainer *gmres_product_data, const double *v)
{
  double number_shift;
  number_shift = *(double *) data_container_data_ptr(gmres_product_data, INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_NUMBER_SHIFT);
  matrix_vector_product_calculate(av, data_container_data_ptr(gmres_product_data, INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_PRODUCT), v);
  if (number_shift != 0.0) {
    int i;
    uint dim;
    dim = *(uint *) data_container_data_ptr(gmres_product_data, INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_DIMENSION);
    for (i = 0; i < dim; i++) {
      av[i] = av[i] - number_shift * v[i];
    }
  }
}

static void inverse_matrix_vector_product_calculate (double *av, DataContainer *inverse_product_data, const double *v)
{
  GMRESSolver *gmres;
  int max_iteration;
  double *solution;
  gmres = (GMRESSolver *) data_container_data_ptr(inverse_product_data, INVERSE_MATRIX_VECTOR_PRODUCT_DATA_GMRES);
  max_iteration = *(int *) data_container_data_ptr(inverse_product_data, INVERSE_MATRIX_VECTOR_PRODUCT_DATA_MAX_ITERATION);
  gmres_solver_set(gmres, v, v); /* No reason why initial value of GMRES is v */
  solution = gmres_solver_iterate_successively(gmres, max_iteration);
  if (!solution) {
    fprintf(stderr, "GMRES method does not converge when inverse iteration is calculated\n");
    abort();
  }
  memcpy(av, solution, gmres_solver_dimension(gmres) * sizeof(double));
  g_free(solution);
}

/**
 * Calculate product of (A - number_shift I)^{-1}, when Av for some vector v is calculated by matrix_vector_product
 * @param[in]    dimension                Number of dimension
 * @param[in]    error_residual_ratio     Error of residual ratio of GMRES method
 * @param[in]    gmres_max_iteration      Maximum number of iterations of GMRES method
 * @param[in]    matrix_vector_product    Product definition that is freed automatically
 * @param[in]    number_shift             Shift number of inverse iterations
 */
InverseMatrixVectorProduct *inverse_matrix_vector_product_alloc (uint dimension, double error_residual_ratio, int gmres_max_iteration, MatrixVectorProduct *matrix_vector_product, double number_shift)
{
  MatrixVectorProduct *inverse_matrix_vector_product, *gmres_matrix_vector_product;
  DataContainer *inverse_product_data, *gmres_product_data;
  GMRESSolver *gmres;
  double *ptr_number_shift;
  int *ptr_max_iteration;
  uint *ptr_dim;
  ptr_number_shift = (double *) g_malloc(sizeof(double));
  *ptr_number_shift = number_shift;
  ptr_max_iteration = (int *) g_malloc(sizeof(int));
  *ptr_max_iteration = gmres_max_iteration;
  ptr_dim = (uint *) g_malloc(sizeof(uint));
  *ptr_dim = dimension;
  gmres_product_data = data_container_alloc(INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_N, matrix_vector_product, matrix_vector_product_duplicate, matrix_vector_product_free, ptr_number_shift, data_container_utils_double_duplicate, g_free, ptr_dim, data_container_utils_int_duplicate, g_free);
  gmres_matrix_vector_product = matrix_vector_product_alloc(gmres_product_data, inverse_matrix_vector_product_gmres_product);
  gmres = gmres_solver_alloc(dimension, error_residual_ratio, gmres_matrix_vector_product);
  inverse_product_data = data_container_alloc(INVERSE_MATRIX_VECTOR_PRODUCT_DATA_N, gmres, (void *(*) (void *data)) gmres_solver_duplicate, gmres_solver_free, ptr_max_iteration, data_container_utils_int_duplicate, g_free);
  inverse_matrix_vector_product = matrix_vector_product_alloc(inverse_product_data, inverse_matrix_vector_product_calculate);
  return inverse_matrix_vector_product;
}

GMRESSolver *inverse_matrix_vector_product_gmres_ptr (InverseMatrixVectorProduct *matrix_vector_product)
{
  DataContainer *matrix_data;
  matrix_data = matrix_vector_product_matrix_data_ptr(matrix_vector_product);
  return data_container_data_ptr(matrix_data, INVERSE_MATRIX_VECTOR_PRODUCT_DATA_GMRES);
}

double inverse_matrix_vector_product_number_shift (InverseMatrixVectorProduct *matrix_vector_product)
{
  DataContainer *matrix_data, *gmres_data;
  matrix_data = matrix_vector_product_matrix_data_ptr(matrix_vector_product);
  gmres_data = data_container_data_ptr(matrix_data, INVERSE_MATRIX_VECTOR_PRODUCT_DATA_GMRES);
  return *(double *) data_container_data_ptr(gmres_data, INVERSE_MATRIX_VECTOR_PRODUCT_GMRES_DATA_NUMBER_SHIFT);
}

/** @} */  /* End of MatrixVectorProduct */
