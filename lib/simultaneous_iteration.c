/**
 * @file simultaneous_iteration.c
 * @brief Functions of simultaneous iteration method
 */

#include "header.h"

/**
 * \addtogroup SimultaneousIteration
 * @{
 */

SimultaneousIteration *simultaneous_iteration_alloc (uint dimension, uint basis_number, MatrixVectorProduct *product)
{
  SimultaneousIteration *simultaneous_iteration;
  if (dimension <= 0) {
    fprintf(stderr, "Dimension of SimultaneousIteration must be positive: %d\n", dimension);
    abort();
  }
  if (basis_number <= 0) {
    fprintf(stderr, "Number of bases of SimultaneousIteration must be positive: %d\n", basis_number);
    abort();
  }
  if (dimension < basis_number) {
    fprintf(stderr, "Number of bases of SimultaneousIteration must be less than or equal to dimension: basis_number=%d, dimension=%d\n", basis_number, dimension);
    abort();
  }
  simultaneous_iteration = (SimultaneousIteration *) g_malloc(sizeof(SimultaneousIteration));
  simultaneous_iteration->dimension = dimension;
  simultaneous_iteration->basis_number = basis_number;
  simultaneous_iteration->product = product;
  simultaneous_iteration->qr = gsl_matrix_alloc(simultaneous_iteration->dimension, simultaneous_iteration->basis_number);
  simultaneous_iteration->q = gsl_matrix_alloc(simultaneous_iteration->dimension, simultaneous_iteration->basis_number);
  simultaneous_iteration->r = gsl_matrix_alloc(simultaneous_iteration->basis_number, simultaneous_iteration->basis_number);
  return simultaneous_iteration;
}

void simultaneous_iteration_free (SimultaneousIteration *simultaneous_iteration)
{
  if (simultaneous_iteration->product) {
    matrix_vector_product_free(simultaneous_iteration->product);
  }
  if (simultaneous_iteration->qr) {
    gsl_matrix_free(simultaneous_iteration->qr);
  }
  if (simultaneous_iteration->q) {
    gsl_matrix_free(simultaneous_iteration->q);
  }
  if (simultaneous_iteration->r) {
    gsl_matrix_free(simultaneous_iteration->r);
  }
  g_free(simultaneous_iteration);
}

/**
 * Set a matrix to simultaneous_iteration->qr and set also simultaneous_iteration->q and simultaneous_iteration->r
 * by decomposing simultaneous_iteration->qr.
 */
void simultaneous_iteration_set_qr (SimultaneousIteration *simultaneous_iteration, const double *qr)
{
  gsl_matrix_set_elements(simultaneous_iteration->qr, qr);
  simultaneous_iteration_decompose(simultaneous_iteration);
}


/**
 * Set simultaneous_iteration->qr randomly and set also simultaneous_iteration->q and simultaneous_iteration->r
 * by decomposing simultaneous_iteration->qr.
 */
void simultaneous_iteration_set_qr_randomly (SimultaneousIteration *simultaneous_iteration)
{
  gsl_matrix_set_random_normalized_orthogonal_columns(simultaneous_iteration->qr);
  simultaneous_iteration_decompose(simultaneous_iteration);
}

/**
 * Update simultaneous_iteration->q and simultaneous_iteration->r from simultaneous_iteration->qr
 */
void simultaneous_iteration_decompose (SimultaneousIteration *simultaneous_iteration)
{
  gsl_wrap_qr_decomp(simultaneous_iteration->q, simultaneous_iteration->r, simultaneous_iteration->qr);
}

/**
 * Update simultaneous_iteration->qr from simultaneous_iteration->q by simultaneous_iteration->product
 */
void simultaneous_iteration_product (SimultaneousIteration *simultaneous_iteration)
{
  if (simultaneous_iteration->product) {
    int i;
    gsl_vector *v1, *v2;
    v1 = gsl_vector_alloc(simultaneous_iteration->dimension);
    v2 = gsl_vector_alloc(simultaneous_iteration->dimension);
    for (i = 0; i < simultaneous_iteration->basis_number; i++) {
      gsl_matrix_get_col(v1, simultaneous_iteration->q, i);
      matrix_vector_product_calculate(v2->data, simultaneous_iteration->product, v1->data);
      gsl_matrix_set_col(simultaneous_iteration->qr, i, v2);
    }
    gsl_vector_free(v1);
    gsl_vector_free(v2);
  } else {
    fprintf(stderr, "Product is not defined\n");
    abort();
  }
}

void simultaneous_iteration_iterate_successively (SimultaneousIteration *simultaneous_iteration, int number_iteration)
{
  int i;
  for (i = 0; i < number_iteration; i++) {
    simultaneous_iteration_product(simultaneous_iteration);
    simultaneous_iteration_decompose(simultaneous_iteration);
  }
}

/**
 * Return an array of real eigenvalues, which is equal to diagonal elements of simultaneous_iteration->r.
 * Because this function deal with real numbers, complex eigenvalues can not be obtained.
 * @param[out]    eigenvector_max    A pointer to store eigenvector of eigenvalue of maximum.
 *                                   Necessary memory is allocated to *eigenvector_max.
 *                                   If we want to ignore the eigenvector
 *                                   absolute value then we set NULL to eigenvector_max
 * @param[in]    simultaneous_iteration    Settings of simultaneous iteration
 * @return    A pointer of an array whose size is simultaneous_iteration->basis_number
 */
double *simultaneous_iteration_eigenvalues (double **eigenvector_max, SimultaneousIteration *simultaneous_iteration)
{
  double *eigenvalues;
  int i;
  eigenvalues = (double *) g_malloc(sizeof(double) * simultaneous_iteration->basis_number);
  for (i = 0; i < simultaneous_iteration->basis_number; i++) {
    eigenvalues[i] = gsl_matrix_get(simultaneous_iteration->r, i, i);
  }
  if (eigenvector_max) {
    gsl_vector_view col;
    *eigenvector_max = (double *) g_malloc(sizeof(double) * simultaneous_iteration->basis_number * simultaneous_iteration->basis_number);
    col = gsl_matrix_column(simultaneous_iteration->q, 0);
    gsl_vector_copy_components(*eigenvector_max, &col.vector);
  }
  return eigenvalues;
}

/** @} */  /* End of SimultaneousIteration */
