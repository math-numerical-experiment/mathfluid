/**
 * @file dynamical_system_ode.c
 * @brief Functions to allocate DynamicalSystem with SystemODESolver
 */

enum {
  DYNAMICAL_SYSTEM_ODE_DATA_CONTAINER,
  DYNAMICAL_SYSTEM_ODE_DATA_N
};

#include "header.h"

/**
 * \addtogroup DynamicalSystem
 * @{
 */

static gboolean dynamical_system_ode_evolve (SystemState *state, DataContainer *data_container, const double t1)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, 0);
  return system_ode_solver_evolve(state, container->solver, t1);
}

static gboolean dynamical_system_ode_time_derivative (double *vec, DataContainer *data_container, const double *x)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, 0);
  return system_ode_time_derivative(vec, container->system, x);
}

static gboolean dynamical_system_ode_jacobian_of_time_derivative (double *jac, DataContainer *data_container, const double *x)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, 0);
  return system_ode_jacobian_of_time_derivative(jac, container->system, x);
}

static void dynamical_system_ode_container_free (void *data)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data;
  if (container->system_free) {
    system_ode_free(container->system);
  }
  if (container->solver_free) {
    system_ode_solver_free(container->solver);
  }
  g_free(container);
}

static SystemODEContainer *system_ode_container_alloc (SystemODESolver *solver, gboolean system_free, gboolean solver_free)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) g_malloc(sizeof(SystemODEContainer));
  container->system = system_ode_solver_system(solver);
  container->solver = solver;
  container->system_free = system_free;
  container->solver_free = solver_free;
  return container;
}

static void *dynamical_system_ode_container_copy (void *data)
{
  SystemODEContainer *container, *container_new;
  SystemODESolver *solver_new;
  container = (SystemODEContainer *) data;
  solver_new = system_ode_solver_duplicate(container->solver);
  container_new = system_ode_container_alloc(solver_new, container->system_free, container->solver_free);
  return (void *) container_new;
}


static gboolean dynamical_system_ode_parameter_derivative_of_time_derivative (double *vec, DataContainer *data_container, const char *parameter_key, const double *x)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, 0);
  return system_ode_parameter_derivative_of_time_derivative(vec, container->system, parameter_key, x);
}

static void dynamical_system_ode_parameter_set (DataContainer *data_container, const char *key, GValue *val)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, 0);
  system_ode_parameter_set(container->system, key, val);
}

static void dynamical_system_ode_parameter_get (GValue *val, DataContainer *data_container, const char *key)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(data_container, DYNAMICAL_SYSTEM_ODE_DATA_CONTAINER);
  system_ode_parameter_get(val, container->system, key);
}

/**
 * @param[in]    solver         SystemODESolver to evolve solutions
 * @param[in]    system_free    Set TRUE if we want to call system_ode_free for
 *                              SystemODE *system together with the parent DynamicalSystem structure.
 * @param[in]    solver_free    Set TRUE if we want to call system_ode_solver_free for
 *                              SystemODESolver *solver together with the parent DynamicalSystem structure.
 */
DynamicalSystem *dynamical_system_ode_alloc (SystemODESolver *solver, gboolean system_free, gboolean solver_free)
{
  gboolean (* func_jacobian) (double *dfdy, DataContainer *data_container, const double *x);
  gboolean (* func_parameter_derivative) (double *vec, DataContainer *data_container, const char *parameter_key, const double *x);
  DataContainer *data_container;
  SystemODEContainer *container;
  DynamicalSystem *dynamical_system;
  container = system_ode_container_alloc(solver, system_free, solver_free);
  data_container = data_container_alloc(DYNAMICAL_SYSTEM_ODE_DATA_N, (void *) container, dynamical_system_ode_container_copy, dynamical_system_ode_container_free);
  if (system_ode_time_jacobian_of_time_derivative_defined_p(system_ode_solver_system(solver))) {
    func_jacobian = dynamical_system_ode_jacobian_of_time_derivative;
  } else {
    func_jacobian = NULL;
  }
  if (system_ode_parameter_derivative_of_time_derivative_defined_p(system_ode_solver_system(solver))) {
    func_parameter_derivative = dynamical_system_ode_parameter_derivative_of_time_derivative;
  } else {
    func_parameter_derivative = NULL;
  }
  dynamical_system = dynamical_system_alloc(system_ode_solver_system(solver)->dimension, data_container, dynamical_system_ode_evolve);
  dynamical_system_set_time_derivative(dynamical_system, dynamical_system_ode_time_derivative);
  dynamical_system_set_jacobian_of_time_derivative(dynamical_system, func_jacobian);
  dynamical_system_set_parameter_derivative_of_time_derivative(dynamical_system, func_parameter_derivative);
  dynamical_system_set_parameter_accessor(dynamical_system, dynamical_system_ode_parameter_set, dynamical_system_ode_parameter_get);
  return dynamical_system;
}

/**
 * @param[in]    dynamical_system    Pointer of DynamicalSystem allocated by dynamical_system_ode_alloc
 */
SystemODE *dynamical_system_ode_system_ode (DynamicalSystem *dynamical_system)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, DYNAMICAL_SYSTEM_ODE_DATA_CONTAINER);
  return container->system;
}

/**
 * @param[in]    dynamical_system    Pointer of DynamicalSystem allocated by dynamical_system_ode_alloc
 */
SystemODESolver *dynamical_system_ode_system_ode_solver (DynamicalSystem *dynamical_system)
{
  SystemODEContainer *container;
  container = (SystemODEContainer *) data_container_data_ptr(dynamical_system->data_container, DYNAMICAL_SYSTEM_ODE_DATA_CONTAINER);
  return container->solver;
}

/** @} */  /* End of DynamicalSystem */
