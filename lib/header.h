#ifndef _HEADER_H_
#define _HEADER_H_

#include "../config.h"
#include "mathfluid.h"
#include "lyapunov_matrix_store.h"
#include "lyapunov_evolve.h"
#include "parameter_continuation_gmres.h"

#define parameter_continuation_gmres_setting_dimension(setting) ((gsl_vector *) data_container_data_ptr(setting, PARAMETER_CONTINUATION_GMRES_PT_WITH_PARAMETER))->size

#endif /* _HEADER_H_ */
