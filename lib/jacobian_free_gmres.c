/**
 * @file jacobian_free_gmres.c
 * @brief Functions to execute Jacobian free GMRES method for DynamicalSystem
 */

#include "header.h"

enum {
  PRODUCT_CONTAINER_GMRES,
  PRODUCT_CONTAINER_N
};

/**
 * \addtogroup JacobianFreeGMRES
 * @{
 * The usage of JacobianFreeGMRES is the following.
 * @code
 * jf_gmres = jacobian_free_gmres_alloc(...);
 * solution = jacobian_free_gmres_solve(jf_gmres, ...);
 * jacobian_free_gmres_free(jf_gmres);
 * @endcode
 */

static void jacobian_free_gmres_product_of_mat_vec (double *av, DataContainer *matrix_data, const double *v)
{
  JacobianFreeGMRES *jacobian_free_gmres;
  jacobian_free_gmres = (JacobianFreeGMRES *) data_container_data_ptr(matrix_data, PRODUCT_CONTAINER_GMRES);
  jacobian_free_gmres->product(av, jacobian_free_gmres, v);
}

/**
 * @param[in]    dynamical_system    Settings of dynamical system
 * @param[in]    dimension           Dimension of linear equation
 * @param[in]    data                Arbitrary data to calculate product of matrix and vector
 * @param[in]    product             Function to calculate product of matrix and vector
 */
JacobianFreeGMRES *jacobian_free_gmres_alloc (DynamicalSystem *dynamical_system, int dimension, void *data, void (* product) (double *av, JacobianFreeGMRES *jacobian_free_gmres, const double *v))
{
  JacobianFreeGMRES *jacobian_free_gmres;
  MatrixVectorProduct *matrix_vector_product;
  jacobian_free_gmres = (JacobianFreeGMRES *) g_malloc(sizeof(JacobianFreeGMRES));
  jacobian_free_gmres->dynamical_system = dynamical_system;
  jacobian_free_gmres->data = data;
  jacobian_free_gmres->product = product;
  matrix_vector_product = matrix_vector_product_alloc(data_container_alloc(PRODUCT_CONTAINER_N, jacobian_free_gmres, NULL, NULL), jacobian_free_gmres_product_of_mat_vec);
  jacobian_free_gmres->gmres_solver = gmres_solver_alloc(dimension, JACOBIAN_FREE_GMRES_DEFAULT_ERROR_RESIDUAL_RATIO, matrix_vector_product);
  jacobian_free_gmres->gmres_max_iteration = gmres_solver_dimension(jacobian_free_gmres->gmres_solver) * 100; /* Default value is sufficiently large integer */
  jacobian_free_gmres->derivative = derivative_of_system_alloc(jacobian_free_gmres->dynamical_system);
  jacobian_free_gmres->ignore_gmres_convergence = FALSE;
  return jacobian_free_gmres;
}

/**
 * Note that jacobian_free_gmres->dynamical_system is freed manually
 * because jacobian_free_gmres_free does not free it.
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 */
void jacobian_free_gmres_free (JacobianFreeGMRES *jacobian_free_gmres)
{
  if (jacobian_free_gmres->gmres_solver) {
    gmres_solver_free(jacobian_free_gmres->gmres_solver);
  }
  if (jacobian_free_gmres->derivative) {
    derivative_of_system_free(jacobian_free_gmres->derivative);
  }
  g_free(jacobian_free_gmres);
}

/**
 * Set maximum number of iterations of each step of GMRES method
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    max_iteration          Maximum number of iterations of GMRES method
 */
void jacobian_free_gmres_set_max_iteration (JacobianFreeGMRES *jacobian_free_gmres, int max_iteration)
{
  jacobian_free_gmres->gmres_max_iteration = max_iteration;
}

/**
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    ignore    If the value is TRUE then jacobian_free_gmres_solve always returns solution,
 *                         that is, it returns solution that is not converged completely
 *                         when GMRES method fails to converge.
 */
void jacobian_free_gmres_set_ignore_gmres_convergence (JacobianFreeGMRES *jacobian_free_gmres, gboolean ignore)
{
  jacobian_free_gmres->ignore_gmres_convergence = ignore;
}

/**
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    x0    Initial solution
 * @param[in]    b     Set b of some linear equation Ax = b
 */
double *jacobian_free_gmres_solve (JacobianFreeGMRES *jacobian_free_gmres, const double *x0, const double *b)
{
  double *x;
  gmres_solver_set(jacobian_free_gmres->gmres_solver, x0, b);
  x = gmres_solver_iterate_successively(jacobian_free_gmres->gmres_solver, jacobian_free_gmres->gmres_max_iteration);
  if (!x && jacobian_free_gmres->ignore_gmres_convergence) {
    x = gmres_solver_solution(jacobian_free_gmres->gmres_solver);
  }
  return x;
}

/**
 * Calculate product of Jacobian of vector field at pt and a vector
 * according to JacobianFreeGMRES::derivative.
 * @param[out]   av    A pointer of array to store result of product
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    pt    A point at which Jacobian of vector field is a matrix of matrix-vector product
 * @param[in]    v     A vector of matrix-vector product
 */
void jacobian_free_gmres_product_jacobian_vector (double *av, JacobianFreeGMRES *jacobian_free_gmres, const double *pt, const double *v)
{
  derivative_of_system_jacobian_vector_product(av, jacobian_free_gmres->derivative, pt, v);
}

/**
 * @param[out]   time_derivative    A pointer of array to store result of time-derivative,
 *                                  that is, vector field of dynamiacl system
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    x    A point at which time-derivative is to be calculated
 */
void jacobian_free_gmres_time_derivative (double *time_derivative, JacobianFreeGMRES *jacobian_free_gmres, const double *x)
{
  derivative_of_system_time_derivative(time_derivative, jacobian_free_gmres->derivative, x);
}

/**
 * @param[out]   parameter_derivative   A pointer of array to store result of parameter-derivative
 * @param[in]    jacobian_free_gmres    Settings of Jacobian-free GMRES method
 * @param[in]    parameter_key          A key of parameter name
 * @param[in]    x    A point at which time-derivative is to be calculated
 */
void jacobian_free_gmres_parameter_derivative (double *parameter_derivative, JacobianFreeGMRES *jacobian_free_gmres, const char* parameter_key, const double *x)
{
  derivative_of_system_parameter_derivative(parameter_derivative, jacobian_free_gmres->derivative, parameter_key, x);
}

/** @} */  /* End of JacobianFreeGMRES */
