/**
 * @file jacobian_free_newton_gmres.c
 * @brief Functions to execute Jacobian free Newton-GMRES method
 */

#include "header.h"

/**
 * \addtogroup JacobianFreeNewtonGMRES
 * @{
 * Initialization of JacobianFreeNewtonGMRES is the following.
 * @code
 * newton_gmres = jacobian_free_newton_gmres_alloc(...);
 * jacobian_free_newton_gmres_set_initial_solution(newton_gmres, ...);
 * ...
 * jacobian_free_newton_gmres_free(newton_gmres);
 * @Endcode
 */

enum {
  JACOBIAN_FREE_NEWTON_GMRES_LOG_INITIAL_VALUE,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE_NORM,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR_NORM,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_RESULT,
  JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE,
  JACOBIAN_FREE_NEWTON_GMRES_N_LOGS,
};

static void jacobian_free_newton_gmres_log_initial_value (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_time_derivative_norm (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_time_derivative (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_error_vector_norm (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_error_vector (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_result (FILE *out, va_list args);
static void jacobian_free_newton_gmres_log_iterate_successively_convergence (FILE *out, va_list args);

static MathFluidLogFunc jacobian_free_newton_gmres_log_function[JACOBIAN_FREE_NEWTON_GMRES_N_LOGS] = {
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_INITIAL_VALUE, MATH_FLUID_LOG_DEBUG, jacobian_free_newton_gmres_log_initial_value },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE_NORM, MATH_FLUID_LOG_INFO, jacobian_free_newton_gmres_log_iterate_time_derivative_norm },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE, MATH_FLUID_LOG_DEBUG, jacobian_free_newton_gmres_log_iterate_time_derivative },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR_NORM, MATH_FLUID_LOG_INFO, jacobian_free_newton_gmres_log_iterate_error_vector_norm },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR, MATH_FLUID_LOG_DEBUG, jacobian_free_newton_gmres_log_iterate_error_vector },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_RESULT, MATH_FLUID_LOG_DEBUG, jacobian_free_newton_gmres_log_iterate_result },
  { JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, MATH_FLUID_LOG_INFO, jacobian_free_newton_gmres_log_iterate_successively_convergence }
};

static void jacobian_free_newton_gmres_log_print_one_vector_at_iteration (FILE *out, va_list args, const char *vector_name)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  double *x;
  newton_gmres = va_arg(args, JacobianFreeNewtonGMRES *);
  x = va_arg(args, double *);
  if (x) {
    fprintf(out, "JacobianFreeNewtonGMRES/iterate %d/%s: ", newton_gmres->iterate, vector_name);
    mathfluid_utils_array_of_double_fprintf(out, jacobian_free_newton_gmres_dimension(newton_gmres), x, "%.14lf", " ");
    fprintf(out, "\n");
  } else {
    fprintf(out, "JacobianFreeNewtonGMRES/iterate %d/%s: NULL\n", newton_gmres->iterate, vector_name);
  }
}

static double jacobian_free_newton_gmres_calculate_norm (JacobianFreeNewtonGMRES *newton_gmres, double *coordinate)
{
  if (newton_gmres->norm_function == NULL) {
    fprintf(stderr, "Function to calculate norm is NULL\n");
    abort();
  }
  return newton_gmres->norm_function(jacobian_free_newton_gmres_dimension(newton_gmres), coordinate, newton_gmres->norm_args);
}

static void jacobian_free_newton_gmres_log_print_one_vector_norm_at_iteration (FILE *out, va_list args, const char *vector_name)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  double *x, norm;
  newton_gmres = va_arg(args, JacobianFreeNewtonGMRES *);
  x = va_arg(args, double *);
  if (x) {
    norm = jacobian_free_newton_gmres_calculate_norm(newton_gmres, x);
    fprintf(out, "JacobianFreeNewtonGMRES/iterate %d/norm of %s: %.14le\n", newton_gmres->iterate, vector_name, norm);
  } else {
    fprintf(out, "JacobianFreeNewtonGMRES/iterate %d/norm of %s: Can not calculate\n", newton_gmres->iterate, vector_name);
  }
}

static void jacobian_free_newton_gmres_log_initial_value (FILE *out, va_list args)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  newton_gmres = va_arg(args, JacobianFreeNewtonGMRES *);
  fprintf(out, "JacobianFreeNewtonGMRES/initial value: ");
  mathfluid_utils_array_of_double_fprintf(out, jacobian_free_newton_gmres_dimension(newton_gmres), newton_gmres->vector->data, "%.14lf", " ");
  fprintf(out, "\n");
}

static void jacobian_free_newton_gmres_log_iterate_time_derivative_norm (FILE *out, va_list args)
{
  jacobian_free_newton_gmres_log_print_one_vector_norm_at_iteration(out, args, "tangent vector");
}

static void jacobian_free_newton_gmres_log_iterate_time_derivative (FILE *out, va_list args)
{
  jacobian_free_newton_gmres_log_print_one_vector_at_iteration(out, args, "tangent vector");
}

static void jacobian_free_newton_gmres_log_iterate_error_vector_norm (FILE *out, va_list args)
{
  jacobian_free_newton_gmres_log_print_one_vector_norm_at_iteration(out, args, "modified vector");
}

static void jacobian_free_newton_gmres_log_iterate_error_vector (FILE *out, va_list args)
{
  jacobian_free_newton_gmres_log_print_one_vector_at_iteration(out, args, "modified vector");
}

static void jacobian_free_newton_gmres_log_iterate_result (FILE *out, va_list args)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  newton_gmres = va_arg(args, JacobianFreeNewtonGMRES *);
  fprintf(out, "JacobianFreeNewtonGMRES/iterate %d/result: ", newton_gmres->iterate);
  mathfluid_utils_array_of_double_fprintf(out, jacobian_free_newton_gmres_dimension(newton_gmres), newton_gmres->vector->data, "%.14lf", " ");
  fprintf(out, "\n");
}

static void jacobian_free_newton_gmres_log_iterate_successively_convergence (FILE *out, va_list args)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  newton_gmres = va_arg(args, JacobianFreeNewtonGMRES *);
  fprintf(out, "JacobianFreeNewtonGMRES/iterate successively/convergent: %.14le\n", newton_gmres->error_cache);
}

/**
 * Allocate memory of JacobianFreeNewtonGMRES
 * @param[in]    gmres      Settings of GMRES
 * @param[in]    function   Function whose roots are searched for
 * @param[in]    damping_parameter    Damping parameter of Newton method
 * @param[in]    max_error  Maximum error of Newton method. If Euclid norm of a modified vector is less than the value then Newton method stops.
 */
JacobianFreeNewtonGMRES *jacobian_free_newton_gmres_alloc (JacobianFreeGMRES *gmres, void (* function) (double *value, JacobianFreeGMRES *gmres, const double *pt), int damping_parameter, double max_error)
{
  JacobianFreeNewtonGMRES *newton_gmres;
  newton_gmres = (JacobianFreeNewtonGMRES *) g_malloc(sizeof(JacobianFreeNewtonGMRES));
  newton_gmres->gmres = gmres;
  newton_gmres->function = function;
  newton_gmres->after_iteration = NULL;
  newton_gmres->norm_function = mathfluid_norm_euclid;
  newton_gmres->norm_args = NULL;
  newton_gmres->vector = gsl_vector_alloc(jacobian_free_newton_gmres_dimension(newton_gmres));
  newton_gmres->vector_last = gsl_vector_alloc(jacobian_free_newton_gmres_dimension(newton_gmres));
  newton_gmres->vector_diff = gsl_vector_alloc(jacobian_free_newton_gmres_dimension(newton_gmres));
  newton_gmres->vector_zero = gsl_vector_alloc(jacobian_free_newton_gmres_dimension(newton_gmres));
  newton_gmres->vector_b = gsl_vector_alloc(jacobian_free_newton_gmres_dimension(newton_gmres));
  newton_gmres->vector_b_updated = FALSE;
  /* newton_gmres->vector_zero is used as an initial value of GMRES method */
  gsl_vector_set_zero(newton_gmres->vector_zero);
  /* -1 means that the initial newton_gmres->vector is not set yet. */
  newton_gmres->iterate = -1;
  newton_gmres->max_error = max_error;
  newton_gmres->practical_error = 0.0;
  newton_gmres->max_uniform_norm = 0.0;
  newton_gmres->logger = mathfluid_logger_alloc(stdout, JACOBIAN_FREE_NEWTON_GMRES_N_LOGS, jacobian_free_newton_gmres_log_function);
  jacobian_free_newton_gmres_set_damping_parameter(newton_gmres, damping_parameter);
  return newton_gmres;
}

/**
 * Free memory of JacobianFreeNewtonGMRES.
 * Note that you should free manually newton_gmres->gmres because newton_gmres->gmres is not freed.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 */
void jacobian_free_newton_gmres_free (JacobianFreeNewtonGMRES *newton_gmres)
{
  if (newton_gmres->vector) {
    gsl_vector_free(newton_gmres->vector);
  }
  if (newton_gmres->vector_last) {
    gsl_vector_free(newton_gmres->vector_last);
  }
  if (newton_gmres->vector_diff) {
    gsl_vector_free(newton_gmres->vector_diff);
  }
  if (newton_gmres->vector_zero) {
    gsl_vector_free(newton_gmres->vector_zero);
  }
  if (newton_gmres->vector_b) {
    gsl_vector_free(newton_gmres->vector_b);
  }
  if (newton_gmres->logger) {
    mathfluid_logger_free(newton_gmres->logger);
  }
  g_free(newton_gmres);
}

/**
 * Set function that is called the last of jacobian_free_newton_gmres_iterate.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    after_iteration    Function called at the last of iteration
 */
void jacobian_free_newton_gmres_set_function_after_iteration (JacobianFreeNewtonGMRES *newton_gmres, void (* after_iteration) (JacobianFreeNewtonGMRES *newton_gmres))
{
  newton_gmres->after_iteration = after_iteration;
}

/**
 * Set log levels of JacobianFreeNewtonGMRES
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    level    Log level of Newton-GMRES method
 */
void jacobian_free_newton_gmres_set_log_level (JacobianFreeNewtonGMRES *newton_gmres, MathFluidLogLevel level)
{
  mathfluid_logger_set_log_level(newton_gmres->logger, level);
}

/**
 * Set function to calculate norm
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    func    Function to calculate norm
 * @param[in]    norm_args    Additional arguments of the function func
 */
void jacobian_free_newton_gmres_set_norm_function (JacobianFreeNewtonGMRES *newton_gmres, MathFluidNormFunc *func, void *norm_args)
{
  newton_gmres->norm_function = func;
  newton_gmres->norm_args = norm_args;
}

/**
 * Set maximum error of Newton method.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    max_error       Maximum error to test convergence of Newton method
 */
void jacobian_free_newton_gmres_set_max_error (JacobianFreeNewtonGMRES *newton_gmres, double max_error)
{
  newton_gmres->max_error = max_error;
}

/**
 * Set practical error. For meanings of practical error,
 * see documents of jacobian_free_newton_gmres_converged_p.
 * If we do not use practical error, set 0.0. The default value is 0.0.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    practical_error    Maximum norm of value of function at current solution
 */
void jacobian_free_newton_gmres_set_practical_error (JacobianFreeNewtonGMRES *newton_gmres, double practical_error)
{
  newton_gmres->practical_error = practical_error;
}

/**
 * Set maximum uniform norm of solution.
 * If we do not test maximum uniform norm, then we set 0.0. The default value is 0.0.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    practical_error    Maximum uniform norm of solution
 */
void jacobian_free_newton_gmres_set_max_uniform_norm (JacobianFreeNewtonGMRES *newton_gmres, double max_uniform_norm)
{
  newton_gmres->max_uniform_norm = max_uniform_norm;
}

/**
 * Set damping parameter of Newton method.
 * When damping parameter is d, 2^{-d} is used as damping coefficient.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    damping_parameter    Damping parameter that is positive integer
 */
void jacobian_free_newton_gmres_set_damping_parameter (JacobianFreeNewtonGMRES *newton_gmres, int damping_parameter)
{
  newton_gmres->damping_parameter = damping_parameter;
  newton_gmres->damping_coefficient = pow(2.0, -newton_gmres->damping_parameter);
}

/**
 * Copy current solution to an array of double.
 * @param[out]   vector    A point of an array of double whose dimension must be same as the dimension of newton_gmres
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @return FALSE if the newton_gmres->vector is not set yet. Otherwise, copy of solution succeeds and TRUE is returned.
 */
gboolean jacobian_free_newton_gmres_solution_copy (double *vector, JacobianFreeNewtonGMRES *newton_gmres)
{
  if (newton_gmres->iterate < 0) {
    return FALSE;
  }
  gsl_vector_copy_components(vector, newton_gmres->vector);
  return TRUE;
}

/**
 * Allocate memory of a vector and copy current solution to the memroy space.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @return       A pointer of an allocated array of double whose size is the dimension of newton_gmres.
 */
double *jacobian_free_newton_gmres_solution (JacobianFreeNewtonGMRES *newton_gmres)
{
  if (newton_gmres->iterate < 0) {
    return NULL;
  }
  return gsl_vector_alloc_copy_components(newton_gmres->vector);
}

/**
 * Set initial vector of Newton method.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @param[in]    vector    A point of an array of double whose dimension must be same as the dimension of newton_gmres
 */
void jacobian_free_newton_gmres_set_initial_solution (JacobianFreeNewtonGMRES *newton_gmres, const double *vector)
{
  newton_gmres->iterate = 0;
  memcpy(newton_gmres->vector->data, vector, sizeof(double) * jacobian_free_newton_gmres_dimension(newton_gmres));
  gsl_vector_set_zero(newton_gmres->vector_last);
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_INITIAL_VALUE, newton_gmres);
  newton_gmres->vector_b_updated = FALSE;
}

/**
 * Return current error. Note that the value is stored to newton_gmres->error_cache.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @return    Current error that is norm of difference of current solution and last solution.
 */
double jacobian_free_newton_gmres_current_error (JacobianFreeNewtonGMRES *newton_gmres)
{
  if (newton_gmres->iterate < 0) {
    fprintf(stderr, "Initial value is not set yet\n");
    abort();
  }
  gsl_vector_memcpy(newton_gmres->vector_diff, newton_gmres->vector_last);
  gsl_vector_sub(newton_gmres->vector_diff, newton_gmres->vector);
  newton_gmres->error_cache = jacobian_free_newton_gmres_calculate_norm(newton_gmres, newton_gmres->vector_diff->data);
  return newton_gmres->error_cache;
}

static void jacobian_free_gmres_update_vector_b (JacobianFreeNewtonGMRES *newton_gmres)
{
  if (!newton_gmres->vector_b_updated) {
    newton_gmres->function(newton_gmres->vector_b->data, newton_gmres->gmres, newton_gmres->vector->data);
    newton_gmres->vector_b_updated = TRUE;
  }
}

/**
 * Test convergence of Newton method. By default, this function test whether Euclid norm of the modified vector
 * is less than newton_gmres->max_error.
 * We can change the function to calculate norms by jacobian_free_newton_gmres_set_norm_function.
 * If newton_gmres->practical_error is a positive number then
 * this function also test whether the absolute value of the target function
 * is less than newton_gmres->practical_error.
 * Note that the value is stored to newton_gmres->error_cache.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @return    TRUE if the differences of current equilibrium and last one is
 *            less than JacobianFreeNewtonGMRES::error. Otherwise, FALSE.
 *            If the norm of newton_gmres->vector_b is larger than
 *            newton_gmres->practical_error or no iteration is executed
 *            then we also obtain FALSE.
 */
gboolean jacobian_free_newton_gmres_converged_p (JacobianFreeNewtonGMRES *newton_gmres)
{
  if (newton_gmres->iterate <= 0) {
    return FALSE;
  }
  if (newton_gmres->practical_error > 0.0) {
    jacobian_free_gmres_update_vector_b(newton_gmres);
    if (jacobian_free_newton_gmres_calculate_norm(newton_gmres, newton_gmres->vector_b->data) >= newton_gmres->practical_error) {
      return FALSE;
    }
  }
  /*
    - It might be better to check newton_gmres->gmres_solver->error.
    - It may be better to test ratio of norms of resultant vector and modified vector.
  */
  return (jacobian_free_newton_gmres_current_error(newton_gmres) < newton_gmres->max_error ? TRUE : FALSE);
}

/**
 * Run a step of Newton method.
 * @param[in]    newton_gmres    Settings of Newton-GMRES method
 * @return    TRUE if the iteration is executed properly. FALSE if the iteration does not finish completely.
 */
gboolean jacobian_free_newton_gmres_iterate (JacobianFreeNewtonGMRES *newton_gmres)
{
  /*
    To solve f(x) = 0,
    we determine dx from A dx = f(x) and we set x_new = x - D dx,
    where D is a damping coefficient.
    newton_gmres->vector_b is f(x) and dx is obtained by GMRES method for A dx = f(x).
  */
  gboolean success;
  double *x;

  if (newton_gmres->iterate < 0) {
    fprintf(stderr, "Initial value is not set yet\n");
    abort();
  }
  jacobian_free_gmres_update_vector_b(newton_gmres);
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE_NORM, newton_gmres, newton_gmres->vector_b->data);
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_TIME_DERIVATIVE, newton_gmres, newton_gmres->vector_b->data);
  x = jacobian_free_gmres_solve(newton_gmres->gmres, newton_gmres->vector_zero->data, newton_gmres->vector_b->data);
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR_NORM, newton_gmres, x);
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_ERROR_VECTOR, newton_gmres, x);
  if (x) {
    gsl_vector_view view_x;
    view_x = gsl_vector_view_array(x, jacobian_free_newton_gmres_dimension(newton_gmres));
    gsl_vector_memcpy(newton_gmres->vector_last, newton_gmres->vector);
    if (newton_gmres->damping_coefficient > 0.0 && newton_gmres->damping_parameter > 0) {
      gsl_vector_scale(&view_x.vector, newton_gmres->damping_coefficient);
    }
    gsl_vector_sub(newton_gmres->vector, &view_x.vector);
    g_free(x);
    success = TRUE;
    if (newton_gmres->max_uniform_norm > 0.0) {
      int i;
      for (i = 0; i < newton_gmres->vector->size; i++) {
        if (gsl_vector_get(newton_gmres->vector, i) > newton_gmres->max_uniform_norm) {
          success = FALSE;
          break;
        }
      }
    }
  } else {
    success = FALSE;
  }
  mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_RESULT, newton_gmres);
  newton_gmres->iterate += 1;
  newton_gmres->vector_b_updated = FALSE;
  if (newton_gmres->after_iteration) {
    newton_gmres->after_iteration(newton_gmres);
  }
  return success;
}

/**
 * Search a root of the function by running Newton method successively.
 * @param[in]    newton_gmres     Settings of Newton-GMRES method
 * @param[in]    max_iteration    Maximum number of iteration of Newton method
 * @return     A number of iterations until the solution converges.
 *             If the error of Newton method becomes less than the maximum error
 *             then the function returns a positive integer.
 *             Otherwise, return a negative integer. Zero is not returned for any case.
 */
int jacobian_free_newton_gmres_iterate_successively (JacobianFreeNewtonGMRES *newton_gmres, uint max_iteration)
{
  int iter;
  for (iter = 1; iter <= max_iteration; iter++) {
    if (!jacobian_free_newton_gmres_iterate(newton_gmres)) {
      fprintf(stderr, "Can not continue Newton-GMRES method\n");
      return -iter;             /* Iteration fails */
    }
    if (jacobian_free_newton_gmres_converged_p(newton_gmres)) {
      mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, newton_gmres);
      return iter;
    }
    if (!isfinite(newton_gmres->error_cache)) {
      fprintf(stderr, "Iteration of JacobianFreeNewtonGMRES can not be continued: error = %le\n", newton_gmres->error_cache);
      return -iter;
    }
  }
  fprintf(stderr, "Iteration of JacobianFreeNewtonGMRES has not been converged: error = %.14le\n", newton_gmres->error_cache);
  return -iter;
}

/**
 * Search a root of the function by running Newton method successively with monotonic convergence.
 * @param[in]    newton_gmres     Settings of Newton-GMRES method
 * @param[in]    max_iteration    Maximum number of iteration of Newton method
 * @return     A number of iterations until the solution converges.
 *             If the error of Newton method becomes less than the maximum error
 *             then the function returns a positive integer.
 *             Otherwise, return a negative integer. Zero is not returned for any case.
 */
int jacobian_free_newton_gmres_iterate_successively_monotonic_convergence (JacobianFreeNewtonGMRES *newton_gmres, uint max_iteration)
{
  int iter;
  double error_last;
  error_last = jacobian_free_newton_gmres_current_error(newton_gmres);
  for (iter = 1; iter <= max_iteration; iter++) {
    if (!jacobian_free_newton_gmres_iterate(newton_gmres)) {
      fprintf(stderr, "Can not continue Newton-GMRES method\n");
      return -iter;             /* Iteration fails */
    }
    if (jacobian_free_newton_gmres_converged_p(newton_gmres)) {
      mathfluid_logger_log(newton_gmres->logger, JACOBIAN_FREE_NEWTON_GMRES_LOG_ITERATE_SUCCESSIVELY_CONVERGENCE, newton_gmres);
      return iter;
    }
    if (!isfinite(newton_gmres->error_cache)) {
      fprintf(stderr, "Iteration of JacobianFreeNewtonGMRES can not be continued: error = %le\n", newton_gmres->error_cache);
      return -iter;
    }
    if (newton_gmres->error_cache > error_last) {
      fprintf(stderr, "Error of JacobianFreeNewtonGMRES increases: error = %le\n", newton_gmres->error_cache);
      return -iter;
    }
    error_last = newton_gmres->error_cache;
  }
  fprintf(stderr, "Iteration of JacobianFreeNewtonGMRES has not been converged: error = %.14le\n", newton_gmres->error_cache);
  return -iter;
}

/** @} */  /* End of JacobianFreeNewtonGMRES */
