/**
 * @file system_ode.c
 * @brief Functions for SystemODE and SystemODESolver.
 *        They deal with ODEs that have explicitly vector field and
 *        evolve solutions by using GSL library.
 */

#include "header.h"

/**
 * \addtogroup SystemODE
 * @{
 */

/**
 * @param[in]    dimension          Dimension of ODE
 * @param[in]    time_derivative    A function to return a value of the vector field of ODE
 * @param[in]    jacobian           A function to return a value of Jacobian of vector field.
 * @param[in]    parameter_derivative    A function to return a value of parameter derivative of time derivative.
 *                                  If the Jacobian is not given explicitly then we set NULL.
 * @param[in]    parameters         Data of parameters of ODE.
 *                                  If we need to know the dimension of ODE in "function" or "jacobian"
 *                                  then parameters->data should store the dimension.
 * @param[in]    parameter_set      A function to set a parameter
 * @param[in]    parameter_get      A function to get a parameter
 * @return       Return a pointer of allocated SystemODE.
 */
SystemODE *system_ode_alloc (uint dimension, int (* time_derivative) (double *dydt, const double *y, DataContainer *params),
                             int (* jacobian) (double *dfdy, const double *y, DataContainer *params),
                             int (* parameter_derivative) (double *dydp, const double *y, DataContainer *params, const char *key),
                             DataContainer *parameters,
                             void (* parameter_set) (DataContainer *parameters, const char *key, GValue *val),
                             void (* parameter_get) (GValue *val, DataContainer *parameters, const char *key))
{
  SystemODE *system_ode;
  system_ode = (SystemODE *) g_malloc(sizeof(SystemODE));
  system_ode->dimension = dimension;
  system_ode->time_derivative = time_derivative;
  system_ode->jacobian_of_time_derivative = jacobian;
  system_ode->parameter_derivative_of_time_derivative = parameter_derivative;
  system_ode->parameters = parameters;
  system_ode->parameter_set = parameter_set;
  system_ode->parameter_get = parameter_get;
  if (!system_ode->time_derivative) {
    fprintf(stderr, "Vector field of SystemODE is NULL!\n");
    abort();
  }
  return system_ode;
}

/**
 * Free memory of system_ode
 */
void system_ode_free (SystemODE *system_ode)
{
  if (system_ode->parameters) {
    data_container_free(system_ode->parameters);
  }
  g_free(system_ode);
}

/**
 * @param[in]    system_ode    Source of duplication
 */
SystemODE *system_ode_duplicate (SystemODE *system_ode)
{
  DataContainer *parameters;
  if (system_ode->parameters) {
    parameters = data_container_duplicate(system_ode->parameters);
  } else {
    parameters = NULL;
  }
  return system_ode_alloc(system_ode->dimension, system_ode->time_derivative, system_ode->jacobian_of_time_derivative, system_ode->parameter_derivative_of_time_derivative, parameters, system_ode->parameter_set, system_ode->parameter_get);
}

void system_ode_parameter_set (SystemODE *system, const char *key, GValue *val)
{
  if (system->parameters && system->parameter_set) {
    system->parameter_set(system->parameters, key, val);
  } else {
    fprintf(stderr, "Can not set parameter %s\n", key);
    abort();
  }
}

void system_ode_parameter_get (GValue *val, SystemODE *system, const char *key)
{
  if (system->parameters && system->parameter_set) {
    return system->parameter_get(val, system->parameters, key);
  } else {
    fprintf(stderr, "Can not get parameter %s\n", key);
    abort();
  }
}

/**
 * @param[out]    time_derivative   Container of tangent vector at y, which must be an array of system->dimension elements.
 * @param[in]     system            System of ODE
 * @param[in]     y                 A pointer of an array that is a point in system
 * @return        Return TRUE if the calculation is finished properly.
 */
gboolean system_ode_time_derivative (double *time_derivative, SystemODE *system, const double *y)
{
  system->time_derivative(time_derivative, y, system->parameters);
  return TRUE;
}

/**
 * @param[out]    jac       Container of Jacobian at y, which must be an array of (system->dimension * system->dimension) elements.
 * @param[in]     system    System of ODE
 * @param[in]     y         A pointer of an array that is a point in system
 * @return        Return TRUE if the calculation is finished properly.
 */
gboolean system_ode_jacobian_of_time_derivative (double *jac, SystemODE *system, const double *y)
{
  if (system_ode_time_jacobian_of_time_derivative_defined_p(system)) {
    system->jacobian_of_time_derivative(jac, y, system->parameters);
    return TRUE;
  } else {
    return FALSE;
  }
}

gboolean system_ode_parameter_derivative_of_time_derivative (double *vec, SystemODE *system, const char *parameter_key, const double *y)
{
  if (system_ode_parameter_derivative_of_time_derivative_defined_p(system)) {
    return system->parameter_derivative_of_time_derivative(vec, y, system->parameters, parameter_key);
  } else {
    return FALSE;
  }
}

/**
 * Convert a function of SystemODE to a function of gsl_odeiv2_system.
 */
static int system_ode_gsl_function (double t, const double y[], double dydt[], void *params)
{
  system_ode_time_derivative(dydt, (SystemODE *) params, y);
  return GSL_SUCCESS;
}

/**
 * Convert a Jacobian of SystemODE to a Jacobian of gsl_odeiv2_system.
 */
static int system_ode_gsl_jacobian (double t, const double y[], double *dfdy, double *dfdt, void *params)
{
  int i;
  SystemODE *system = (SystemODE *) params;
  system->jacobian_of_time_derivative(dfdy, y, system->parameters);
  for (i = 0; i < system->dimension; i++) {
    dfdt[i] = 0.0;
  }
  return GSL_SUCCESS;
}

/**
 * Allocate memory of SystemODE.
 * gsl_driver of the allocated SystemODESolver is a driver allocated by gsl_odeiv2_driver_alloc_y_new(system, gsl_odeiv2_step_rk8pd, 1e-8, 1e-8, 0.0).
 * If we want to use other driver then we set the driver by calling system_ode_solver_set_driver.
 * @param[in]      system      Target ODE system
 */
SystemODESolver *system_ode_solver_alloc (SystemODE *system)
{
  return system_ode_solver_alloc2(system, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk8pd, SYSTEM_ODE_SOLVER_DEFAULT_INITIAL_STEP_SIZE, SYSTEM_ODE_SOLVER_DEFAULT_EVOLUTION_ERROR, 0.0);
}

void system_ode_solver_free (SystemODESolver *solver)
{
  if (solver->gsl_driver) {
    gsl_odeiv2_driver_free(solver->gsl_driver);
  }
  if (solver->gsl_driver_arguments) {
    g_ptr_array_free(solver->gsl_driver_arguments, TRUE);
  }
  g_free(solver);
}

static void system_ode_solver_add_driver_argument_of_double (SystemODESolver *solver, double v)
{
  double *val;
  val = (double *) g_malloc(sizeof(double));
  *val = v;
  g_ptr_array_add(solver->gsl_driver_arguments, val);
}

static void system_ode_solver_add_driver_argument_of_double_from_va_list (SystemODESolver *solver, va_list args)
{
  system_ode_solver_add_driver_argument_of_double(solver, va_arg(args, double));
}

static void system_ode_solver_parse_driver_arguments (SystemODESolver *solver, SystemODEDriverType driver_type, va_list args)
{
  if (solver->gsl_driver_arguments) {
    g_ptr_array_free(solver->gsl_driver_arguments, TRUE);
  }
  solver->gsl_driver_arguments = g_ptr_array_new_with_free_func(g_free);

  solver->gsl_step_type = va_arg(args, gsl_odeiv2_step_type *);
  /* hstart */
  system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
  /* epsabs */
  system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
  /* epsrel */
  system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);

  if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_STANDARD) {
    /* a_y */
    system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
    /* a_dydt */
    system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
  } else if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_SCALED) {
    double *scale_abs, *scale_abs_new;
    int ary_size;
    /* a_y */
    system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
    /* a_dydt */
    system_ode_solver_add_driver_argument_of_double_from_va_list(solver, args);
    /* scale_abs */
    scale_abs = va_arg(args, double *);
    ary_size = sizeof(double) * system_ode_solver_system(solver)->dimension;
    scale_abs_new = g_malloc(ary_size);
    memcpy(scale_abs_new, scale_abs, ary_size);
    g_ptr_array_add(solver->gsl_driver_arguments, scale_abs_new);
  }
}

static void system_ode_solver_gsl_driver_init (SystemODESolver *solver)
{
  gsl_odeiv2_driver *gsl_driver;
  double hstart, epsabs, epsrel;
  hstart = *(double *) g_ptr_array_index(solver->gsl_driver_arguments, 0);
  epsabs = *(double *) g_ptr_array_index(solver->gsl_driver_arguments, 1);
  epsrel = *(double *) g_ptr_array_index(solver->gsl_driver_arguments, 2);
  solver->initial_step_size = hstart;

  if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_Y) {
    gsl_driver = gsl_odeiv2_driver_alloc_y_new(solver->gsl_system, solver->gsl_step_type, hstart, epsabs, epsrel);
  } else if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_YP) {
    gsl_driver = gsl_odeiv2_driver_alloc_yp_new(solver->gsl_system, solver->gsl_step_type, hstart, epsabs, epsrel);
  } else {
    double a_y, a_dydt;
    a_y = *(double *) g_ptr_array_index(solver->gsl_driver_arguments, 3);
    a_dydt = *(double *) g_ptr_array_index(solver->gsl_driver_arguments, 4);
    if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_STANDARD) {
      gsl_driver = gsl_odeiv2_driver_alloc_standard_new(solver->gsl_system, solver->gsl_step_type, hstart, epsabs, epsrel, a_y, a_dydt);
    } else if (solver->gsl_driver_type == SYSTEM_ODE_DRIVER_SCALED) {
      double *scale_abs;
      scale_abs = (double *) g_ptr_array_index(solver->gsl_driver_arguments, 5);
      gsl_driver = gsl_odeiv2_driver_alloc_scaled_new(solver->gsl_system, solver->gsl_step_type, hstart, epsabs, epsrel, a_y, a_dydt, scale_abs);
    } else {
      fprintf(stderr, "Invalid type of driver\n");
      abort();
    }
  }

  if (solver->gsl_driver) {
    gsl_odeiv2_driver_free(solver->gsl_driver);
  }
  solver->gsl_driver = gsl_driver;
  solver->initial_step_size = hstart;
}

static void system_ode_solver_set_driver_arguments_copy (SystemODESolver *solver_new, SystemODESolver *solver_old)
{
  int i, index_max;
  if (solver_new->gsl_driver_arguments) {
    g_ptr_array_free(solver_new->gsl_driver_arguments, TRUE);
  }
  solver_new->gsl_driver_arguments = g_ptr_array_new_with_free_func(g_free);
  solver_new->gsl_step_type = solver_old->gsl_step_type;
  index_max = MIN(5, solver_old->gsl_driver_arguments->len);
  for (i = 0; i < index_max; i++) {
    system_ode_solver_add_driver_argument_of_double(solver_new, *(double *) g_ptr_array_index(solver_old->gsl_driver_arguments, i));
  }
  if (solver_new->gsl_driver_type == SYSTEM_ODE_DRIVER_SCALED) {
    double *scale_abs_new;
    int ary_size;
    ary_size = sizeof(double) * system_ode_solver_system(solver_new)->dimension;
    scale_abs_new = g_malloc(ary_size);
    memcpy(scale_abs_new, g_ptr_array_index(solver_old->gsl_driver_arguments, i), ary_size);
    g_ptr_array_add(solver_new->gsl_driver_arguments, scale_abs_new);
  }
}

static SystemODESolver *system_ode_solver_alloc_without_driver (SystemODE *system, SystemODEDriverType driver_type)
{
  SystemODESolver *solver;
  solver = g_malloc(sizeof(SystemODESolver));
  solver->gsl_system->function = system_ode_gsl_function;
  if (system_ode_time_jacobian_of_time_derivative_defined_p(system)) {
    solver->gsl_system->jacobian = system_ode_gsl_jacobian;
  } else {
    solver->gsl_system->jacobian = NULL;
  }
  solver->gsl_system->dimension = system->dimension;
  solver->gsl_system->params = (void *) system;
  solver->gsl_driver = NULL;
  solver->gsl_driver_type = driver_type;
  solver->gsl_driver_arguments = NULL;
  solver->gsl_step_type = NULL;
  return solver;
}

/**
 * Allocate solver with configuration of GSL ODE driver.
 * The arguments are same as system_ode_solver_set_driver.
 * @param[in]      system      Target ODE system
 * @param[in]      driver_type        Type of GSL driver
 */
SystemODESolver *system_ode_solver_alloc2 (SystemODE *system, SystemODEDriverType driver_type, ...)
{
  SystemODESolver *solver;
  va_list args;
  solver = system_ode_solver_alloc_without_driver(system, driver_type);
  va_start(args, driver_type);
  system_ode_solver_parse_driver_arguments(solver, driver_type, args);
  system_ode_solver_gsl_driver_init(solver);
  va_end(args);
  return solver;
}

SystemODESolver *system_ode_solver_duplicate (SystemODESolver *solver)
{
  SystemODESolver *solver_new;
  SystemODE *system;
  system = system_ode_duplicate(system_ode_solver_system(solver));
  solver_new = system_ode_solver_alloc_without_driver(system, solver->gsl_driver_type);
  system_ode_solver_set_driver_arguments_copy(solver_new, solver);
  system_ode_solver_gsl_driver_init(solver_new);
  return solver_new;
}

/**
 * Set new gsl_odeiv2_driver to solver->gsl_driver. The driver that has been already set is freed.
 * @param[in]      solver      Solver of ODE
 * @param[in]      driver_type Type of driver that corresponds to gsl_odeiv2_step_type
 * @param[in]      ...         The arguments of the corresponding driver allocation function after "const gsl_odeiv2_step_type *"
 *
 * @code
 * system_ode_solver_set_driver(solver, SYSTEM_ODE_DRIVER_Y, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
 * system_ode_solver_set_driver(solver, SYSTEM_ODE_DRIVER_YP, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
 * system_ode_solver_set_driver(solver, SYSTEM_ODE_DRIVER_STANDARD, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel, ay, adydt);
 * system_ode_solver_set_driver(solver, SYSTEM_ODE_DRIVER_SCALED, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel, ay, adydt, scale_abs);
 * @endcode
 */
void system_ode_solver_set_driver (SystemODESolver *solver, SystemODEDriverType driver_type, ...)
{
  va_list args;
  va_start(args, driver_type);
  system_ode_solver_parse_driver_arguments(solver, driver_type, args);
  system_ode_solver_gsl_driver_init(solver);
  va_end(args);
}

/**
 * This function evolves the solution starting from state->coordinate from state->time to t1.
 * state->coordinate and state->time are updated by the result of the evolution.
 * (for detail, see the entry of the function gsl_odeiv2_driver_apply in the GSL manual).
 *
 * @param[in,out]  state   State whose time and coordinate should be updated
 * @param[in]      solver  Solver of ODE
 * @param[in]      t1      Target time of the evolved solution
 * @return         True if the solution evolves properly. Otherwise, false.
 * @note    Returned status of gsl_odeiv2_driver_apply function is stored in solver->gsl_driver_status.
 *          If this function returns FALSE then we can obtain more information from solver->gsl_driver_status.
 * @note    This function evolves a solution by gsl_odeiv2_driver_apply
 *          after it resets initial step size to solver->initial_step_size.
 */
gboolean system_ode_solver_evolve (SystemState *state, SystemODESolver *solver, const double t1)
{
  gsl_odeiv2_driver_reset_hstart(solver->gsl_driver, solver->initial_step_size);
  solver->gsl_driver_status = gsl_odeiv2_driver_apply(solver->gsl_driver, &state->time, t1, state->coordinate);
  return solver->gsl_driver_status == GSL_SUCCESS;
}

/** @} */  /* End of SystemODE */
