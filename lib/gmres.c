/**
 * @file gmres.c
 * @brief Functions of GMRES methods for systems of ODE
 */

#include "header.h"

/**
 * \addtogroup GMRESSolver
 * @{
 * Typical usage of GMRESSolver is the following.
 * @code
 * gmres = gmres_solver_alloc(...); // or use gmres_solver_restarted_alloc
 * gmres_solver_set(gmres, ...);
 * solution = gmres_solver_iterate_successively(gmres, ...);
 * gmres_solver_free(gmres);
 * @endcode
 */

#define gmres_solver_malloc_vector(solver) ((double *) g_malloc(sizeof(double) * gmres_solver_dimension(solver)))
#define gmres_solver_malloc_vector2(dim) ((double *) g_malloc(sizeof(double) * (dim)))
#define gmres_solver_copy_vector(out, in, solver) memcpy(out, in, sizeof(double) * gmres_solver_dimension(solver))
#define gmres_solver_work_memory_allocated_p(solver) (solver->givens_rotation)

enum {
  GMRES_SOLVER_ITERATE_LOG,
  GMRES_SOLVER_ITERATE_SUCCESSIVELY_BEGIN_LOG,
  GMRES_SOLVER_N_LOGS,
};

static void gmres_solver_iterate_log (FILE *out, va_list args);
static void gmres_solver_iterate_successively_begin_log (FILE *out, va_list args);

static MathFluidLogFunc gmres_solver_log_function[GMRES_SOLVER_N_LOGS] = {
  { GMRES_SOLVER_ITERATE_LOG, MATH_FLUID_LOG_DEBUG, gmres_solver_iterate_log },
  { GMRES_SOLVER_ITERATE_SUCCESSIVELY_BEGIN_LOG, MATH_FLUID_LOG_DEBUG, gmres_solver_iterate_successively_begin_log }
};

/**
 * Allocate memory of GMRESSolver with no restart.
 * @param[in]    dimension    Dimension of vectors
 * @param[in]    error_residual_ratio     Maximum ration of error of residual
 * @param[in]    matrix_vector_product    Definition of matrix vector product
 * @return Solver of GMRES method with no restart.
 */
GMRESSolver *gmres_solver_alloc (uint dimension, double error_residual_ratio, MatrixVectorProduct *matrix_vector_product)
{
  return gmres_solver_restarted_alloc(dimension, 0, error_residual_ratio, matrix_vector_product);
}

static void gmres_solver_setting_restart (GMRESSolver *solver, uint dimension, int restart)
{
  solver->dimension = dimension;
  if (restart <= 0 || restart > solver->dimension) {
    solver->restart = solver->dimension;
    solver->restart_type = GMRES_NO_RESTART;
  } else {
    solver->restart = restart;
    solver->restart_type = GMRES_RESTART_FIXED;
  }
}

static void gmres_solver_setting_initialize (GMRESSolver *solver, uint dimension, int restart, double error_residual_ratio, MatrixVectorProduct *matrix_vector_product)
{
  gmres_solver_setting_restart(solver, dimension, restart);
  solver->matrix_vector_product = matrix_vector_product;
  solver->arnoldi_method = arnoldi_method_alloc(dimension, solver->restart, solver->matrix_vector_product);
  solver->min_error = GMRES_SOLVER_MIN_ERROR;
  solver->error_residual_ratio = error_residual_ratio;

  solver->vector_x0 = NULL;
  solver->vector_b = NULL;
  solver->vector_e = NULL;
  solver->hessenberg = NULL;
  solver->givens_rotation = NULL;
}

/**
 * This function must be called after calling gmres_solver_setting_initialize.
 */
static void gmres_solver_work_memory_alloc (GMRESSolver *solver)
{
  int i;
  solver->vector_e = gmres_solver_malloc_vector2(solver->restart + 1);
  solver->givens_rotation = (double *) g_malloc(sizeof(double) * solver->restart * 2);
  /*
    ci = solver->givens_rotation[2 * i]
    si = solver->givens_rotation[2 * i + 1]
    The matrix
    ci -si
    si  ci
    is Givens rotation matrix of i-th step.
  */
  solver->hessenberg = (double **) g_malloc(sizeof(double *) * solver->restart);
  for (i = 0; i < solver->restart; i++) {
    solver->hessenberg[i] = NULL;
  }
}

static void gmres_solver_work_memory_free (GMRESSolver *solver)
{
  if (solver->vector_e) {
    g_free(solver->vector_e);
    solver->vector_e = NULL;
  }
  if (solver->hessenberg) {
    int i;
    double *column;
    for (i = 0; i < solver->restart; i++) {
      column = solver->hessenberg[i];
      if (column) {
        g_free(column);
      } else {
        break;
      }
    }
    g_free(solver->hessenberg);
    solver->hessenberg = NULL;
  }
  if (solver->givens_rotation) {
    g_free(solver->givens_rotation);
    solver->givens_rotation = NULL;
  }
}

/**
 * Allocate memory of GMRESSolver with restart.
 * @param[in]    dimension    Dimension of vectors
 * @param[in]    restart      Maximum number of iterations without restart
 * @param[in]    error_residual_ratio     Maximum ration of error of residual
 * @param[in]    matrix_vector_product    Definition of matrix vector product
 * @return Solver of GMRES method with fixed restart parameter.
 */
GMRESSolver *gmres_solver_restarted_alloc (uint dimension, int restart, double error_residual_ratio, MatrixVectorProduct *matrix_vector_product)
{
  GMRESSolver *solver;
  solver = (GMRESSolver *) g_malloc(sizeof(GMRESSolver));
  gmres_solver_setting_initialize(solver, dimension, restart, error_residual_ratio, matrix_vector_product);
  solver->logger = mathfluid_logger_alloc(stdout, GMRES_SOLVER_N_LOGS, gmres_solver_log_function);
  return solver;
}

/**
 * Free memory of GMRESSolver.
 * @param[in]    solver           A pointer of GMRESSolver
 */
void gmres_solver_free (GMRESSolver *solver)
{
  gmres_solver_work_memory_free(solver);
  if (solver->arnoldi_method) {
    arnoldi_method_free(solver->arnoldi_method);
  }
  if (solver->vector_x0) {
    g_free(solver->vector_x0);
  }
  if (solver->vector_b) {
    g_free(solver->vector_b);
  }
  if (solver->logger) {
    mathfluid_logger_free(solver->logger);
  }
  g_free(solver);
}

/**
 * Duplacet GMRESSolver
 * @return A pointer of duplicated GMRESSolver
 */
GMRESSolver *gmres_solver_duplicate (GMRESSolver *solver)
{
  GMRESSolver *solver_duplicate;
  solver_duplicate = gmres_solver_alloc(solver->dimension, solver->error_residual_ratio, matrix_vector_product_duplicate(solver->matrix_vector_product));
  switch (solver->restart_type) {
  case GMRES_NO_RESTART:
    gmres_solver_set_restart_parameter(solver_duplicate, GMRES_NO_RESTART);
    break;
  case GMRES_RESTART_FIXED:
    gmres_solver_set_restart_parameter(solver_duplicate, GMRES_RESTART_FIXED, solver->restart);
    break;
  }
  mathfluid_logger_copy_settings(solver_duplicate->logger, solver->logger);
  return solver_duplicate;
}

static void gmres_solver_init_vector_e (GMRESSolver *solver, double residual)
{
  int i;
  solver->vector_e[0] = residual;
  for (i = 1; i <= solver->restart; i++) {
    solver->vector_e[i] = 0.0;
  }
}

/* Calculate solver->error from solver->vector_b and solver->error_residual_ratio */
static void gmres_solver_set_error (GMRESSolver *solver)
{
  if (solver->vector_b) {
    gsl_vector_const_view view_b = gsl_vector_const_view_array(solver->vector_b, solver->dimension);
    solver->error = gsl_blas_dnrm2(&view_b.vector) * solver->error_residual_ratio;
    if (solver->error < solver->min_error) {
      solver->error = solver->min_error;
    }
  }
}

/**
 * Set maximum ratio of residual.
 * If the ratio of norms of initial and current residual is less than this value,
 * GMRESSolver stops its iteration.
 * @param[in]    solver           A pointer of GMRESSolver
 * @param[in]    error_residual_ratio    Maximum ratio of residuals
 */
void gmres_solver_set_error_residual_ratio (GMRESSolver *solver, double error_residual_ratio)
{
  solver->error_residual_ratio = error_residual_ratio;
  gmres_solver_set_error(solver);
}

/**
 * Set minimum norm of residual.
 * Because the value of minimum norm of residual become too small
 * for small error_residual_ratio or initial residual,
 * this value min_error is used.
 * The default value is GSL_DBL_EPSILON.
 * @param[in]    solver           A pointer of GMRESSolver
 * @param[in]    min_error        Minimum norm of residual
 */
void gmres_solver_set_min_error (GMRESSolver *solver, double min_error)
{
  solver->min_error = min_error;
  gmres_solver_set_error(solver);
}

/**
 * Set solver->vector_b and solver->error.
 * @note solver->error is equal to maximum number of
 *       the product ||solver->vector_b|| * solver->error_residual_ratio and solver->min_error.
 *       Default value of solver->min_error is GMRES_SOLVER_MIN_ERROR.
 */
static void gmres_solver_set_vector_b (GMRESSolver *solver, const double *b)
{
  if (!solver->vector_b) {
    solver->vector_b = gmres_solver_malloc_vector(solver);
  }
  gmres_solver_copy_vector(solver->vector_b, b, solver);
  gmres_solver_set_error(solver);
}

static void gmres_solver_set_vector_x0 (GMRESSolver *solver, const double *x0)
{
  if (!solver->vector_x0) {
    solver->vector_x0 = gmres_solver_malloc_vector(solver);
  }
  gmres_solver_copy_vector(solver->vector_x0, x0, solver);
}

/**
 * solver->vector_b must be set before calling this function.
 */
static double *gmres_solver_residual_vector (GMRESSolver *solver, double *residual, const double *x)
{
  double *vec_r, *vec_tmp;
  gsl_vector_view view_r, view_tmp;
  vec_r = gmres_solver_malloc_vector(solver);
  vec_tmp = gmres_solver_malloc_vector(solver);
  view_r = gsl_vector_view_array(vec_r, solver->dimension);
  view_tmp = gsl_vector_view_array(vec_tmp, solver->dimension);

  arnoldi_method_matrix_vector_product(vec_tmp, solver->arnoldi_method, x);
  gmres_solver_copy_vector(vec_r, solver->vector_b, solver);
  gsl_vector_sub(&view_r.vector, &view_tmp.vector);

  if (residual) {
    *residual = gsl_blas_dnrm2(&view_r.vector);
  }

  g_free(vec_tmp);
  return vec_r;
}

/**
 * Set an initial vector x0 and a vector b of Ax = b.
 * @param[in]    solver   A pointer of GMRESSolver
 * @param[in]    x0       A pointer of array whose length is dimension of system
 * @param[in]    b        A pointer of array whose length is dimension of system
 */
void gmres_solver_set (GMRESSolver *solver, const double *x0, const double *b)
{
  double *vec_r;
  if (!gmres_solver_work_memory_allocated_p(solver)) {
    gmres_solver_work_memory_alloc(solver);
  }
  solver->total_iterate = 0;
  solver->step_iterate = 0;

  gmres_solver_set_vector_b(solver, b);
  gmres_solver_set_vector_x0(solver, x0);
  vec_r = gmres_solver_residual_vector(solver, &solver->residual, x0);
  if (solver->residual >= solver->error) {
    arnoldi_method_set(solver->arnoldi_method, vec_r);
  }
  gmres_solver_init_vector_e(solver, solver->residual);

  g_free(vec_r);
}

static void gmres_solver_init_parameters_at_restart (GMRESSolver *solver)
{
  double *x0, *vec_r;

  x0 = gmres_solver_solution(solver);
  if (!x0) {
    return;
  }
  /* We must initialize parameters after calling gmres_solver_solution
     because the function gmres_solver_solution needs old parameters. */
  solver->step_iterate = 0;

  gmres_solver_copy_vector(solver->vector_x0, x0, solver);
  /* Restarted solver inherits solver->residual. Do not renew solver->residual before. */
  vec_r = gmres_solver_residual_vector(solver, NULL, x0);
  if (solver->residual >= solver->error) {
    arnoldi_method_set(solver->arnoldi_method, vec_r);
  }
  gmres_solver_init_vector_e(solver, solver->residual);

  g_free(x0);
  g_free(vec_r);
}

/**
 * @param[in] step_iterate    Number of step of Arnoldi process
 */
static void gmres_solver_Arnoldi_method (GMRESSolver *solver, int step_iterate)
{
  const double *hessenberg_column;
  arnoldi_method_iterate(solver->arnoldi_method);
  hessenberg_column = arnoldi_method_hessenberg_column(solver->arnoldi_method);
  if (!solver->hessenberg[step_iterate]) {
    solver->hessenberg[step_iterate] = (double *) g_malloc(sizeof(double) * (step_iterate + 2));
  }
  memcpy(solver->hessenberg[step_iterate], hessenberg_column, sizeof(double) * (step_iterate + 2));
}

static double gmres_solver_hessenberg_get (GMRESSolver *solver, int row, int col)
{
  return solver->hessenberg[col][row];
}

static void gmres_solver_hessenberg_set (GMRESSolver *solver, int row, int col, double val)
{
  solver->hessenberg[col][row] = val;
}

/**
 * Update solver->hessenberg, solver->gives_rotation, solver->vector_e, and solver->residual.
 */
static void gmres_solver_triangulation_by_Givens_rotation (GMRESSolver *solver, int step_iterate)
{
  int i;
  double h1, h2, ci, si, e1, e2, h1_new;
  for (i = 0; i < step_iterate; i++) {
    ci = solver->givens_rotation[2 * i];
    si = solver->givens_rotation[2 * i + 1];
    h1 = gmres_solver_hessenberg_get(solver, i, step_iterate);
    h2 = gmres_solver_hessenberg_get(solver, i + 1, step_iterate);
    gmres_solver_hessenberg_set(solver, i, step_iterate, ci * h1 - si * h2);
    gmres_solver_hessenberg_set(solver, i + 1, step_iterate, si * h1 + ci * h2);
  }

  h1 = gmres_solver_hessenberg_get(solver, step_iterate, step_iterate);
  h2 = gmres_solver_hessenberg_get(solver, step_iterate + 1, step_iterate);
  ci = sqrt((h1 * h1) / (h1 * h1 + h2 * h2));
  si = - (h2 / h1) * ci;
  solver->givens_rotation[2 * step_iterate] = ci;
  solver->givens_rotation[2 * step_iterate + 1] = si;

  e1 = solver->vector_e[step_iterate];
  e2 = solver->vector_e[step_iterate + 1];
  solver->vector_e[step_iterate] = ci * e1 - si * e2;
  solver->vector_e[step_iterate + 1] = si * e1 + ci * e2;
  solver->residual = fabs(solver->vector_e[step_iterate + 1]);

  h1_new = sqrt(h1 * h1 + h2 * h2);
  if (h1 < 0) {
    h1_new = -h1_new;
  }
  gmres_solver_hessenberg_set(solver, step_iterate, step_iterate, h1_new);
  gmres_solver_hessenberg_set(solver, step_iterate + 1, step_iterate, 0.0);
}

/**
 * Set parameters of restart.
 * @code
 * gmres_solver_set_restart_parameter(GMRESSolver *solver, GMRES_NO_RESTART);
 * gmres_solver_set_restart_parameter(GMRESSolver *solver, GMRES_RESTART_FIXED, int restart_number);
 * @endcode
 */
void gmres_solver_set_restart_parameter (GMRESSolver *solver, GMRESRestartType restart_type, ...)
{
  va_list args;
  va_start(args, restart_type);
  gmres_solver_set_restart_parameter_va_list(solver, restart_type, args);
  va_end(args);
}

/**
 * Set parameters of restart. This function is va_list version of gmres_solver_set_restart_parameter.
 */
void gmres_solver_set_restart_parameter_va_list (GMRESSolver *solver, GMRESRestartType restart_type, va_list args)
{
  double *x0, *b;
  if (restart_type == GMRES_NO_RESTART) {
    solver->restart = solver->dimension;
    solver->restart_type = GMRES_NO_RESTART;
  } else if (restart_type == GMRES_RESTART_FIXED) {
    solver->restart_type = GMRES_RESTART_FIXED;
    solver->restart = va_arg(args, int);
  } else {
    fprintf(stderr, "Invalid restart type for GMRESSolver\n");
    abort();
  }
  x0 = gmres_solver_solution(solver);
  if (x0) {
    b = gmres_solver_malloc_vector(solver);
    gmres_solver_copy_vector(b, solver->vector_b, solver);
  }
  gmres_solver_work_memory_free(solver);
  if (x0) {
    gmres_solver_set(solver, x0, b);
    g_free(x0);
    g_free(b);
  }
}

/**
 * @param[in]    solver           A pointer of GMRESSolver
 * @return TRUE if the iteration of GMRES method has converged. Otherwise, FALSE.
 */
gboolean gmres_solver_converged_p (GMRESSolver *solver)
{
  return (solver->residual < solver->error ? TRUE : FALSE);
}

static void gmres_solver_iterate_log (FILE *out, va_list args)
{
  GMRESSolver *solver = va_arg(args, GMRESSolver *);
  if (solver->restart > 0) {
    fprintf(out, "GMRESSolver/iterate %d %d: residual=%.8le\n", solver->total_iterate, solver->step_iterate, solver->residual);
  } else {
    fprintf(out, "GMRESSolver/iterate %d: residual=%.8le\n", solver->total_iterate, solver->residual);
  }
}

/**
 * Advance an iteration step of GMRES method.
 * @param[in]    solver           A pointer of GMRESSolver
 * @return       TRUE if the number of iteration step is less than solver->restart. Otherwise, False.
 */
gboolean gmres_solver_iterate (GMRESSolver *solver)
{
  if (solver->step_iterate < 0) {
    fprintf(stderr, "Invalid step number of iteration for GMRESSolver: %d\n", solver->step_iterate);
    abort();
  }
  if (solver->step_iterate >= solver->restart) {
    return FALSE;
  }
  if (!gmres_solver_initial_solution_set_p(solver)) {
    fprintf(stderr, "Initial solution for GMRESSolver is not set\n");
    abort();
  }
  if (!gmres_solver_work_memory_allocated_p(solver)) {
    fprintf(stderr, "Work memory for GMRESSolver is not allocated\n");
    abort();
  }
  gmres_solver_Arnoldi_method(solver, solver->step_iterate);
  gmres_solver_triangulation_by_Givens_rotation(solver, solver->step_iterate);
  solver->step_iterate += 1;
  solver->total_iterate += 1;
  mathfluid_logger_log(solver->logger, GMRES_SOLVER_ITERATE_LOG, solver);
  if (solver->residual > GSL_DBL_MAX || isnan(solver->residual)) {
    fprintf(stderr, "Residual diverges: residual=%.8le\n", solver->residual);
    abort();
  }
  return TRUE;
}

static gsl_matrix *gmres_solver_upper_triangular_matrix_from_hessenberg (GMRESSolver *solver)
{
  gsl_matrix *upper;
  int i, j;
  upper = gsl_matrix_alloc(solver->step_iterate, solver->step_iterate);
  gsl_matrix_set_zero(upper);
  for (i = 0; i < solver->step_iterate; i++) {
    for (j = i; j < solver->step_iterate; j++) {
      gsl_matrix_set(upper, i, j, gmres_solver_hessenberg_get(solver, i, j));
    }
  }
  return upper;
}

/**
 * Calculate solution from values saved in the solver.
 * @param[in]    solver   A pointer of GMRESSolver
 * @return        A pointer of an array of double whose size is dimension. If the allocation fails, the program is aborted.
 *                This pointer must be freed when it is no longer used.
 */
double *gmres_solver_solution (GMRESSolver *solver)
{
  double *solution, *vk;
  gsl_vector_view view_solution, view_e;
  gsl_vector *y;
  gsl_vector_view view_y, view_x0;
  gsl_matrix *upper;
  gsl_matrix_view view_vk;

  if (!gmres_solver_initial_solution_set_p(solver)) {
    return NULL;
  }
  if (solver->step_iterate < 0) {
    fprintf(stderr, "GMRESSolver Can not calculate solution\n");
    abort();
  }
  if (solver->step_iterate == 0) {
    solution = gmres_solver_malloc_vector(solver);
    gmres_solver_copy_vector(solution, solver->vector_x0, solver);
    return solution;
  }

  solution = gmres_solver_malloc_vector(solver);
  view_solution = gsl_vector_view_array(solution, solver->dimension);
  y = gsl_vector_alloc(solver->step_iterate);
  upper = gmres_solver_upper_triangular_matrix_from_hessenberg(solver);
  view_e = gsl_vector_view_array(solver->vector_e, solver->restart + 1);
  view_y = gsl_vector_subvector(&view_e.vector, 0, solver->step_iterate);
  view_x0 = gsl_vector_view_array(solver->vector_x0, solver->dimension);
  gsl_vector_memcpy(y, &view_y.vector);

  gsl_blas_dtrsv(CblasUpper, CblasNoTrans, CblasNonUnit, upper, y);
  gsl_vector_memcpy(&view_solution.vector, &view_x0.vector);
  vk = arnoldi_method_basis_matrix(solver->arnoldi_method);
  view_vk = gsl_matrix_view_array(vk, solver->arnoldi_method->dimension, solver->arnoldi_method->iterate);
  gsl_blas_dgemv(CblasNoTrans, 1.0, &view_vk.matrix, y, 1.0, &view_solution.vector);

  g_free(vk);
  gsl_vector_free(y);
  gsl_matrix_free(upper);
  return solution;
}

static void gmres_solver_iterate_successively_begin_log (FILE *out, va_list args)
{
  GMRESSolver *solver = va_arg(args, GMRESSolver *);
  gsl_vector_const_view view_b = gsl_vector_const_view_array(solver->vector_b, solver->dimension);
  double norm_b = gsl_blas_dnrm2(&view_b.vector);
  fprintf(out, "GMRESSolver/iterate_successively_begin:\n");
  fprintf(out, "  norm_b=%.8le, error=%.8le, residual=%.8le\n", norm_b, solver->error, solver->residual);
}

/**
 * Repeat iterations until the residual converges and return the solution.
 * @param[in]    solver           A pointer of GMRESSolver
 * @param[in]    max_iteration    Maximum number of iteration steps.
 *                                If we set restarted GMRES method to the solver, the solver automatically restart
 *                                and total number of iteration steps is less than max_iteration.
 * @return    A pointer that is a return value of gmres_solver_solution if the residual is less than maximum error
 *            of solver. Otherwise, return NULL.
 */
double *gmres_solver_iterate_successively (GMRESSolver *solver, int max_iteration)
{
  int i;
  if (max_iteration <= 0) {
    fprintf(stderr, "Maximum number of iterations of GMRESSolver must be positive: %d\n", max_iteration);
    abort();
  }
  if (!gmres_solver_initial_solution_set_p(solver)) {
    fprintf(stderr, "Initial solution for GMRESSolver is not set\n");
    abort();
  }
  if (gmres_solver_converged_p(solver)) {
    return gmres_solver_solution(solver);
  }
  mathfluid_logger_log(solver->logger, GMRES_SOLVER_ITERATE_SUCCESSIVELY_BEGIN_LOG, solver);
  for (i = 0; i < max_iteration; i++) {
    if (!gmres_solver_iterate(solver)) {
      if (solver->restart_type == GMRES_RESTART_FIXED) {
        gmres_solver_init_parameters_at_restart(solver);
        if (gmres_solver_converged_p(solver)) {
          fprintf(stderr, "Restart of GMRESSolver is not needed\n");
          abort();
        }
        if (!gmres_solver_iterate(solver)) {
          fprintf(stderr, "GMRESSolver can not restart\n");
          abort();
        }
      } else {
        break;
      }
    }
    if (gmres_solver_converged_p(solver)) {
      return gmres_solver_solution(solver);
    }
  }
  return NULL;
}

/** @} */  /* End of GMRESSolver */
