#include <cutter.h>
#include "mathfluid.h"

static LyapunovVector *lyapunov_vector_get_sample ()
{
  LyapunovVector *v;
  int dim, num, i, j;
  dim = 4;
  num = 3;
  v = lyapunov_vector_alloc(dim, num);
  v->state->time = ((double) rand() / (double) RAND_MAX) * 10.0;
  for (i = 0; i < v->state->dimension; i++) {
    v->state->coordinate[i] = ((double) rand() / (double) RAND_MAX - 0.5) * 10.0;
  }
  for (i = 0; i < v->number; i++) {
    for (j = 0; j < v->state->dimension; j++) {
      v->vector[i][j] = ((double) rand() / (double) RAND_MAX - 0.5) * 10.0;
    }
  }
  return v;
}

LyapunovVector *lyapunov_vector_get_copy (LyapunovVector *v)
{
  LyapunovVector *v_new;
  int i, j;
  v_new = lyapunov_vector_alloc(v->state->dimension, v->number);
  v_new->state->time = v->state->time;
  for (i = 0; i < v->state->dimension; i++) {
    v_new->state->coordinate[i] = v->state->coordinate[i];
  }
  for (i = 0; i < v->number; i++) {
    for (j = 0; j < v->state->dimension; j++) {
      v_new->vector[i][j] = v->vector[i][j];
    }
  }
  return v_new;
}

void test_lyapunov_vector_set_push_pop ()
{
  char *dir, *path;
  LyapunovVectorSet *vector_set;
  LyapunovVector *v1, *v2;
  int i, j;
  mathfluid_utils_tmpdir_clear();
  vector_set = lyapunov_vector_set_alloc();
  vector_set->stack->db_cache_max_number = 1;
  dir = mathfluid_utils_tmpdir_get_subdir("mathfluid.test");
  path = g_strdup_printf("%s/lyapunov_vector_set", dir);
  lyapunov_vector_set_set_db(vector_set, path);

  v1 = lyapunov_vector_get_sample();
  v2 = lyapunov_vector_get_copy(v1);

  lyapunov_vector_set_push(vector_set, v1);
  v1 = lyapunov_vector_set_pop(vector_set);

  cut_assert_equal_int(v2->number, v1->number);
  cut_assert_equal_double(v2->state->time, 0.0, v1->state->time);
  for (i = 0; i < v1->state->dimension; i++) {
    cut_assert_equal_double(v2->state->coordinate[i], 0.0, v1->state->coordinate[i]);
  }
  for (i = 0; i < v1->number; i++) {
    for (j = 0; j < v1->state->dimension; j++) {
      cut_assert_equal_double(v2->vector[i][j], 0.0, v1->vector[i][j]);
    }
  }

  g_free(dir);
  g_free(path);
  lyapunov_vector_set_free(vector_set);
  lyapunov_vector_free(v1);
  lyapunov_vector_free(v2);
  mathfluid_utils_tmpdir_clear();
}
