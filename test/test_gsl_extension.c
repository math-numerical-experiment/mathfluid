#include <cutter.h>
#include "mathfluid.h"

void test_gsl_matrix_normalize_all_columns (void)
{
  double norm;
  gsl_matrix *m;
  m = gsl_matrix_alloc(3, 2);
  gsl_matrix_set(m, 0, 0, 2.0);
  gsl_matrix_set(m, 1, 0, -1.0);
  gsl_matrix_set(m, 2, 0, 3.0);
  gsl_matrix_set(m, 0, 1, 1.0);
  gsl_matrix_set(m, 1, 1, 4.0);
  gsl_matrix_set(m, 2, 1, -7.0);
  gsl_matrix_normalize_all_columns(m);

  norm = sqrt(2.0 * 2.0 + 1.0 + 3.0 * 3.0);
  cut_assert_equal_double(2.0 / norm, 1e-8, gsl_matrix_get(m, 0, 0));
  cut_assert_equal_double(-1.0 / norm, 1e-8, gsl_matrix_get(m, 1, 0));
  cut_assert_equal_double(3.0 / norm, 1e-8, gsl_matrix_get(m, 2, 0));
  norm = sqrt(1.0 + 4.0 * 4.0 + 7.0 * 7.0);
  cut_assert_equal_double(1.0 / norm, 1e-8, gsl_matrix_get(m, 0, 1));
  cut_assert_equal_double(4.0 / norm, 1e-8, gsl_matrix_get(m, 1, 1));
  cut_assert_equal_double(-7.0 / norm, 1e-8, gsl_matrix_get(m, 2, 1));
}
