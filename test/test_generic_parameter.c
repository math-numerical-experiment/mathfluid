#include <cutter.h>
#include "mathfluid.h"

void test_generic_parameter_double ()
{
  GenericParameter *params;
  double array_double[] = { 1.0, 2.0, -3.0 };
  char *keys[] = { "a", "b", "c" };
  int i, len = 3;
  params = generic_parameter_alloc();
  generic_parameter_set_double(params, len, array_double, keys);
  for (i = 0; i < len; i++) {
    cut_assert_equal_double(array_double[i], 0.0, generic_parameter_double(params, i));
  }
  generic_parameter_free(params);
}

void test_generic_parameter_int ()
{
  GenericParameter *params;
  int array_int[] = { 1, 2, -3 };
  char *keys[] = { "a", "b", "c" };
  int i, len = 3;
  params = generic_parameter_alloc();
  generic_parameter_set_int(params, len, array_int, keys);
  for (i = 0; i < len; i++) {
    cut_assert_equal_int(array_int[i], generic_parameter_int(params, i));
  }
  generic_parameter_free(params);
}

void test_generic_parameter_get ()
{
  GenericParameter *params;
  double array_double[] = { 1.0, 2.0, -3.0 };
  int array_int[] = { 0, -1, -2, 3 };
  char *keys_double[] = { "a", "b", "c" }, *keys_int[] = { "d", "e", "f", "g" };
  int i, len_double = 3, len_int = 4;
  GValue val = G_VALUE_INIT;
  params = generic_parameter_alloc();
  generic_parameter_set_double(params, len_double, array_double, keys_double);
  generic_parameter_set_int(params, len_int, array_int, keys_int);
  for (i = 0; i < len_double; i++) {
    generic_parameter_get(&val, params, keys_double[i]);
    cut_assert(G_VALUE_HOLDS_DOUBLE(&val));
    cut_assert_equal_double(array_double[i], 0.0, g_value_get_double(&val));
  }
  for (i = 0; i < len_int; i++) {
    generic_parameter_get(&val, params, keys_int[i]);
    cut_assert(G_VALUE_HOLDS_INT(&val));
    cut_assert_equal_int(array_int[i], g_value_get_int(&val));
  }
  generic_parameter_free(params);
}

void test_generic_parameter_empty_case ()
{
  GenericParameter *params;
  params = generic_parameter_alloc();
  cut_assert_null(generic_parameter_array_double(params));
  cut_assert_null(generic_parameter_array_int(params));
  generic_parameter_free(params);
}
