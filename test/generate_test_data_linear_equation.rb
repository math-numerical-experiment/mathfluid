require "matrix"
require "digest/md5"
require "optparse"

help_message =<<HELP
Usage: ruby #{File.basename(__FILE__)} --dense 10 --sparse 10 --min-dim 3 > gmres_tests_source.c

Generate tests of GMRES method for linear equations.
All generated linear equations are solvable.
HELP

options = { :dense => 0, :sparse => 0, :min_dim => 3, :solver_type => "basic" }

begin
  OptionParser.new(help_message) do |opt|
    opt.on("--dense NUM", Integer, "Number of tests of dense matrices") do |v|
      options[:dense] = v
    end
    opt.on("--sparse NUM", Integer, "Number of tests of sparse matrices") do |v|
      options[:sparse] = v
    end
    opt.on("--min-dim NUM", Integer, "Minimal dimension of tests") do |v|
      options[:min_dim] = v
    end
    opt.on("--solver TYPE", String, "Type of GMRES solver: 'basic' or 'restarted'") do |v|
      options[:solver_type] = v
    end
    opt.on("--positive-definite", "Create positive definite matrices") do |v|
      options[:positive_definite] = true
    end
    opt.parse!(ARGV)
  end
rescue OptionParser::InvalidOption
  $stderr.print <<MES
error: Invalid Option
#{help_message}
MES
  exit(2)
rescue OptionParser::InvalidArgument
  $stderr.print <<MES
error: Invalid Argument
#{help_message}
MES
  exit(2)
end

class MatrixGenerator
  NUMBER_MAX = 50

  def generate_random_numbers(n, &block)
    if block_given?
      n.times.map do |i|
        num = rand(NUMBER_MAX) - NUMBER_MAX / 2.0
        yield(num)
      end
    else
      n.times.map do |i|
        rand(NUMBER_MAX) - NUMBER_MAX / 2.0
      end
    end
  end
  private :generate_random_numbers

  def generate_dense_matrix(dim)
    nums = generate_random_numbers(dim ** 2)
    Matrix[*nums.each_slice(dim).to_a]
  end

  def generate_sparse_matrix(dim)
    nums = generate_random_numbers(dim ** 2) do |num|
      (rand < 0.3 ? num : 0.0)
    end
    Matrix[*nums.each_slice(dim).to_a]
  end

  def generate_vector(dim)
    generate_random_numbers(dim)
  end

  def generate_regular_matrices(method_name, dim)
    m = nil
    loop do
      m = send(method_name, dim)
      if m.regular?
        break
      end
    end
    [m, nil]
  end

  def generate_positive_definite_matrices(method_name, dim)
    m, eigenvalues = generate_regular_matrices(method_name, dim)
    mat = m * m.transpose
    eigenvalues = []
    diagonal = mat.eigen.d
    dim.times do |n|
      eigenvalues << diagonal[n, n]
    end
    [mat, eigenvalues]
  end

  def generate_matrix_data(dim, opts= {})
    method_name = (opts[:positive_definite] ? "generate_positive_definite_matrices" : "generate_regular_matrices")
    generate_method = (opts[:sparse] ? "generate_sparse_matrix" : "generate_dense_matrix")
    send(method_name, generate_method, dim)
  end
  private :generate_matrix_data

  def generate_linear_equation(dim, opts = {})
    matrix, eigenvalues = generate_matrix_data(dim, opts)
    mat = matrix.to_a.flatten.map(&:to_s).join(",")
    eq = {
      :dim => dim,
      :matrix => mat,
      :vector => generate_vector(dim).map(&:to_s).join(","),
      :guess => generate_vector(dim).map(&:to_s).join(",")
    }
    eq[:md5] = Digest::MD5.hexdigest("#{mat};#{eq[:guess]};#{eq[:vector]}")
    eq[:eigenvalues] = eigenvalues.map(&:to_s).join(",") if eigenvalues
    eq
  end
end

generator = MatrixGenerator.new

matrices = []
options[:dense].times do |i|
  dim = i + options[:min_dim]
  matrices << generator.generate_linear_equation(dim,
                                                 :positive_definite => options[:positive_definite],
                                                 :sparse => false)
end

options[:dense].times do |i|
  dim = i + options[:min_dim]
  matrices << generator.generate_linear_equation(dim,
                                                 :positive_definite => options[:positive_definite],
                                                 :sparse => true)
end

test_type = options[:solver_type]
test_type += "_positive_definite" if options[:positive_definite]
tests = []
matrices.each do |m|
  if options[:positive_definite]
    tests << <<CODE
void test_gmres_#{test_type}_linear_equation_#{m[:md5]} ()
{
  int dim = #{m[:dim]};
  double mat[] = {#{m[:matrix]}};
  double x0[] = {#{m[:guess]}};
  double b[] = {#{m[:vector]}};
  double eigenvalues[] = {#{m[:eigenvalues]}};
  gmres_#{test_type}_solve_linear_equation(dim, mat, x0, b, eigenvalues);
}
CODE
  else
    tests << <<CODE
void test_gmres_#{test_type}_linear_equation_#{m[:md5]} ()
{
  int dim = #{m[:dim]};
  double mat[] = {#{m[:matrix]}};
  double x0[] = {#{m[:guess]}};
  double b[] = {#{m[:vector]}};
  gmres_#{options[:solver_type]}_solve_linear_equation(dim, mat, x0, b);
}
CODE
  end
end

puts tests.join("\n")
