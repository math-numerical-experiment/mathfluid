#include <cutter.h>
#include "mathfluid.h"

static char *string_dump (size_t *size, gpointer data)
{
  char *dump;
  *size = strlen((char *)data);
  dump = (char *) g_malloc(sizeof(char) * *size);
  memcpy(dump, data, *size);
  return dump;
}

static gpointer string_load (char *dump, size_t size)
{
  char *str;
  str = (char *) g_malloc(sizeof(char) * (size + 1));
  memcpy(str, dump, sizeof(char) * size);
  str[size] = '\0';
  return (gpointer) str;
}

static LevelDBStackCache *database_create (const char *path)
{
  LevelDBStackCache *db;
  db = leveldb_stack_cache_alloc(string_dump, string_load, g_free);
  leveldb_stack_cache_set_db(db, path);
  leveldb_stack_cache_push(db, g_strdup("hello"));
  leveldb_stack_cache_push(db, g_strdup("world"));
  return db;
}

void test_leveldb_stack_cache_put_get ()
{
  LevelDBStackCache *db;
  char *str;
  db = database_create("test/stack_cache_test1");
  leveldb_stack_cache_flush(db);
  str = (char *) leveldb_stack_cache_pop(db);
  cut_assert_equal_string_with_free("world", str);
  str = (char *) leveldb_stack_cache_pop(db);
  cut_assert_equal_string_with_free("hello", str);
  leveldb_stack_cache_free(db, TRUE);
}

void test_leveldb_stack_cache_get_from_closed_database ()
{
  LevelDBStackCache *db;
  char *str, *path = "test/stack_cache_test2";
  db = database_create(path);
  leveldb_stack_cache_flush(db);
  leveldb_stack_cache_free(db, FALSE);
  db = leveldb_stack_cache_alloc(string_dump, string_load, g_free);
  leveldb_stack_cache_set_db(db, path);
  str = (char *) leveldb_stack_cache_pop(db);
  cut_assert_equal_string_with_free("world", str);
  str = (char *) leveldb_stack_cache_pop(db);
  cut_assert_equal_string_with_free("hello", str);
  leveldb_stack_cache_free(db, TRUE);
}
