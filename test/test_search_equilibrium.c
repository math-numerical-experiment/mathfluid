#include <cutter.h>
#include "mathfluid.h"

static int quadratic_polynomial (double *dydt, const double *y, DataContainer *params)
{
  dydt[0] = y[0] * y[0] - 3.0;
  return GSL_SUCCESS;
}

static int quadratic_polynomial_derivative (double *dfdy, const double *y, DataContainer *params)
{
  dfdy[0] = 2.0 * y[0];
  return GSL_SUCCESS;
}

void test_search_equilibrium_quadratic_polynomial ()
{
  SystemODE *system_ode;
  SystemODESolver *solver;
  DynamicalSystem *dynamical_system;
  SearchEquilibrium *search_equilibrium;
  gboolean success;
  double *solution, initial_value;
  system_ode = system_ode_alloc(1, quadratic_polynomial, quadratic_polynomial_derivative, NULL, NULL, NULL, NULL);
  solver = system_ode_solver_alloc(system_ode);
  dynamical_system = dynamical_system_ode_alloc(solver, TRUE, TRUE);
  search_equilibrium = search_equilibrium_alloc(dynamical_system, 0, 1e-10);
  initial_value = 3.0;
  search_equilibrium_set(search_equilibrium, &initial_value);
  success = search_equilibrium_iterate_successively(search_equilibrium, 1000);
  solution = search_equilibrium_solution(search_equilibrium);
  cut_assert_true(success);
  cut_assert_equal_double(sqrt(3.0), 1e-8, solution[0]);
  g_free(solution);
  search_equilibrium_free(search_equilibrium);
  dynamical_system_free(dynamical_system);
}
