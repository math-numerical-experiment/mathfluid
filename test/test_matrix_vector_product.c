#include <cutter.h>
#include "mathfluid.h"

#define VECTOR_ERROR 1e-8

static void assert_gsl_vector (gsl_vector *expected, double error, gsl_vector *actual)
{
  int i;
  for (i = 0; i < expected->size; i++) {
    cut_assert_equal_double(gsl_vector_get(expected, i), error, gsl_vector_get(actual, i));
  }
}

void test_usual_matrix_vector_product ()
{
  MatrixVectorProduct *matrix_vector_product;
  double matrix_data[] = { 0.5, -0.2, 0.8, 1.1, -0.5, -0.7, -0.2, 1.0, 0.4 };
  double vector_data[] = { 2.3, 1.4, -1.7 };
  int dim;
  gsl_matrix_view m;
  gsl_vector *actual, *expected;
  gsl_vector_view v;
  dim = 3;
  m = gsl_matrix_view_array(matrix_data, dim, dim);
  actual = gsl_vector_alloc(dim);
  expected = gsl_vector_alloc(dim);
  v = gsl_vector_view_array(vector_data, dim);
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, matrix_data);
  matrix_vector_product_calculate(actual->data, matrix_vector_product, vector_data);
  gsl_blas_dgemv(CblasNoTrans, 1.0, &m.matrix, &v.vector, 0.0, expected);
  assert_gsl_vector(expected, VECTOR_ERROR, actual);
  gsl_vector_free(actual);
  gsl_vector_free(expected);
  matrix_vector_product_free(matrix_vector_product);
}
