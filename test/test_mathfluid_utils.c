#include <cutter.h>
#include "mathfluid.h"

static void free_tmpdir_string_with_deletion (char *tmpdir_root)
{
  if (tmpdir_root) {
    if (g_file_test(tmpdir_root, G_FILE_TEST_EXISTS)) {
      if (!mathfluid_utils_remove_recursively(tmpdir_root)) {
        cut_message("Can not delete temporary directory: %s\n", tmpdir_root);
      }
    }
    g_free(tmpdir_root);
  }
}

void test_tmpdir_clear ()
{
  char *tmpdir_root;
  mathfluid_utils_tmpdir_clear();
  tmpdir_root = mathfluid_utils_tmpdir_root();
  cut_assert_false(tmpdir_root);
  free_tmpdir_string_with_deletion(tmpdir_root);
}

void test_tmpdir_set_root_automatically ()
{
  char *tmpdir_root;
  mathfluid_utils_tmpdir_clear();
  mathfluid_utils_tmpdir_set_root(NULL);
  tmpdir_root = mathfluid_utils_tmpdir_root();
  cut_assert_true(tmpdir_root);
  cut_assert_file_exist(tmpdir_root);
  free_tmpdir_string_with_deletion(tmpdir_root);
}

void test_tmpdir_set_root_current_directory ()
{
  char *tmpdir_root;
  mathfluid_utils_tmpdir_clear();
  mathfluid_utils_tmpdir_set_root(".");
  tmpdir_root = mathfluid_utils_tmpdir_root();
  cut_assert_true(tmpdir_root);
  cut_assert_file_exist(tmpdir_root);
  cut_assert_match("^./", tmpdir_root);
  free_tmpdir_string_with_deletion(tmpdir_root);
}

void test_tmpdir_get_subdirectory ()
{
  char *tmpdir_root, *subdir;
  mathfluid_utils_tmpdir_clear();
  mathfluid_utils_tmpdir_set_root("test");
  tmpdir_root = mathfluid_utils_tmpdir_root();
  subdir = mathfluid_utils_tmpdir_get_subdir("subdir");
  cut_assert_file_exist(subdir);
  cut_assert_match("^test/mathfluid\\......./subdir\\.......$", subdir);
  free_tmpdir_string_with_deletion(tmpdir_root);
  g_free(subdir);
}

void test_file_get_data_line ()
{
  MathFluidFile *file;
  char *str;
  int count = 1;
  if (g_file_test("test_data/file.txt", G_FILE_TEST_EXISTS)) {
    file = mathfluid_utils_file_open("test_data/file.txt");
  } else if (g_file_test("test/test_data/file.txt", G_FILE_TEST_EXISTS)) {
    file = mathfluid_utils_file_open("test/test_data/file.txt");
  } else {
    return;
  }
  while (TRUE) {
    str = mathfluid_utils_file_get_data_line(file);
    if (str != NULL) {
      char *val;
      val = g_strdup_printf("data %d\n", count);
      cut_assert_equal_string(val, str);
      g_free(val);
      g_free(str);
    } else {
      break;
    }
    count += 1;
  }
  mathfluid_utils_file_close(file);
}
