#include <cutter.h>
#include "mathfluid.h"

#define GMRES_ERROR_RESIDUAL_RATIO 1e-10
#define GMRES_PARAMETER_RESTART 5

static gsl_vector *matrix_vector_product_residual_vector (MatrixVectorProduct *matrix_vector_product, double *b, double *x)
{
  gsl_vector *residual_vector;
  gsl_vector_view view_b;
  int dimension;
  DataContainer *data;
  data = matrix_vector_product_matrix_data_ptr(matrix_vector_product);
  dimension = ((gsl_matrix *) data_container_data_ptr(data, 0))->size1;
  residual_vector = gsl_vector_alloc(dimension);
  view_b = gsl_vector_view_array(b, dimension);
  matrix_vector_product_calculate(residual_vector->data, matrix_vector_product, x);
  gsl_vector_scale(residual_vector, -1.0);
  gsl_vector_add(residual_vector, &view_b.vector);
  return residual_vector;
}

static double matrix_vector_product_residual (MatrixVectorProduct *matrix_vector_product, double *b, double *x)
{
  double residual;
  gsl_vector *residual_vector;
  residual_vector = matrix_vector_product_residual_vector(matrix_vector_product, b, x);
  residual = gsl_blas_dnrm2(residual_vector);
  gsl_vector_free(residual_vector);
  return residual;
}

static void gmres_linear_equation_solver_solution (GMRESSolver *solver, int max_iteration, char *method_name)
{
  double *solution;
  solution = gmres_solver_iterate_successively(solver, max_iteration);
  if (solution) {
    int i;
    gsl_vector *residual_vector;
    residual_vector = matrix_vector_product_residual_vector(solver->matrix_vector_product, solver->vector_b, solution);
    for (i = 0; i < solver->dimension; i++) {
      cut_assert_equal_double(0.0, solver->error, gsl_vector_get(residual_vector, i));
    }
    gsl_vector_free(residual_vector);
    g_free(solution);
  } else {
    cut_error("Solution has not been calculated: %s\n", method_name);
  }
}

/**
 * @param[in] dim    Dimension of an equation
 * @param[in] mat    A pointer of an array of a matrix: [(1,1), (1,2), ..., (1,dim), (2,1), (2,2), ...]
 * @param[in] x0     A pointer of an array of a vector
 * @param[in] b      A pointer of an array of a vector
 */
static void gmres_basic_solve_linear_equation (int dim, double *mat, double *x0, double *b)
{
  GMRESSolver *solver;
  MatrixVectorProduct *matrix_vector_product;
  int max_iteration;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, mat);
  max_iteration = dim * 5;

  solver = gmres_solver_alloc(dim, GMRES_ERROR_RESIDUAL_RATIO, matrix_vector_product);
  gmres_solver_set(solver, x0, b);
  gmres_linear_equation_solver_solution(solver, max_iteration, "GMRES");
  gmres_solver_free(solver);
}

static void gmres_restarted_positive_definite_solve_linear_equation (int dim, double *mat, double *x0, double *b, double *eigenvalues)
{
  GMRESSolver *solver;
  MatrixVectorProduct *matrix_vector_product;
  int max_iteration, i;
  double max_abs_eigenvalue, min_abs_eigenvalue, val, convergence_rate, error_residual_ratio;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, mat);
  max_iteration = dim * 1000;

  /* The convergence of restarted GMRES is slow.
     We estimate the error after iterations of "max_iteration" times
     by using the theorem of convergences for symmetric positive definite matrices. */
  max_abs_eigenvalue = 0.0;
  min_abs_eigenvalue = INT_MAX;
  for (i = 0; i < dim; i++) {
    val = fabs(eigenvalues[i]);
    if (val > max_abs_eigenvalue) {
      max_abs_eigenvalue = val;
    }
    if (val < min_abs_eigenvalue) {
      min_abs_eigenvalue = val;
    }
  }
  convergence_rate = 1.0 - (min_abs_eigenvalue * min_abs_eigenvalue) / (max_abs_eigenvalue * max_abs_eigenvalue);
  error_residual_ratio = pow(convergence_rate, max_iteration);
  if (error_residual_ratio < GMRES_ERROR_RESIDUAL_RATIO) {
    error_residual_ratio = GMRES_ERROR_RESIDUAL_RATIO;
  }

  solver = gmres_solver_restarted_alloc(dim, GMRES_PARAMETER_RESTART, error_residual_ratio, matrix_vector_product);
  gmres_solver_set(solver, x0, b);
  gmres_linear_equation_solver_solution(solver, max_iteration, "Restarted GMRES");
  gmres_solver_free(solver);
}

#define ERROR_22_SYMMETRICAL_MATRIX 1e-8

void test_gmres_2x2_symmetrical_matrix ()
{
  int dim = 2;
  double mat[4] = { 3, 6, 6, 3 };
  double b[2] = { -2, 7 };
  double x0[2] = { -2.0, -3.0 };
  /* The inverse of mat is { -1.0/9.0, 2.0/9.0, 2.0/9.0, -1.0/9.0} */
  /* The solution of mat * x = b is { 16.0/9.0, -11.0/9 } */

  double *solution, h1, h2, c, s, residual_last, residual_current;
  GMRESSolver *solver;
  MatrixVectorProduct *matrix_vector_product;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, mat);

  solver = gmres_solver_alloc(dim, GMRES_ERROR_RESIDUAL_RATIO, matrix_vector_product);
  gmres_solver_set(solver, x0, b);
  residual_last = matrix_vector_product_residual(matrix_vector_product, b, x0);
  cut_assert_equal_double(residual_last, ERROR_22_SYMMETRICAL_MATRIX, solver->residual);
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 0)[0], ERROR_22_SYMMETRICAL_MATRIX, 11.0 / sqrt(317));
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 0)[1], ERROR_22_SYMMETRICAL_MATRIX, 14.0 / sqrt(317));
  cut_assert_false(arnoldi_method_basis_ptr(solver->arnoldi_method, 1));

  if (!gmres_solver_iterate(solver)) {
    cut_error("Can not advance first iteration step\n");
  }
  h1 = 2799.0 / 317.0;
  h2 = 50.0 / 317.0 * sqrt(25677.0 / 317.0);
  c = sqrt(h1 * h1 / (h1 * h1 + h2 * h2));
  s = - h2 / h1 * c;
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 0)[0], ERROR_22_SYMMETRICAL_MATRIX, 11.0 / sqrt(317));
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 1)[0], ERROR_22_SYMMETRICAL_MATRIX, 126.0 / sqrt(25677));
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 0)[1], ERROR_22_SYMMETRICAL_MATRIX, 14.0 / sqrt(317));
  cut_assert_equal_double(arnoldi_method_basis_ptr(solver->arnoldi_method, 1)[1], ERROR_22_SYMMETRICAL_MATRIX, -99.0 / sqrt(25677));
  cut_assert_equal_double(solver->givens_rotation[0], ERROR_22_SYMMETRICAL_MATRIX, c);
  cut_assert_equal_double(solver->givens_rotation[1], ERROR_22_SYMMETRICAL_MATRIX, s);
  cut_assert_equal_double(solver->hessenberg[0][0], ERROR_22_SYMMETRICAL_MATRIX, sqrt(h1 * h1 + h2 * h2));

  solution = gmres_solver_solution(solver);
  residual_current = matrix_vector_product_residual(matrix_vector_product, b, solution);
  g_free(solution);
  cut_assert_true(residual_current < residual_last);
  cut_assert_equal_double(solver->residual, ERROR_22_SYMMETRICAL_MATRIX, residual_current);
  residual_last = residual_current;

  if (!gmres_solver_iterate(solver)) {
    cut_error("Can not advance second iteration step\n");
  }
  cut_assert_true(gmres_solver_converged_p(solver));
  solution = gmres_solver_solution(solver);
  residual_current = matrix_vector_product_residual(matrix_vector_product, b, solution);
  cut_assert_true(residual_current < residual_last);
  g_free(solution);
  gmres_solver_free(solver);
}

#include "gmres_tests_source.c"
