#include <cutter.h>
#include "mathfluid.h"

#define ERROR_TEST_RAILEIGH_RITZ 1e-8
#define ITERATION_SIMULTANEOUS_ITERATION 1000

static gboolean double_complex_equal_p (double complex a, double complex b, double error)
{
  return (fabs(creal(a) - creal(b)) < error) && (fabs(cimag(a) - cimag(b)) < error);
}

static void assert_equal_double_complex (double complex expected, double error, double complex actual)
{
  cut_assert_equal_double(creal(expected), error, creal(actual));
  cut_assert_equal_double(cimag(expected), error, cimag(actual));
}

static gboolean array_of_real_numbers_p (int dim, double complex *array)
{
  int i;
  for (i = 0; i < dim; i++) {
    if (cimag(array[i]) != 0.0) {
      return FALSE;
    }
  }
  return TRUE;
}

static void assert_eigenvalues (int dim, double complex *expected, double error, double complex *actual)
{
  int i;
  for (i = 0; i < dim; i++) {
    if (cimag(expected[i]) == 0.0) {
      assert_equal_double_complex(expected[i], error, actual[i]);
    } else {
      if (double_complex_equal_p(expected[i], actual[i], error)) {
        assert_equal_double_complex(expected[i], error, actual[i]);
        if (i + 1 < dim) {
          assert_equal_double_complex(expected[i + 1], error, actual[i + 1]);
        }
      } else {
        if (i + 1 < dim) {
          assert_equal_double_complex(expected[i], error, actual[i + 1]);
          assert_equal_double_complex(expected[i + 1], error, actual[i]);
        } else {
          assert_equal_double_complex(expected[i], error, actual[i]);
        }
        i += 1;
      }
    }
  }
}

static void assert_normalized_eigenvector (int dim, double complex *expected, double error, double complex *actual)
{
  double complex *ratio;
  int i, num_ratio;
  ratio = (double complex *) g_malloc(sizeof(double complex) * dim);
  num_ratio = 0;
  for (i = 0; i < dim; i++) {
    if (fabs(creal(expected[i])) < error && fabs(cimag(expected[i])) < error) {
      cut_assert_true(fabs(creal(actual[i])) < error && fabs(cimag(actual[i])) < error);
    } else {
      ratio[num_ratio] = actual[i] / expected[i];
      num_ratio += 1;
    }
  }
  if (num_ratio >= 2) {
    for (i = 1; i < num_ratio; i++) {
      assert_equal_double_complex(ratio[0], error, ratio[i]);
    }
  }
  g_free(ratio);
}

static void assert_eigenvectors (int dim, int number_eigenvectors, double complex *expected, double error, double complex *actual)
{
  int i;
  double complex *vec_expected, *vec_actual;
  for (i = 0; i < number_eigenvectors; i++) {
    vec_expected = expected + i * dim;
    vec_actual = actual + i * dim;
    assert_normalized_eigenvector(dim, vec_expected, error, vec_actual);
  }
}

static void fix_order_of_eigenvector (int dim, double error, double complex *eigenvalue1, double complex *eigenvalue2, double complex *eigenvector)
{
  int i;
  for (i = 0; i < dim; i++) {
    if (!double_complex_equal_p(eigenvalue1[i], eigenvalue2[i], error) && i < dim - 1) {
      double complex *tmp;
      size_t mem_size;
      mem_size = sizeof(double complex) * dim;
      tmp = (double complex *) g_malloc(mem_size);
      memcpy(tmp, eigenvector + dim * i, mem_size);
      memcpy(eigenvector + dim * i, eigenvector + dim * (i + 1), mem_size);
      memcpy(eigenvector + dim * (i + 1), tmp, mem_size);
      g_free(tmp);
      i += 1;
    }
  }
}

static int eigen_by_rayleigh_ritz_with_simultaneous_iteration (double complex **eigenvalue, double complex **eigenvector, int dim, double *matrix_elements)
{
  MatrixVectorProduct *matrix_vector_product;
  SimultaneousIteration *simultaneous_iteration;
  RayleighRitz *rayleigh_ritz;
  int number_eigenvectors;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, matrix_elements);
  simultaneous_iteration = simultaneous_iteration_alloc(dim, dim, matrix_vector_product_duplicate(matrix_vector_product));
  simultaneous_iteration_set_qr_randomly(simultaneous_iteration);
  simultaneous_iteration_iterate_successively(simultaneous_iteration, ITERATION_SIMULTANEOUS_ITERATION);
  rayleigh_ritz = rayleigh_ritz_alloc(dim, matrix_vector_product);
  rayleigh_ritz_set_basis(rayleigh_ritz, dim, simultaneous_iteration_q_ptr(simultaneous_iteration));
  number_eigenvectors = rayleigh_ritz_eigen(eigenvalue, eigenvector, rayleigh_ritz);
  simultaneous_iteration_free(simultaneous_iteration);
  rayleigh_ritz_free(rayleigh_ritz);
  return number_eigenvectors;
}

static int eigen_by_rayleigh_ritz_with_inverse_simultaneous_iteration (double complex **eigenvalue, double complex **eigenvector, int dim, double *matrix_elements)
{
  MatrixVectorProduct *matrix_vector_product, *inverse_matrix_vector_product;
  SimultaneousIteration *simultaneous_iteration;
  RayleighRitz *rayleigh_ritz;
  int number_eigenvectors;
  double number_shift;
  number_shift = -0.5;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, matrix_elements);
  inverse_matrix_vector_product = inverse_matrix_vector_product_alloc(dim, 1e-14, dim, matrix_vector_product, number_shift);
  simultaneous_iteration = simultaneous_iteration_alloc(dim, dim, matrix_vector_product_duplicate(inverse_matrix_vector_product));
  simultaneous_iteration_set_qr_randomly(simultaneous_iteration);
  simultaneous_iteration_iterate_successively(simultaneous_iteration, ITERATION_SIMULTANEOUS_ITERATION);
  rayleigh_ritz = rayleigh_ritz_alloc(dim, inverse_matrix_vector_product);
  rayleigh_ritz_set_basis(rayleigh_ritz, dim, simultaneous_iteration_q_ptr(simultaneous_iteration));
  number_eigenvectors = rayleigh_ritz_eigen(eigenvalue, eigenvector, rayleigh_ritz);
  rayleigh_ritz_convert_inverse_eigenvalues(*eigenvalue, number_eigenvectors, number_shift);
  rayleigh_ritz_eigen_sort_by_absolute_value(*eigenvalue, *eigenvector, number_eigenvectors, dim);
  simultaneous_iteration_free(simultaneous_iteration);
  rayleigh_ritz_free(rayleigh_ritz);
  return number_eigenvectors;
}

static void assertion_of_rayleigh_ritz_with_simultaneous_iteration (int dim, double *matrix_elements, double error, int (*func) (double complex **eigenvalue, double complex **eigenvector, int dim, double *matrix_elements))
{
  double complex *eigenvalue_gsl, *eigenvector_gsl, *eigenvalue_rayleigh_ritz, *eigenvector_rayleigh_ritz;
  gsl_matrix_view m;
  int num_gsl, num_rayleigh_ritz;
  m = gsl_matrix_view_array(matrix_elements, dim, dim);
  num_gsl = gsl_wrap_eigen(&eigenvalue_gsl, &eigenvector_gsl, &m.matrix);
  num_rayleigh_ritz = func(&eigenvalue_rayleigh_ritz, &eigenvector_rayleigh_ritz, dim, matrix_elements);
  cut_assert_equal_int(num_gsl, num_rayleigh_ritz);
  assert_eigenvalues(num_gsl, eigenvalue_gsl, error, eigenvalue_rayleigh_ritz);
  fix_order_of_eigenvector(dim, error, eigenvalue_rayleigh_ritz, eigenvalue_gsl, eigenvector_gsl);
  assert_eigenvectors(dim, num_gsl, eigenvector_gsl, error, eigenvector_rayleigh_ritz);
  g_free(eigenvalue_gsl);
  g_free(eigenvector_gsl);
  g_free(eigenvalue_rayleigh_ritz);
  g_free(eigenvector_rayleigh_ritz);
}

static void assert_rayleigh_ritz_with_simultaneous_iteration (int dim, double *matrix_elements, double error)
{
  assertion_of_rayleigh_ritz_with_simultaneous_iteration(dim, matrix_elements, error, eigen_by_rayleigh_ritz_with_simultaneous_iteration);
}

static void assert_rayleigh_ritz_with_inverse_simultaneous_iteration (int dim, double *matrix_elements, double error)
{
  assertion_of_rayleigh_ritz_with_simultaneous_iteration(dim, matrix_elements, error, eigen_by_rayleigh_ritz_with_inverse_simultaneous_iteration);
}

static int eigen_by_rayleigh_ritz_with_arnoldi_method (double complex **eigenvalue, double complex **eigenvector, int dim, double *matrix_elements)
{
  MatrixVectorProduct *matrix_vector_product;
  ArnoldiMethod *arnoldi_method;
  RayleighRitz *rayleigh_ritz;
  int number_eigenvectors, row_number, column_number;
  double *hessenberg, *invariant_space;
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, matrix_elements);
  arnoldi_method = arnoldi_method_alloc(dim, dim, matrix_vector_product_duplicate(matrix_vector_product));
  arnoldi_method_set_randomly(arnoldi_method);
  hessenberg = arnoldi_method_iterate_successively(&row_number, &column_number, arnoldi_method);
  invariant_space = arnoldi_method_basis_matrix(arnoldi_method);
  rayleigh_ritz = rayleigh_ritz_alloc(dim, matrix_vector_product_duplicate(matrix_vector_product));
  rayleigh_ritz_set_basis(rayleigh_ritz, dim, invariant_space);
  rayleigh_ritz_set_matrix(rayleigh_ritz, hessenberg);
  number_eigenvectors = rayleigh_ritz_eigen(eigenvalue, eigenvector, rayleigh_ritz);
  matrix_vector_product_free(matrix_vector_product);
  arnoldi_method_free(arnoldi_method);
  rayleigh_ritz_free(rayleigh_ritz);
  g_free(hessenberg);
  g_free(invariant_space);
  return number_eigenvectors;
}

static void assert_rayleigh_ritz_with_arnoldi_method (int dim, double *matrix_elements, double error)
{
  double complex *eigenvalue_gsl, *eigenvector_gsl, *eigenvalue_rayleigh_ritz, *eigenvector_rayleigh_ritz;
  gsl_matrix_view m;
  int num_gsl, num_rayleigh_ritz;
  m = gsl_matrix_view_array(matrix_elements, dim, dim);
  num_gsl = gsl_wrap_eigen(&eigenvalue_gsl, &eigenvector_gsl, &m.matrix);
  num_rayleigh_ritz = eigen_by_rayleigh_ritz_with_arnoldi_method(&eigenvalue_rayleigh_ritz, &eigenvector_rayleigh_ritz, dim, matrix_elements);
  cut_assert_equal_int(num_gsl, num_rayleigh_ritz);
  assert_eigenvalues(num_gsl, eigenvalue_gsl, error, eigenvalue_rayleigh_ritz);
  fix_order_of_eigenvector(dim, error, eigenvalue_rayleigh_ritz, eigenvalue_gsl, eigenvector_gsl);
  assert_eigenvectors(dim, num_gsl, eigenvector_gsl, error, eigenvector_rayleigh_ritz);
  g_free(eigenvalue_gsl);
  g_free(eigenvector_gsl);
  g_free(eigenvalue_rayleigh_ritz);
  g_free(eigenvector_rayleigh_ritz);
}

void test_simultaneous_iteration_rayleigh_ritz_01 ()
{
  double matrix_elements[] = { 1.0, 3.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, 1.0 };
  assert_rayleigh_ritz_with_simultaneous_iteration(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 4.0, -3.0, 1.0 */
}

void test_simultaneous_iteration_rayleigh_ritz_02 ()
{
  double matrix_elements[] = { -1.0, 2.0, 2.0, -3.0, 2.0, 3.0, 1.0, 3.0, 3.0 };
  assert_rayleigh_ritz_with_simultaneous_iteration(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 5.0, -(1.0 + sqrt(3) * _Complex_I) / 2.0, -(1.0 - sqrt(3) * _Complex_I) / 2.0 */
}

void test_inverse_simultaneous_iteration_rayleigh_ritz_01 ()
{
  double matrix_elements[] = { 1.0, 3.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, 1.0 };
  assert_rayleigh_ritz_with_inverse_simultaneous_iteration(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 4.0, -3.0, 1.0 */
}

void test_inverse_simultaneous_iteration_rayleigh_ritz_02 ()
{
  double matrix_elements[] = { -1.0, 2.0, 2.0, -3.0, 2.0, 3.0, 1.0, 3.0, 3.0 };
  assert_rayleigh_ritz_with_inverse_simultaneous_iteration(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 5.0, -(1.0 + sqrt(3) * _Complex_I) / 2.0, -(1.0 - sqrt(3) * _Complex_I) / 2.0 */
}

void test_arnoldi_method_rayleigh_ritz_01 ()
{
  double matrix_elements[] = { 1.0, 3.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, 1.0 };
  assert_rayleigh_ritz_with_arnoldi_method(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 4.0, -3.0, 1.0 */
}

void test_arnoldi_method_rayleigh_ritz_02 ()
{
  double matrix_elements[] = { -1.0, 2.0, 2.0, -3.0, 2.0, 3.0, 1.0, 3.0, 3.0 };
  assert_rayleigh_ritz_with_arnoldi_method(3, matrix_elements, ERROR_TEST_RAILEIGH_RITZ);
  /* eigenvalues: 5.0, -(1.0 + sqrt(3) * _Complex_I) / 2.0, -(1.0 - sqrt(3) * _Complex_I) / 2.0 */
}

static void assert_simultaneous_iteration (int dim, double *matrix_elements)
{
  double complex *eigenvalue_gsl, *eigenvector_gsl;
  gsl_matrix_view m;
  int num_gsl;
  double *eigenvalues, *eigenvector;
  MatrixVectorProduct *matrix_vector_product;
  SimultaneousIteration *simultaneous_iteration;
  m = gsl_matrix_view_array(matrix_elements, dim, dim);
  num_gsl = gsl_wrap_eigen(&eigenvalue_gsl, &eigenvector_gsl, &m.matrix);
  cut_assert_equal_int(num_gsl, dim);
  matrix_vector_product = usual_matrix_vector_product_alloc(dim, dim, matrix_elements);
  simultaneous_iteration = simultaneous_iteration_alloc(dim, dim, matrix_vector_product);
  simultaneous_iteration_set_qr_randomly(simultaneous_iteration);
  simultaneous_iteration_iterate_successively(simultaneous_iteration, ITERATION_SIMULTANEOUS_ITERATION);
  eigenvalues = simultaneous_iteration_eigenvalues(&eigenvector, simultaneous_iteration);
  if (array_of_real_numbers_p(dim, eigenvalue_gsl)) {
    int i;
    gboolean negative;
    double val;
    for (i = 0; i < dim; i++) {
      cut_assert_equal_double(eigenvalues[i], ERROR_TEST_RAILEIGH_RITZ, creal(eigenvalue_gsl[i]));
    }
    negative = fabs(eigenvector[0] - creal(eigenvector_gsl[0])) > ERROR_TEST_RAILEIGH_RITZ;
    for (i = 0; i < dim; i++) {
      val = creal(eigenvector_gsl[i]);
      if (negative) {
        val = -val;
      }
      cut_assert_equal_double(eigenvector[i], ERROR_TEST_RAILEIGH_RITZ, val);
    }
  } else {
    fprintf(stderr, "Not suitable matrix for test of SimultaneousIteration\n");
  }
  simultaneous_iteration_free(simultaneous_iteration);
  g_free(eigenvalues);
  g_free(eigenvector);
  g_free(eigenvalue_gsl);
  g_free(eigenvector_gsl);
}

void test_simultaneous_iteration_01 ()
{
  double matrix_elements[] = { 1.0, 3.0, 3.0, 3.0, 0.0, -3.0, 3.0, 2.0, 1.0 };
  assert_simultaneous_iteration(3, matrix_elements);
}
